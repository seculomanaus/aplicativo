using System;
using Android.App;
using Android.Content;
using Android.Gms.Gcm;
using Android.Gms.Iid;
using Android.OS;
using Android.Util;
using Seculos.DependencyService.Droid;

namespace SeculosApp.Droid
{
	[Service(Exported = false)]
	public class GcmRegistrationService : IntentService
	{
		static object locker = new object();

		public GcmRegistrationService() : base("GcmRegistrationService") { }

		protected override void OnHandleIntent(Intent intent)
		{
			try
			{
				Log.Info("GcmRegistrationService", "Calling InstanceID.GetToken");

				lock (locker)
				{
					var token = new Token().generateTokenGcm();

					try
					{
						if (SeculosBO.Instance.SeculosGeneral?.GCMToken == null)
						{
							SeculosBO.Instance.SeculosGeneral.GCMToken = token;
						}
					}
					catch
					{

					}

					SeculosBO.Instance.SendRegistrationToAppServer(token);

				}
			}
			catch (Exception e)
			{
				Log.Debug("GcmRegistrationService", "Failed to get a registration token");
				return;
			}
		}
	}

	[Service(Exported = false), IntentFilter(new[] { "com.google.android.gms.iid.InstanceID" })]
	class MyInstanceIDListenerService : InstanceIDListenerService
	{
		public override void OnTokenRefresh()
		{
			var intent = new Intent(this, typeof(GcmRegistrationService));
			StartService(intent);
		}
	}

	[Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
	public class SeculoGcmListenerService : GcmListenerService
	{
		public override void OnMessageReceived(string from, Bundle data)
		{
			var message = data.GetString("cd_alerta");
			Log.Debug("SeculoGcmListenerService", "From:    " + from);
			Log.Debug("SeculoGcmListenerService", "cd_alerta: " + message);

			NotificationUtils.createNotificationByBundle(this, data);

		}
	}
}