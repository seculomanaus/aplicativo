﻿using Android.App;
using Android.Content;
using Android.OS;
using SeculosApp.Model;
using static Android.Support.V4.App.NotificationCompat;
namespace SeculosApp.Droid
{
	public class NotificationUtils
	{
		public static int NOTIFICATION_NORMAL = 0; 
		public static int NOTIFICATION_FOREGROUND = 1;
		public static string CD_ALERTA = "cd_alerta";
		public static string NM_ALERTA = "nm_alerta";
		public static string DS_ALERTA = "ds_alerta";
		public static string DS_FOTO = "ds_foto";
		public static string DS_FOTO_MINI = "ds_foto_mini";
		public static string DT_CADASTRO = "dt_cadastro";
		public static string NM_TIPO = "nm_tipo";
		public static string CD_EVENTO = "cd_evento";
		public static string CD_ALUNO = "cd_aluno";
		public static string NM_ALUNO = "nm_aluno";
		public static string DT_EVENTO = "dt_evento";
		public static string DS_HORARIO = "ds_horario";
		public static string VL_EVENTO = "vl_evento";
		public static string FL_LIDO = "fl_lido";
		public static string FL_PAGO = "fl_pago";
		public static string FL_CONFIRMACAO = "fl_confirmacao";


		public static void createNotificationByBundle(Context context, Bundle bundle)
		{
			Notificacao notificacao = createNotificacaoByBundle(bundle);

			Intent intent = new Intent(context, typeof(MainActivity));
			intent.PutExtra(CD_ALERTA, notificacao.CdAlert);
			intent.PutExtra(NM_ALERTA, notificacao.NmAlert);
			intent.PutExtra(DS_ALERTA, notificacao.DsAlert);
			intent.PutExtra(DS_FOTO, notificacao.DsFoto);
			intent.PutExtra(DS_FOTO_MINI, notificacao.DsFotoMini);
			intent.PutExtra(DT_CADASTRO, notificacao.DtCadastro);
			intent.PutExtra(NM_TIPO, notificacao.NmTipo);
			intent.PutExtra(CD_EVENTO, notificacao.CdEvento);
			intent.PutExtra(CD_ALUNO, notificacao.CdAluno);
			intent.PutExtra(NM_ALUNO, notificacao.NmAluno);
			intent.PutExtra(DT_EVENTO, notificacao.DtEvento);
			intent.PutExtra(DS_HORARIO, notificacao.DsHorario);
			intent.PutExtra(VL_EVENTO, notificacao.VlEvento);
			intent.PutExtra(FL_LIDO, notificacao.FlLido);
			intent.PutExtra(FL_PAGO, notificacao.FlPago);
			intent.PutExtra(FL_CONFIRMACAO, notificacao.FlConfirmado);
			intent.SetFlags(ActivityFlags.SingleTop);

			const int pendingIntentId = 0;
			PendingIntent pendingIntent = PendingIntent.GetActivity(context, pendingIntentId, intent, PendingIntentFlags.UpdateCurrent);

			// Instantiate the builder and set notification elements:
			Builder builder = new Builder(context)
				.SetContentTitle(notificacao.NomeAlerta)
				.SetContentText(notificacao.DescricaoAlertaMin)
				.SetSmallIcon(Resource.Drawable.ic_launcher)
				.SetTicker(notificacao.NomeAlerta)
				.SetContentIntent(pendingIntent)
				.SetStyle(new BigTextStyle().BigText(notificacao.DescricaoAlertaMin))
				.SetPriority((int)NotificationPriority.High)
				.SetAutoCancel(true);

			if ((int)Android.OS.Build.VERSION.SdkInt >= 21)
			{
				builder.SetCategory(Android.App.Notification.CategoryMessage);
				builder.SetVisibility(VisibilityPrivate);
			}

			// Build the notification:
			Android.App.Notification notification = builder.Build();
			notification.Flags = NotificationFlags.AutoCancel | NotificationFlags.ShowLights;
			// Get the notification manager:
			NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;

			// Publish the notification:
			const int notificationId = 0;
			notificationManager.Notify(notificationId, notification);
		}

		public static Notificacao createNotificacaoByBundle(Bundle bundle)
		{ 
			Notificacao notificacao = new Notificacao();
			notificacao.CdAlert = bundle.GetString(CD_ALERTA);
			notificacao.NmAlert = bundle.GetString(NM_ALERTA);
			notificacao.DsAlert = bundle.GetString(DS_ALERTA);
			notificacao.DsFoto = bundle.GetString(DS_FOTO);
			notificacao.DsFotoMini = bundle.GetString(DS_FOTO_MINI);
			notificacao.DtCadastro = bundle.GetString(DT_CADASTRO);
			notificacao.NmTipo = bundle.GetString(NM_TIPO);
			notificacao.CdEvento = bundle.GetString(CD_EVENTO);
			notificacao.CdAluno = bundle.GetString(CD_ALUNO);
			notificacao.NmAluno = bundle.GetString(NM_ALUNO);
			notificacao.DtEvento = bundle.GetString(DT_EVENTO);
			notificacao.DsHorario = bundle.GetString(DS_HORARIO);
			notificacao.VlEvento = bundle.GetString(VL_EVENTO);
			notificacao.FlLido = bundle.GetString(FL_LIDO);
			notificacao.FlPago = bundle.GetString(FL_PAGO);
			notificacao.FlConfirmado = bundle.GetString(FL_CONFIRMACAO);

			return notificacao;
		}

		public static Notificacao verifyNotification(Intent intent)
		{
			Notificacao notification = null;
			if (intent.Extras != null && SeculosBO.Instance.SeculosGeneral != null && SeculosBO.Instance.SeculosGeneral.UserConnected != null && intent.Extras.ContainsKey(CD_ALERTA))
			{
				Bundle extras = intent.Extras;
				notification = createNotificacaoByBundle(extras);
				SeculosBO.Instance.addNotificationByGcm(notification);
			}
			return notification;
		}
	}
}
