﻿using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.OS;
using FFImageLoading.Forms.Droid;
using Android.Content;
using Xamarin.Forms;
using Android.Gms.Common;
using SeculosApp.Model;
using System;
using System.IO;

namespace SeculosApp.Droid
{
	[Activity(Label = "Século", Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme.Splash", WindowSoftInputMode = SoftInput.AdjustPan, LaunchMode = LaunchMode.SingleInstance, MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.SensorPortrait)]
	[IntentFilter(new[] { "OPEN_MAIN_ACTIVITY" }, Categories = new[] { Intent.CategoryDefault })]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			if (IsPlayServicesAvailable())
			{
				var intent = new Intent(this, typeof(GcmRegistrationService));
				this.StartService(intent);
			}

			Forms.Init(this, bundle);
			//ExceptionTracker.SendLogToServer("http://testbed.tap4.com.br/brec/ws/nilson/seculoCrashs/", ExceptionTracker.CheckCrashLog());
			CachedImageRenderer.Init();
			XamForms.Controls.Droid.Calendar.Init();

			DependencyService.Register<Seculos.DependencyService.Droid.Environment>();
			DependencyService.Register<Seculos.DependencyService.Droid.Message>();

			var width = Resources.DisplayMetrics.WidthPixels;
			var height = Resources.DisplayMetrics.HeightPixels;
			var density = Resources.DisplayMetrics.Density;

			App.ScreenWidth = (width - 0.5f) / density;
			App.ScreenHeight = (height) / density;

			LoadApplication(new App());

			Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
			{
				VerifyNotificationAndRedirectPage(this.Intent);
				return false;
			});
		}

		public bool IsPlayServicesAvailable()
		{
			int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
			if (resultCode != ConnectionResult.Success)
			{
				if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
					System.Diagnostics.Debug.WriteLine("GCM: " + GoogleApiAvailability.Instance.GetErrorString(resultCode));
				else
				{
					System.Diagnostics.Debug.WriteLine("GCM: Sorry, this device is not supported");
					Finish();
				}
				return false;
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("GCM: Google Play Services is available.");
				return true;
			}
		}

		protected override void OnNewIntent(Intent intent)
		{
			base.OnNewIntent(intent);

			System.Diagnostics.Debug.WriteLine("OnNewIntent: " + intent);
			VerifyNotificationAndRedirectPage(intent);
		}

		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			switch (requestCode)
			{
				case OpenGallery.SELECT_PHOTO:
					if (resultCode == Android.App.Result.Ok)
					{
						try
						{
							var imageUri = data.Data;
							var stream = ContentResolver.OpenInputStream(imageUri);
							var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
							var filePath = Path.Combine(documentsPath, "photo_profile.jpg");

							using (var fileStream = File.Create(filePath))
							{
								await stream.CopyToAsync(fileStream);
							}

							var gallery = DependencyService.Get<IOpenGallery>();
							var dataConverted = Convert.ToBase64String(File.ReadAllBytes(filePath));
							gallery.FetchBitmap(dataConverted);
						}
						catch (Exception ex) 
						{
							System.Diagnostics.Debug.WriteLine(ex.Message);
						}
					}

					break;
			}
		}

		public void VerifyNotificationAndRedirectPage(Intent intent)
		{
			Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
			{
				Notificacao notificacao = NotificationUtils.verifyNotification(intent);
				if (notificacao != null)
				{
					SeculosBO.Instance.NotificationSelected = SeculosBO.Instance.searchNotificatioByCode(notificacao.CdAlert, SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications);

					if (SeculosBO.Instance.Navigation != null)
					{
						if (SeculosBO.Instance.NotificationSelected.isNotComunication)
						{
							SeculosBO.Instance.Navigation.PushAsync(new NotificationDetailView());
						}
						else
						{
							SeculosBO.Instance.ComunicationSelected = SeculosBO.Instance.NotificationSelected;
							SeculosBO.Instance.OpenModal(EnumModal.COMUNICATION, false, AbstractContentPage._AbstractContentPage);
						}
					}
				}
				return false;
			});
		}
	}
}

