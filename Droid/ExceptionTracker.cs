﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Android.Runtime;
using System.Net.Http;

namespace SeculosApp.Droid
{
	public static class ExceptionTracker
	{
		public static TimeSpan RequestTimeout = TimeSpan.FromSeconds(10);
		public static long MaxResponseContentBufferSize = 256000;

		static bool LOG_ENABLED = true;
		static readonly string FilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "stack.trace");

		public static void ExceptionHandler(object sender, RaiseThrowableEventArgs e)
		{
			string report = "--------- Stack trace ---------\n\n";
			report += e.Exception.StackTrace + "\n\n";

			report += "--------- Inner Exception ---------\n\n";
			report += e.Exception.InnerException + "\n\n";

			report += "--------- Message ---------\n\n";
			report += e.Exception.Message + "\n\n";

			report += "--------- Source ---------\n\n";
			report += e.Exception.Source + "\n\n";

			report += "--------- TargetSite ---------\n\n";
			report += e.Exception.TargetSite + "\n\n";

			report += "--------- HelpLink ---------\n\n";
			report += e.Exception.HelpLink + "\n\n";

			report += "--------- HResult ---------\n\n";
			report += e.Exception.HResult + "\n\n";

			// If the exception was thrown in a background thread inside
			// AsyncTask, then the actual exception can be found with getCause
			File.WriteAllText(FilePath, report);
		}

		static void Log(string msg)
		{
			if (LOG_ENABLED)
			{
				Console.WriteLine(msg);
			}
		}

		public static void SendLogToServer(string url, string logToSend, Action<bool> successCallback = null)
		{
			if (logToSend != null)
			{
				Task.Factory.StartNew(async () =>
				{
					try
					{
						using (var client = new HttpClient())
						{
							client.MaxResponseContentBufferSize = MaxResponseContentBufferSize;
							client.Timeout = RequestTimeout;

							var uri = new Uri(url);
							var content = new StringContent(logToSend, Encoding.UTF8);

							HttpResponseMessage response = null;
							response = await client.PostAsync(uri, content);

							if (successCallback != null)
							{
								successCallback(response.IsSuccessStatusCode);
							}
						}
					}
					catch (Exception ex)
					{
						Log(ex.Message);

						if (successCallback != null)
						{
							successCallback(false);
						}
					}
					finally
					{
						if (File.Exists(FilePath))
						{
							File.Delete(FilePath);
						}
					}
				});
			}
		}

		public static string CheckCrashLog()
		{
			string content = null;

			try
			{
				content = File.ReadAllText(FilePath);
			}
			catch (FileNotFoundException ex) 
			{
				Log(ex.Message);
			}

			return content;
		}
	}
}
