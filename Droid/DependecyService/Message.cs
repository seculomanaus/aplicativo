using Android.App;
using Android.Widget;
using SeculosApp;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.Droid.Message))]
namespace Seculos.DependencyService.Droid
{
	public class Message : IMessage
	{
		static Message Instance { get; set; }

		public Message() { }

		public void Msg(string text)
		{
			Android.Widget.Toast.MakeText(Application.Context, text, ToastLength.Long).Show();
		}
	}
}