using Android.Content;
using SeculosApp;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.Droid.SendMail))]
namespace Seculos.DependencyService.Droid
{
	public class SendMail : ISendEmail
	{
		static SendMail Instance { get; set; }

		void ISendEmail.SendMail(string to, string name, string phone, string from, string msg, string subject)
		{
			var body = msg+"\n\n\n"+name + "\n" + phone + "\n" + from + "";
			var email = new Intent(Intent.ActionSend);
			email.PutExtra(Intent.ExtraEmail, new string[]{to});
			email.PutExtra(Intent.ExtraSubject, subject);
			email.PutExtra(Intent.ExtraText, body);
			email.PutExtra(Intent.ExtraHtmlText, true);
			email.SetType("message/rfc822");
			Forms.Context.StartActivity(email);
		}
	}
}