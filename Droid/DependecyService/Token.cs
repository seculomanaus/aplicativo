﻿using System;
using Android.App;
using Android.Gms.Gcm;
using Android.Gms.Iid;
using SeculosApp.IDependecyService;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.Droid.Token))]
namespace Seculos.DependencyService.Droid
{
	public class Token : IToken
	{
		public const string SenderId = "880626634232";

		static Token Instance { get; set; }

		public string generateTokenGcm()
		{
			var instanceID = InstanceID.GetInstance(Application.Context);
			var token = instanceID.GetToken(SenderId, GoogleCloudMessaging.InstanceIdScope, null);

			#if DEBUG
			instanceID.DeleteToken(token, GoogleCloudMessaging.InstanceIdScope);
			instanceID.DeleteInstanceID();
			token = instanceID.GetToken(SenderId, GoogleCloudMessaging.InstanceIdScope, null);
			#endif

			Subscribe(token);

			return token;
		}

		public void generateTokenGcm(Action<string> callback)
		{
			// do nothing
		}

		public void Subscribe(string token)
		{
			var pubSub = GcmPubSub.GetInstance(Application.Context);
			pubSub.Subscribe(token, "/topics/global", null);
		}

		public void DeleteToken()
		{
			var instanceID = InstanceID.GetInstance(Application.Context);
			var token = instanceID.GetToken(SenderId, GoogleCloudMessaging.InstanceIdScope, null);
			instanceID.DeleteToken(token, GoogleCloudMessaging.InstanceIdScope);
			instanceID.DeleteInstanceID();
		}
	}
}
