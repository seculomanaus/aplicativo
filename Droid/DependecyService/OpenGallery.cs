﻿using System;
using Android.App;
using Android.Content;
using SeculosApp.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(OpenGallery))]
namespace SeculosApp.Droid
{
	public class OpenGallery : IOpenGallery
	{
		public const int SELECT_PHOTO = 666;
		  
		Action<string> _callback;

		public void GetImage(Action<string> callback)
		{
			_callback = callback;

			var photoPickerIntent = new Intent(Intent.ActionPick);
			photoPickerIntent.SetType("image/*");
			((Activity)global::Xamarin.Forms.Forms.Context).StartActivityForResult(photoPickerIntent, SELECT_PHOTO);
		}

		public void FetchBitmap(string imageConverted)
		{
			if (_callback != null)
			{
				_callback(imageConverted);
			}
		}
	}
}