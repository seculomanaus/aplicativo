using System;
using System.IO;
using Seculos.Listener;

[assembly: Xamarin.Forms.Dependency(typeof(Environment))]
namespace Seculos.DependencyService.Droid
{
	public class Environment : IEnvioriment
	{
		static Environment Instance { get; set; }

		public Environment() { }

		public string GetPath()
		{
			return System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
		}

		public string ReadAllText(string filePath)
		{
			return File.ReadAllText(filePath);
		}

		public bool WriteAllText(string filePath, string text)
		{
			File.WriteAllText(filePath, text);
			return true;
		}
	}
}