﻿using Android.App;
using Android.Content;
using SeculosApp;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.Droid.ClipBoard))]
namespace Seculos.DependencyService.Droid
{
	public class ClipBoard : IClipBoard
	{
		public void CopyToClipboard(string text)
		{
			var activity = ((Activity)global::Xamarin.Forms.Forms.Context);
			var clipboard = (ClipboardManager)activity.GetSystemService(Context.ClipboardService);
			var clip = ClipData.NewPlainText("link", text);

			clipboard.PrimaryClip = clip;
		}
	}
}