using Xamarin.Forms.Platform.Android;
using Seculo.Droid.Renderer;
using Android.Graphics;
using Xamarin.Forms;
using SeculosApp;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomButtonRenderer : Xamarin.Forms.Platform.Android.AppCompat.ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);
			var label = (Android.Support.V7.Widget.AppCompatButton)Control;

			Typeface font = null;

			CustomButton el = (CustomButton)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Bold.otf");
			}
			else if (el.FontType.Equals("Book"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Book.otf");
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Medium.otf");
			}
			else if (el.FontType.Equals("Light"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}
			else
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}


			label.SetTextSize(Android.Util.ComplexUnitType.Dip, (float)Element.FontSize);
			label.Typeface = font;

		}
	}
}