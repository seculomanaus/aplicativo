using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms;
using SeculosApp;
using Seculo.Droid.Renderer;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			var label = (Android.Widget.EditText)Control;
			Typeface font = null;

			CustomEntry el = (CustomEntry)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Bold.otf");
			}
			else if (el.FontType.Equals("Book"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Book.otf");
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Medium.otf");
			}
			else if (el.FontType.Equals("Light"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}
			else
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}


			label.SetTextSize(Android.Util.ComplexUnitType.Dip, (float)Element.FontSize);
			label.Typeface = font;

		}
	}
}