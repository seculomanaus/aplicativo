using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms;
using SeculosApp;
using Seculo.Droid.Renderer;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);
			var label = (TextView)Control;
			Typeface font = null;

			CustomLabel el = (CustomLabel)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Bold.otf");
			}
			else if (el.FontType.Equals("Book"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Book.otf");
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Medium.otf");
			}
			else if (el.FontType.Equals("Light"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}
			else
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}

			label.SetTextSize(Android.Util.ComplexUnitType.Dip, (float)Element.FontSize);
			label.Typeface = font;
		}
	}
}