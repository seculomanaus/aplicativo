using Xamarin.Forms.Platform.Android;
using Seculo.Droid.Renderer;
using Android.Graphics;
using Xamarin.Forms;
using SeculosApp;
using Android.Support.V4.Content;

[assembly: ExportRenderer(typeof(CustomImageButton), typeof(CustomImageButtonRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomImageButtonRenderer : Xamarin.Forms.Platform.Android.AppCompat.ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);

			CustomImageButton CustomImageButton = (CustomImageButton)this.Element;
			var button = (Android.Support.V7.Widget.AppCompatButton)Control;

			button.SetBackground(Resources.GetDrawable(CustomImageButton.Drawable));

			SetNativeControl(button);

		}
	}
}