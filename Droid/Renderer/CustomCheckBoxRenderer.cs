using Seculo.Droid.Renderer;
using SeculosApp;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomCheckBox), typeof(CustomCheckBoxRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomCheckBoxRenderer : ViewRenderer<CustomCheckBox, Android.Support.V7.Widget.AppCompatCheckBox>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<CustomCheckBox> e)
		{
			var checkBox = new Android.Support.V7.Widget.AppCompatCheckBox(this.Context);
			base.OnElementChanged(e);
			if (this.Control == null)
			{
				checkBox = new Android.Support.V7.Widget.AppCompatCheckBox(this.Context);
				this.SetNativeControl(checkBox);
			}
			else
			{
				checkBox = (Android.Support.V7.Widget.AppCompatCheckBox)Control;
			}
			checkBox.Clickable = false;
			System.Diagnostics.Debug.WriteLine("CustomCheckBox: "+checkBox.Checked);
			//checkBox.SetBackground(Resources.GetDrawable(Resource.Drawable.));
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			System.Diagnostics.Debug.WriteLine("OnElementPropertyChanged: " + Element.Checked+" ± "+Control.Checked);
			Control.Checked = Element.Checked;
		}
	}
}