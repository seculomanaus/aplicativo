﻿using SeculosApp;
using SeculosApp.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]
namespace SeculosApp.Droid.Renderer
{
	public class CustomWebViewRenderer : WebViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
		{
			base.OnElementChanged(e);
			if(Control != null)
			{
				var settings = Control.Settings;
				settings.LoadWithOverviewMode = true;
				settings.UseWideViewPort = true;
			}
		}

		protected override void OnDetachedFromWindow()
		{
			base.OnDetachedFromWindow();

			if (Control != null)
			{
				Control.StopLoading();
				Control.LoadUrl(string.Empty);
				Control.Reload();
			}
		}
	}
}
