﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Seculo.Droid.Renderer;
using SeculosApp;

[assembly: ExportRendererAttribute(typeof(RoundedBoxView), typeof(RoundedBoxViewRenderer))]

namespace Seculo.Droid.Renderer
{
	public class RoundedBoxViewRenderer : BoxRenderer
    {
		public RoundedBoxViewRenderer()
		{
			this.SetWillNotDraw(false);
		}

		public override void Draw(Canvas canvas)
		{
			RoundedBoxView rbv = (RoundedBoxView)this.Element;

			Rect rc = new Rect();
			GetDrawingRect(rc);

			Rect interior = rc;
			interior.Inset((int)rbv.StrokeThickness, (int)rbv.StrokeThickness);

			Paint p = new Paint() {
				Color = rbv.Color.ToAndroid(),
				AntiAlias = true,
			};

			canvas.DrawRoundRect(new RectF(interior), (float)rbv.CornerRadius, (float)rbv.CornerRadius, p);

			p.Color = rbv.Stroke.ToAndroid();
			p.StrokeWidth = (float)rbv.StrokeThickness;
			p.SetStyle(Paint.Style.Stroke);

			canvas.DrawRoundRect(new RectF(rc), (float)rbv.CornerRadius, (float)rbv.CornerRadius, p);
		}


    }
}

