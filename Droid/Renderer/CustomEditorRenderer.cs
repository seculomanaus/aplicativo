using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms;
using SeculosApp;
using Seculo.Droid.Renderer;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace Seculo.Droid.Renderer
{
	public class CustomEditorRenderer : EditorRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);
			var label = (Android.Widget.EditText)Control;
			Typeface font = null;

			CustomEditor el = (CustomEditor)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Bold.otf");
			}
			else if (el.FontType.Equals("Book"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Book.otf");
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Medium.otf");
			}
			else if (el.FontType.Equals("Light"))
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}
			else
			{
				font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/Gotham-Light.otf");
			}


			label.ImeOptions = Android.Views.InputMethods.ImeAction.Done;
			label.InputType = Android.Text.InputTypes.ClassText;

			label.SetTextSize(Android.Util.ComplexUnitType.Dip, (float)Element.FontSize);
			label.Typeface = font;

		}
	}
}