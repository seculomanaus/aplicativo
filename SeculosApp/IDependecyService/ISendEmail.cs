﻿namespace SeculosApp
{
	public interface ISendEmail
	{
		void SendMail(string to, string name, string phone, string from, string msg, string subject);
	}
}
