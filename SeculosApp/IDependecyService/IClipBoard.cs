﻿using System;
namespace SeculosApp
{
	public interface IClipBoard
	{
		void CopyToClipboard(string text);
	}
}
