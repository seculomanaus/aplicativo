﻿using System;
namespace SeculosApp
{
	public interface IOpenGallery
	{
		void GetImage(Action<string> callback);
		void FetchBitmap(string imageConverted);
	}
}
