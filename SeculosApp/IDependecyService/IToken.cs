﻿using System;

namespace SeculosApp.IDependecyService
{
	public interface IToken
	{
		string generateTokenGcm();
		void generateTokenGcm(Action<string> callback);
		void Subscribe(string token);
		void DeleteToken();
	}
}
