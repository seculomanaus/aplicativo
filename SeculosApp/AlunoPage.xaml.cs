﻿using System;
using System.IO;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class AlunoPage : AbstractContentPage
	{

		bool isResp;
		public PanelLayoutView PanelLayoutView;
		Command command;

		public AlunoPage()
		{
			InitializeComponent();

			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;

			command = new Command((obj) => onClick(obj));
			btCamera.GestureRecognizers.Add(new TapGestureRecognizer { Command = command, CommandParameter = btCamera.ClassId });

			System.Diagnostics.Debug.WriteLine("User connected: " + SeculosBO.Instance.SeculosGeneral.UserConnected.Nome);
			isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

			initview(true);

			var back = new TapGestureRecognizer();
			var isBack = false;
			var nextPage = false;
			back.Tapped += async (s, e) =>
			{
				if (!isBack && !nextPage)
				{
					isBack = true;
					System.Diagnostics.Debug.WriteLine("back");
					await Utils.FadeView(imgBackStudent);

					try
					{
						App._Current.SendMessage(MessagingConstants.BASE_PHOTO_CHANGED);
						await Navigation.PopAsync();
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("Student back exception: " + exception.Message + " Type: " + exception.GetType());
					}
				}
				System.Diagnostics.Debug.WriteLine("nextPage: " + nextPage + " isBack: " + isBack);
			};
			imgBackStudent.GestureRecognizers.Add(back);

			var tapAcademic = new TapGestureRecognizer();
			tapAcademic.Tapped += async (s, e) =>
			{
				if (!nextPage && !isBack)
				{
					nextPage = true;
					System.Diagnostics.Debug.WriteLine("tapAcademic: ");
					await Utils.FadeView(btnAcademic);
					await Navigation.PushAsync(new StudentProfileDetail(0, "AcademicSemiView"));

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(600).ContinueWith((obj) =>
						{
							nextPage = false;
						});
					});
				}
				System.Diagnostics.Debug.WriteLine("nextPage: " + nextPage + " isBack: " + isBack);
			};
			btnAcademic.GestureRecognizers.Add(tapAcademic);

			var tapFinance = new TapGestureRecognizer();
			tapFinance.Tapped += async (s, e) =>
			{
				if (!nextPage && !isBack)
				{
					nextPage = true;
					System.Diagnostics.Debug.WriteLine("tapFinanceDetailView: ");
					await Utils.FadeView(btnFinance);
					await Navigation.PushAsync(new StudentProfileDetail(2, "FinanceBilletByStudentView"));

					await Task.Factory.StartNew(() =>
					 {
						 Task.Delay(600).ContinueWith((obj) =>
						 {
							 nextPage = false;
							 System.Diagnostics.Debug.WriteLine("nextPage: " + nextPage);
						 });

					 });

				}
			};
			btnFinance.GestureRecognizers.Add(tapFinance);

			var tapNotes = new TapGestureRecognizer();
			tapNotes.Tapped += async (s, e) =>
			{
				if (!nextPage && !isBack)
				{
					nextPage = true;
					System.Diagnostics.Debug.WriteLine("tapGradesView: ");
					await Utils.FadeView(btnNotes);


                    //pai conectado
                    if (SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
                    {
                        await Navigation.PushAsync(new StudentProfileDetail(1, "CantinaView"));
                    }
                    else {
                        await Navigation.PushAsync(new CardapioWebView());
                    }

					await Task.Factory.StartNew(() =>
					 {
						 Task.Delay(600).ContinueWith((obj) =>
						 {
							 nextPage = false;
							 System.Diagnostics.Debug.WriteLine("nextPage: " + nextPage);
						 });

					 });

				}
			};
			btnNotes.GestureRecognizers.Add(tapNotes);

			var tapContact = new TapGestureRecognizer();
			tapContact.Tapped += async (s, e) =>
			{
				if (!nextPage && !isBack)
				{
					nextPage = true;
					System.Diagnostics.Debug.WriteLine("tapContact: ");

					SeculosBO.Instance.OpenModal(EnumModal.CONTACT, false, this);
					await Utils.FadeView(btnContact);

					await Task.Factory.StartNew(() =>
					 {
						 Task.Delay(600).ContinueWith((obj) =>
						 {
							 nextPage = false;
							 System.Diagnostics.Debug.WriteLine("nextPage: " + nextPage);
						 });

					 });

				}
			};
			btnContact.GestureRecognizers.Add(tapContact);


			MessagingCenter.Subscribe<App>(this, MessagingConstants.SALDO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					updateView(createDepentendeObject());
				}
				catch { }
			});

			MessagingCenter.Subscribe<App>(this, MessagingConstants.LIMITE_DIARIO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					updateView(createDepentendeObject());
				}
				catch { }
			});
		}

		void initview(bool init)
		{
			var user = new Dependente();
			if (isResp)
			{
				user.NmNome = SeculosBO.Instance.StudentSelected.NmNome;
				user.NmCurso = SeculosBO.Instance.StudentSelected.NmCurso;
				user.lkFoto = SeculosBO.Instance.StudentSelected.lkFoto;
				user.FotoBase = SeculosBO.Instance.StudentSelected.FotoBase;
				user.TurmaDependente = SeculosBO.Instance.StudentSelected.Turma;
				user.SaldoDependente = SeculosBO.Instance.StudentSelected.Saldo;
				user.LimiteDependente = SeculosBO.Instance.StudentSelected.Limite;

                System.Diagnostics.Debug.WriteLine("user.NmNome: "+user.NmNome);
				updateView(user);
			}
			else
			{
				stkStudentProfile.IsVisible = false;
				stkBottomSection.IsVisible = false;
				loadStudentView.IsVisible = true;
				loadStudentView.Color = Color.FromHex(SeculosBO.Instance.SeculosGeneral.UserStudentConnected.ColorSerie);

				getStudentInfo(user, () =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						System.Diagnostics.Debug.WriteLine("update student info END!");
						user.NmNome = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.NmNome;
						user.NmCurso = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.NmCurso;
						user.lkFoto = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.lkFoto;
						user.FotoBase = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.FotoBase;
						user.TurmaUser = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.Turma;
						user.VlSaldoUser = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.Saldo;
						user.VlLimiteUser = SeculosBO.Instance.SeculosGeneral.UserStudentConnected.Limite;
						TopBackLayout.Padding = new Thickness(0, 0, 0, 0);
						updateView(user);
						SeculosBO.Instance.fromLogin = false;
					});
				});
			}
		}

		async void onClick(object obj)
		{
			var str = (string)obj;

			if (str.Equals(btCamera.ClassId))
			{
				await Utils.FadeView(btCamera);
				var gallery = DependencyService.Get<IOpenGallery>();
				SeculosBO.Instance.changePhotoFromGallery = true;

				gallery.GetImage((imageBase64) =>
				{
					SeculosBO.Instance.changePhotoFromGallery = false;
					if (imageBase64 != null && !imageBase64.Equals(""))
					{
						imgStudentPhoto.Source = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(imageBase64)));

                       

						SeculosBO.Instance.basePhotoChanged = true;

						if (isResp)
						{
                            System.Diagnostics.Debug.WriteLine("galeria 1");
							SeculosBO.Instance.SetBase64ToDependent(SeculosBO.Instance.StudentSelected, imageBase64);
                            System.Diagnostics.Debug.WriteLine("galeria 2");
						}
						else
						{
							SeculosBO.Instance.SetBase64ToDependent(SeculosBO.Instance.SeculosGeneral.UserStudentConnected, imageBase64);

						}

					}
				});

              
			}
		}

		public async void getStudentInfo(Dependente user, Action callback)
		{
			// User keep connected, update User student info
			if (!SeculosBO.Instance.fromLogin)
			{
				System.Diagnostics.Debug.WriteLine("update student info!");
				// update user info
				string msg = await SeculosBO.Instance.getStudentWait(SeculosBO.Instance.SeculosGeneral.UserConnected);
				System.Diagnostics.Debug.WriteLine("Student info updated! " + Application.Current.Resources[msg] as String);
				if (!msg.Equals(EnumCallback.SUCCESS))
				{
					System.Diagnostics.Debug.WriteLine("getStudentInfo error!");
					SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[msg] as String);
				}
				callback();
			}
			else
			{
				callback();
			}
		}

		Dependente createDepentendeObject()
		{
			var user = new Dependente();
			if (isResp)
			{
				user.NmNome = SeculosBO.Instance.StudentSelected.NmNome;
				user.NmCurso = SeculosBO.Instance.StudentSelected.NmCurso;
				user.lkFoto = SeculosBO.Instance.StudentSelected.lkFoto;
				user.FotoBase = SeculosBO.Instance.StudentSelected.FotoBase;
				user.TurmaDependente = SeculosBO.Instance.StudentSelected.Turma;
				user.SaldoDependente = SeculosBO.Instance.StudentSelected.Saldo;
				user.LimiteDependente = SeculosBO.Instance.StudentSelected.Limite;
			}
			return user;
		}

		public void updateView(Dependente user)
		{

            lblStudentName.Text = user.NomeAluno;
			lblCursoName.Text = user.NomeCurso;
			lblCursoName.TextColor = Color.FromHex(user.ColorSerie);
			lblChoiceSection.BackgroundColor = Color.FromHex(user.ColorSerie);

			imgStudentPhoto.Source = user.Foto;

			lblClass.Text = lblClass.Text.ToUpper();
			lblBalance.Text = lblBalance.Text.ToUpper();
			lblLimit.Text = lblLimit.Text.ToUpper();

			lblCodClass.Text = user.Turma.ToUpper();
			try
			{
				var saldo = user.SaldoCash;
				if (saldo.Equals(string.Empty))
				{
					saldo = "00,00";
				}
				lblBalanceValue.Text = "R$ " + saldo;
				if (saldo.Contains("-"))
				{
					lblBalanceValue.TextColor = Color.FromHex("#ff3232");
				}

			}
			catch
			{
				lblBalanceValue.Text = (Application.Current.Resources["not_informed"] as String).ToLower();
			}
			try
			{
				var limite = user.LimiteCash;
				if (limite.Equals(string.Empty))
				{
					limite = "00,00";
				}

				lblLimitValue.Text = "R$ " + limite;
			}
			catch
			{
				lblLimitValue.Text = (Application.Current.Resources["not_informed"] as String).ToLower();
			}

			if (isResp)
			{
				relTopBar.HeightRequest = 0;
				imgBackStudent.IsVisible = true;
				stackFinanceContact.IsVisible = true;
			}
			else
			{
				PanelLayoutView = new PanelLayoutView(configPanel, panelBack, btnConfig, btnNoti, btnChat,
							  btnContactPanel, btnAddCard, btnExit, Navigation, this);
				btnDayleConsumption.IsVisible = false;
				btnAddCard.IsVisible = false;

				lblChoiceSection.HeightRequest = 60;
				lblStudentName.Margin = new Thickness(0, 10, 0, 0);
				btnAcademic.Margin = new Thickness(0, 0, -2, 0);
				btnNotes.Margin = new Thickness(-2, 0, 0, 0);
				StackLayout tempRel = null;
				foreach (Xamarin.Forms.View rel in relAlunoPage.Children)
				{
					if (rel != null && rel.ClassId != null && "stkBottomSectionId".Equals(rel.ClassId))
					{
						tempRel = rel as StackLayout;
						relAlunoPage.Children.Remove(rel);
						break;
					}
				}

				if (tempRel != null)
				{
					tempRel.Margin = new Thickness(10, 10, 10, 10);
					relAlunoPage.Children.Add(tempRel,
												  widthConstraint: Constraint.RelativeToParent((parent) => { return parent.Width; }),
												  heightConstraint: Constraint.RelativeToParent((parent) => { return (.4 * parent.Height); }),//.4
												  yConstraint: Constraint.RelativeToView(stkStudentProfile, (parent, sibling) => { return sibling.Height + (.1 * parent.Height); })
											 );
					relAlunoPage.RaiseChild(panelBack);
					relAlunoPage.RaiseChild(configPanel);
					relAlunoPage.RaiseChild(relContentModal);
				}
			}

			stkStudentProfile.Opacity = 0;
			stkBottomSection.Opacity = 0;
			stkStudentProfile.IsVisible = true;
			stkBottomSection.IsVisible = true;
			loadStudentView.IsVisible = false;

			stkStudentProfile.FadeTo(1, 300, null);
			stkBottomSection.FadeTo(1, 300, null);
		}

		protected override bool OnBackButtonPressed()
		{
			var superBack = base.OnBackButtonPressed();

			if (!isResp && PanelLayoutView != null && PanelLayoutView.PanelLayout.isOpen())
			{
				if (!PanelLayoutView.PanelLayout.isProgress())
				{
					PanelLayoutView.PanelLayout.tooglePanel();
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Panel Layout Animation in progress...");
				}
				return true;
			}
			else
			{
				App._Current.SendMessage(MessagingConstants.BASE_PHOTO_CHANGED);
				return superBack;
			}
		}

		protected override void OnAppearing()
		{
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;

			if (App.ScreenWidth <= 320)
			{
				DayleConsumptionLabel.FontSize = 14.5;
				ContactLabel.FontSize = 14.5;
				AddCardLabel.FontSize = 14.5;
			}

			base.OnAppearing();
		}
	}
}