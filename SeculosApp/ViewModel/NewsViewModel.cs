﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp.ViewModel
{
	public class NewsViewModel : BaseViewModel
	{
		public ObservableCollection<Noticia> News { get; set; }

		private ObservableCollection<Noticia> auxListNews;

		public NewsViewModel()
		{
			Title = "Noticia";
			News = new ObservableCollection<Noticia>();
			auxListNews = new ObservableCollection<Noticia>();

		}

		Command loadNewsCommand;

		public Command LoadNewsCommand
		{
			get
			{
				return loadNewsCommand ??
				  (loadNewsCommand = new Command(async () =>
				  {
					  await ExecuteLoadNewsCommand();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

	    async Task ExecuteLoadNewsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			CanLoadMore = false;
			var msg = await SeculosBO.Instance.getNoticiasWait();

            System.Diagnostics.Debug.WriteLine("msg noticias: " + msg);

			LoadNewsCommand.ChangeCanExecute();

			var listNews = SeculosBO.Instance.SeculosGeneral.listNoticias;

			lblErrorEmpty.IsVisible = false;
			var visible = false;

			if (msg.Equals(EnumCallback.SUCCESS))
			{
				News.Clear();
				if (listNews == null || listNews.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_NEWS_EMPTY, lblErrorEmpty, null);
				}
				else
				{
					await AddNotification();

					for (var i = 0; i < listNews.Count; i++)
                    {
                        if (i<7)
                        {
							News.Add(listNews[i]);
                        }
                        else
                        {
							auxListNews.Add(listNews[i]);
                        }

                    }

				}
			}
			else if (msg.Equals(EnumCallback.DATA_NEWS_ERROR))
			{
				if (listNews == null || listNews.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_NEWS_ERROR, lblErrorEmpty, null);
				}
			}
			else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
			{
				if ((listNews != null && listNews.Count > 0) && News.Count == 0)
				{
					await AddNotification();
					foreach (Noticia noticia in listNews)
					{
						News.Add(noticia);
					}
				}
				visible = true;
			}
			if (viewError.IsVisible && !visible && viewError.TranslationY == 0)
			{
				// close
				await viewError.TranslateTo(0, viewError.Height, 80, Easing.SpringIn);
				viewError.IsVisible = false;
				await viewError.TranslateTo(0, 0, 0);
			}
			else if (!viewError.IsVisible && visible && viewError.TranslationY == 0)
			{
				try
				{
					// open
					await viewError.TranslateTo(0, viewError.Height, 0);
					viewError.IsVisible = true;
					await viewError.TranslateTo(0, 0, 100, Easing.SpringIn);
				}
				catch { }
			}

			IsBusy = false;
			CanLoadMore = true;
			LoadNewsCommand.ChangeCanExecute();
		}

		async Task AddNotification()
		{
			var msgNotification = await SeculosBO.Instance.getNotificationWaitWs(SeculosBO.Instance.SeculosGeneral.UserConnected);
			ObservableCollection<Notificacao> listNotifications = SeculosBO.Instance.SeculosGeneral.UserConnected.getAllListNotifications();
			if (listNotifications != null)
			{
				Noticia noticiaNoti = new Noticia();
				for (int i = 0; i < listNotifications.Count; i++)
				{
					if (i == 0)
					{
						noticiaNoti.notificacao1 = listNotifications[i];
						noticiaNoti.isNotification1 = true;
					}
					else if (i == 1)
					{
						noticiaNoti.notificacao2 = listNotifications[i];
						noticiaNoti.isNotification2 = true;
					}
					else if (i == 2)
					{
						noticiaNoti.notificacao3 = listNotifications[i];
						noticiaNoti.isNotification3 = true;
					}
					else if (i == 3)
					{
						noticiaNoti.isSeeAll = true;
					}
					noticiaNoti.isNotification = true;
				}
				if (noticiaNoti.isNotification)
				{
					News.Add(noticiaNoti);
				}
			}
		}

		public void AddMoreNews()
        {
            foreach (Noticia noticia in auxListNews)
            {
                News.Add(noticia);
            }

			auxListNews.Clear();
			CanLoadMore = false;
			LoadNewsCommand.ChangeCanExecute();
		}


	}
}
