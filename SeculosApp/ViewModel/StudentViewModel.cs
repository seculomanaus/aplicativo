﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plugin.FirebasePushNotification;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp.ViewModel
{
	public class StudentViewModel : BaseViewModel
	{
		public ObservableCollection<Dependente> Students { get; set; }

		public StudentViewModel()
		{
			Title = "Alunos";
			Students = new ObservableCollection<Dependente>();

		}

		Command loadStudentsCommand;

		public Command LoadStudentsCommand
		{
			get
			{
				return loadStudentsCommand ??
				  (loadStudentsCommand = new Command(async () =>
				  {
					  await ExecuteLoadStudentsCommand();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

		public Command loadStudentsCommandOffLine;

		public Command LoadStudentsCommandOffLine
		{
			get
			{
				return loadStudentsCommandOffLine ??
				  (loadStudentsCommandOffLine = new Command(async () =>
				  {
					  await ExecuteLoadStudentsCommandOffLine();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

		private async Task ExecuteLoadStudentsCommandOffLine()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			loadStudentsCommandOffLine.ChangeCanExecute();

			var listStudents = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;

			lblErrorEmpty.IsVisible = false;

			Students.Clear();
			if (listStudents.Count == 0)
			{
				SeculosBO.Instance.showViewError(EnumCallback.DATA_STUDENT_EMPTY, lblErrorEmpty, null);
			}
			else
			{
				foreach (Dependente dep in listStudents)
				{
					Students.Add(dep);
				}
			}

			IsBusy = false;
			loadStudentsCommandOffLine.ChangeCanExecute();
		}

		private async Task ExecuteLoadStudentsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			var msg = await SeculosBO.Instance.getDependentesWait(SeculosBO.Instance.SeculosGeneral.UserConnected);

            System.Diagnostics.Debug.WriteLine("msg alunos: " + msg);

			LoadStudentsCommand.ChangeCanExecute();

			var listStudents = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;
	
			lblErrorEmpty.IsVisible = false;
			var visible = false;

			if (msg.Equals(EnumCallback.SUCCESS))
			{
				Students.Clear();
				if (listStudents == null || listStudents.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_STUDENT_EMPTY, lblErrorEmpty, null);
				}
				else
				{
					foreach (Dependente dependente in listStudents)
					{
                        //System.Diagnostics.Debug.WriteLine("dependente.Turma " + dependente.Turma);
                        CrossFirebasePushNotification.Current.Subscribe(dependente.Turma + "");
						Students.Add(dependente);
					}
				}
			}
			else if (msg.Equals(EnumCallback.DATA_STUDENT_ERROR))
			{
			    if (listStudents == null || listStudents.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_STUDENT_ERROR, lblErrorEmpty, null);
				}
		    }
			else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
			{
				if ((listStudents != null && listStudents.Count > 0) && Students.Count == 0)
				{
					foreach (Dependente dependente in listStudents)
					{
						Students.Add(dependente);
					}
				}
				visible = true;
			}

			if (viewError.IsVisible && !visible && viewError.TranslationY == 0)
			{
				// close
				await viewError.TranslateTo(0, viewError.Height, 80, Easing.SpringIn);
				viewError.IsVisible = false;
				await viewError.TranslateTo(0, 0, 0);
			}
			else if (!viewError.IsVisible && visible && viewError.TranslationY == 0)
			{
				try
				{
					// open
					await viewError.TranslateTo(0, viewError.Height, 0);
					viewError.IsVisible = true;
					await viewError.TranslateTo(0, 0, 100, Easing.SpringIn);
				}
				catch { }
			}

			IsBusy = false;
			LoadStudentsCommand.ChangeCanExecute();
		}
	}
}
