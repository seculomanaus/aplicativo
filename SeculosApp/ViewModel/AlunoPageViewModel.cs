using System.ComponentModel;
using Xamarin.Forms;

namespace SeculosApp
{

	public class AlunoPageViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		ConstraintExpression constraintExpression;

		public AlunoPageViewModel(ConstraintExpression constraintExpression)
		{
			this.constraintExpression = constraintExpression;
		}

		public ConstraintExpression HeightConstraint
		{
			set
			{
				if (constraintExpression != value)
				{
					constraintExpression = value;

					if (PropertyChanged != null)
					{
						PropertyChanged(this,
							new PropertyChangedEventArgs("HeightConstraint"));
					}
				}
			}
			get
			{
				return constraintExpression;
			}
		}
	}
}
