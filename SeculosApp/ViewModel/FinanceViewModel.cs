﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.View;
using Xamarin.Forms;

namespace SeculosApp
{
	public class FinanceViewModel: BaseViewModel
	{
		public ObservableCollection<Boleto> Billets { get; set; }

		public FinanceViewModel()
		{
			Title = "Boletos";
			Billets = new ObservableCollection<Boleto>();

		}

		private Command loaBilletsCommand;

		public Command LoadBilletsCommand
		{
			get
			{
				return loaBilletsCommand ??
				  (loaBilletsCommand = new Command(async () =>
				  {
					  await ExecuteLoadBilletsCommand();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

		private async Task ExecuteLoadBilletsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			var msg = await SeculosBO.Instance.getBoletosWait(SeculosBO.Instance.SeculosGeneral.UserConnected);
			LoadBilletsCommand.ChangeCanExecute();

            System.Diagnostics.Debug.WriteLine("msg financas: " + msg);

			var monthSelectedPicker = SeculosBO.Instance.monthSelectedPicker;

			if (FinanceView.StudentDetail)
			{
				monthSelectedPicker = SeculosBO.Instance.monthStudentSelectedPicker;
			}

			if (SeculosBO.Instance.SeculosGeneral == null || SeculosBO.Instance.SeculosGeneral.UserConnected == null)
			{
				IsBusy = false;
				LoadBilletsCommand.ChangeCanExecute();
				return;
			}

			var listBillets = SeculosBO.Instance.SeculosGeneral.UserConnected.listBilletByStudent(SeculosBO.Instance.studentSelectedPicker, monthSelectedPicker);

			lblErrorEmpty.IsVisible = false;
			var visible = false;


            if (msg.Equals(EnumCallback.SUCCESS))
			{
				Billets.Clear();
				if (listBillets == null || listBillets.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_BILLET_EMPTY, lblErrorEmpty, null);
				}
				else
				{
					foreach (Boleto boleto in listBillets)
					{
						Billets.Add(boleto);
					}
				}
			}
			else if (msg.Equals(EnumCallback.DATA_BILLET_ERROR))
			{
				if (listBillets == null || listBillets.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_BILLET_ERROR, lblErrorEmpty, null);
				}
			}
			else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
			{
				if ((listBillets != null && listBillets.Count > 0) && Billets.Count == 0)
				{
					foreach (Boleto boleto in listBillets)
					{
						Billets.Add(boleto);
					}
				}
				visible = true;
			}

			if (viewError.IsVisible && !visible && viewError.TranslationY == 0)
			{
				// close
				await viewError.TranslateTo(0, viewError.Height, 80, Easing.SpringIn);
				viewError.IsVisible = false;
				await viewError.TranslateTo(0, 0, 0);
			}
			else if (!viewError.IsVisible && visible && viewError.TranslationY == 0)
			{
				try
				{
					// open
					await viewError.TranslateTo(0, viewError.Height, 0);
					viewError.IsVisible = true;
					await viewError.TranslateTo(0, 0, 100, Easing.SpringIn);
				}
				catch { }
			}

			IsBusy = false;
			LoadBilletsCommand.ChangeCanExecute();
		}

		private Command loaBilletsCommandOffLine;

		public Command LoadBilletsCommandOffLine
		{
			get
			{
				return loaBilletsCommandOffLine ??
				  (loaBilletsCommandOffLine = new Command(() =>
                  {
                      ExecuteLoadBilletsCommandOffLine();
                  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

		void ExecuteLoadBilletsCommandOffLine()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			LoadBilletsCommandOffLine.ChangeCanExecute();

			var monthSelectedPicker = SeculosBO.Instance.monthSelectedPicker;

			if (FinanceView.StudentDetail)
			{
				monthSelectedPicker = SeculosBO.Instance.monthStudentSelectedPicker;
			}

			var listBillets = SeculosBO.Instance.SeculosGeneral.UserConnected.listBilletByStudent(SeculosBO.Instance.studentSelectedPicker, monthSelectedPicker);

			lblErrorEmpty.IsVisible = false;

			Billets.Clear();

			if (listBillets != null)
			{
				if (listBillets.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_BILLET_EMPTY, lblErrorEmpty, null);
				}
				else
				{
					foreach (Boleto value in listBillets)
					{
						Billets.Add(value);
					}
				}
			}
			else
			{
				SeculosBO.Instance.showViewError(EnumCallback.DATA_BILLET_EMPTY, lblErrorEmpty, null);
			}

            IsBusy = false;
            LoadBilletsCommandOffLine.ChangeCanExecute();
        }
    }
}
