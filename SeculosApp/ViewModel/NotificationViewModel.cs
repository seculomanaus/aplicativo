﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public class NotificationViewModel: BaseViewModel
	{
		public ObservableCollection<Notificacao> Notification { get; set; }

		public NotificationViewModel()
		{
			Title = "Notificações";
			Notification = new ObservableCollection<Notificacao>();

		}

		private Command loadNotificationCommand;

		public Command LoadNotificationCommand
		{
			get
			{
				return loadNotificationCommand ??
				  (loadNotificationCommand = new Command(async () =>
				  {
					  await ExecuteLoadNotificationCommand();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

		private async Task ExecuteLoadNotificationCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			var msg = await SeculosBO.Instance.getNotificationWaitWs(SeculosBO.Instance.SeculosGeneral.UserConnected);

            System.Diagnostics.Debug.WriteLine("msg notification: " + msg);

			LoadNotificationCommand.ChangeCanExecute();

			var listNotifications = SeculosBO.Instance.SeculosGeneral.UserConnected.getAllListNotifications();

			lblErrorEmpty.IsVisible = false;
			var visible = false;

			if (msg.Equals(EnumCallback.SUCCESS))
			{
				Notification.Clear();
				if (listNotifications == null || listNotifications.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblErrorEmpty, null);
				}
				else
				{
					foreach (Notificacao notificacao in listNotifications)
					{
						Notification.Add(notificacao);
					}
				}
			}
			else if (msg.Equals(EnumCallback.NOTIFICATION_ERROR))
			{
				if (listNotifications == null || listNotifications.Count == 0)
				{
					SeculosBO.Instance.showViewError(EnumCallback.NOTIFICATION_ERROR, lblErrorEmpty, null);
				}
				else
				{
					foreach (Notificacao notificacao in listNotifications)
					{
						Notification.Add(notificacao);
					}
				}
			}
			else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
			{
				if ((listNotifications != null && listNotifications.Count > 0) && Notification.Count == 0)
				{
					foreach (Notificacao notificacao in listNotifications)
					{
						Notification.Add(notificacao);
					}
				}
				visible = true;
			}

			if (viewError.IsVisible && !visible && viewError.TranslationY == 0)
			{
				// close
				await viewError.TranslateTo(0, viewError.Height, 100, Easing.SpringIn);
				viewError.IsVisible = false;
				await viewError.TranslateTo(0, 0, 0);
			}
			else if (!viewError.IsVisible && visible && viewError.TranslationY == 0)
			{
				// open
				await viewError.TranslateTo(0, viewError.Height, 0);
				viewError.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				viewError.TranslateTo(0, 0, 120, Easing.SpringIn);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			}

			IsBusy = false;
			LoadNotificationCommand.ChangeCanExecute();
		}

		private Command loadNotificationCommandOffLine;

		public Command LoadNotificationCommandOffLine
		{
			get
			{
				return loadNotificationCommandOffLine ??
				  (loadNotificationCommandOffLine = new Command(async () =>
				  {
					  await ExecuteLoadNotificationCommandOffLine();
				  }, () =>
				  {
					  return !IsBusy;
				  }));
			}
		}

#pragma warning disable CS1998 // Este método assíncrono não possui operadores 'await' e será executado de modo síncrono. É recomendável o uso do operador 'await' para aguardar chamadas à API desbloqueadas ou do operador 'await Task.Run(...)' para realizar um trabalho associado à CPU em um thread em segundo plano.
		private async Task ExecuteLoadNotificationCommandOffLine()
#pragma warning restore CS1998 // Este método assíncrono não possui operadores 'await' e será executado de modo síncrono. É recomendável o uso do operador 'await' para aguardar chamadas à API desbloqueadas ou do operador 'await Task.Run(...)' para realizar um trabalho associado à CPU em um thread em segundo plano.
		{
			if (IsBusy)
				return;

			IsBusy = true;

			LoadNotificationCommandOffLine.ChangeCanExecute();

			var listNotifications = SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications;

			lblErrorEmpty.IsVisible = false;

			Notification.Clear();
			if (listNotifications.Count == 0)
			{
				SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblErrorEmpty, null);
			}
			else
			{
				foreach (Notificacao notificacao in listNotifications)
				{
					Notification.Add(notificacao);
				}
			}

			IsBusy = false;
			LoadNotificationCommandOffLine.ChangeCanExecute();
		}
	}
}
