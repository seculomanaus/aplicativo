﻿using System;

using Xamarin.Forms;

namespace SeculosApp
{
	public abstract class AbstractContentPage : ContentPage
	{
		public static AbstractContentPage _AbstractContentPage;
		public static Grid RelContentModal;
		public Grid relContentModalExtends { get; set; }
		public AbstractContentPage abstractContentPageExtends { get; set; }

		public AbstractContentPage()
		{
			_AbstractContentPage = this;

			NavigationPage.SetHasNavigationBar(this, false);

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				Padding = new Thickness(0, 40, 0, 0);
			}
		}

		protected override bool OnBackButtonPressed()
		{
			base.OnBackButtonPressed();

			if (RelContentModal != null && RelContentModal.Children.Count > 0)
			{
				if (!CreditLimitModal.loading && !ComunicationModal.loading && !PayCreditModal.loading)
				{
					RelContentModal.Children.RemoveAt(RelContentModal.Children.Count - 1);
					RelContentModal.IsVisible = false;
				}

				return true;
			}
			else if (relContentModalExtends != null && relContentModalExtends.Children.Count > 0)
			{
				if (!CreditLimitModal.loading && !ComunicationModal.loading && !PayCreditModal.loading)
				{
					relContentModalExtends.Children.RemoveAt(relContentModalExtends.Children.Count - 1);
					relContentModalExtends.IsVisible = false;
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			_AbstractContentPage = this;
			SeculosBO.Instance.OnResumeVerifyUpdate();
			SeculosBO.Instance.Navigation = Navigation;
		}

		public async void openNotification(string notificationCode)
		{
			await Navigation.PushAsync(new NotificationView(notificationCode));
		}
	}
}

