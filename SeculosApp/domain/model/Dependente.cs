﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using Newtonsoft.Json;
using Seculos.Model;
using Xamarin.Forms;

namespace SeculosApp.Model
{
	public class Dependente : GenericModel, INotifyPropertyChanged
	{
		public Dependente()
		{
			mapOcorrencia = new Dictionary<string, ObservableCollection<Ocorrencia>>();
			mapDiario = new Dictionary<string, ObservableCollection<Diario>>();
			mapRegistroDiario = new Dictionary<string, ObservableCollection<RegistroDiario>>();
			listTempo = new ObservableCollection<Tempo>();
			listConteudo= new ObservableCollection<Conteudo>();
			listAcompanhamentoInfantil = new ObservableCollection<AcompanhamentoInfantil>();
			Frequencia = new ObservableCollection<Frequencia>();
			Cardapios = new ObservableCollection<Cardapio>();
			CalendarioProva = new ObservableCollection<Prova>();
			mapAcompanhamentoDiario = new Dictionary<string, ObservableCollection<AcompanhamentoDiario>>();
			mapGabarito = new Dictionary<string, ObservableCollection<Gabarito>>();
			listCalendarAcademic = new ObservableCollection<CalendarAcademic>();
			listAgendaEventos = new ObservableCollection<CalendarAcademic>();
			listConsumo = new List<Consumo>();
		}

		public ConteudoBim Conteudo1Bim { get; set; }
		public ConteudoBim Conteudo2Bim { get; set; }
		public ConteudoBim Conteudo3Bim { get; set; }
		public ConteudoBim Conteudo4Bim { get; set; }

		public InfantilBim Infantil1Bim { get; set; }
		public InfantilBim Infantil2Bim { get; set; }
		public InfantilBim Infantil3Bim { get; set; }
		public InfantilBim Infantil4Bim { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "cd_matricula")]
		public string CdMatricula { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		public string NmNome { get; set; }

		[JsonIgnore]
		public string NomeAluno
		{
			get
			{
				return Utils.PascalCase(NmNome);
			}
		}

		[JsonProperty(PropertyName = "lk_foto")]
		public string lkFoto { get; set; }

		ImageSource _Foto;

		[JsonIgnore]
		public ImageSource Foto
		{
			get
			{
				if (FotoBase != null)
				{
					return ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(FotoBase)));
				}
				else
				{
					return ImageSource.FromUri(new Uri(lkFoto));
				}
			}
			set
			{
				_Foto = value;
			}
		}

		public string FotoBase { get; set; }

		[JsonProperty(PropertyName = "cd_curso")]
		public int CdCurso { get; set; }

		[JsonProperty(PropertyName = "nm_curso")]
		public string NmCurso { get; set; }

		[JsonIgnore]
		public string NomeCurso
		{
			get
			{
				return NmCurso;
			}
		}

		[JsonProperty(PropertyName = "cd_serie")]
		public int CdSerie { get; set; }

		[JsonProperty(PropertyName = "email")]
		public string Email { get; set; }

		[JsonProperty(PropertyName = "celuar")]
		public string Celuar { get; set; }

		[JsonProperty(PropertyName = "turma")]
		public string TurmaDependente { get; set; }

		[JsonProperty(PropertyName = "cd_turma")]
		public string TurmaUser { get; set; }

		[JsonIgnore]
		public string Turma
		{
			get
			{
				if (TurmaDependente != null)
				{
					return TurmaDependente;
				}
				else
				{
					return TurmaUser;
				}
			}
		}

		[JsonProperty(PropertyName = "saldo")]
		string _SaldoDependente { get; set; }
		public string SaldoDependente
		{
			get
			{
				return getDefault(_SaldoDependente);
			}
			set
			{
				_SaldoDependente = value;
			}
		}

		[JsonProperty(PropertyName = "limite")]
		string _LimiteDependente { get; set; }
		public string LimiteDependente
		{
			get
			{
				return getDefault(_LimiteDependente);
			}
			set
			{
				_LimiteDependente = value;
			}
		}

		[JsonProperty(PropertyName = "vl_saldo")]
		string _VlSaldoUser { get; set; }
		public string VlSaldoUser
		{
			get
			{
				return getDefault(_VlSaldoUser);
			}
			set
			{
				_VlSaldoUser = value;
			}
		}

		[JsonProperty(PropertyName = "vl_limite")]
		string _VlLimiteUser { get; set; }
		public string VlLimiteUser
		{
			get
			{
				return getDefault(_VlLimiteUser);
			}
			set
			{
				_VlLimiteUser = value;
			}
		}

		[JsonIgnore]
		public string Saldo
		{
			get
			{
				if (SaldoDependente != null && !SaldoDependente.Equals(SeculosBO.EMPTY))
				{
					return SaldoDependente;
				}
				else if (VlSaldoUser != null && !VlSaldoUser.Equals(SeculosBO.EMPTY))
				{
					return VlSaldoUser;
				}
				else 
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		[JsonIgnore]
		public string SaldoCash
		{
			get
			{
				if (SaldoDependente != null && !SaldoDependente.Equals(SeculosBO.EMPTY))
				{
					return Utils.formatCurrency(20, SaldoDependente);
				}
				else if (VlSaldoUser != null && !VlSaldoUser.Equals(SeculosBO.EMPTY))
				{
					return Utils.formatCurrency(20, VlSaldoUser);
				}
				else
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		[JsonIgnore]
		public string Limite
		{
			get
			{
				if (LimiteDependente != null && !LimiteDependente.Equals(SeculosBO.EMPTY))
				{
					return LimiteDependente.Trim();
				}
				else if (VlLimiteUser != null && !VlLimiteUser.Equals(SeculosBO.EMPTY))
				{
					return VlLimiteUser;
				}
				else
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		[JsonIgnore]
		public string LimiteCash
		{
			get
			{
				if (LimiteDependente != null && LimiteDependente.ToLower().Equals("ilimitado"))
				{
					return LimiteDependente;
				}
				else if (LimiteDependente != null && !LimiteDependente.Equals(SeculosBO.EMPTY))
				{
					return Utils.formatCurrency(20, LimiteDependente);
				}
				else if (VlLimiteUser != null && VlLimiteUser.ToLower().Equals("ilimitado"))
				{
					return VlLimiteUser;
				}
				else if (VlLimiteUser != null && !VlLimiteUser.Equals(SeculosBO.EMPTY))
				{
					return Utils.formatCurrency(20, VlLimiteUser);
				}
				else
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		public bool IsInfantil()
		{

			if (NmCurso.ToUpper().Contains("Infantil".ToUpper()))
			{
				return true;
			}
			return false;

		}

		public bool IsFundI()
		{

			if (CdCurso == 2)
			{
				return true;
			}
			return false;

		}

		[JsonIgnore]
		public string ColorSerie
		{
			get
			{
				if (NmCurso.ToUpper().Contains("Fundamental".ToUpper()))
				{
					return "#f8ce33";
				}
				else if (NmCurso.ToUpper().Contains("Médio".ToUpper()))
				{
					return "#e149b0";
				}
				else if (NmCurso.ToUpper().Contains("Infantil".ToUpper()))
				{
					return "#41c454";
				}
				return string.Empty;
			}
		}

		[JsonIgnore]
		public string ArrowRight
		{
			get
			{
				if (NmCurso.ToUpper().Contains("Fundamental".ToUpper()))
				{
					return "arrow_right_yellow_ic.png";
				}
				else if (NmCurso.ToUpper().Contains("Médio".ToUpper()))
				{
					return "arrow_right_pink_ic.png";
				}
				else if (NmCurso.ToUpper().Contains("Infantil".ToUpper()))
				{
					return "arrow_right_green_ic.png";
				}
				return string.Empty;
			}
		}

		[JsonIgnore]
		public string ArrowUp
		{
			get
			{
				if (NmCurso.ToUpper().Contains("Fundamental".ToUpper()))
				{
					return "arrow_up_yellow_ic.png";
				}
				else if (NmCurso.ToUpper().Contains("Médio".ToUpper()))
				{
					return "arrow_up_pink_ic.png";
				}
				else if (NmCurso.ToUpper().Contains("Infantil".ToUpper()))
				{
					return "arrow_up_green_ic.png";
				}
				return string.Empty;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var changed = PropertyChanged;
			if (changed != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		[JsonIgnore]
		string limitCurrency;

		[JsonIgnore]
		public string LimitCurrency
		{
			get
			{
				System.Diagnostics.Debug.WriteLine("LimiteTemp limitCurrency: " + limitCurrency);
				return limitCurrency;
			}

			set
			{
				limitDecimal = Utils.stringToDecimal(value);
				limitCurrency = value;
			}
		}

		[JsonIgnore]
		string limitDecimal;


		[JsonIgnore]
		public string LimitDecimal
		{
			get
			{
				return limitDecimal;
			}

		}

		public Dictionary<string, ObservableCollection<Ocorrencia>> mapOcorrencia { get; set; }

		public ObservableCollection<Model.Ocorrencia> listOcorrenciaByDate<Ocorrencia>(string date)
		{
			ObservableCollection<Model.Ocorrencia> value;
			if (mapOcorrencia.TryGetValue(date, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		public Dictionary<string, ObservableCollection<RegistroDiario>> mapRegistroDiario { get; set; }

		public ObservableCollection<Model.RegistroDiario> listRegistroDiarioByDate<RegistroDiario>(string date)
		{
			ObservableCollection<Model.RegistroDiario> value;
			if (mapRegistroDiario.TryGetValue(date, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		public Dictionary<string, ObservableCollection<Diario>> mapDiario { get; set; }

		public ObservableCollection<Model.Diario> listDiarioByDate<Diario>(string date)
		{
			ObservableCollection<Model.Diario> value;
			if (mapDiario.TryGetValue(date, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		public ObservableCollection<Tempo> listTempo { get; set; }
		public ObservableCollection<TempoDia> listSeg { get; set; }
		public ObservableCollection<TempoDia> listTer { get; set; }
		public ObservableCollection<TempoDia> listQua { get; set; }
		public ObservableCollection<TempoDia> listQui { get; set; }
		public ObservableCollection<TempoDia> listSex { get; set; }
		public ObservableCollection<TempoDia> listSab { get; set; }
		public ObservableCollection<TempoDia> GetListByDay(ObservableCollection<TempoDia> list, string turno)
		{
			ObservableCollection<TempoDia> tempList = new ObservableCollection<TempoDia>();
			foreach (TempoDia tempoDia in list)
			{
				if (tempoDia.Turno.Equals(turno))
				{
					tempList.Add(tempoDia);
				}
			}
			return tempList;
		}

		public void createListTempoDia()
		{
			listSeg = new ObservableCollection<TempoDia>();
			listTer = new ObservableCollection<TempoDia>();
			listQua = new ObservableCollection<TempoDia>();
			listQui = new ObservableCollection<TempoDia>();
			listSex = new ObservableCollection<TempoDia>();
			listSab = new ObservableCollection<TempoDia>();
			foreach (Tempo tempo in listTempo)
			{
				listSeg.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.SgDisciplina, tempo.FlTurno, tempo.SgConteudo, tempo.SgProfessor));
				listTer.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.TcDisciplina, tempo.FlTurno, tempo.TcConteudo, tempo.TcProfessor));
				listQua.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.QaDisciplina, tempo.FlTurno, tempo.QaConteudo, tempo.QaProfessor));
				listQui.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.QiDisciplina, tempo.FlTurno, tempo.QiConteudo, tempo.QiProfessor));
				listSex.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.SxDisciplina, tempo.FlTurno, tempo.SxConteudo, tempo.SxProfessor));
				listSab.Add(new TempoDia(tempo.NrTempo, tempo.HrInicio, tempo.SbDisciplina, tempo.FlTurno, tempo.SbConteudo, tempo.SbProfessor));
			}

		}

		public ObservableCollection<Conteudo> listConteudo { get; set; }

		public Dictionary<string, ObservableCollection<Conteudo>> mapConteudoBim { get; set; }

		public ObservableCollection<Conteudo> listConteudo1Bim { get; set; }
		public ObservableCollection<Conteudo> listConteudo2Bim { get; set; }
		public ObservableCollection<Conteudo> listConteudo3Bim { get; set; }
		public ObservableCollection<Conteudo> listConteudo4Bim { get; set; }

		public void createListConteudoBim()
		{
			listConteudo1Bim = new ObservableCollection<Conteudo>();
			listConteudo2Bim = new ObservableCollection<Conteudo>();
			listConteudo3Bim = new ObservableCollection<Conteudo>();
			listConteudo4Bim = new ObservableCollection<Conteudo>();
			foreach (Conteudo conteudo in listConteudo)
			{
				switch (conteudo.NrBimestre)
				{
					case "1":
						listConteudo1Bim.Add(conteudo);
						break;
					case "2":
						listConteudo2Bim.Add(conteudo);
						break;
					case "3":
						listConteudo3Bim.Add(conteudo);
						break;
					case "4":
						listConteudo4Bim.Add(conteudo);
						break;
				}

			}
			Conteudo1Bim = new ConteudoBim();
			createListConteudoDisciplina(listConteudo1Bim, Conteudo1Bim);
			Conteudo2Bim = new ConteudoBim();
			createListConteudoDisciplina(listConteudo2Bim, Conteudo2Bim);
			Conteudo3Bim = new ConteudoBim();
			createListConteudoDisciplina(listConteudo3Bim, Conteudo3Bim);
			Conteudo4Bim = new ConteudoBim();
			createListConteudoDisciplina(listConteudo4Bim, Conteudo4Bim);
		}

		public void createListConteudoDisciplina(ObservableCollection<Conteudo> list, ConteudoBim ConteudoBim)
		{
			foreach (Conteudo conteudo in list)
			{
				if (ConteudoBim.listConteudoDisciplinaByDisc<ConteudoDisciplina>(conteudo.NomeDisciplina) == null)
				{
					ObservableCollection<ConteudoDisciplina> listDis = new ObservableCollection<ConteudoDisciplina>();
					ConteudoBim.mapConteudoDisciplina.Add(conteudo.NomeDisciplina, listDis);
				}
				ConteudoDisciplina ConteudoDisciplina = new ConteudoDisciplina(conteudo.TpProva, conteudo.DsConteudo);
				ConteudoBim.listConteudoDisciplinaByDisc<ConteudoDisciplina>(conteudo.NomeDisciplina).Add(ConteudoDisciplina);

			}
		}

		public Dictionary<string, ObservableCollection<AcompanhamentoDiario>> mapAcompanhamentoDiario { get; set; }

		public ObservableCollection<Model.AcompanhamentoDiario> listAcompanhamentoDiarioByDate<AcompanhamentoInfantil>(string date)
		{
			ObservableCollection<Model.AcompanhamentoDiario> value;
			if (mapAcompanhamentoDiario.TryGetValue(date, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		public Dictionary<string, ObservableCollection<Gabarito>> mapGabarito { get; set; }

		public ObservableCollection<Model.Gabarito> listGabaritoByType<Gabarito>(string type)
		{
			ObservableCollection<Model.Gabarito> value;
			if (mapGabarito.TryGetValue(type, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

		public void createMapGabaritoByType(ObservableCollection<Gabarito> userGabaritoResult)
		{
			List<String> gabaritoTypes = new List<string>();
			foreach (Gabarito gabarito in userGabaritoResult)
			{
				if (!gabaritoTypes.Contains(gabarito.NmProva.ToUpper()))
				{
					gabaritoTypes.Add(gabarito.NmProva.ToUpper());
				}
			}
			foreach (String type in gabaritoTypes)
			{
				if (listGabaritoByType<Gabarito>(type) != null)
				{
					mapGabarito.Remove(type);
				}

				ObservableCollection<Gabarito> list = new ObservableCollection<Gabarito>();

				foreach (Gabarito gabarito in userGabaritoResult)
				{
					if (gabarito.NmProva.Equals(type))
					{
						list.Add(gabarito);
					}
				}

				mapGabarito.Add(type, new ObservableCollection<Gabarito>(list));
			}
		}

		public ObservableCollection<Boletim> listBoletim { get; set; }
		public ObservableCollection<Demonstrativo> listDemonstrativo { get; set; }

		public ObservableCollection<AcompanhamentoInfantil> listAcompanhamentoInfantil { get; set; }
		public ObservableCollection<Frequencia> Frequencia { get; set; }
		public ObservableCollection<Cardapio> Cardapios { get; set; }
		public ObservableCollection<Prova> CalendarioProva { get; set; }


		public Dictionary<string, ObservableCollection<AcompanhamentoInfantil>> mapAcompanhamentoInfantil { get; set; }

		public ObservableCollection<AcompanhamentoInfantil> listInfantil1Bim { get; set; }
		public ObservableCollection<AcompanhamentoInfantil> listInfantil2Bim { get; set; }
		public ObservableCollection<AcompanhamentoInfantil> listInfantil3Bim { get; set; }
		public ObservableCollection<AcompanhamentoInfantil> listInfantil4Bim { get; set; }

		public ObservableCollection<CalendarAcademic> listCalendarAcademic { get; set; }

		public ObservableCollection<CalendarAcademic> listAgendaEventos{ get; set; }

		public ObservableCollection<RegistroDiario> listRegistroDiario { get; set; }

		public List<Consumo> listConsumo { get; set; }

		public void createListInfantilBim()
		{
			listInfantil1Bim = new ObservableCollection<AcompanhamentoInfantil>();
			listInfantil2Bim = new ObservableCollection<AcompanhamentoInfantil>();
			listInfantil3Bim = new ObservableCollection<AcompanhamentoInfantil>();
			listInfantil4Bim = new ObservableCollection<AcompanhamentoInfantil>();
			foreach (AcompanhamentoInfantil acompanhamentoInfantil in listAcompanhamentoInfantil)
			{
				listInfantil1Bim.Add(acompanhamentoInfantil);
				
				listInfantil2Bim.Add(acompanhamentoInfantil);
				
				listInfantil3Bim.Add(acompanhamentoInfantil);
				
				listInfantil4Bim.Add(acompanhamentoInfantil);
			}
			Infantil1Bim = new InfantilBim();
			createListInfantilDivisao(listInfantil1Bim, Infantil1Bim, 1);
			Infantil2Bim = new InfantilBim();
			createListInfantilDivisao(listInfantil2Bim, Infantil2Bim, 2);
			Infantil3Bim = new InfantilBim();
			createListInfantilDivisao(listInfantil3Bim, Infantil3Bim, 3);
			Infantil4Bim = new InfantilBim();
			createListInfantilDivisao(listInfantil4Bim, Infantil4Bim, 4);
		}

		public void createListInfantilDivisao(ObservableCollection<AcompanhamentoInfantil> list, InfantilBim InfantilBim, int bimesters)
		{
			foreach (AcompanhamentoInfantil acompanhamentoInfantil in list)
			{
				if (bimesters.Equals(1))
				{
					acompanhamentoInfantil.Resposta = acompanhamentoInfantil.DcResposta1B;

				}
				else if (bimesters.Equals(2))
				{
					acompanhamentoInfantil.Resposta = acompanhamentoInfantil.DcResposta2B;

				}
				else if (bimesters.Equals(3))
				{
					acompanhamentoInfantil.Resposta = acompanhamentoInfantil.DcResposta3B;

				}
				else if (bimesters.Equals(4))
				{
					acompanhamentoInfantil.Resposta = acompanhamentoInfantil.DcResposta4B;

				}
					
				if (InfantilBim.listConteudoDisciplinaByDisc<AcompanhamentoInfantil>(acompanhamentoInfantil.DcDivisao) == null)
				{
					ObservableCollection<InfantilDivisao> listDis = new ObservableCollection<InfantilDivisao>();
					InfantilBim.mapAcompanhamentoInfantilDivisao.Add(acompanhamentoInfantil.DcDivisao, listDis);
				}
				InfantilDivisao InfantilDivisao = new InfantilDivisao(acompanhamentoInfantil.DcPergunta, acompanhamentoInfantil.Resposta);
				InfantilBim.listConteudoDisciplinaByDisc<InfantilDivisao>(acompanhamentoInfantil.DcDivisao).Add(InfantilDivisao);
			}
		}
	}
}
