﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using Newtonsoft.Json;
using SeculosApp;
using SeculosApp.Model;
using Xamarin.Forms;

namespace Seculos.Model
{
	/**
	 * Class to represents Responsavel or Aluno User
	 * */
	public class User : GenericModel
	{
		[JsonProperty(PropertyName = "token_id")]
		[DefaultValue("")]
		public string TokenId { get; set; }

		[JsonProperty(PropertyName = "cd_usuario")]
		[DefaultValue("")]
		public string CdUsuario { get; set; }

		[JsonProperty(PropertyName = "nm_usuario")]
		[DefaultValue("")]
		public string Nome { get; set; }

		[JsonIgnore]
		public string NomeUsuario
		{
			get
			{
				return Utils.PascalCase(Nome);
			}
		}

		[JsonProperty(PropertyName = "dc_tipo")]
		[DefaultValue("")]
		public string DcTipo { get; set; }

		public static explicit operator User(Dependente v)
		{
			throw new NotImplementedException();
		}

		[JsonProperty(PropertyName = "nr_celular")]
		[DefaultValue("")]
		public string NrCelular { get; set; }

		[JsonProperty(PropertyName = "cd_matricula")]
		[DefaultValue("")]
		public string CdMatricula { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonProperty(PropertyName = "lk_foto")]
		[DefaultValue("")]
		public string lkFoto { get; set; }

		ImageSource _Foto;

		[JsonIgnore]
		public ImageSource Foto
		{
			get
			{
				if (FotoBase != null)
				{
					return ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(FotoBase)));
				}
				else
				{
					return ImageSource.FromUri(new Uri(lkFoto));
				}
			}
			set
			{
				_Foto = value;
			}
		}

		[JsonProperty(PropertyName = "foto_base")]
		[DefaultValue("")]
		public string FotoBase { get; set; }

		[JsonProperty(PropertyName = "cd_curso")]
		[DefaultValue("")]
		public int CdCurso { get; set; }

		[JsonProperty(PropertyName = "nm_curso")]
		[DefaultValue("")]
		public string NomeCurso { get; set; }

		[JsonIgnore]
		public string NmCurso
		{
			get
			{
				return Utils.PascalCase(NomeCurso);
			}
		}

		[JsonProperty(PropertyName = "nr_serie")]
		[DefaultValue("")]
		public int NrSerie { get; set; }

		[JsonProperty(PropertyName = "dc_email")]
		[DefaultValue("")]
		public string DcEmail { get; set; }

		[JsonProperty(PropertyName = "cd_turma")]
		[DefaultValue("")]
		public string CdTurma { get; set; }

		[JsonProperty(PropertyName = "vl_saldo")]
		[DefaultValue("")]
		public string VlSaldo { get; set; }

		[JsonProperty(PropertyName = "vl_limite")]
		[DefaultValue("")]
		public string VlLimite { get; set; }

		public Dependente UserDependenteConnected { get; set; }

		public ObservableCollection<Dependente> listDependente { get; set; }

		public ObservableCollection<Boleto> listBilletByStudent(string name, string month)
		{
			string all = Application.Current.Resources["all"] as String;

			if ((name == null || name.Equals(string.Empty) || name.Equals(all)) && (month == null || month.Equals(string.Empty) || month.Equals(all)))
			{
				return listBillets;
			}

			ObservableCollection<Boleto> listBilletsTemp = new ObservableCollection<Boleto>();

            if (listBillets != null)
            {
				foreach (Boleto billet in listBillets)
				{
					if (name == null || name.Equals(string.Empty) || name.Equals(all) || billet.NomeAluno.Equals(name))
					{
						if (month == null || month.Equals(string.Empty) || month.Equals(all) || billet.Month.Equals(month))
						{
							listBilletsTemp.Add(billet);
						}
					}
				}
			}

			return listBilletsTemp;
		}

		public ObservableCollection<Notificacao> listNotifications { get; set; }
		public ObservableCollection<Boleto> listBillets { get; set; }

		public ObservableCollection<Notificacao> getAllListNotifications()
		{
            ObservableCollection<Notificacao> list = new ObservableCollection<Notificacao>();

			if (listNotifications != null)
			{
                foreach (Notificacao notificacao in listNotifications)
				{
					//if (notificacao.GetNotificationType.Equals(Notificacao.NOTIFICACAO_SIMPLES))
					//{
						list.Add(notificacao);
					//}
				}
			}

			return list;

		}

		public User()
		{
			listDependente = new ObservableCollection<SeculosApp.Model.Dependente>();
		}

		public ObservableCollection<Notificacao> getAllListComunications(string StudentName = null, string Month = null)
		{
			string all = Application.Current.Resources["all"] as String;

			ObservableCollection<Notificacao> listComunications = new ObservableCollection<Notificacao>();

			if (listNotifications != null)
			{
				foreach (Notificacao notificacao in listNotifications)
				{
					if (notificacao.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_PAGO)
						|| notificacao.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_GRATIS))
					{
						listComunications.Add(notificacao);
					}
				}
			}

			if ((StudentName == null || StudentName.Equals(string.Empty) || StudentName.Equals(all)) && (Month == null || Month.Equals(string.Empty) || Month.Equals(all)))
			{
				return listComunications;
			}
			else
			{
				ObservableCollection<Notificacao> listComunicationsFiltered = new ObservableCollection<Notificacao>();
				foreach (Notificacao notificacao in listComunications)
				{
					if (StudentName == null || StudentName.Equals(string.Empty) || StudentName.Equals(all) || (!notificacao.Dependente.NmNome.Equals(SeculosBO.EMPTY) && notificacao.Dependente.NomeAluno.Equals(StudentName)))
					{
						if (Month == null || Month.Equals(string.Empty) || Month.Equals(all) || (!notificacao.Month.Equals(SeculosBO.EMPTY) && notificacao.Month.Equals(Month)))
						{
							listComunicationsFiltered.Add(notificacao);
						}
					}
				}
				return listComunicationsFiltered;
			}
		}
	}
}
