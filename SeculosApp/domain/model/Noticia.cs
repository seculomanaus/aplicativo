﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace SeculosApp.Model
{
	public class Noticia : GenericModel, INotifyPropertyChanged
	{
		public Noticia()
		{
			tapCommand = new Command(OnTapped);
			tapCommandSeeAll = new Command(OnTappedSeeAll);
		}

		[JsonProperty(PropertyName = "cd_noticia")]
		public int CdNoticia { get; set; }

		[JsonProperty(PropertyName = "nm_titulo")]
		string _NmTitulo { get; set; }
		public string NmTitulo
		{
			get
			{
				return getDefault(_NmTitulo);
			}
			set
			{
				_NmTitulo = value;
			}
		}

		[JsonProperty(PropertyName = "dc_noticia")]
		public string _DcNoticia { get; set; }
		public string DcNoticia
		{
			get
			{
				return getDefault(_DcNoticia);
			}
			set
			{
				_DcNoticia = value;
			}
		}

		[JsonProperty(PropertyName = "lk_imagem")]
		public string _LkImagem { get; set; }
		public string LkImagem
		{
			get
			{
				return getDefault(_LkImagem);
			}
			set
			{
				_LkImagem = value;
			}
		}

		[JsonProperty(PropertyName = "dt_cadastro")]
		public string _DtCadastro { get; set; }
		public string DtCadastro
		{
			get
			{
				return getDefault(_DtCadastro);
			}
			set
			{
				_DtCadastro = value;
			}
		}

		[JsonIgnore]
		public string Title { get; set; }

		[JsonIgnore]
		public Notificacao notificacao1 { get; set; }
		[JsonIgnore]
		public Notificacao notificacao2 { get; set; }
		[JsonIgnore]
		public Notificacao notificacao3 { get; set; }
		[JsonIgnore]
		public bool isNotification { get; set; }

		[JsonIgnore]
		public bool isNotification_
		{
			get
			{
				return !isNotification;
			}
		}
		[JsonIgnore]
		public bool isSeeAll { get; set; }
		[JsonIgnore]
		public bool isNotification1 { get; set; }
		[JsonIgnore]
		public bool isNotification2 { get; set; }
		[JsonIgnore]
		public bool isNotification3 { get; set; }

		[JsonIgnore]
		public string SubTitle
		{
			get
			{

				if (!DcNoticia.Equals(SeculosBO.EMPTY))
				{
					var text = Utils.HtmlStrip(DcNoticia);
					text = WebUtility.HtmlDecode(text);

					if (text.Length > 99)
					{
						text = string.Concat(text.Substring(0, 100), "...");
					}

					return text;
				}

				return DcNoticia;
			}
		}

		[JsonIgnore]
		public string Day
		{
			get
			{
				if (DtCadastro.Equals(SeculosBO.EMPTY))
				{
					return DtCadastro;
				}
				else
				{
					return Convert.ToDateTime(DtCadastro, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
				}
			}
		}

		ICommand tapCommand;
		ICommand tapCommandSeeAll;
#pragma warning disable CS0067 // O evento "Noticia.PropertyChanged" nunca é usado
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067 // O evento "Noticia.PropertyChanged" nunca é usado
		public ICommand ShowNotification
		{
			get { return tapCommand; }
		}

		public ICommand SeeAllNotifications
		{
			get { return tapCommandSeeAll; }
		}

		async void OnTapped(object s)
		{
			SeculosBO.Instance.NotificationSelected = s as Notificacao;
			SeculosBO.Instance.ComunicationSelected = s as Notificacao;
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
                if (SeculosBO.Instance.NotificationSelected.CdEvento.Equals("9999"))
				{
                    SeculosBO.Instance.IMessage.Msg("Abrindo comunicado no seu navegador...");
                    Device.OpenUri(new System.Uri(SeculosBO.Instance.NotificationSelected.DsAlert));
				}
				else
				{
                    await SeculosBO.Instance.Navigation.PushAsync(new NotificationDetailView());
				}

				await Task.Factory.StartNew(() =>
				 {
					 Task.Delay(500).ContinueWith((obj) =>
					{
						App.GlobalBlockClick = false;
					});
				 });
			}
		}

		async void OnTappedSeeAll(object s)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				await SeculosBO.Instance.Navigation.PushAsync(new NotificationView());
				await Task.Factory.StartNew(() =>
				 {
					 Task.Delay(500).ContinueWith((obj) =>
					{
						App.GlobalBlockClick = false;
					});
				 });
			}
		}
	}
}
