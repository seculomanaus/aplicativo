﻿using System.Collections.ObjectModel;
using SeculosApp.Model;

namespace Seculos.Model
{
	public class SeculosGeneral
	{

		public string GCMToken { get; set; }

		public bool KeepConnected { get; set; }

		public User UserConnected { get; set; }

		public Dependente UserStudentConnected { get; set; }

		public ObservableCollection<User> UserList { get; set; }

		public ObservableCollection<Noticia> listNoticias { get; set; }

		public SeculosGeneral()
		{
			UserList = new ObservableCollection<User>();
			listNoticias = new ObservableCollection<Noticia>();
		}
	}
}
