﻿namespace Seculos.Listener
{
	public interface IEnvioriment
	{
		string GetPath();
		string ReadAllText(string filePath);
		bool WriteAllText(string filePath, string text);
	}
}
