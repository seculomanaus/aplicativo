﻿using Seculos.Model;

namespace Seculos.Listener
{
	public interface IJsonSerializer
	{
		string SerializeObj(object obj);
		SeculosGeneral DeserializeObj(string json);
		bool SaveFile(string filename, string text);
		string LoadFile(string filename);
		bool SerializeAndSave(object obj);
		SeculosGeneral LoadAndDeserializeObj();

	}
}
