using Newtonsoft.Json;
using System;
using System.IO;
using Seculos.Listener;
using Seculos.Model;

namespace Seculos.Persistence
{
	public class SerializerJson : IJsonSerializer
	{
		public static string FILENAME = "SeculosGeneralTESTE";

		public SeculosGeneral DeserializeObj(string json)
		{
			SeculosGeneral data = null;

			if (json != null)
			{
				try
				{
					data = JsonConvert.DeserializeObject<SeculosGeneral>(json);
				}
				catch (Exception)
				{
				}
			}

			return data;
		}

		public SeculosGeneral LoadAndDeserializeObj()
		{
			var file = LoadFile(FILENAME);
			if (file != null)
			{
				return DeserializeObj(file);
			}
			else
			{
				return null;
			}
		}

		public string LoadFile(string filename)
		{
			string content = null;

			try
			{
				var IEnvioriment = Xamarin.Forms.DependencyService.Get<IEnvioriment>();
				var documentsPath = IEnvioriment.GetPath();
				var filePath = Path.Combine(documentsPath, filename);
				content = IEnvioriment.ReadAllText(filePath);
			}
			catch
			{
				return "Erro ao ler a base de dados.";
			}

			return content;
		}

		public bool SaveFile(string filename, string text)
		{
			try
			{
				var IEnvioriment = Xamarin.Forms.DependencyService.Get<IEnvioriment>();
				var documentsPath = IEnvioriment.GetPath();
				var filePath = Path.Combine(documentsPath, filename);
				return IEnvioriment.WriteAllText(filePath, text);
			}
			catch
			{
				return false;
			}
		}

		public bool SerializeAndSave(object obj)
		{
			var serialized = SerializeObj(obj);
			return SaveFile(FILENAME, serialized);
		}

		public string SerializeObj(object obj)
		{
			return JsonConvert.SerializeObject(obj, Formatting.Indented);
		}
	}
}