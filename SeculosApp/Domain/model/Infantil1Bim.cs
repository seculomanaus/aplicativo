﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SeculosApp.Model
{
	public class InfantilBim
	{
		public Dictionary<string, ObservableCollection<InfantilDivisao>> mapAcompanhamentoInfantilDivisao { get; set; }
		string Disciplina { get; set; }

		public InfantilBim()
		{ 
			mapAcompanhamentoInfantilDivisao = new Dictionary<string, ObservableCollection<InfantilDivisao>>();
		}

		public ObservableCollection<Model.InfantilDivisao> listConteudoDisciplinaByDisc<InfantilDivisao>(string divisao)
		{
			ObservableCollection<Model.InfantilDivisao> value;
			if (mapAcompanhamentoInfantilDivisao.TryGetValue(divisao, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

	}
}
