﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class AcompanhamentoInfantil
	{
		[JsonProperty(PropertyName = "dc_quest")]
		[DefaultValue("")]
		public string DcQuest { get; set; }

		[JsonProperty(PropertyName = "dc_divisao")]
		[DefaultValue("")]
		public string DcDivisao { get; set; }

		[JsonIgnore]
		public string DcDivisaoName
		{ 
			get 
			{
				return Utils.PascalCase(DcDivisao);
			}
		}

		[JsonProperty(PropertyName = "nr_ordem")]
		[DefaultValue("")]
		public string NrOrdem { get; set; }

		[JsonProperty(PropertyName = "dc_pergunta")]
		[DefaultValue("")]
		public string DcPergunta { get; set; }

		[JsonProperty(PropertyName = "dc_resposta_1b")]
		[DefaultValue("")]
		public string DcResposta1B { get; set; }

		[JsonProperty(PropertyName = "dc_resposta_2b")]
		[DefaultValue("")]
		public string DcResposta2B { get; set; }

		[JsonProperty(PropertyName = "dc_resposta_3b")]
		[DefaultValue("")]
		public string DcResposta3B { get; set; }

		[JsonProperty(PropertyName = "dc_resposta_4b")]
		[DefaultValue("")]
		public string DcResposta4B { get; set; }

		[JsonIgnore]
		public string Resposta { get; set; }
	}
}
