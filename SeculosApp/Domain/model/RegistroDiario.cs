﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class RegistroDiario
	{
		[JsonProperty(PropertyName = "cd_registro")]
		[DefaultValue("")]
		public string CdRegistro { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonProperty(PropertyName = "nm_tipo")]
		[DefaultValue("")]
		public string NmTipo { get; set; }

		[JsonProperty(PropertyName = "ds_registro")]
		[DefaultValue("")]
		public string DsRegistro { get; set; }

		public string DsRegistroTruncated
		{
			get
			{
				string text = DsRegistro;

				if (text.Length > 99)
				{
					text = string.Concat(text.Substring(0, 100), "...");
				}

				return text;
			}
		}

		[JsonProperty(PropertyName = "nm_usuario")]
		[DefaultValue("")]
		public string NmUsuario { get; set; }

		[JsonProperty(PropertyName = "dt_registro")]
		[DefaultValue("")]
		public string DtRegistro { get; set; }

		[JsonIgnore]
		public string Registro
		{
			get
			{
				return "Registro " + CdRegistro;
			}
		}

		[JsonIgnore]
		public string Tipo
		{
			get
			{
				return "Tipo: " + NmTipo;
			}
		}
	}
}
