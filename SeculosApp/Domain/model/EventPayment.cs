﻿
namespace SeculosApp.Model
{
	public class EventPayment
	{
		public string cd_responsavel { get; set; }
		public string cd_matricula { get; set; }
		public string ds_alerta { get; set; }
		public string lk_foto { get; set; }
		public string nm_aluno { get; set; }
		public string nm_curso { get; set; }
		public string color_serie { get; set; }
		public string nm_alerta { get; set; }
		public string dt_evento { get; set; }
		public string ds_horario { get; set; }
		public string valor_moeda { get; set; }
		public string cd_evento { get; set; }
	}
}
