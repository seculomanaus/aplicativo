﻿
namespace SeculosApp.Model
{
	public class BilletPayment
	{
		public string cd_responsavel { get; set; }
		public string cd_matricula { get; set; }
		public string cd_boleto { get; set; }
		public string nm_produto { get; set; }
		public string dt_vencimento { get; set; }
		public string vl_boleto { get; set; }
		public string lk_foto { get; set; }
		public string nm_aluno { get; set; }
		public string nm_curso { get; set; }
		public string color_serie { get; set; }

	}
}
