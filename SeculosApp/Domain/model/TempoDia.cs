﻿namespace SeculosApp
{
	public class TempoDia
	{
		public string Conteudo { get; set; }
		public string Professor { get; set; }
		public string Tempo { get; set; }
		public string Disciplina { get; set; }
		public string Horario { get; set; }
		public string Turno { get; set; }

		public string NomeDisciplina
		{
			get
			{
				return Utils.PascalCase(Disciplina);//Utils.PascalCase(Utils.PascalCase(Disciplina), "\n");
			}
		}

		public string NomeTurno
		{
			get
			{
				if (Turno.Equals("A"))
				{
					return "Manhã";
				}
				else if (Turno.Equals("B"))
				{
					return "Tarde";
				}
				else
				{
					return "Turno não definido";
				}
			}
		}

		public TempoDia(string tempo, string horario, string disciplina, string turno, string conteudo, string professor) 
		{
			Tempo = tempo;
			Horario = horario;
			Disciplina = disciplina;
			Turno = turno;
			Conteudo = conteudo;
			Professor = professor;
		}
	}
}
