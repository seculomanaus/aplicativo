﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Gabarito
	{
		[JsonProperty(PropertyName = "nr_prova")]
		[DefaultValue("")]
		public string NrProva { get; set; }

		[JsonProperty(PropertyName = "dc_gabarito")]
		[DefaultValue("")]
		public string DcGabarito { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_prova")]
		[DefaultValue("")]
		public string NmProva { get; set; }

		[JsonProperty(PropertyName = "dc_nota")]
		[DefaultValue("")]
		public string DcNota { get; set; }

		[JsonProperty(PropertyName = "dc_tipo")]
		[DefaultValue("")]
		public string DcTipo { get; set; }

		[JsonProperty(PropertyName = "dc_disciplina")]
		[DefaultValue("")]
		public string DcDisciplina { get; set; }

		[JsonProperty(PropertyName = "dc_resposta")]
		[DefaultValue("")]
		public string DcResposta { get; set; }

		[JsonProperty(PropertyName = "cd_bimestre")]
		[DefaultValue("")]
		public string CdBimestre { get; set; }

		[JsonProperty(PropertyName = "nr_acerto")]
		[DefaultValue("")]
		public string NrAcerto { get; set; }

		[JsonProperty(PropertyName = "nr_branco")]
		[DefaultValue("")]
		public string NrBranco { get; set; }

		[JsonProperty(PropertyName = "nr_nulo")]
		[DefaultValue("")]
		public string NrNulo { get; set; }

		[JsonProperty(PropertyName = "pt_objetiva")]
		[DefaultValue("")]
		public string PtObjetiva { get; set; }

		[JsonProperty(PropertyName = "pt_dissertativa")]
		[DefaultValue("")]
		public string PtDissertativa
		{ get; set; }
	}
}
