﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace SeculosApp
{
	public class Boleto
	{
		[JsonProperty(PropertyName = "cd_boleto")]
		public string CdBoleto { get; set; }

		[JsonProperty(PropertyName = "nr_cpf")]
		public string NrCpf { get; set; }

		[JsonProperty(PropertyName = "nr_nosso")]
		public string NrNosso { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		public string NmAluno { get; set; }

		[JsonIgnore]
		public string NomeAluno
		{
			get
			{
				return Utils.PascalCase(NmAluno);
			}
		}

		[JsonProperty(PropertyName = "cd_aluno")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "dc_referencia")]
		public string DcReferencia { get; set; }

		[JsonProperty(PropertyName = "nm_produto")]
		public string NmProduto { get; set; }

		[JsonProperty(PropertyName = "codigo_barra")]
		public string CodigoBarra { get; set; }

		[JsonIgnore]
		public string NomeProduto
		{
			get
			{
				return Utils.PascalCase(NmProduto);
			}
		}

		[JsonProperty(PropertyName = "dt_vencimento")]
		public string DtVencimento { get; set; }

		[JsonIgnore]
		public string DataVencimento
		{
			get
			{
				return Convert.ToDateTime(DtVencimento, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
			}
		}

		[JsonIgnore]
		public string Month
		{
			get
			{
				var month = Int32.Parse(Convert.ToDateTime(DtVencimento, System.Globalization.CultureInfo.InvariantCulture).ToString("MM"));

				for (var i = 0; i < SeculosBO.Instance.Months.Length; i++) 
				{
					if (i == month) 
					{
						return SeculosBO.Instance.Months[i];
					}
				}
				return "Não encontrado";
			}
		}

		[JsonProperty(PropertyName = "vl_boleto")]
		public string VlBoleto { get; set; }

		public string ValorBoletoMoeda
		{
			get
			{
				try
				{
					return "R$ " + Utils.getCashValue(VlBoleto);
				}
				catch
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		[JsonProperty(PropertyName = "vl_recebido")]
		public string VlRecebido { get; set; }

		[JsonProperty(PropertyName = "dt_baixa")]
		public string DtBaixa { get; set; }

		[JsonIgnore]
		public string DataBaixa
		{
			get
			{
				return Convert.ToDateTime(DtBaixa, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
			}
		}

		[JsonProperty(PropertyName = "nr_nfse")]
		public string NrNfse { get; set; }

		[JsonProperty(PropertyName = "nfse_code")]
		public string NfseCode { get; set; }

		[JsonProperty(PropertyName = "fl_protesto")]
		public string FlProtesto { get; set; }

		[JsonProperty(PropertyName = "fl_vencido")]
		public string FLVencido { get; set; }

		[JsonProperty(PropertyName = "fl_juridicao")]
		public string FlJuridicao { get; set; }

		[JsonIgnore]
		public string ImgBoleto
		{
			get
			{
				if (DtBaixa == null || DtBaixa.Equals(string.Empty))
				{
					return "boleto_nao_pago_ic.png";
				}
				else
				{ 
					return "boleto_pago_ic.png";
				}
			}
		}

		[JsonIgnore]
		public string Situacao
		{
			get
			{
				if (DtBaixa == null || DtBaixa.Equals(string.Empty))
				{
					return (Application.Current.Resources["opened"] as String).ToUpper();
				}
				else
				{
					return (Application.Current.Resources["paid"] as String).ToUpper();
				}
			}
		}

		[JsonIgnore]
		public string ColorBoleto
		{
			get
			{
				if (DtBaixa == null || DtBaixa.Equals(string.Empty))
				{
					return "#d74e54";
				}
				else
				{
					return "#f8c700";
				}
			}
		}
	}
}
