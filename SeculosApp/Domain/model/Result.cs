﻿using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Result
	{
		[JsonProperty(PropertyName = "success")]
		public bool Return { get; set; }

		[JsonProperty(PropertyName = "data")]
		public Data Data { get; set; }

		[JsonProperty(PropertyName = "erro")]
		public string Error { get; set; }
	}
}
