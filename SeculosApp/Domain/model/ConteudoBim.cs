﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SeculosApp.Model
{
	public class ConteudoBim
	{
		public Dictionary<string, ObservableCollection<ConteudoDisciplina>> mapConteudoDisciplina { get; set; }
		string Disciplina { get; set; }

		public ConteudoBim()
		{ 
			mapConteudoDisciplina = new Dictionary<string, ObservableCollection<ConteudoDisciplina>>();
		}

		public ObservableCollection<Model.ConteudoDisciplina> listConteudoDisciplinaByDisc<ConteudoDisciplina>(string disciplina)
		{
			ObservableCollection<Model.ConteudoDisciplina> value;
			if (mapConteudoDisciplina.TryGetValue(disciplina, out value))
			{
				return value;
			}
			else
			{
				return null;
			}
		}

	}
}
