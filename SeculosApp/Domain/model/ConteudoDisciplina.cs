﻿namespace SeculosApp.Model
{
	public class ConteudoDisciplina
	{

		public string TipoProva { get; set; }
		public string Conteudo { get; set; }
		public ConteudoDisciplina(string tipoProva, string conteudo)
		{
			TipoProva = tipoProva;
			Conteudo = conteudo;
		}
	}
}
