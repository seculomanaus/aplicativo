﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Conteudo
	{
		[JsonProperty(PropertyName = "nr_bimestre")]
		[DefaultValue("")]
		public string NrBimestre { get; set; }

		[JsonProperty(PropertyName = "nm_disciplina")]
		[DefaultValue("")]
		public string NmDisciplina { get; set; }

		public string NomeDisciplina
		{
			get
			{
				return Utils.PascalCase(NmDisciplina);
			}
		}

		[JsonProperty(PropertyName = "dt_prova")]
		[DefaultValue("")]
		public string DtProva { get; set; }

		[JsonProperty(PropertyName = "tp_prova")]
		[DefaultValue("")]
		public string TpProva { get; set; }

		[JsonProperty(PropertyName = "ds_conteudo")]
		[DefaultValue("")]
		public string DsConteudo { get; set; }

		public Conteudo()
		{
		}
	}
}
