﻿using System;
using System.Net;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace SeculosApp.Model
{
	public class Notificacao : GenericModel
	{

		public const string NOTIFICACAO_SIMPLES = "NOTIFICACAO_SIMPLES";
		public const string NOTIFICACAO_EVENTO_GRATIS = "NOTIFICACAO_EVENTO_GRATIS";
		public const string NOTIFICACAO_EVENTO_PAGO = "NOTIFICACAO_EVENTO_PAGO";
		public const string NOTIFICACAO_FORA_DO_PADRAO = "NOTIFICACAO_FORA_DO_PADRAO";

		[JsonProperty(PropertyName = "cd_alerta")]
		string _CdAlert { get; set; }
		public string CdAlert
		{
			get
			{
				return getDefault(_CdAlert);
			}
			set
			{
				_CdAlert = value;
			}
		}

		[JsonProperty(PropertyName = "nm_alerta")]
		string _NmAlert { get; set; }
		public string NmAlert
		{
			get
			{
				return getDefault(_NmAlert);
			}
			set
			{
				_NmAlert = value;
			}
		}

		[JsonProperty(PropertyName = "ds_alerta")]
		string _DsAlert { get; set; }
		public string DsAlert
		{
			get
			{
				return getDefault(_DsAlert);
			}
			set
			{
				_DsAlert = value;
			}
		}

		[JsonProperty(PropertyName = "ds_foto")]
		string _DsFoto { get; set; }
		public string DsFoto
		{
			get
			{
				return getDefault(_DsFoto);
			}
			set
			{
				_DsFoto = value;
			}
		}

		[JsonProperty(PropertyName = "ds_foto_mini")]
		string _DsFotoMini { get; set; }
		public string DsFotoMini
		{
			get
			{
				return getDefault(_DsFotoMini);
			}
			set
			{
				_DsFotoMini = value;
			}
		}

		[JsonProperty(PropertyName = "dt_cadastro")]
		string _DtCadastro { get; set; }
		public string DtCadastro
		{
			get
			{
				return getDefault(_DtCadastro);
			}
			set
			{
				_DtCadastro = value;
			}
		}

		[JsonProperty(PropertyName = "nm_tipo")]
		string _NmTipo { get; set; }
		public string NmTipo
		{
			get
			{
				return getDefault(_NmTipo);
			}
			set
			{
				_NmTipo = value;
			}
		}

		[JsonProperty(PropertyName = "cd_evento")]
		string _CdEvento { get; set; }
		public string CdEvento
		{
			get
			{
				return getDefault(_CdEvento);
			}
			set
			{
				_CdEvento = value;
			}
		}

		[JsonProperty(PropertyName = "cd_aluno")]
		string _CdAluno { get; set; }
		public string CdAluno
		{
			get
			{
				return getDefault(_CdAluno);
			}
			set
			{
				_CdAluno = value;
			}
		}

		[JsonProperty(PropertyName = "nm_aluno")]
		string _NmAluno { get; set; }
		public string NmAluno
		{
			get
			{
				return getDefault(_NmAluno);
			}
			set
			{
				_NmAluno = value;
			}
		}

		[JsonProperty(PropertyName = "dt_evento")]
		string _DtEvento { get; set; }
		public string DtEvento
		{
			get
			{
				return getDefault(_DtEvento);
			}
			set
			{
				_DtEvento = value;
			}
		}

		[JsonProperty(PropertyName = "ds_horario")]
		string _DsHorario { get; set; }
		public string DsHorario
		{
			get
			{
				return getDefault(_DsHorario);
			}
			set
			{
				_DsHorario = value;
			}
		}

		[JsonProperty(PropertyName = "vl_evento")]
		string _VlEvento { get; set; }
		public string VlEvento
		{
			get
			{
				return getDefault(_VlEvento);
			}
			set
			{
				_VlEvento = value;
			}
		}

		[JsonProperty(PropertyName = "fl_pago")]
		string _FlPago { get; set; }
		public string FlPago
		{
			get
			{
				return getDefault(_FlPago);
			}
			set
			{
				_FlPago = value;
			}
		}

		[JsonProperty(PropertyName = "fl_lido")]
		string _FlLido { get; set; }
		public string FlLido
		{
			get
			{
				return getDefault(_FlLido);
			}
			set
			{
				_FlLido = value;
			}
		}

		[JsonProperty(PropertyName = "fl_confirmacao")]
		string _FlConfirmado { get; set; }
		public string FlConfirmado
		{
			get
			{
				return getDefault(_FlConfirmado);
			}
			set
			{
				_FlConfirmado = value;
			}
		}

		[JsonIgnore]
		public Dependente Dependente { get; set; }

		[JsonIgnore]
		public DateTime readAt { get; set; }

		[JsonIgnore]
		public string NomeAlerta
		{
			get
			{
				if (NmAlert.Equals(SeculosBO.EMPTY))
				{
					return NmAlert;
				}
				else
				{
					return Utils.PascalCase(NmAlert);
				}
			}
		}

		[JsonIgnore]
		public string DescricaoAlerta
		{
			get
			{
				if (DsAlert.Equals(SeculosBO.EMPTY))
				{
					var text = Utils.HtmlStrip(DsAlert);
					text = WebUtility.HtmlDecode(text);
					return text;
				}
				else
				{
					var text = Utils.HtmlStrip(DsAlert);
					text = WebUtility.HtmlDecode(text);
					return Utils.PascalCase(text);
				}
			}
		}

		[JsonIgnore]
		public string DescricaoAlertaMin
		{
			get
			{
				if (!DsAlert.Equals(SeculosBO.EMPTY))
				{
					var text = Utils.HtmlStrip(DsAlert);
					text = WebUtility.HtmlDecode(text);

					if (text.Length > 99)
					{
						text = string.Concat(text.Substring(0, 100), "...");
					}

					return text;
				}

				return DescricaoAlerta;
			}
		}



		[JsonIgnore]
		public string DataCadastro
		{
			get
			{
				if (DtCadastro.Equals(SeculosBO.EMPTY))
				{
					return DtCadastro;
				}
				else
				{
					return Convert.ToDateTime(DtCadastro, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
				}
			}
		}

		[JsonIgnore]
		public string Month
		{
			get
			{
				var month = Int32.Parse(Convert.ToDateTime(DtCadastro, System.Globalization.CultureInfo.InvariantCulture).ToString("MM"));

				for (var i = 0; i < SeculosBO.Instance.Months.Length; i++)
				{
					if (i == month)
					{
						return SeculosBO.Instance.Months[i];
					}
				}
				return "Não encontrado";
			}
		}

		[JsonIgnore]
		public string NomeAluno
		{
			get
			{
				if (NmAluno.Equals(SeculosBO.EMPTY))
				{
					return NmAluno;
				}
				else
				{
					return Utils.PascalCase(NmAluno);
				}
			}
		}

		[JsonIgnore]
		public string ValorEvento
		{
			get
			{
				if (VlEvento.Equals(SeculosBO.EMPTY))
				{
					return VlEvento;
				}
				else
				{
					return Utils.getCashValue(VlEvento);
				}
			}
		}

		[JsonIgnore]
		public string ValorEventoMoeda
		{
			get
			{
				return "R$ " + ValorEvento;
			}
		}

		[JsonIgnore]
		public string GetNotificationType
		{
			get
			{
				if (CdEvento.Equals(SeculosBO.EMPTY) || CdEvento.Equals("0"))
				{
					return NOTIFICACAO_SIMPLES;
				}
				else if (!CdEvento.Equals(SeculosBO.EMPTY) && (VlEvento.Equals(SeculosBO.EMPTY) || VlEvento.Equals("0")))
				{
					return NOTIFICACAO_EVENTO_GRATIS;
				}
				else if (!CdEvento.Equals(SeculosBO.EMPTY) && !VlEvento.Equals(SeculosBO.EMPTY))
				{
					return NOTIFICACAO_EVENTO_PAGO;
				}
				else
				{
					return NOTIFICACAO_FORA_DO_PADRAO;
				}
			}
		}

		[JsonIgnore]
		public string Status
		{
			get
			{
				if (GetNotificationType.Equals(NOTIFICACAO_SIMPLES))
				{
					if (FlLido.Equals("N"))
					{
						return Application.Current.Resources["opened"] as String;
					}
					else if (FlLido.Equals("S") || FlLido.Equals("0"))
					{
						return Application.Current.Resources["readed"] as String;
					}
					else
					{
						return FlLido;
					}
				}
				else if (GetNotificationType.Equals(NOTIFICACAO_EVENTO_GRATIS))
				{
					// lido; em aberto; recusado; confirmado;
					if (FlLido.Equals("N"))
					{
						return Application.Current.Resources["opened"] as String;
					}
					else if (FlLido.Equals("S") || FlLido.Equals("0"))
					{
						if (FlConfirmado.Equals("S"))
						{
							return Application.Current.Resources["confirmed"] as String;
						}
						else
						{
							return Application.Current.Resources["refused"] as String;
						}

					}

					return SeculosBO.EMPTY;
				}
				else if (GetNotificationType.Equals(NOTIFICACAO_EVENTO_PAGO))
				{
					// lido; em aberto; recusado; confirmado; realizar pagamento;
					if (FlLido.Equals("S") || FlLido.Equals("0"))
					{
						if (FlConfirmado.Equals("N"))
						{
							return Application.Current.Resources["refused"] as String;
						}
						else if (FlPago.Equals("N"))
						{
							return Application.Current.Resources["pendent_payment"] as String;
						}
						else
						{
							return Application.Current.Resources["confirmed"] as String;
						}
					}
					else if (FlLido.Equals("N"))
					{
						return Application.Current.Resources["opened"] as String;
					}
					else
					{
						return SeculosBO.EMPTY;
					}
				}
				else
				{
					return SeculosBO.EMPTY;
				}
			}
		}

		[JsonIgnore]
		public string IconStatus
		{
			get
			{
				if (Status.Equals(Application.Current.Resources["opened"] as String))
				{
					return "comunicado_green_ic.png";
				}
				else if (Status.Equals(Application.Current.Resources["confirmed"] as String))
				{
					return "comunicado_checked_ic.png";
				}
				else if (Status.Equals(Application.Current.Resources["refused"] as String))
				{
					return "comunicado_recused_ic.png";
				}
				else if (Status.Equals(Application.Current.Resources["pendent_payment"] as String))
				{
					return "comunicado_yellow_ic.png";
				}
				else
				{
					return "";
				}
			}
		}

		[JsonIgnore]
		public bool isComunication
		{
			get
			{
				if (GetNotificationType.Equals(NOTIFICACAO_EVENTO_GRATIS) || GetNotificationType.Equals(NOTIFICACAO_EVENTO_PAGO))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		[JsonIgnore]
		public bool isNotComunication
		{
			get
			{
				if (!GetNotificationType.Equals(NOTIFICACAO_EVENTO_GRATIS) && !GetNotificationType.Equals(NOTIFICACAO_EVENTO_PAGO))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
}
