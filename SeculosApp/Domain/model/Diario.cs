﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Diario
	{
		[JsonProperty(PropertyName = "cd_aula")]
		[DefaultValue("")]
		public string CdAula { get; set; }

		[JsonProperty(PropertyName = "cd_turma")]
		[DefaultValue("")]
		public string CdTurma { get; set; }

		[JsonProperty(PropertyName = "nm_disciplina")]
		[DefaultValue("")]
		public string NmDisciplina { get; set; }

		[JsonProperty(PropertyName = "nm_professor")]
		[DefaultValue("")]
		public string NmProfessor { get; set; }

		[JsonIgnore]
		public string NomeProfessor
		{
			get
			{
				return Utils.PascalCase(NmProfessor);
			}
		}

		[JsonProperty(PropertyName = "dt_aula")]
		[DefaultValue("")]
		public string DtAula { get; set; }

		[JsonIgnore]
		public string Day
		{
			get
			{
				return Convert.ToDateTime(DtAula, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
			}
		}

		[JsonProperty(PropertyName = "dc_conteudo")]
		[DefaultValue("")]
		public string DcConteudo { get; set; }

		[JsonProperty(PropertyName = "dc_tarefa")]
		[DefaultValue("")]
		public string DcTarefa { get; set; }

	}
}
