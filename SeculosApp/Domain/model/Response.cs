﻿using Newtonsoft.Json;
using Seculos.Model;

namespace SeculosApp.Model
{
	public class Response : GenericModel
	{
		[JsonProperty(PropertyName = "ds_transacao")]
		string _Result { get; set; }
		public string Result
		{
			get
			{
				return getDefault(_Result);
			}
			set
			{
				_Result = value;
			}
		}

		[JsonProperty(PropertyName = "ds_mensagem")]
		string _DsMensagem { get; set; }
		public string DsMensagem
		{
			get
			{
				return getDefault(_DsMensagem);
			}
			set
			{
				_DsMensagem = value;
			}
		}
	}
}
