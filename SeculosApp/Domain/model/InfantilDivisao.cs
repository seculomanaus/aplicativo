﻿namespace SeculosApp.Model
{
	public class InfantilDivisao
	{

		public string Pergunta { get; set; }
		public string Resposta { get; set; }
		public InfantilDivisao(string pergunta, string resposta)
		{
			Pergunta = pergunta;
			if (resposta != null)
			{
				Resposta = resposta;
			}
			else
			{ 
				Resposta = "*";
			}
		}
	}
}
