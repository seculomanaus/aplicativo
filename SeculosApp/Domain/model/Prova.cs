﻿using System;
using Newtonsoft.Json;

namespace SeculosApp
{
	public class Prova
	{
		[JsonProperty(PropertyName = "cd_calendario")]
		public int CodCalendario { get; set; }

		[JsonProperty(PropertyName = "perido")]
		public string Periodo { get; set; }

		[JsonProperty(PropertyName = "serie")]
		public int Serie { get; set; }

		[JsonProperty(PropertyName = "nm_curso")]
		public string NomeCurso { get; set; }

		[JsonProperty(PropertyName = "cd_turma")]
		public string CodTurma { get; set; }

		[JsonProperty(PropertyName = "bimestre")]
		public int Bimestre { get; set; }

		[JsonProperty(PropertyName = "nota")]
		public string Nota { get; set; }

		[JsonProperty(PropertyName = "dt_prova")]
		public DateTime DataProva { get; set; }

		[JsonProperty(PropertyName = "nm_tipo")]
		public string Tipo { get; set; }

		[JsonProperty(PropertyName = "nr_chamada")]
		public int NmChamada { get; set; }
	}
}
