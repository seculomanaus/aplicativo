﻿using System;
using Newtonsoft.Json;
using Seculos.Model;

namespace SeculosApp.Model
{
	public class CalendarAcademic : GenericModel
	{

		[JsonProperty(PropertyName = "dt_calendario")]
		string _DtCalendario { get; set; }
		public string DtCalendario
		{
			get
			{
				var dtCalendario = getDefault(_DtCalendario);
				if (dtCalendario.Equals(SeculosBO.EMPTY))
				{
					return dtCalendario;
				}
				else
				{
					try
					{
						var date = Convert.ToDateTime(dtCalendario, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
						return date;
					}
#pragma warning disable CS0168 // A variável "ex" está declarada, mas nunca é usada
					catch (FormatException ex)
#pragma warning restore CS0168 // A variável "ex" está declarada, mas nunca é usada
					{
						return SeculosBO.EMPTY;
					}
				}
			}
			set
			{
				_DtCalendario = value;
			}
		}

		[JsonProperty(PropertyName = "dt_evento")]
		string _DtEvento { get; set; }
		public string DtEvento
		{
			get
			{
				var dtEvento = getDefault(_DtEvento);
				if (dtEvento.Equals(SeculosBO.EMPTY))
				{
					return dtEvento;
				}
				else
				{
					try
					{
						var date = Convert.ToDateTime(dtEvento, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
						return date;
					}
#pragma warning disable CS0168 // A variável "ex" está declarada, mas nunca é usada
					catch (FormatException ex)
#pragma warning restore CS0168 // A variável "ex" está declarada, mas nunca é usada
					{
						return SeculosBO.EMPTY;
					}
				}
			}
			set
			{
				_DtEvento = value;
			}
		}

		[JsonProperty(PropertyName = "nr_dias")]
		string _NrDias { get; set; }
		public string NrDias
		{
			get
			{
				return getDefault(_NrDias);
			}
			set
			{
				_NrDias = value;
			}
		}

		[JsonProperty(PropertyName = "tp_cor")]
		string _TpColor { get; set; }
		public string TpColor
		{
			get
			{
				return getDefault(_TpColor);
			}
			set
			{
				_TpColor = value;
			}
		}

		[JsonProperty(PropertyName = "dc_calendario")]
		string _DcCalendario { get; set; }
		public string DcCalendario
		{
			get
			{
				return getDefault(_DcCalendario);
			}
			set
			{
				_DcCalendario = value;
			}
		}

		[JsonIgnore]
		public string DescCalendario
		{
			get
			{
				var text = DcCalendario;
				if (text.Length > 99)
				{
					text = string.Concat(text.Substring(0, 100), "...");
				}
				return Utils.PascalCase(DcCalendario);
			}
		}

		[JsonProperty(PropertyName = "dc_evento")]
		string _DcEvento { get; set; }
		public string DcEvento
		{
			get
			{
				return getDefault(_DcEvento);
			}
			set
			{
				_DcEvento = value;
			}
		}

		[JsonProperty(PropertyName = "peso")]
		string _Peso { get; set; }
		public string Peso
		{
			get
			{
				return getDefault(_Peso);
			}
			set
			{
				_Peso = value;
			}
		}

		[JsonProperty(PropertyName = "nota_prova")]
		string _NotaProva { get; set; }
		public string NotaProva
		{
			get
			{
				return getDefault(_NotaProva);
			}
			set
			{
				_NotaProva = value;
			}
		}

		[JsonProperty(PropertyName = "info_prova")]
		string _InfoProva { get; set; }
		public string InfoProva
		{
			get
			{
				return getDefault(_InfoProva);
			}
			set
			{
				_InfoProva = value;
			}
		}

		[JsonProperty(PropertyName = "anexo")]
		string _Anexo { get; set; }
		public string Anexo {
			get
			{
				return getDefault(_Anexo);
			}
			set
			{
				_Anexo = value;
			}
		}

		[JsonIgnore]
		public string DescEvento
		{
			get
			{
				var text = DcEvento;
				if (text.Length > 99)
				{
					text = string.Concat(text.Substring(0, 100), "...");
				}
				return Utils.PascalCase(DcEvento);
			}
		}
	}
}