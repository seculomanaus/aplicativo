﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Ocorrencia
	{
		[JsonProperty(PropertyName = "cd_registro")]
		[DefaultValue("")]
		public string CdRegistro { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonIgnore]
		public string NomeAluno
		{
			get
			{
				return Utils.PascalCase(NmAluno);
			}
		}

		[JsonProperty(PropertyName = "nm_registro")]
		[DefaultValue("")]
		public string NmRegistro { get; set; }

		[JsonProperty(PropertyName = "nm_tipo")]
		[DefaultValue("")]
		public string NmTipo { get; set; }

		[JsonProperty(PropertyName = "ds_registro")]
		[DefaultValue("")]
		public string DsResgistro { get; set; }

		[JsonProperty(PropertyName = "dt_registro")]
		[DefaultValue("")]
		public string DtRegistro { get; set; }

		[JsonIgnore]
		public string Day
		{
			get
			{
				return Convert.ToDateTime(DtRegistro, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
			}
		}
	}
}
