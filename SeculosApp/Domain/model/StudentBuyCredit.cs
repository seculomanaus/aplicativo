﻿namespace SeculosApp.Model
{
	public class StudentBuyCredit
	{
		public string cd_aluno { get; set; }
		public string vl_credito { get; set; }
		public string nm_aluno { get; set; }
		public string lk_foto { get; set; }
	}
}
