﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class AcompanhamentoDiario
	{
		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonIgnore]
		public string NomeAluno
		{
			get
			{
				return Utils.PascalCase(NmAluno);
			}
		}

		[JsonProperty(PropertyName = "lk_foto")]
		[DefaultValue("")]
		public string LkFoto { get; set; }

		[JsonProperty(PropertyName = "dt_acompanhamento")]
		[DefaultValue("")]
		public string DtAcompanhamento { get; set; }

		[JsonIgnore]
		public string Day
		{
			get
			{
				return Convert.ToDateTime(DtAcompanhamento, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
			}
		}

		[JsonProperty(PropertyName = "colacao")]
		[DefaultValue("")]
		public string Colacao { get; set; }

		[JsonProperty(PropertyName = "almoco")]
		[DefaultValue("")]
		public string Almoco { get; set; }

		[JsonProperty(PropertyName = "lanche")]
		[DefaultValue("")]
		public string Lanche { get; set; }

		[JsonProperty(PropertyName = "sono")]
		[DefaultValue("")]
		public string Sono { get; set; }

		[JsonProperty(PropertyName = "evacuacao")]
		[DefaultValue("")]
		public string Evacuacao { get; set; }
	}
}
