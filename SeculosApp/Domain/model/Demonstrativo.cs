﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Demonstrativo
	{
		[JsonProperty(PropertyName = "cd_disciplina")]
		[DefaultValue("")]
		public string CdDisciplina { get; set; }

		[JsonProperty(PropertyName = "nm_disciplina")]
		[DefaultValue("")]
		public string NmDisciplina { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonProperty(PropertyName = "p1_1b")]
		[DefaultValue("")]
		public string P11BModel { get; set; }

		public string P11B
		{
			get
			{
				return getFinalValue(P11BModel);
			}
		}

		[JsonProperty(PropertyName = "p1_2b")]
		[DefaultValue("")]
		public string P12BModel { get; set; }

		public string P12B
		{
			get
			{
				return getFinalValue(P12BModel);
			}
		}

		[JsonProperty(PropertyName = "p1_3b")]
		[DefaultValue("")]
		public string P13BModel { get; set; }

		public string P13B
		{
			get
			{
				return getFinalValue(P13BModel);
			}
		}

		[JsonProperty(PropertyName = "p1_4b")]
		[DefaultValue("")]
		public string P14BModel { get; set; }

		public string P14B
		{
			get
			{
				return getFinalValue(P14BModel);
			}
		}

		[JsonProperty(PropertyName = "p2_1b")]
		[DefaultValue("")]
		public string P21BModel { get; set; }

		public string P21B
		{
			get
			{
				return getFinalValue(P21BModel);
			}
		}

		[JsonProperty(PropertyName = "p2_2b")]
		[DefaultValue("")]
		public string P22BModel { get; set; }

		public string P22B
		{
			get
			{
				return getFinalValue(P22BModel);
			}
		}

		[JsonProperty(PropertyName = "p2_3b")]
		[DefaultValue("")]
		public string P23BModel { get; set; }

		public string P23B
		{
			get
			{
				return getFinalValue(P23BModel);
			}
		}

		[JsonProperty(PropertyName = "p2_4b")]
		[DefaultValue("")]
		public string P24BModel { get; set; }

		public string P24B
		{
			get
			{
				return getFinalValue(P24BModel);
			}
		}

		[JsonProperty(PropertyName = "maic_1b")]
		[DefaultValue("")]
		public string Maic1BModel { get; set; }

		public string Maic1B
		{
			get
			{
				return getFinalValue(Maic1BModel);
			}
		}

		[JsonProperty(PropertyName = "maic_2b")]
		[DefaultValue("")]
		public string Maic2BModel { get; set; }

		public string Maic2B
		{
			get
			{
				return getFinalValue(Maic2BModel);
			}
		}

		[JsonProperty(PropertyName = "maic_3b")]
		[DefaultValue("")]
		public string Maic3BModel { get; set; }

		public string Maic3B
		{
			get
			{
				return getFinalValue(Maic3BModel);
			}
		}

		[JsonProperty(PropertyName = "maic_4b")]
		[DefaultValue("")]
		public string Maic4BModel { get; set; }

		public string Maic4B
		{
			get
			{
				return getFinalValue(Maic4BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_1b")]
		[DefaultValue("")]
		public string Mb1BModel { get; set; }

		public string Mb1B
		{
			get
			{
				return getFinalValue(Mb1BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_2b")]
		[DefaultValue("")]
		public string Mb2BModel { get; set; }

		public string Mb2B
		{
			get
			{
				return getFinalValue(Mb2BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_3b")]
		[DefaultValue("")]
		public string Mb3BModel { get; set; }

		public string Mb3B
		{
			get
			{
				return getFinalValue(Mb3BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_4b")]
		[DefaultValue("")]
		public string Mb4BModel { get; set; }

		public string Mb4B
		{
			get
			{
				return getFinalValue(Mb4BModel);
			}
		}

		[JsonProperty(PropertyName = "mbt_1b")]
		[DefaultValue("")]
		public string Mbt1BModel { get; set; }

		public string Mbt1B
		{
			get
			{
				return getFinalValue(Mbt1BModel);
			}
		}

		[JsonProperty(PropertyName = "mbt_2b")]
		[DefaultValue("")]
		public string Mbt2BModel { get; set; }

		public string Mbt2B
		{
			get
			{
				return getFinalValue(Mbt2BModel);
			}
		}

		[JsonProperty(PropertyName = "mbt_3b")]
		[DefaultValue("")]
		public string Mbt3BModel { get; set; }

		public string Mbt3B
		{
			get
			{
				return getFinalValue(Mbt3BModel);
			}
		}

		[JsonProperty(PropertyName = "mbt_4b")]
		[DefaultValue("")]
		public string Mbt4BModel { get; set; }

		public string Mbt4B
		{
			get
			{
				return getFinalValue(Mbt4BModel);
			}
		}

		[JsonProperty(PropertyName = "nrec_1b")]
		[DefaultValue("")]
		public string Nrec1BModel { get; set; }

		public string Nrec1B
		{
			get
			{
				return getFinalValue(Nrec1BModel);
			}
		}

		[JsonProperty(PropertyName = "nrec_2b")]
		[DefaultValue("")]
		public string Nrec2BModel { get; set; }

		public string Nrec2B
		{
			get
			{
				return getFinalValue(Nrec2BModel);
			}
		}

		[JsonProperty(PropertyName = "nrec_3b")]
		[DefaultValue("")]
		public string Nrec3BModel { get; set; }

		public string Nrec3B
		{
			get
			{
				return getFinalValue(Nrec3BModel);
			}
		}

		[JsonProperty(PropertyName = "nrec_4b")]
		[DefaultValue("")]
		public string Nrec4BModel { get; set; }

		public string Nrec4B
		{
			get
			{
				return getFinalValue(Nrec4BModel);
			}
		}

		[JsonProperty(PropertyName = "lg_obs")]
		[DefaultValue("")]
		public string LgObsModel { get; set; }

		public string LgObs
		{
			get
			{
				if (LgObsModel != null)
				{
					return LgObsModel.Replace(" <br/> ", "\n").Replace(" <br> ", "\n").Replace("<br/> ", "\n").Replace("<br> ", "\n").Replace(" <br/>", "\n").Replace(" <br>", "\n").Replace("<br/>", "\n").Replace("<br>", "\n");
				}
				return "*";
			}
		}

		[JsonProperty(PropertyName = "legenda13")]
		[DefaultValue("")]
		public string Legenda13Model { get; set; }

		public string Legenda13
		{
			get
			{
				if (Legenda13Model != null)
				{
					return Legenda13Model.Replace(" <br/> ", "\n").Replace(" <br> ", "\n").Replace("<br/> ", "\n").Replace("<br> ", "\n").Replace(" <br/>", "\n").Replace(" <br>", "\n").Replace("<br/>", "\n").Replace("<br>", "\n");
				}
				return "*";
			}
		}

		[JsonProperty(PropertyName = "legenda24")]
		[DefaultValue("")]
		public string Legenda24Model { get; set; }

		public string Legenda24
		{
			get
			{
				if (Legenda24Model != null)
				{
					return Legenda24Model.Replace(" <br/> ", "\n").Replace(" <br> ", "\n").Replace("<br/> ", "\n").Replace("<br> ", "\n").Replace(" <br/>", "\n").Replace(" <br>", "\n").Replace("<br/>", "\n").Replace("<br>", "\n");
				}
				return "*";
			}
		}

		string getFinalValue(string text)
		{
			if (text == null || text.Equals(string.Empty) || text.Equals("-"))
			{
				return "*";
			}
			else
			{
				return text;
			}
		}
	}
}
