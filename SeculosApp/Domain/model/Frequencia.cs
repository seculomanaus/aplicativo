﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Frequencia
	{
		[JsonProperty(PropertyName = "cd_aluno")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		public string NomeAluno { get; set; }

		[JsonProperty(PropertyName = "nm_disciplina")]
		public string NomeDisciplina { get; set; }

		[JsonProperty(PropertyName = "nr_grade")]
		public int Grade { get; set; }

		[JsonProperty(PropertyName = "nr_falta1b")]
		public int NumFalta1b { get; set; }

		[JsonProperty(PropertyName = "nr_falta2b")]
		public int NumFalta2b { get; set; }

		[JsonProperty(PropertyName = "nr_falta3b")]
		public int NumFalta3b { get; set; }

		[JsonProperty(PropertyName = "nr_falta4b")]
		public int NumFalta4b { get; set; }

		[JsonProperty(PropertyName = "nr_TTfaltas")]
		public int TotalFaltas { get; set; }

		[JsonProperty(PropertyName = "nr_TTAusencia")]
		public int TotalAusencia { get; set; }

		[JsonProperty(PropertyName = "nr_percent")]
		public float PercentualFaltas { get; set; }
	}

}
