﻿using System;
using Newtonsoft.Json;

namespace SeculosApp
{
	public class Cardapio
	{
		[JsonProperty(PropertyName = "dt_cardapio")]
		public DateTime dtCardapio { get; set; }

		[JsonProperty(PropertyName = "dc_dia")]
	 	public string dcDia { get; set; }

		[JsonProperty(PropertyName = "ds_manha")]
		public string dsManha { get; set; }

		[JsonProperty(PropertyName = "ds_almoco")]
		public string dsAlmoco { get; set; }

		[JsonProperty(PropertyName = "ds_tarde")]
		public string dsTarde { get; set; }
	}
}
