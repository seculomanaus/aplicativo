﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Tempo
	{

		[JsonProperty(PropertyName = "cd_periodo")]
		[DefaultValue("")]
		public string Cdperiodo { get; set; }

		[JsonProperty(PropertyName = "fl_turno")]
		[DefaultValue("")]
		public string FlTurno { get; set; }

		[JsonProperty(PropertyName = "nr_tempo")]
		[DefaultValue("")]
		public string NrTempo { get; set; }

		[JsonProperty(PropertyName = "sg_disciplina")]
		[DefaultValue("")]
		public string SgDisciplina { get; set; }

		[JsonProperty(PropertyName = "sg_professor")]
		[DefaultValue("")]
		public string SgProfessor { get; set; }

		[JsonProperty(PropertyName = "sg_conteudo")]
		[DefaultValue("")]
		public string SgConteudo { get; set; }

		[JsonProperty(PropertyName = "tc_disciplina")]
		[DefaultValue("")]
		public string TcDisciplina { get; set; }

		[JsonProperty(PropertyName = "tc_professor")]
		[DefaultValue("")]
		public string TcProfessor { get; set; }

		[JsonProperty(PropertyName = "tc_conteudo")]
		[DefaultValue("")]
		public string TcConteudo { get; set; }

		[JsonProperty(PropertyName = "qa_disciplina")]
		[DefaultValue("")]
		public string QaDisciplina { get; set; }

		[JsonProperty(PropertyName = "qa_professor")]
		[DefaultValue("")]
		public string QaProfessor { get; set; }

		[JsonProperty(PropertyName = "qa_conteudo")]
		[DefaultValue("")]
		public string QaConteudo { get; set; }

		[JsonProperty(PropertyName = "qi_disciplina")]
		[DefaultValue("")]
		public string QiDisciplina { get; set; }

		[JsonProperty(PropertyName = "qi_professor")]
		[DefaultValue("")]
		public string QiProfessor { get; set; }

		[JsonProperty(PropertyName = "qi_conteudo")]
		[DefaultValue("")]
		public string QiConteudo { get; set; }

		[JsonProperty(PropertyName = "sx_disciplina")]
		[DefaultValue("")]
		public string SxDisciplina { get; set; }

		[JsonProperty(PropertyName = "sx_professor")]
		[DefaultValue("")]
		public string SxProfessor { get; set; }

		[JsonProperty(PropertyName = "sx_conteudo")]
		[DefaultValue("")]
		public string SxConteudo { get; set; }

		[JsonProperty(PropertyName = "sb_disciplina")]
		[DefaultValue("")]
		public string SbDisciplina { get; set; }

		[JsonProperty(PropertyName = "sb_professor")]
		[DefaultValue("")]
		public string SbProfessor { get; set; }

		[JsonProperty(PropertyName = "sb_conteudo")]
		[DefaultValue("")]
		public string SbConteudo { get; set; }

		[JsonProperty(PropertyName = "hr_inicio")]
		[DefaultValue("")]
		public string HrInicio { get; set; }

		[JsonProperty(PropertyName = "hr_fim")]
		[DefaultValue("")]
		public string HrFim { get; set; }

	}
}