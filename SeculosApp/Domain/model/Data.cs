﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Data
	{
		[JsonProperty(PropertyName = "id_token")]
		[DefaultValue(false)]
		public string TokenId { get; set; }

		[JsonProperty(PropertyName = "message")]
		[DefaultValue("")]
		public string Message { get; set; }
	}
}
