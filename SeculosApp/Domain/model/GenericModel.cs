﻿using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class GenericModel
	{
		public GenericModel()
		{
			Error = string.Empty;
		}

		[JsonProperty(PropertyName = "erro")]
		public string Error { get; set; }

		public string getDefault(string value)
		{
			if (value == null || value.Equals("null") || value.Equals("") || value.Equals("(null)"))
			{
				return SeculosBO.EMPTY;
			}
			return value;
		}
	}
}
