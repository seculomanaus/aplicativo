﻿using Newtonsoft.Json;
using SeculosApp.Model;

namespace SeculosApp
{
	public class Consumo : GenericModel
	{
		[JsonProperty(PropertyName = "cd_aluno")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		public string NmAluno { get; set; }

		[JsonProperty(PropertyName = "cd_venda")]
		public string CdVenda { get; set; }

		[JsonProperty(PropertyName = "dt_venda")]
		public string DtVenda { get; set; }

		string _nmProduto;

		[JsonProperty(PropertyName = "nm_produto")]
		public string NmProduto
		{
			get
			{
				try
				{
					return _nmProduto.Substring(0, 20);
				}
				catch
				{
					return _nmProduto;
				}
			}
			set
			{
				_nmProduto = value;
			}
		}

		[JsonProperty(PropertyName = "nr_qtde")]
		public string NrQtde { get; set; }

		[JsonProperty(PropertyName = "vl_unitario")]
		public string VlUnitario { get; set; }

		[JsonProperty(PropertyName = "vl_total")]
		public string VlTotal { get; set; }
	}
}