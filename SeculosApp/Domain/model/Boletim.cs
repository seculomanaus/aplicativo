﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace SeculosApp.Model
{
	public class Boletim
	{
		[JsonProperty(PropertyName = "cd_disciplina")]
		[DefaultValue("")]
		public string CdDisciplina { get; set; }

		[JsonProperty(PropertyName = "nm_disciplina")]
		[DefaultValue("")]
		public string NmDisciplina { get; set; }

		[JsonProperty(PropertyName = "cd_aluno")]
		[DefaultValue("")]
		public string CdAluno { get; set; }

		[JsonProperty(PropertyName = "nm_aluno")]
		[DefaultValue("")]
		public string NmAluno { get; set; }

		[JsonProperty(PropertyName = "f_1b")]
		[DefaultValue("")]
		public string F1BModel { get; set; }

		public string F1B
		{
			get
			{
				return getFinalValue(F1BModel);
			}
		}

		[JsonProperty(PropertyName = "f_2b")]
		[DefaultValue("")]
		public string F2BModel { get; set; }

		public string F2B
		{
			get
			{
				return getFinalValue(F2BModel);
			}
		}

		[JsonProperty(PropertyName = "f_3b")]
		[DefaultValue("")]
		public string F3BModel { get; set; }

		public string F3B
		{
			get
			{
				return getFinalValue(F3BModel);
			}
		}

		[JsonProperty(PropertyName = "f_4b")]
		[DefaultValue("")]
		public string F4BModel { get; set; }

		public string F4B
		{
			get
			{
				return getFinalValue(F4BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_1b")]
		[DefaultValue("")]
		public string Mb1BModel { get; set; }

		public string Mb1B
		{
			get
			{
				return getFinalValue(Mb1BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_2b")]
		[DefaultValue("")]
		public string Mb2BModel { get; set; }

		public string Mb2B
		{
			get
			{
				return getFinalValue(Mb2BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_3b")]
		[DefaultValue("")]
		public string Mb3BModel { get; set; }

		public string Mb3B
		{
			get
			{
				return getFinalValue(Mb3BModel);
			}
		}

		[JsonProperty(PropertyName = "mb_4b")]
		[DefaultValue("")]
		public string Mb4BModel { get; set; }

		public string Mb4B
		{
			get
			{
				return getFinalValue(Mb4BModel);
			}
		}

		[JsonProperty(PropertyName = "maf")]
		[DefaultValue("")]
		public string MafModel { get; set; }

		public string Maf
		{
			get
			{
				return getFinalValue(MafModel);
			}
		}

		[JsonProperty(PropertyName = "nrf")]
		[DefaultValue("")]
		public string NrfModel { get; set; }

		public string Nrf
		{
			get
			{
				return getFinalValue(NrfModel);
			}
		}

		[JsonProperty(PropertyName = "marf")]
		[DefaultValue("")]
		public string MarfModel { get; set; }

		public string Marf
		{
			get
			{
				return getFinalValue(MarfModel);
			}
		}

		[JsonProperty(PropertyName = "tf")]
		[DefaultValue("")]
		public string TfModel { get; set; }

		public string Tf
		{
			get
			{
				return getFinalValue(TfModel);
			}
		}

		[JsonProperty(PropertyName = "lg_obs")]
		[DefaultValue("")]
		public string LgObsModel { get; set; }

		public string LgObs
		{
			get
			{
				if (LgObsModel != null)
				{
					return LgObsModel.Replace(" <br/> ", "\n").Replace(" <br> ", "\n").Replace("<br/> ", "\n").Replace("<br> ", "\n").Replace(" <br/>", "\n").Replace(" <br>", "\n").Replace("<br/>", "\n").Replace("<br>", "\n");
				}
				return "*";
			}
		}

		[JsonProperty(PropertyName = "lg_media")]
		[DefaultValue("")]
		public string LgMediaModel { get; set; }

		public string LgMedia
		{
			get
			{
				if (LgMediaModel != null)
				{
					return LgMediaModel.Replace(" <br/> ", "\n").Replace(" <br> ", "\n").Replace("<br/> ", "\n").Replace("<br> ", "\n").Replace(" <br/>", "\n").Replace(" <br>", "\n").Replace("<br/>", "\n").Replace("<br>", "\n");
				}
				return "*";
			}
		}

		string getFinalValue(string text)
		{
			if (text == null || text.Equals(string.Empty) || text.Equals("-"))
			{
				return "*";
			}
			else
			{
				return text;
			}
		}
	}
}
