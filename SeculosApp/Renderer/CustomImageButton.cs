﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomImageButton : Button
	{
		public CustomImageButton()
		{
		}

		public static readonly BindableProperty NameProperty = BindableProperty.Create("Source", typeof(string), typeof(CustomImageButton), "");

		public static readonly BindableProperty NameDrawable = BindableProperty.Create("Drawable", typeof(string), typeof(CustomImageButton), "");

		public string Source
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}

		public string Drawable
		{
			get { return (string)GetValue(NameDrawable); }
			set { SetValue(NameDrawable, value); }
		}
	}
}
