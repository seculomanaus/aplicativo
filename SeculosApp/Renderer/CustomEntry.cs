﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomEntry : Entry
	{
		public CustomEntry()
		{
		}

		public static readonly BindableProperty NameProperty = BindableProperty.Create("FontType", typeof(string), typeof(CustomEntry), "");

		public string FontType
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}

		public static readonly BindableProperty MaskProperty = BindableProperty.Create("MaskType", typeof(string), typeof(CustomEntry), "");

		public string MaskType
		{
			get { return (string)GetValue(MaskProperty); }
			set { SetValue(MaskProperty, value); }
		}
	}
}
