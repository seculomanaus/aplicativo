﻿using System;
using Xamarin.Forms;

namespace SeculosApp
{
	public class RoundedBoxView : BoxView
    {
		public static readonly BindableProperty CornerRadiusProperty =
#pragma warning disable CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."
			BindableProperty.Create<RoundedBoxView, double>(p => p.CornerRadius, 0);
#pragma warning restore CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."

		public double CornerRadius
		{
			get { return (double)base.GetValue(CornerRadiusProperty);}
			set { base.SetValue(CornerRadiusProperty, value);}
		}

		public static readonly BindableProperty StrokeProperty = 
#pragma warning disable CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."
			BindableProperty.Create<RoundedBoxView,Color>(
				p => p.Stroke, Color.Transparent);
#pragma warning restore CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."

		public Color Stroke {
			get { return (Color)GetValue(StrokeProperty); }
			set { SetValue(StrokeProperty, value); }
		}

		public static readonly BindableProperty StrokeThicknessProperty = 
#pragma warning disable CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."
			BindableProperty.Create<RoundedBoxView,double>(
				p => p.StrokeThickness, default(double));
#pragma warning restore CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."

		public double StrokeThickness {
			get { return (double)GetValue(StrokeThicknessProperty); }
			set { SetValue(StrokeThicknessProperty, value); }
		}

		public static readonly BindableProperty HasShadowProperty = 
#pragma warning disable CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."
			BindableProperty.Create<RoundedBoxView,bool>(
				p => p.HasShadow, default(bool));
#pragma warning restore CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."

		public bool HasShadow {
			get { return (bool)GetValue(HasShadowProperty); }
			set { SetValue(HasShadowProperty, value); }
		}
    }
}

