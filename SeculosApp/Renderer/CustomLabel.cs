﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomLabel : Label
	{
		public CustomLabel()
		{
		}

		public static readonly BindableProperty NameProperty = BindableProperty.Create("FontType", typeof(string), typeof(CustomLabel), "");

		public string FontType
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}

	}
}
