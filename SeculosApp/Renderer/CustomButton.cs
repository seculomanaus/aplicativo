﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomButton : Button
	{
		public CustomButton()
		{
		}

		public static readonly BindableProperty NameProperty = BindableProperty.Create("FontType", typeof(string), typeof(CustomButton), "");

		public string FontType
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}
	}
}
