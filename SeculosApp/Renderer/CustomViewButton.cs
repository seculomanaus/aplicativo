﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomViewButton : RelativeLayout
	{
		public CustomViewButton()
		{
		}

		public static readonly BindableProperty NameDrawable = BindableProperty.Create("Drawable", typeof(string), typeof(CustomViewButton), "");

		public string Drawable
		{
			get { return (string)GetValue(NameDrawable); }
			set { SetValue(NameDrawable, value); }
		}
	}
}
