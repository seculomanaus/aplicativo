﻿using System;
using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomCheckBox : Button
	{
		public CustomCheckBox()
		{
			
		}
		public static readonly BindableProperty CheckedProperty =
#pragma warning disable CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."
		BindableProperty.Create<CustomCheckBox, bool>(
				p => p.Checked, false, BindingMode.TwoWay, propertyChanged: OnCheckedPropertyChanged);
#pragma warning restore CS0618 // "BindableProperty.Create<TDeclarer, TPropertyType>(Expression<Func<TDeclarer, TPropertyType>>, TPropertyType, BindingMode, BindableProperty.ValidateValueDelegate<TPropertyType>, BindableProperty.BindingPropertyChangedDelegate<TPropertyType>, BindableProperty.BindingPropertyChangingDelegate<TPropertyType>, BindableProperty.CoerceValueDelegate<TPropertyType>, BindableProperty.CreateDefaultValueDelegate<TDeclarer, TPropertyType>)" é obsoleto: "Generic versions of Create () are no longer supported and deprecated. They will be removed soon."

		//public static readonly BindableProperty CheckedProperty = BindableProperty.Create("Checked", typeof(bool), typeof(CustomCheckBox), false, BindingMode.TwoWay);


		public bool Checked
		{
			get 
			{ 
				return (bool) GetValue(CheckedProperty); 
			}
			set
			{ 
				if (this.Checked != value)
				{
					this.SetValue(CheckedProperty, value);
				}
				SetValue(CheckedProperty, value); 
			}
		}

		static void OnCheckedPropertyChanged(BindableObject bindable, bool oldValue, bool newValue)
		{
			var checkBox = (CustomCheckBox)bindable;
			checkBox.Checked = newValue;
		}
	}
}
