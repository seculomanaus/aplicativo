﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public class CustomEditor : Editor
	{
		public CustomEditor()
		{
			TextChanged += (sender, e) => { 
				var editor = sender as Editor;

				if (!String.IsNullOrWhiteSpace(editor.Text))
				{
					if (editor.Text.Length > MaxLenght)
					{
						editor.Text = editor.Text.Remove(MaxLenght);
					}
				}
			};

			Focused += async (sender, e) =>
			{
				var editor = sender as Editor;
				if (editor.Text != null && editor.Text.Trim().Equals(PlaceHolder))
				{
					await Task.Delay(10);
					editor.Text = string.Empty;
					editor.TextColor = Color.Black;
				}
			};

			Unfocused += (sender, e) =>
			{
				var editor = sender as Editor;
				if (editor.Text != null)
				{
					if (editor.Text.Trim().Equals(string.Empty))
					{
						editor.Text = PlaceHolder;
						editor.TextColor = Color.FromHex(PlaceHolderColor);
					}

					editor.Text = editor.Text.Trim();
				}
			};
		}

		public static readonly BindableProperty NameMaxLenght = BindableProperty.Create("MaxLenght", typeof(int), typeof(CustomEditor), 1000);

		public int MaxLenght
		{
			get { return (int)GetValue(NameMaxLenght); }
			set { SetValue(NameMaxLenght, value); }
		}

		public static readonly BindableProperty NameProperty = BindableProperty.Create("FontType", typeof(string), typeof(CustomEditor), "");

		public string FontType
		{
			get { return (string)GetValue(NameProperty); }
			set { SetValue(NameProperty, value); }
		}

		public static readonly BindableProperty NamePlaceHolder = BindableProperty.Create("PlaceHolder", typeof(string), typeof(CustomEditor), "");

		public string PlaceHolder
		{
			get { return (string)GetValue(NamePlaceHolder); }
			set { SetValue(NamePlaceHolder, value); Text = value;}
		}

		public static readonly BindableProperty NamePlaceHolderColor = BindableProperty.Create("PlaceHolderColor", typeof(string), typeof(CustomEditor), "");

		public string PlaceHolderColor
		{
			get { return (string)GetValue(NamePlaceHolderColor); }
			set { SetValue(NamePlaceHolderColor, value); TextColor = Color.FromHex(value); }
		}
	}
}
