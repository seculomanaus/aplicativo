﻿using System;
using Seculos.Model;

namespace SeculosApp
{
	public interface ILoadSave
	{
		void load(SeculosGeneral seculosGeneral);
		void save();
	}
}
