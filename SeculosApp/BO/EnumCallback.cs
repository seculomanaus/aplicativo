﻿namespace SeculosApp
{
	public class EnumCallback
	{
		
		public static string DATA_EMPTY = "general_empty";
		public static string CONNECTION_ERROR = "connection_error";
		public static string LOGIN_INVALID = "login_password_invalid";
		public static string LOGIN_EMPTY = "login_empty";
		public static string PASSWORD_EMPTY = "password_empty";
		public static string DATA_ERROR = "data_error";
		public static string SUCCESS = "data_success";
		public static string DATA_NEWS_ERROR = "data_news_error";
		public static string DATA_NEWS_EMPTY = "data_news_empty";
		public static string DATA_STUDENT_ERROR = "data_student_error";
		public static string DATA_STUDENT_EMPTY = "data_student_empty";
		public static string DATA_BILLET_ERROR = "data_billet_error";
		public static string DATA_BILLET_EMPTY = "data_billet_empty";
		public static string DATA_STUDENT_USER_ERROR = "data_student_user_error";
		public static string DATA_STUDENT_USER_EMPTY = "data_student_user_empty";
		public static string LIMIT_EMPTY = "limit_empty";
		public static string LIMIT_DAILY_SUCCESS = "limit_daily_success";
		public static string LIMIT_DAILY_ERROR = "limit_daily_erro";
		public static string LIMIT_DAILY_SUCCESS_ERROR = "limit_daily_success_error";
		public static string ADD_CREDIT_EMPTY = "add_credit_empty";
		public static string DATA_OCCURRENCE_ERROR = "data_occurrence_error";
		public static string DATA_OCCURRENCE_EMPTY = "data_occurrence_empty";
		public static string DATA_DIARIO_ERROR = "data_diario_error";
		public static string TEMPO_ERROR = "tempo_error";
		public static string CONTEUDO_ERROR = "conteudo_error";
		public static string DAILY_MONITORING_ERROR = "daily_monitoring_error";
		public static string TEMPLATE_ERROR = "template_error";
		public static string NOTIFICATION_ERROR = "notification_error";
		public static string BOLETIM_ERROR = "boletim_error";
		public static string DEMONSTRATIVO_ERROR = "demonstrativo_error";
		public static string FREQUENCIA_ERROR = "frequencia_error";
		public static string CARDAPIO_ERROR = "cardapio_error";

		public static string CALENDARIO_PROVA_ERROR = "calendario_prova_error";
		public static string CALENDARIO_PROVA_EMPTY = "calendario_prova_empty";
		public static string CARDAPIO_EMPTY = "cardapio_empty";

		public static string DATA_CALENDARIO_ACADEMICO_ERROR = "data_calendario_academico_error";
		public static string DATA_CALENDARIO_ACADEMICO_EMPTY = "data_calendario_academico_empty";

		public static string INFANTIL_ERROR = "infantil_error";

		public static string DATA_REGISTRO_DIARIO_ERROR = "data_registro_diario_error";
		public static string DATA_REGISTRO_DIARIO_EMPTY = "data_registro_diario_empty";

		public static string DATA_AGENDA_EVENTOS_ERROR = "data_agenda_eventos_error";
		public static string DATA_AGENDA_EVENTOS_EMPTY = "data_agenda_eventos_empty";

		public static string DATA_CONSUMO_ERROR = "data_consumo_error";
		public static string DATA_CONSUMO_EMPTY = "data_consumo_empty";
	}
}
