﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Seculos.Model;
using Seculos.Persistence;
using SeculosApp.IDependecyService;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{

    public class SeculosBO
    {
        static SeculosBO instance;
        LoginPage LoginPage { get; set; }
        public SerializerJson SerializerJson { get; set; }
        public SeculosGeneral SeculosGeneral { get; set; }
        public WebService WebService { get; set; }
        public IMessage IMessage { get; set; }
        public bool LoadNews { get; set; }
        public bool LoadDependentes { get; set; }
        public Noticia NoticiaSelected { get; set; }
        public Dependente StudentSelected { get; set; }
        public Boleto BilletSelected { get; set; }
        public Notificacao NotificationSelected { get; set; }
        public Notificacao ComunicationSelected { get; set; }
        public string errorNews { get; set; }
        public string errorStudents { get; set; }
        public string errorFinance { get; set; }
        public string[] Months = { "TODOS", "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };
        public string studentSelectedPicker { get; set; }
        public string monthSelectedPicker { get; set; }
        public string monthStudentSelectedPicker { get; set; }
        public string studentSelectedComunicationsPicker { get; set; }
        public string monthSelectedComunicationsPicker { get; set; }
        public bool fromLogin { get; set; }
        public INavigation Navigation { get; set; }
        public bool basePhotoChanged { get; set; }
        public static string EMPTY = "(vazio)";
        public bool changePhotoFromGallery;
        public bool loadingGlobal;

        public Dependente DependenteConnected
        {
            get
            {
                if (isResp(SeculosGeneral.UserConnected))
                {
                    return StudentSelected;
                }
                return SeculosGeneral.UserStudentConnected;
            }
        }

        private SeculosBO()
        {
            SerializerJson = new SerializerJson();
            IMessage = DependencyService.Get<IMessage>();
            errorNews = string.Empty;
            errorStudents = string.Empty;
            errorFinance = string.Empty;
            studentSelectedPicker = string.Empty;
            monthSelectedPicker = string.Empty;
            studentSelectedPicker = string.Empty;
            studentSelectedPicker = string.Empty;
            studentSelectedComunicationsPicker = string.Empty;
            monthSelectedComunicationsPicker = string.Empty;
        }

        public static SeculosBO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SeculosBO();
                }
                return instance;
            }
        }

        public bool LoadSeculosGeneral()
        {
            SeculosGeneral = SerializerJson.LoadAndDeserializeObj();
            if (SeculosGeneral != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

   
		public bool SaveSeculosGeneral()
		{
			return SerializerJson.SerializeAndSave(SeculosGeneral);
		}

		public async Task login(string login, string password, bool keepConnected, Action<string> callback)
		{
			App._Current.MainPage.BackgroundColor = Color.White;
			try{
				Task<User> taskLogin = new WebService().GetGenericResult<User>(WebService.AUTENTICACAO+"?login=" + login + "&senha=" + password);


				if (taskLogin != null)
				{
					User userResult = await taskLogin;
					if (userResult != null)
					{
						if (!userResult.Error.Equals(string.Empty))
						{
							callback(userResult.Error);
						}
						else if (userResult.CdUsuario != null)
						{
							var logedAnyTime = false;
							//First User logging in App
							if (SeculosGeneral == null)
							{
								SeculosGeneral = new SeculosGeneral();
							}
							else
							{
								//verify if user already login any time
								if (searchUser(userResult) != null)
								{
									System.Diagnostics.Debug.WriteLine("User already login any time");
									logedAnyTime = true;
								}
							}


							if (userResult.DcTipo.Equals("responsavel")) {
								if (userResult.CdUsuario.Length < 11)
                                {
									userResult.CdUsuario = login;

								}
                            }

							// this user never has logged in app
							if (!logedAnyTime)
							{
								SeculosGeneral.UserList.Add(userResult);
							}

							SeculosGeneral.UserConnected = userResult;
							SeculosGeneral.KeepConnected = keepConnected;
							SaveSeculosGeneral();

							System.Diagnostics.Debug.WriteLine("CdUsuario: " + userResult.CdUsuario);
							System.Diagnostics.Debug.WriteLine("Nome: " + userResult.Nome);
							System.Diagnostics.Debug.WriteLine("Tipo: " + userResult.DcTipo);
							System.Diagnostics.Debug.WriteLine("Celular: " + userResult.NrCelular);

                            System.Diagnostics.Debug.WriteLine("End gerate token");
							if (!isResp(userResult))
							{
								fromLogin = true;
								callback(await getStudentWait(userResult));
							}
							else
							{ 
								// redirect to Main Page
								callback(null);
							}
						}
						else
						{
							// usuario ou senha invalida
							System.Diagnostics.Debug.WriteLine("userResult null: ");
							callback(EnumCallback.LOGIN_INVALID);
						}
					}
					else
					{
						// usuario ou senha invalida
						System.Diagnostics.Debug.WriteLine("userResult null: ");
						callback(EnumCallback.LOGIN_INVALID);
					}
				}
				else
				{
					// erro na requisicao
					System.Diagnostics.Debug.WriteLine("sizeTask null: ");
					callback(EnumCallback.LOGIN_INVALID);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO login exception: " + exception.Message+" Type: "+exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// // Message.Msg(Application.Current.Resources["server_error"] as String);
					callback("server_error");
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException")) 
				{
					callback(EnumCallback.CONNECTION_ERROR);	
				}
			}

		}

		public void logout(Action callback) 
		{
			studentSelectedPicker = string.Empty;
			monthSelectedPicker = string.Empty;
			studentSelectedPicker = string.Empty;
			studentSelectedComunicationsPicker = string.Empty;
			monthSelectedComunicationsPicker = string.Empty;
			App._Current.MainPage.BackgroundColor = Color.Transparent;

			IToken iToken = DependencyService.Get<IToken>();
			Task.Factory.StartNew(() =>
			{
				iToken.DeleteToken();	
			});
			SendRegistrationToAppServer(null);
			SeculosGeneral.KeepConnected = false;
			SeculosGeneral.UserConnected = null;
			SeculosGeneral.UserStudentConnected = null;
			SaveSeculosGeneral();
			callback();
		}

		public User searchUser(User User)
		{
			User findUser = null;
			foreach (User user in SeculosGeneral.UserList)
			{
				if (user.CdUsuario.Equals(User.CdUsuario))
				{
					System.Diagnostics.Debug.WriteLine("User already login any time");
					user.Nome = User.Nome;
					user.DcTipo = User.DcTipo;
					user.NrCelular = User.NrCelular;
					User.listDependente = user.listDependente;
					User.TokenId = user.TokenId;
					findUser = user;
					break;
				}
			}
			return findUser;
		}

		public object SearchUserByDependent(string cdUser)
		{
			if (!isResp(SeculosGeneral.UserConnected))
			{
				// procurar por usuario logado
				foreach (User user in SeculosGeneral.UserList)
				{
					if (user.CdUsuario.Equals(cdUser))
					{
						return user;
					}
				}
			}
			else
			{
				// procurar por dependente
				foreach (User user in SeculosGeneral.UserList)
				{
					if (user.listDependente != null)
					{
						foreach (Dependente dep in user.listDependente)
						{
							if (dep.CdAluno != null && dep.CdAluno.Equals(cdUser) || dep.CdMatricula != null && dep.CdMatricula.Equals(cdUser))
							{
								return dep;
							}
						}
					}
				}
			}

			return null;
		}

		public void SetBase64ToDependent(Dependente dep, string imageBase64)
		{
			dep.FotoBase = imageBase64;
			var userCode = dep.CdAluno == null ? dep.CdMatricula : dep.CdAluno;
			var user = SearchUserByDependent(userCode);

			if (user != null)
			{
				var userObject = user as User;
				var depObject = user as Dependente;
				if (userObject != null)
				{
                    try{
                        userObject.FotoBase = imageBase64;
                        if (SeculosGeneral.UserConnected.CdUsuario.Equals(userCode))
                        {
                            SeculosGeneral.UserConnected.FotoBase = imageBase64;
                        }
                        if (SeculosGeneral.UserStudentConnected.CdMatricula.Equals(userCode) || SeculosGeneral.UserStudentConnected.CdAluno.Equals(userCode))
                        {
                            SeculosGeneral.UserStudentConnected.FotoBase = imageBase64;
                        }
                    }
                    catch(Exception e){
                        System.Diagnostics.Debug.WriteLine("Exception image: "+e.Message);
                    }
					
				}
				else if (depObject != null)
				{
					depObject.FotoBase = imageBase64;
				}
			}

			SaveSeculosGeneral();
           
		}

		public User searchUserByCode(string userCode)
		{
			User findUser = null;
			foreach (User user in SeculosGeneral.UserList)
			{
				if ((user.CdUsuario != null && user.CdUsuario.Equals(userCode)) || (user.CdMatricula != null && user.CdMatricula.Equals(userCode)))
				{
					findUser = user;
					break;
				}
			}
			return findUser;
		}

		public Notificacao searchNotificatioByCode(string notificacaoCode, ObservableCollection<Notificacao> listNotifications)
		{
			Notificacao Notificacao = null;
			if (listNotifications != null)
			{
				foreach (Notificacao notificacao in listNotifications)
				{
					if (notificacao.CdAlert.Equals(notificacaoCode))
					{
						Notificacao = notificacao;
						break;
					}
				}
			}

			return Notificacao;
		}


		public bool isResp(User user) 
		{
			if (user.DcTipo.Equals("responsavel"))
			{
				return true;
			}
			else 
			{
				return false;
			}
		}

		public void showToastOrView<T>(ICollection<T> list, string enumMsg, Label label, string msg)
		{
			var text = "";
			if (enumMsg != null)
			{
				text = Application.Current.Resources[enumMsg] as String;
			}
			else 
			{
				text = msg;
			}
			if (list.Count > 0)
			{
				//show toast
				SeculosBO.Instance.IMessage.Msg(text);
			}
			else if (label != null)
			{
				// show text label
				label.Text = text;
				label.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				Utils.FadeView(label);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			}
		}

		public void showViewError(string enumMsg, Label label, string msg)
		{
			var text = "";
			if (enumMsg != null)
			{
				text = Application.Current.Resources[enumMsg] as String;
			}
			else
			{
				text = msg;
			}

			// show text label
			label.Text = text;
			label.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			Utils.FadeView(label);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
		}

		public async Task<string> getNoticiasWait()
		{
			try
			{
				Task<ObservableCollection<Noticia>> taskNoticia = new WebService().GetGenericResult<ObservableCollection<Noticia>>(WebService.NOTICIA + "?fake=0");


				if (taskNoticia != null)
				{
					ObservableCollection<Noticia> userNoticiaList = await taskNoticia;
					if (userNoticiaList != null)
					{
						// Sucesso na recuperacao dos dados de Noticia
						System.Diagnostics.Debug.WriteLine("userNoticiaList success: ");
						SeculosGeneral.listNoticias = new ObservableCollection<Noticia>(userNoticiaList);
						SaveSeculosGeneral();
						return EnumCallback.SUCCESS;
					}
					else
					{
						// erro na recuperacao dos dados de Noticia
						System.Diagnostics.Debug.WriteLine("userNoticia null: ");
						return EnumCallback.DATA_NEWS_ERROR;
					}
				}
				else
				{ 
					return EnumCallback.DATA_NEWS_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getNoticiasWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.DATA_NEWS_ERROR;
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))//System.NullReferenceException
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{ 
					return EnumCallback.SUCCESS;
				}
			}
		}

		public async Task<string> getDependentesWait(User user)
		{
			try
			{
				
				Task<ObservableCollection<Dependente>> taskDependente = new WebService().GetGenericResult<ObservableCollection<Dependente>>(WebService.DEPENDENTE + "?responsavel=" + user.CdUsuario);

				if (taskDependente != null)
				{
					ObservableCollection<Dependente> userDependenteList = await taskDependente;
					if (userDependenteList != null)
					{
						// Sucesso na recuperacao dos dados de Dependentes
						System.Diagnostics.Debug.WriteLine("userDependenteList success: ");

						setDependenteToNotifications(user.listNotifications, user);

						if (user.listDependente != null)
						{
							foreach (Dependente newDependent in userDependenteList)
							{
								var cdAlunoNew = newDependent.CdMatricula;
								if (cdAlunoNew == null || cdAlunoNew.Equals(SeculosBO.EMPTY))
								{
									cdAlunoNew = newDependent.CdAluno;
								}
								foreach (Dependente oldDependent in user.listDependente)
								{
									var cdAlunoOld = oldDependent.CdMatricula;

									if (cdAlunoOld == null || cdAlunoOld.Equals(SeculosBO.EMPTY))
									{
										cdAlunoOld = oldDependent.CdAluno;
									}
									if (cdAlunoNew.Equals(cdAlunoOld))
									{
										newDependent.FotoBase = oldDependent.FotoBase;
									}
								}
							}
						}

						user.listDependente = new ObservableCollection<Dependente>(userDependenteList);
						SaveSeculosGeneral();
						return EnumCallback.SUCCESS;
					}
					else
					{
						// erro na recuperacao dos dados de Dependentes
						System.Diagnostics.Debug.WriteLine("userDependenteList null: ");
						return EnumCallback.DATA_STUDENT_ERROR;
					}
				}
				else
				{
					return EnumCallback.DATA_STUDENT_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getDependentesWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.DATA_STUDENT_ERROR;
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{
					return EnumCallback.SUCCESS;
				}
			}
		}

		public async Task<string> getBoletosWait(User user)
		{
			try
			{
				Task<ObservableCollection<Boleto>> taskDependente = new WebService().GetGenericResult<ObservableCollection<Boleto>>(WebService.BOLETO + "?responsavel=" + user.CdUsuario);
               

				if (taskDependente != null)
				{
					ObservableCollection<Boleto> userBilletsList = await taskDependente;
					if (userBilletsList != null)
					{
						// Sucesso na recuperacao dos dados de Boletos
						System.Diagnostics.Debug.WriteLine("userBilletsList success: ");

						user.listBillets = new ObservableCollection<Boleto>(userBilletsList);
						SaveSeculosGeneral();
						return EnumCallback.SUCCESS;
					}
					else
					{
						// erro na recuperacao dos dados de Boletos
						System.Diagnostics.Debug.WriteLine("userBilletsList null: ");
						return EnumCallback.DATA_BILLET_ERROR;
					}
				}
				else
				{
					return EnumCallback.DATA_BILLET_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getBoletosWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.DATA_BILLET_ERROR;
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))//System.NullReferenceException
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{
					return EnumCallback.SUCCESS;
				}
			}
		}

		public async Task<string> getStudentWait(User user)
		{
			try
			{
				Task<Dependente> taskAluno = new WebService().GetGenericResult<Dependente>(WebService.ALUNO + "?matricula=" + user.CdUsuario);

				if (taskAluno != null)
				{
					Dependente userAlunoResult = await taskAluno;
					if (userAlunoResult != null && !userAlunoResult.CdMatricula.Equals(string.Empty))
					{
						foreach (User userList in SeculosGeneral.UserList)
						{
							if (userList.CdUsuario.Equals(userAlunoResult.CdMatricula))
							{
								userAlunoResult.FotoBase = userList.FotoBase;
							}
						}

						SeculosGeneral.UserStudentConnected = userAlunoResult;
						user.UserDependenteConnected = userAlunoResult;
						SaveSeculosGeneral();
						return EnumCallback.SUCCESS;
					}
					else
					{
						// erro na recuperacao dos dados do Aluno
						System.Diagnostics.Debug.WriteLine("userAlunoResult null: ");
						return EnumCallback.DATA_STUDENT_USER_ERROR;
					}
				}
				else
				{
					// erro na recuperacao dos dados do Aluno
					System.Diagnostics.Debug.WriteLine("taskAluno null: ");
					return EnumCallback.DATA_STUDENT_USER_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getStudentWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.DATA_STUDENT_USER_ERROR;
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))//System.NullReferenceException
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{
					return EnumCallback.SUCCESS;
				}
			}
		}

		public async Task<string> setLimitDaily(Dependente user)
		{
			try
			{
				var limite = user.LimitDecimal;

				if (limite.Equals(EMPTY))
				{
					limite = null;
				}

				Task<Response> taskStudent = new WebService().GetGenericResult<Response>(WebService.LIMIT + "?matricula=" + user.CdAluno+ "&limite=" + limite);

				if (taskStudent != null)
				{
					Response userStudentResult = await taskStudent;
					if (userStudentResult != null && !userStudentResult.Result.Equals(string.Empty))
					{
						if (limite == null)
						{
							user.LimiteDependente = "ilimitado";
						}
						else
						{
							user.LimiteDependente = user.LimitDecimal;
						}

						SaveSeculosGeneral();
						return EnumCallback.LIMIT_DAILY_SUCCESS;
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userStudentResult null: ");
						return EnumCallback.LIMIT_DAILY_ERROR;
					}
				}
				else
				{
					// erro na recuperacao dos dados do Aluno
					System.Diagnostics.Debug.WriteLine("taskStudent null: ");
					return EnumCallback.LIMIT_DAILY_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO setLimitDaily exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.LIMIT_DAILY_ERROR;
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{
					return EnumCallback.LIMIT_DAILY_SUCCESS;
				}
			}
		}

		public async void getOccurrenceWait(Dependente user, string date, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Ocorrencia>> taskOcorrencia = new WebService().GetGenericResult<ObservableCollection<Ocorrencia>>(WebService.OCORRENCIA + "?matricula=" + userCode + "&data="+date);

				if (taskOcorrencia != null)
				{
					ObservableCollection<Ocorrencia> userOcorrenciaResult = await taskOcorrencia;
					if (userOcorrenciaResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else 
						{
							dependentePersist = user;

						}

						if (dependentePersist.listOcorrenciaByDate<Ocorrencia>(date) != null) {
							dependentePersist.mapOcorrencia.Remove(date);
						}

						dependentePersist.mapOcorrencia.Add(date, new ObservableCollection<Ocorrencia>(userOcorrenciaResult));

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userOcorrenciaResult null: ");
						callback(EnumCallback.DATA_OCCURRENCE_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskOcorrencia null: ");
					callback(EnumCallback.DATA_OCCURRENCE_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getOccurrenceWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.LIMIT_DAILY_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getDiarioWS(Dependente user, string date, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Diario>> taskDiario = new WebService().GetGenericResult<ObservableCollection<Diario>>(WebService.DIARIO + "?turma=" + user.Turma+ "&aluno=" + userCode + "&data=" + date);

				if (taskDiario != null)
				{
					ObservableCollection<Diario> userDiarioResult = await taskDiario;
					if (userDiarioResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						if (dependentePersist.listDiarioByDate<Diario>(date) != null)
						{
							dependentePersist.mapDiario.Remove(date);
						}

						dependentePersist.mapDiario.Add(date, new ObservableCollection<Diario>(userDiarioResult));

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userDiarioResult null: ");
						callback(EnumCallback.DATA_DIARIO_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskDiario null: ");
					callback(EnumCallback.DATA_DIARIO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getDiarioWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DATA_DIARIO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getTempoWS(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Tempo>> taskTempo = new WebService().GetGenericResult<ObservableCollection<Tempo>>(WebService.TEMPO + "?matricula=" + userCode);

				if (taskTempo != null)
				{
					ObservableCollection<Tempo> userTempoResult = await taskTempo;
					if (userTempoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						dependentePersist.listTempo = new ObservableCollection<Tempo>(userTempoResult);

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userTempoResult null: ");
						callback(EnumCallback.TEMPO_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskTempo null: ");
					callback(EnumCallback.TEMPO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getTempoWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.TEMPO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getConteudoWS(Dependente user, Action<string> callback)
		{
			try
			{
				Task<ObservableCollection<Conteudo>> taskConteudo = new WebService().GetGenericResult<ObservableCollection<Conteudo>>(WebService.CONTEUDO + "?turma=" + user.Turma);

				if (taskConteudo != null)
				{
					ObservableCollection<Conteudo> userConteudoResult = await taskConteudo;
					if (userConteudoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						dependentePersist.listConteudo = new ObservableCollection<Conteudo>(userConteudoResult);

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userConteudoResult null: ");
						callback(EnumCallback.CONTEUDO_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskConteudo null: ");
					callback(EnumCallback.CONTEUDO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getConteudoWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.CONTEUDO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getAcompanhamentoDiarioWS(Dependente user, string date, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				
                Task<ObservableCollection<AcompanhamentoDiario>> taskAcompanhamento = new WebService().GetGenericResult<ObservableCollection<AcompanhamentoDiario>>(WebService.ACOMPANHAMENTO + "?matricula=" + userCode+ "&data=" + date);


				if (taskAcompanhamento != null)
				{
                    
                    ObservableCollection<AcompanhamentoDiario> userAcompanhamentoResult = await taskAcompanhamento;

                    if (userAcompanhamentoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						if (dependentePersist.listAcompanhamentoDiarioByDate<AcompanhamentoDiario>(date) != null)
						{
							dependentePersist.mapAcompanhamentoDiario.Remove(date);
						}

						dependentePersist.mapAcompanhamentoDiario.Add(date, new ObservableCollection<AcompanhamentoDiario>(userAcompanhamentoResult));

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userAcompanhamentoResult null: ");
						callback(EnumCallback.DAILY_MONITORING_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskAcompanhamento null: ");
					callback(EnumCallback.DAILY_MONITORING_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getAcompanhamentoInfantilWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DAILY_MONITORING_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getGabaritoWS(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Gabarito>> tasGabarito = new WebService().GetGenericResult<ObservableCollection<Gabarito>>(WebService.GABARITO + "?matricula=" + userCode);

				if (tasGabarito != null)
				{
					ObservableCollection<Gabarito> userGabaritoResult = await tasGabarito;
					if (userGabaritoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						dependentePersist.createMapGabaritoByType(userGabaritoResult);

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userGabaritoResult null: ");
						callback(EnumCallback.TEMPLATE_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskGabarito null: ");
					callback(EnumCallback.TEMPLATE_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getGabaritoWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.TEMPLATE_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async Task<string> getNotificationWaitWs(User user)
		{
			var type = 0;
			if (isResp(user))
			{
				// tipo de usuário responsável
				type = 2;
			}
			else {
				// tipo de usuário aluno
				type = 1;
			}
			try
			{
				Task<ObservableCollection<Notificacao>> taskNotificacao = new WebService().GetGenericResult<ObservableCollection<Notificacao>>(WebService.NOTIFICACAO + "?usuario=" + user.CdUsuario+ "&tipo="+type);

                System.Diagnostics.Debug.WriteLine("taskNotificacao: "+taskNotificacao);

                System.Diagnostics.Debug.WriteLine("taskNotificacao: " + taskNotificacao);


				if (taskNotificacao != null)
				{
					ObservableCollection<Notificacao> userNotificationList = await taskNotificacao;

                    System.Diagnostics.Debug.WriteLine("userNotificationList: " + userNotificationList);

					if (userNotificationList != null)
					{
						System.Diagnostics.Debug.WriteLine("userNotificationList success: ");

						User userConn = searchUserByCode(user.CdUsuario);

						if (userConn != null && userConn.listNotifications != null && user.listNotifications != null)
						{
							foreach (Notificacao notificacao in user.listNotifications)
							{
								foreach (Notificacao notificacaoNew in userNotificationList)
								{
									if (notificacao.CdAlert.Equals(notificacaoNew.CdAlert))
									{
										notificacaoNew.readAt = notificacao.readAt;
									}
								}
							}
						}

						setDependenteToNotifications(userNotificationList, userConn);

						List<Notificacao> userNotificationListReverse = userNotificationList.ToList();
						userNotificationListReverse.Reverse();
						userConn.listNotifications = new ObservableCollection<Notificacao>(userNotificationListReverse);
						SeculosGeneral.UserConnected.listNotifications = userConn.listNotifications;
						SaveSeculosGeneral();
						return EnumCallback.SUCCESS;
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userNotificationList null: ");
						return EnumCallback.NOTIFICATION_ERROR;
					}
				}
				else
				{
					return EnumCallback.NOTIFICATION_ERROR;
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getNotificationWaitWs exception: " + exception.Message + " Type: " + exception.GetType());

				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException"))
				{
					if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException"))
					{
						try
						{
							Task<GenericModel> taskNotificacao = new WebService().GetGenericResult<GenericModel>(WebService.NOTIFICACAO + "?usuario=" + user.CdUsuario + "&tipo=" + type);

							if (taskNotificacao != null)
							{
								GenericModel consumoResult = await taskNotificacao;
								if (consumoResult != null)
								{
									if (consumoResult.Error != null && !consumoResult.Error.Equals(string.Empty))
									{
										// IMessage.Msg(consumoResult.Error);
										return EnumCallback.NOTIFICATION_ERROR;
									}
									else
									{
										// Message.Msg(Application.Current.Resources["server_error"] as String);
										return EnumCallback.NOTIFICATION_ERROR;
									}
								}
								else
								{
									// Message.Msg(Application.Current.Resources["server_error"] as String);
									return EnumCallback.NOTIFICATION_ERROR;
								}
							}
							else
							{
								// Message.Msg(Application.Current.Resources["server_error"] as String);
								return EnumCallback.NOTIFICATION_ERROR;
							}

						}
						catch (System.Exception exceptionInner)
						{
							System.Diagnostics.Debug.WriteLine("BO getNotificationWaitWs exceptionInner: " + exceptionInner.Message + " Type: " + exceptionInner.GetType());
							if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   				|| exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
							{
								// Message.Msg(Application.Current.Resources["server_error"] as String);
								return EnumCallback.NOTIFICATION_ERROR;
							}
                            else if (exception.GetType().ToString().Equals("System.Net.WebException"))
							{
								return EnumCallback.CONNECTION_ERROR;
							}
							else
							{
								// Message.Msg(Application.Current.Resources["server_error"] as String);
								return EnumCallback.NOTIFICATION_ERROR;
							}
						}
					}
					else
					{
						// Message.Msg(Application.Current.Resources["server_error"] as String);
						return EnumCallback.NOTIFICATION_ERROR;
					}
				}
				else if (exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					return EnumCallback.NOTIFICATION_ERROR;
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))
				{
					return EnumCallback.CONNECTION_ERROR;
				}
				else
				{
					return EnumCallback.SUCCESS;
				}
			}
		}

		public void setDependenteToNotifications(ObservableCollection<Notificacao> userNotificationList, User userConn)
		{
			if (userNotificationList != null)
			{
				foreach (Notificacao notificacao in userNotificationList)
				{
					if (isResp(SeculosGeneral.UserConnected))
					{
						foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
						{
							if (dependente.CdAluno.Equals(notificacao.CdAluno))
							{
								notificacao.Dependente = dependente;
								break;
							}
						}

						foreach (Dependente dependente in userConn.listDependente)
						{
							if (dependente.CdAluno.Equals(notificacao.CdAluno))
							{
								notificacao.Dependente = dependente;
								break;
							}
						}
					}
				}
			}
		}

		public async void getBoletimWS(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Boletim>> tasBoletim = new WebService().GetGenericResult<ObservableCollection<Boletim>>(WebService.BOLETIM + "?matricula=" + userCode);

				if (tasBoletim != null)
				{
					ObservableCollection<Boletim> userBoletimResult = await tasBoletim;
					if (userBoletimResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						dependentePersist.listBoletim = new ObservableCollection<Boletim>(userBoletimResult);

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userBoletimResult null: ");
						callback(EnumCallback.BOLETIM_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("tasBoletim null: ");
					callback(EnumCallback.BOLETIM_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getBoletimWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.BOLETIM_ERROR);
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getDemonstrativoWS(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Demonstrativo>> taskDemonstrativo = new WebService().GetGenericResult<ObservableCollection<Demonstrativo>>(WebService.DEMONSTRATIVO + "?matricula=" + userCode);

				if (taskDemonstrativo != null)
				{
					ObservableCollection<Demonstrativo> userDemonstrativoResult = await taskDemonstrativo;
					if (userDemonstrativoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						dependentePersist.listDemonstrativo = new ObservableCollection<Demonstrativo>(userDemonstrativoResult);

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userDemonstrativoResult null: ");
						callback(EnumCallback.DEMONSTRATIVO_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskDemonstrativo null: ");
					callback(EnumCallback.DEMONSTRATIVO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getDemonstrativoWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DEMONSTRATIVO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void GetFrequenciaAluno(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Frequencia>> taskAcompanhamentoInfantil = new WebService().GetGenericResult<ObservableCollection<Frequencia>>(WebService.FREQUENCIA + "?matricula=" + userCode);

				if (taskAcompanhamentoInfantil != null)
				{
					ObservableCollection<Frequencia> userAcompanhamentoInfantilResult = await taskAcompanhamentoInfantil;
					if (userAcompanhamentoInfantilResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;
						}

						dependentePersist.Frequencia = new ObservableCollection<Frequencia>(userAcompanhamentoInfantilResult);
						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userFrequenciaResult null: ");
						callback(EnumCallback.FREQUENCIA_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskFrequencia null: ");
					callback(EnumCallback.FREQUENCIA_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO geFrequencialWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.FREQUENCIA_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}		
		}

		public async void GetCardapio(Dependente user, Action<string> callback)
		{
			try
			{
				Task<ObservableCollection<Cardapio>> taskCardapio = new WebService().GetGenericResult<ObservableCollection<Cardapio>>(WebService.CARDAPIO+"?");

				if (taskCardapio != null)
				{
					ObservableCollection<Cardapio> cardapioResult = await taskCardapio;
					if (cardapioResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;
						}

						dependentePersist.Cardapios = new ObservableCollection<Cardapio>(cardapioResult);
						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("cardapioResult null: ");
						callback(EnumCallback.CARDAPIO_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskCardapio null: ");
					callback(EnumCallback.CARDAPIO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO geFrequencialWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.CARDAPIO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void GetCalendarioProva(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<Prova>> taskCalendarioProva = new WebService().GetGenericResult<ObservableCollection<Prova>>(WebService.CALENDARIO_PROVA + "?turma=" + user.Turma);

				if (taskCalendarioProva != null)
				{
					ObservableCollection<Prova> caledarioProvaResult = await taskCalendarioProva;
					if (caledarioProvaResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}
						dependentePersist.CalendarioProva = new ObservableCollection<Prova>(caledarioProvaResult);
						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("caledarioProvaResult null: ");
						callback(EnumCallback.CALENDARIO_PROVA_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskCalendarioProva null: ");
					callback(EnumCallback.CALENDARIO_PROVA_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO GetCalendarioProva exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.CALENDARIO_PROVA_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getAcompanhamentoInfantilWS(Dependente user, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<AcompanhamentoInfantil>> taskAcompanhamentoInfantil = new WebService().GetGenericResult<ObservableCollection<AcompanhamentoInfantil>>(WebService.INFANTIL + "?matricula=" + userCode+"&turma="+user.Turma);
                System.Diagnostics.Debug.WriteLine("acompanhamento 1: ");

				if (taskAcompanhamentoInfantil != null)
				{
					ObservableCollection<AcompanhamentoInfantil> userAcompanhamentoInfantilResult = await taskAcompanhamentoInfantil;
                    System.Diagnostics.Debug.WriteLine("acompanhamento 2: ");
					if (userAcompanhamentoInfantilResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}
						dependentePersist.listAcompanhamentoInfantil = new ObservableCollection<AcompanhamentoInfantil>(userAcompanhamentoInfantilResult);
						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("userAcompanhamentoInfantilResult null: ");
						callback(EnumCallback.INFANTIL_ERROR);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskAcompanhamentoInfantil null: ");
					callback(EnumCallback.INFANTIL_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO geAcompanhamentoInfantilWS exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.INFANTIL_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public string createBuyCreditObject(ObservableCollection<Dependente> listStudents, string totalAmoutCredit)
		{ 
			ObservableCollection<StudentBuyCredit> listStudentsBuy = new ObservableCollection<StudentBuyCredit>();

			foreach (Dependente student in listStudents)
			{
				if (student.LimitDecimal != null && !student.LimitDecimal.Equals("0") && !student.LimitDecimal.Equals(SeculosBO.EMPTY))
				{
					StudentBuyCredit studentBuy = new StudentBuyCredit();
					studentBuy.cd_aluno = student.CdAluno;
					studentBuy.vl_credito = student.LimitDecimal;
					studentBuy.nm_aluno = student.NomeAluno;
					studentBuy.lk_foto = student.lkFoto;
					listStudentsBuy.Add(studentBuy);
				}

			}
			var BuyCredit = new
			{
				cd_usuario = SeculosGeneral.UserConnected.CdUsuario,
				alunos = listStudentsBuy,
				vl_total = Utils.stringToDecimal(totalAmoutCredit)
			};

			string json = JsonConvert.SerializeObject(BuyCredit);
			System.Diagnostics.Debug.WriteLine("json: " + json);
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
		}

		public async Task OnResumeUpdate()
		{
			//ComunicationModal.updateByPayCreditCard || ComunicationModal.updateByPayCreditStudent
			// atualizar alerta
			// atualizar boleto

			//buyCredit
			// atualizar dependente

			//PayCreditModal.updateByBillet
			//atualizar alerta
			// atualizar boleto
			// atualizar dependente

			//PayCreditModal.updateByEvent
			//atualizar alerta
			// atualizar boleto
			// atualizar dependente

			//mandar mensagem para view se atualizar
			// TODO atualizar view
			if (ComunicationModal.updateByPayCreditCard || BilletDetailView.updateByPayCreditCard || BuyCreditModal.buyCredit || PayCreditModal.updateByBillet || PayCreditModal.updateByEvent || ComunicationModal.updateByPayCreditStudent)
			{
				loadingGlobal = true;
				var msgNotification = await getNotificationWaitWs(SeculosGeneral.UserConnected);

				App._Current.SendMessage(MessagingConstants.ALERTA_HAS_BEEN_UPDATED);

				if (BuyCreditModal.buyCredit || PayCreditModal.updateByBillet || PayCreditModal.updateByEvent)
				{
					var dep = await getDependentesWait(SeculosGeneral.UserConnected);
					UpdateStudentSelected();
					App._Current.SendMessage(MessagingConstants.SALDO_HAS_BEEN_UPDATED);
				}

				if (!BuyCreditModal.buyCredit)
				{
					var boleto = await getBoletosWait(SeculosGeneral.UserConnected);
					UpdateBilletSelected();
					App._Current.SendMessage(MessagingConstants.BOLETO_HAS_BEEN_UPDATED);
				}

				BilletDetailView.updateByPayCreditCard = false;
				ComunicationModal.updateByPayCreditStudent = false;
				ComunicationModal.updateByPayCreditCard = false;
				BuyCreditModal.buyCredit = false;
				PayCreditModal.updateByBillet = false;
				PayCreditModal.updateByEvent = false;
				loadingGlobal = false;
			}
		}

		public void OnResumeVerifyUpdate()
		{
			if (ComunicationModal.updateByPayCreditCard || BilletDetailView.updateByPayCreditCard || BuyCreditModal.buyCredit || PayCreditModal.updateByBillet || PayCreditModal.updateByEvent || ComunicationModal.updateByPayCreditStudent)
			{
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				OnResumeUpdate();
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			}
		}

		public void UpdateStudentSelected()
		{
			if (StudentSelected != null)
			{
				foreach (var dep in SeculosGeneral.UserConnected.listDependente)
				{
					if (dep.CdAluno.Equals(StudentSelected.CdAluno))
					{
						StudentSelected = dep;
						break;
					}
				}
			}
		}

		public void UpdateBilletSelected()
		{
			if (BilletSelected != null)
			{
				foreach (Boleto boleto in SeculosGeneral.UserConnected.listBillets)
				{
					if (boleto.CdBoleto.Equals(BilletSelected.CdBoleto))
					{
						BilletSelected = boleto;
						break;
					}
				}
			}
		}

		public async Task<ObservableCollection<Notificacao>> verifySendNotification()
		{

			ObservableCollection<Notificacao> userNotificationList = new ObservableCollection<Notificacao>();

			if (SeculosGeneral != null && SeculosGeneral.UserConnected != null)
			{
				var msg = await getNotificationWaitWs(SeculosGeneral.UserConnected);

				var listNotifications = SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications;

				if (msg.Equals(EnumCallback.SUCCESS))
				{

					if (listNotifications != null && listNotifications.Count > 0)
					{
						foreach (Notificacao notificacao in listNotifications)
						{
							if (notificacao.readAt == new DateTime())
							{
								// add to array send notification
								userNotificationList.Add(notificacao);
							}
						}
					}
				}
			}

			return userNotificationList;
		}

		public void saveReadNotification(Notificacao Notification)
		{ 
			User userConnected = searchUserByCode(SeculosGeneral.UserConnected.CdUsuario);

			if (userConnected != null)
			{
				var notification = searchNotificatioByCode(Notification.CdAlert, userConnected.listNotifications);

				if (notification != null && notification.readAt == new DateTime())
				{
					notification.readAt = DateTime.Now;
					var notificatioConn = searchNotificatioByCode(Notification.CdAlert,SeculosGeneral.UserConnected.listNotifications);
					if (notificatioConn != null)
					{
						notificatioConn.readAt = notification.readAt;
					}
					Notification = notification;
					new Task(() =>
					{
						SaveSeculosGeneral();
					}).Start();
				}
			}
		}

		public void addNotificationByGcm(Notificacao notification)
		{
			if (notification == null)
				return;
			
			bool added = false;
			var listNotifications = SeculosGeneral?.UserConnected?.listNotifications;
			if (listNotifications == null)
			{
				SeculosGeneral.UserConnected.listNotifications = new ObservableCollection<Notificacao>();
				SeculosGeneral.UserConnected.listNotifications.Add(notification);
				listNotifications = SeculosGeneral.UserConnected.listNotifications;
			}
			else
			{
				
				foreach (Notificacao notificacao in listNotifications)
				{
					if (notificacao.CdAlert.Equals(notification.CdAlert))
					{
						added = true;
					}
				}

				if (!added)
				{
					listNotifications.Add(notification);
				}
			}

			User user = searchUserByCode(SeculosGeneral.UserConnected.CdUsuario);
			if (user.listNotifications == null)
			{
				user.listNotifications = new ObservableCollection<Notificacao>();
				user.listNotifications.Add(notification);
			}
			else
			{
				added = false;
				foreach (Notificacao notificacao in user.listNotifications)
				{
					if (notificacao.CdAlert.Equals(notification.CdAlert))
					{
						added = true;
					}
				}

				if (!added)
				{
					listNotifications.Add(notification);
				}
			}

			setDependenteToNotifications(listNotifications, user);

			SaveSeculosGeneral();
		}

		public async void SendRegistrationToAppServer(string token_device)
		{
			var json = new
			{
				token_id = SeculosGeneral?.UserConnected?.TokenId,
				cd_usuario = SeculosGeneral?.UserConnected?.CdUsuario,
				token = token_device
			};

			if (SeculosGeneral != null)
			{
				SeculosGeneral.GCMToken = token_device;
			}

			try
			{
				Task<Result> taskResult = new WebService().GetGenericResult<Result>(WebService.TOKEN_GCM + "?cd_usuario=" + json.cd_usuario + "&token_banco=" + json.token_id + "&token_gcm=" + json.token);

				if (taskResult != null)
				{
					Result userResult = await taskResult;
					if (userResult != null)
					{
						SeculosGeneral.UserConnected.TokenId = userResult.Data.TokenId;
						searchUserByCode(SeculosGeneral.UserConnected.CdUsuario).TokenId = SeculosGeneral.UserConnected.TokenId;
						SaveSeculosGeneral();
						System.Diagnostics.Debug.WriteLine("UPDATE TOKEN: " + userResult.Return + " Message: " + userResult.Data.Message);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("UPDATE TOKEN ERROR");
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("UPDATE TOKEN ERROR TASK NULL");
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("UPDATE TOKEN exception: " + exception.Message + " Type: " + exception.GetType());
			}
		}

		public async void getCalendarWait(string matricula, Action<string> callback)
		{
			try
			{
                var cdTurma = StudentSelected?.Turma;
				var user = SeculosGeneral.UserConnected;
				var userCode = user.CdMatricula == null ? user.CdUsuario : user.CdMatricula;
				Task<ObservableCollection<CalendarAcademic>> taskCalendar = new WebService().GetGenericResult<ObservableCollection<CalendarAcademic>>(WebService.CALENDARIO_ESCOLAR + (cdTurma != null ? "?turma=" + cdTurma : ""));

				if (taskCalendar != null)
				{
					ObservableCollection<CalendarAcademic> calendarResutl = await taskCalendar;
					if (calendarResutl != null)
					{
						if (isResp(user))
						{
							var userSearched = searchUserByCode(user.CdUsuario);

							if (userSearched != null)
							{
								foreach (var dep in user.listDependente)
								{
									dep.listCalendarAcademic = calendarResutl;
								}
							}
						}
						else
						{
							SeculosGeneral.UserStudentConnected.listCalendarAcademic = calendarResutl;
						}

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						callback(EnumCallback.DATA_CALENDARIO_ACADEMICO_ERROR);
					}
				}
				else
				{
					callback(EnumCallback.DATA_CALENDARIO_ACADEMICO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getCalendarWait exception: " + exception.Message + " Type: " + exception.GetType());

				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DATA_CALENDARIO_ACADEMICO_ERROR);
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getAgendaEventosWait(Action<string> callback)
		{
			try
			{
				var user = SeculosGeneral.UserConnected;
                string json = JsonConvert.SerializeObject(user, Formatting.Indented);
				var responsavel = user.CdMatricula == null ? user.CdUsuario : user.CdMatricula;
				Task<ObservableCollection<CalendarAcademic>> taskAgendaEventos = new WebService().GetGenericResult<ObservableCollection<CalendarAcademic>>(WebService.CALENDARIO_EVENTO + "?responsavel=" + responsavel);

				if (taskAgendaEventos != null)
				{
					ObservableCollection<CalendarAcademic> calendarResutl = await taskAgendaEventos;
					if (calendarResutl != null)
					{
						if (isResp(user))
						{
							var userSearched = searchUserByCode(user.CdUsuario);

							if (userSearched != null)
							{
								foreach (var dep in user.listDependente)
								{
									dep.listAgendaEventos = calendarResutl;
								}
							}
						}
						else
						{
							SeculosGeneral.UserStudentConnected.listAgendaEventos = calendarResutl;
						}

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						callback(EnumCallback.DATA_AGENDA_EVENTOS_ERROR);
					}
				}
				else
				{
					callback(EnumCallback.DATA_AGENDA_EVENTOS_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getAgendaEventosWait exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DATA_AGENDA_EVENTOS_ERROR);
				}
                else if (exception.GetType().ToString().Equals("System.Net.WebException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		}

		public async void getRegistroDiarioWait(Dependente user, string date, Action<string> callback)
		{
			try
			{
				var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
				Task<ObservableCollection<RegistroDiario>> taskRegistro = new WebService().GetGenericResult<ObservableCollection<RegistroDiario>>(WebService.REGISTRO_DIARIO + "?matricula=" + userCode + "&data=" + date);

				if (taskRegistro != null)
				{
					ObservableCollection<RegistroDiario> registroResutl = await taskRegistro;
					if (registroResutl != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;

						}

						if (dependentePersist.mapRegistroDiario == null)
						{
							dependentePersist.mapRegistroDiario = new Dictionary<string, ObservableCollection<RegistroDiario>>();
						}

						if (dependentePersist.listRegistroDiarioByDate<RegistroDiario>(date) != null)
						{
							dependentePersist.mapRegistroDiario.Remove(date);
						}

						dependentePersist.mapRegistroDiario.Add(date, new ObservableCollection<RegistroDiario>(registroResutl));

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						callback(EnumCallback.DATA_REGISTRO_DIARIO_ERROR);
					}
				}
				else
				{
					callback(EnumCallback.DATA_REGISTRO_DIARIO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getRegistroDiarioWait exception: " + exception.Message + " Type: " + exception.GetType());

				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DATA_REGISTRO_DIARIO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}
		
		}

		public async void getConsumoWait(Dependente user, string date, Action<string> callback)
		{
			var userCode = user.CdAluno == null ? user.CdMatricula : user.CdAluno;
			try
			{

				Task<ObservableCollection<Consumo>> taskConsumo = new WebService().GetGenericResult<ObservableCollection<Consumo>>(WebService.EXTRATO_CONSUMO + "?matricula=" + userCode + "&data=" + date);

				if (taskConsumo != null)
				{
					ObservableCollection<Consumo> consumoResult = await taskConsumo;
					if (consumoResult != null)
					{
						Dependente dependentePersist = null;
						if (isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
						{
							foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
							{
								if (dependente.CdAluno.Equals(user.CdAluno))
								{
									dependentePersist = dependente;
									break;
								}
							}
						}
						else
						{
							dependentePersist = user;
						}

						dependentePersist.listConsumo = new List<Consumo>(consumoResult.OrderBy(o => o.CdVenda));

						SaveSeculosGeneral();
						callback(EnumCallback.SUCCESS);
					}
					else
					{
						callback(EnumCallback.DATA_CONSUMO_ERROR);
					}
				}
				else
				{
					callback(EnumCallback.DATA_CONSUMO_ERROR);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO getConsumoWait exception: " + exception.Message + " Type: " + exception.GetType());

				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException"))
				{
					if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException"))
					{
						try
						{
							Task<GenericModel> taskConsumoError = new WebService().GetGenericResult<GenericModel>(WebService.EXTRATO_CONSUMO + "?matricula=" + userCode + "&data=" + date);

							if (taskConsumoError != null)
							{
								GenericModel consumoResult = await taskConsumoError;
								if (consumoResult != null)
								{
									if (consumoResult.Error != null && !consumoResult.Error.Equals(string.Empty))
									{
										callback(EnumCallback.DATA_CONSUMO_EMPTY);
									}
									else
									{
										// Message.Msg(Application.Current.Resources["server_error"] as String);
										callback(EnumCallback.DATA_CONSUMO_ERROR);
									}
								}
							}
						}
						catch (System.Exception exceptionInner)
						{
							System.Diagnostics.Debug.WriteLine("BO getConsumoWait exceptionInner: " + exceptionInner.Message + " Type: " + exceptionInner.GetType());
							if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   				|| exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
							{
								// Message.Msg(Application.Current.Resources["server_error"] as String);
								callback(EnumCallback.DATA_CONSUMO_ERROR);
							}
							else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
							{
								callback(EnumCallback.CONNECTION_ERROR);
							}
							else
							{
								callback(EnumCallback.DATA_CONSUMO_EMPTY);
							}
						}
					}
					else
					{
						// Message.Msg(Application.Current.Resources["server_error"] as String);
						callback(EnumCallback.DATA_CONSUMO_ERROR);
					}
				}
				else if (exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(EnumCallback.DATA_CONSUMO_ERROR);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(EnumCallback.CONNECTION_ERROR);
				}
				else
				{
					callback(EnumCallback.SUCCESS);
				}
			}

		}

		public async Task ConfirmEvent(Notificacao notification, string action, Action<bool, string> callback)
		{
			try
			{
				Task<Response> taskResponse = new WebService().GetGenericResult<Response>(WebService.CONFIRMACAO_EVENTO + "?responsavel="+SeculosGeneral.UserConnected.CdUsuario
				                                                                          +"&evento="+notification.CdEvento+"&matricula="+notification.Dependente.CdAluno
				                                                                          +"&confirmacao="+action);

				if (taskResponse != null)
				{
					Response responseResult = await taskResponse;
					if (responseResult != null)
					{
						notification.FlLido = "S";
						notification.FlConfirmado = action;

						foreach (Notificacao notificationTemp in SeculosGeneral.UserConnected.listNotifications)
						{
							if (notification.CdAlert.Equals(notificationTemp.CdAlert))
							{
								notificationTemp.FlLido = "S";
								notificationTemp.FlConfirmado = action;
							}
						}

						User userCon = searchUserByCode(SeculosGeneral.UserConnected.CdUsuario);

						foreach (Notificacao notificationUserList in userCon.listNotifications)
						{
							if (notification.CdAlert.Equals(notificationUserList.CdAlert))
							{
								notificationUserList.FlLido = "S";
								notificationUserList.FlConfirmado = action;
							}
						}


						SaveSeculosGeneral();
						callback(true, responseResult.DsMensagem);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("responseResult null: ");
						callback(false, Application.Current.Resources["server_error"] as String);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskResponse null: ");
					callback(false, Application.Current.Resources["server_error"] as String);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO ConfirmEvent exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(false, Application.Current.Resources["server_error"] as String);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(false, Application.Current.Resources[EnumCallback.CONNECTION_ERROR] as String);
				}
				else
				{
					callback(true, Application.Current.Resources["event_updated"] as String);
				}
			}
		}

		public async Task SetPagamentoCredito(Dependente dependente, string eventoId, string boletoId, Action<bool, string> callback)
		{
			try
			{
				Task<Response> taskResponse = new WebService().GetGenericResult<Response>(WebService.PAGAMENTO_CREDITO + "?responsavel=" + SeculosGeneral.UserConnected.CdUsuario
				                                                                          + "&evento=" + eventoId + "&matricula=" + dependente.CdAluno
																						  + "&boleto=" + boletoId);
				if (taskResponse != null)
				{
					Response responseResult = await taskResponse;
					if (responseResult != null)
					{
						if (responseResult.DsMensagem.Equals(SeculosBO.EMPTY))
						{
							callback(false, Application.Current.Resources["server_error"] as String);
						}
						else 
						{
							if (!eventoId.Equals(""))
							{
								var notification = ComunicationSelected;
								notification.FlLido = "S";
								notification.FlConfirmado = "S";


								foreach (Notificacao notificationTemp in SeculosGeneral.UserConnected.listNotifications)
								{
									if (notification.CdAlert.Equals(notificationTemp.CdAlert))
									{
										notificationTemp.FlLido = "S";
										notificationTemp.FlConfirmado = "S";
									}
								}

								User userCon = searchUserByCode(SeculosGeneral.UserConnected.CdUsuario);

								foreach (Notificacao notificationUserList in userCon.listNotifications)
								{
									if (notification.CdAlert.Equals(notificationUserList.CdAlert))
									{
										notificationUserList.FlLido = "S";
										notificationUserList.FlConfirmado = "S";
									}
								}
							}

							SaveSeculosGeneral();
							callback(true, responseResult.DsMensagem);
						}
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("responseResult null: ");
						callback(false, Application.Current.Resources["server_error"] as String);
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("taskResponse null: ");
					callback(false, Application.Current.Resources["server_error"] as String);
				}
			}
			catch (System.Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO SetPagamentoCredito exception: " + exception.Message + " Type: " + exception.GetType());
				if (exception.GetType().ToString().Equals("Newtonsoft.Json.JsonReaderException") || exception.GetType().ToString().Equals("Newtonsoft.Json.JsonSerializationException")
				   || exception.GetType().ToString().Equals("System.Net.Http.HttpRequestException"))
				{
					// Message.Msg(Application.Current.Resources["server_error"] as String);
					callback(false, Application.Current.Resources["server_error"] as String);
				}
				else if (!exception.GetType().ToString().Equals("System.NullReferenceException"))
				{
					callback(false, Application.Current.Resources[EnumCallback.CONNECTION_ERROR] as String);
				}
				else
				{
					callback(true, Application.Current.Resources["event_updated"] as String);
				}
			}
		}


		// Método responsável por abrir os Modais do App
		public void OpenModal(string typeModal, bool individual, AbstractContentPage Page)
		{
			if (Page != null && Page.relContentModalExtends != null && Page.abstractContentPageExtends != null)
			{
				Page.relContentModalExtends.IsVisible = true;
				AbstractModal modal = null;
				if (typeModal.Equals(EnumModal.BUY_CREDIT))
				{
					modal = new BuyCreditModal(Page, individual);
				}
				else if (typeModal.Equals(EnumModal.COMUNICATION))
				{
					modal = new ComunicationModal(Page);
				}
				else if (typeModal.Equals(EnumModal.CONTACT))
				{
					modal = new ContactModal(Page);
				}
				else if (typeModal.Equals(EnumModal.CREDIT_LIMIT))
				{
					modal = new CreditLimitModal(Page, individual);
				}
				else if (typeModal.Equals(EnumModal.PAY_CREDIT))
				{
					modal = new PayCreditModal(Page);
				}
				if (modal != null)
				{
					Page.relContentModalExtends.Children.Add(modal as RelativeLayout, 0, 1, 0, 1);
					modal.showModal();
				}
				else
				{
					IMessage.Msg("Não implementado");
				}
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("Grid para inflar layout está null");
				IMessage.Msg("Não foi possível exibir.");
			}
		}


		public void OpenModalCalendar(CalendarAcademic calendar,  AbstractContentPage Page, string dateString)
		{
			if (Page != null && Page.relContentModalExtends != null && Page.abstractContentPageExtends != null)
			{
                Page.relContentModalExtends.IsVisible = true;
                AbstractModal modal = new EventCalendarModal(Page, calendar, dateString);

                if (modal != null)
                {
                    Page.relContentModalExtends.Children.Add(modal as RelativeLayout, 0, 1, 0, 1);
                    modal.showModal();
                }
                else
                {
                    IMessage.Msg("Não implementado");
                }
            }
			else
			{
				System.Diagnostics.Debug.WriteLine("Grid para inflar layout está null");
				IMessage.Msg("Não foi possível exibir.");
			}
		}


		public string CreateEventPayment(Notificacao notificacaoEvent)
		{
			EventPayment eventPayment = new EventPayment();
			eventPayment.cd_responsavel = SeculosGeneral.UserConnected.CdUsuario;
			eventPayment.cd_matricula = notificacaoEvent.Dependente.CdMatricula != null ? notificacaoEvent.Dependente.CdMatricula: notificacaoEvent.Dependente.CdAluno;
			eventPayment.ds_alerta = Utils.HtmlStrip(notificacaoEvent.DescricaoAlerta);
			eventPayment.lk_foto = notificacaoEvent.Dependente.lkFoto;
			eventPayment.nm_aluno = notificacaoEvent.Dependente.NomeAluno;
			eventPayment.nm_curso = notificacaoEvent.Dependente.NomeCurso;
			eventPayment.color_serie = notificacaoEvent.Dependente.ColorSerie;
			eventPayment.nm_alerta = notificacaoEvent.NomeAlerta;
			eventPayment.dt_evento = notificacaoEvent.DtEvento;
			eventPayment.ds_horario = notificacaoEvent.DsHorario;
			eventPayment.valor_moeda = notificacaoEvent.ValorEventoMoeda;
			eventPayment.cd_evento = notificacaoEvent.CdEvento;

			string json = JsonConvert.SerializeObject(eventPayment);
			System.Diagnostics.Debug.WriteLine("json: " + json);
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
		}

		public string CreateBilletPayment(Boleto boleto)
		{
			BilletPayment billetPayment = new BilletPayment();
			billetPayment.cd_responsavel = SeculosGeneral.UserConnected.CdUsuario;
			billetPayment.cd_matricula = boleto.CdAluno;
			billetPayment.cd_boleto = boleto.CdBoleto;
			billetPayment.nm_produto = boleto.NomeProduto;
			billetPayment.dt_vencimento = boleto.DataVencimento;
			billetPayment.vl_boleto = boleto.ValorBoletoMoeda;
			if (SeculosGeneral.UserConnected.listDependente != null)
			{
				foreach (Dependente dependente in SeculosGeneral.UserConnected.listDependente)
				{
					if (dependente.CdMatricula != null && dependente.CdMatricula.Equals(boleto.CdAluno)
					   || dependente.CdAluno != null && dependente.CdAluno.Equals(boleto.CdAluno))
					{
						billetPayment.nm_aluno = dependente.NomeAluno;
						billetPayment.lk_foto = dependente.lkFoto;
						billetPayment.nm_curso = dependente.NomeCurso;
						billetPayment.color_serie = dependente.ColorSerie;
						break;
					}
				}
			}

			string json = JsonConvert.SerializeObject(billetPayment);
			System.Diagnostics.Debug.WriteLine("json: " + json);
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
		}
	}
}