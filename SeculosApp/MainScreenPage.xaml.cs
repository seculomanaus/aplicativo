﻿using Xamarin.Forms;
using SeculosApp.View;

namespace SeculosApp
{

	public partial class MainScreenPage : AbstractContentPage
	{

		public PanelLayout PanelLayout;
		public PanelLayoutView MainScreenView;
		public TabView TabView;
		public FinanceView FinanceView { get; set; }
		public NewsView NewsView { get; set; }

		StudentView studentView;

		public Label LabelError
		{
			get
			{
				return lblErrorMain;
			}
		}

		public Label LabelUpdate
		{
			get
			{
				return lblErrorMainUpdate;
			}
		}

		public Image ImageCloud
		{
			get
			{
				return imgCloudMain;
			}
		}

		public Xamarin.Forms.View ViewError
		{
			get
			{
				return stkErrorMain;
			}
		}

		public MainScreenPage()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;

			MainScreenView = new PanelLayoutView(configPanel, panelBack, btnConfig, btnNoti, btnChat,
							  btnContact, btnAddCard, btnExit, Navigation, this);

			FinanceView = new FinanceView();
			studentView = new StudentView();
			NewsView = new NewsView();
			carouselMainScreen.Children.Add(FinanceView, 0, 1, 0, 1);
			carouselMainScreen.Children.Add(studentView, 0, 1, 0, 1);
			carouselMainScreen.Children.Add(NewsView, 0, 1, 0, 1);

			TabView = new TabView(carouselMainScreen, imgTab, App.ScreenWidth, tabHome, tabStudents, tabFinance);

			System.Diagnostics.Debug.WriteLine("User connected: " + SeculosBO.Instance.SeculosGeneral.UserConnected.Nome);

			var tapLoading = new TapGestureRecognizer();
			tapLoading.Tapped += (s, e) =>
			{
				// do nothing
				System.Diagnostics.Debug.WriteLine("viewLoading: ");
			};
			viewLoading.GestureRecognizers.Add(tapLoading);

			var tapCloseError = new TapGestureRecognizer();
			tapCloseError.Tapped += async (s, e) =>
			{
				await Utils.FadeView(btnCloseError);
				await stkErrorMain.TranslateTo(0, stkErrorMain.Height, 80);
				stkErrorMain.IsVisible = false;
				await stkErrorMain.TranslateTo(0, 0, 0);
				System.Diagnostics.Debug.WriteLine("tapCloseError btnCloseError: ");
			};
			btnCloseError.GestureRecognizers.Add(tapCloseError);

			MessagingCenter.Subscribe<App>(this, MessagingConstants.BASE_PHOTO_CHANGED, (obj) => 
			{
				try
				{
					if (SeculosBO.Instance.basePhotoChanged)
					{
						SeculosBO.Instance.basePhotoChanged = false;
						studentView.ViewModel.LoadStudentsCommandOffLine.Execute(null);
					}
				}
				catch { }
			});

			MessagingCenter.Subscribe<App>(this, MessagingConstants.BOLETO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					FinanceView.ViewModel.LoadBilletsCommandOffLine.Execute(null);
				}
				catch { }
			});

			MessagingCenter.Subscribe<App>(this, MessagingConstants.SALDO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					studentView.ViewModel.LoadStudentsCommandOffLine.Execute(null);
				}
				catch { }
			});

			MessagingCenter.Subscribe<App>(this, MessagingConstants.ALERTA_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					NewsView.ViewModel.LoadNewsCommand.Execute(null);
				}
				catch { }
			});
		}

		protected override bool OnBackButtonPressed()
		{
			var superBack = base.OnBackButtonPressed();

			if (MainScreenView.PanelLayout.isOpen())
			{
				if (!MainScreenView.PanelLayout.isProgress())
				{
					MainScreenView.PanelLayout.tooglePanel();
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Panel Layout Animation in progress...");
				}
				return true;
			}
			else
			{
				return superBack;
			}
		}

		protected override void OnAppearing()
		{
			relContentModalExtends = relContentModal;
			RelContentModal = relContentModal;

			if (FinanceView != null)
			{
				SeculosBO.Instance.studentSelectedPicker = FinanceView.LblStudentSelected.Text;
				SeculosBO.Instance.monthSelectedPicker = FinanceView.LblMonthSelected.Text;
			}

			if (App.ScreenWidth <= 340)
			{
				//ComunicationsHistoryLabel.FontSize = 14.5;
				//DayleConsumptionLabel.FontSize = 14.5;
				ContactLabel.FontSize = 14.5;
				AddCardLabel.FontSize = 14.5;
			}

			base.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			//MessagingCenter.Unsubscribe<App>(this, MessagingConstants.BOLETO_HAS_BEEN_UPDATED);
			//MessagingCenter.Unsubscribe<App>(this, MessagingConstants.SALDO_HAS_BEEN_UPDATED);
			//MessagingCenter.Unsubscribe<App>(this, MessagingConstants.ALERTA_HAS_BEEN_UPDATED);
		}
	}
}
