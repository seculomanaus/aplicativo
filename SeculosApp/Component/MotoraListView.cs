﻿using System;
using Xamarin.Forms;

namespace SeculosApp
{
	public class MotoraListView : ListView
	{
		public MotoraListView()
			:base(Device.OS.Equals(TargetPlatform.iOS) ? ListViewCachingStrategy.RetainElement : ListViewCachingStrategy.RecycleElement)
		{
			
		}
	}
}
