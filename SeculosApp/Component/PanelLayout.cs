﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public class PanelLayout
	{
		public Xamarin.Forms.View LayoutPanel;
		public uint upDuration { get; set; }
		public uint downDuration { get; set; }
		public double minFadePanel { get; set; }
		public Xamarin.Forms.View ViewBack { get; set; }
		private bool progress;

		public PanelLayout(Xamarin.Forms.View LayoutPanel, Xamarin.Forms.View vBack)
		{
			this.LayoutPanel = LayoutPanel;
			this.ViewBack = vBack;
			upDuration = 150;
			downDuration = 100;
			minFadePanel = 0.3;

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				if (isOpen()) 
				{
					tooglePanel();
				}
			};

			this.ViewBack.GestureRecognizers.Add(tapGestureRecognizer);
			ViewBack.FadeTo(0, 0);
		}

		public async void tooglePanel() 
		{
			if (!progress)
			{
				progress = true;
				if (LayoutPanel.TranslationY < 0)
				{
					await Task.WhenAll(
						ViewBack.FadeTo(0, downDuration),
						LayoutPanel.FadeTo(minFadePanel, downDuration),
						LayoutPanel.TranslateTo(0, LayoutPanel.Height, downDuration)
					);

					await WaitAndExecute(46, () => { 
						ViewBack.IsVisible = false;
						progress = false;
					});
				}
				else
				{
					ViewBack.IsVisible = true;
					await Task.WhenAll(
						ViewBack.FadeTo(minFadePanel, downDuration),
						LayoutPanel.FadeTo(1, upDuration),
						LayoutPanel.TranslateTo(0, -LayoutPanel.Height, upDuration)
					);

					await WaitAndExecute(41, () =>
					{
						progress = false;
					});
				}
			}
		}

		private async Task WaitAndExecute(int milisec, Action actionToExecute)
		{
			await Task.Delay(milisec);
			actionToExecute();
		}

		public bool isOpen() {
			if (LayoutPanel.TranslationY < 0)
			{
				return true;
			}
			else 
			{ 
				return false;
			}
		}

		public bool isProgress()
		{
			return progress;
		}
	}
}
