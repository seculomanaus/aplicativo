﻿using System;
using System.Threading.Tasks;
using Plugin.FirebasePushNotification;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class LoginPage : ContentPage
	{
		public static LoginPage _LoginPage;

		public LoginPage()
		{
			InitializeComponent();

			_LoginPage = this;

			NavigationPage.SetHasNavigationBar(this, false);

			// "Next" buttons. It seems next buttons aren't implemented yet in Forms
			login.Completed += async (s, e) =>
			{
				await Task.Delay(80);
				await Task.Factory.StartNew(() =>
				{
					password.Focus();
				});
			};

			if (Device.OS.Equals(TargetPlatform.iOS) && App.ScreenWidth <= 340)
			{
				welcome.FontSize = 14.5;
				welcomeTo.FontSize = 14.5;
			}

          

			btnLogin.Clicked += async (sender, e) =>
			{
				if (!App.GlobalBlockClick)
				{
					App.GlobalBlockClick = true;
					System.Diagnostics.Debug.WriteLine("Click no Login");
					//login_empty
					if (login.Text == null || login.Text.Trim().Equals(""))
					{
						System.Diagnostics.Debug.WriteLine("Login empty!");
						SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.LOGIN_EMPTY] as String);
					}
					else if (password.Text == null || password.Text.Trim().Equals(""))
					{
						System.Diagnostics.Debug.WriteLine("Password empty!");
						SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.PASSWORD_EMPTY] as String);
					}
					else
					{
						viewLoading.IsVisible = true;
						load.IsVisible = true;
						await SeculosBO.Instance.login(login.Text, password.Text, cbKeepConnected.Checked, async (msg) =>
						  {
							  if (msg == null || msg.Equals(EnumCallback.SUCCESS))
							  {
								  try
								  {
									  if (SeculosBO.Instance.SeculosGeneral.UserConnected.DcTipo.Equals("responsavel"))
									  {

										  System.Diagnostics.Debug.WriteLine("SUBSCRIBE IN TOPIC ALL");

										  CrossFirebasePushNotification.Current.Subscribe("all");

										  System.Diagnostics.Debug.WriteLine("SUBSCRIBE IN TOPIC :" + SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario);

										  CrossFirebasePushNotification.Current.Subscribe(SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario+"");
                                         
                                         await Navigation.PushAsync(new MainScreenPage());
									  }
									  else if (SeculosBO.Instance.SeculosGeneral.UserConnected.DcTipo.Equals("aluno"))
									  {
										  await Navigation.PushAsync(new AlunoPage());
									  }
									  try
									  {
										  Navigation.RemovePage(_LoginPage);
									  }
									  catch (Exception exception)
									  {
										  System.Diagnostics.Debug.WriteLine("LoginPage RemovePage login exception: " + exception.Message + " Type: " + exception.GetType());
									  }
								  }
								  catch (System.Exception exception)
								  {
									  SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.DATA_ERROR] as String);
									  System.Diagnostics.Debug.WriteLine("LoginPage login exception: " + exception.Message + " Type: " + exception.GetType());
								  }
							  }
							  //else if (msg != null && msg.Equals(WebService.ERROR_NO_SERVER))
							  //{ 
							  // System.Diagnostics.Debug.WriteLine("Login error in server!");
							  //    SeculosBO.Instance.IMessage.Msg(WebService.ERROR_NO_SERVER);
							  //}
							  else if (!msg.Equals("server_error"))
							  {
								  System.Diagnostics.Debug.WriteLine("Login or password invalid!");
								  SeculosBO.Instance.IMessage.Msg(Application.Current.Resources["login_password_error"] as String);
							  }
                              else
                              {
								  System.Diagnostics.Debug.WriteLine("Login error!");
								  SeculosBO.Instance.IMessage.Msg("Não foi possível realizar o login.");
							  }

							  Device.BeginInvokeOnMainThread(() =>
							  {
								  viewLoading.IsVisible = false;
								  load.IsVisible = false;
							  });
						  });
					}

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((o) =>
						{
							App.GlobalBlockClick = false;
						});
					});
				}
			};

			var tapRelKeepConnected = new TapGestureRecognizer();
			tapRelKeepConnected.NumberOfTapsRequired = 1;
			tapRelKeepConnected.Tapped += (s, e) =>
			{
				cbKeepConnected.Checked = !cbKeepConnected.Checked;
				System.Diagnostics.Debug.WriteLine("Click tapRelKeepConnected keep connected: " + cbKeepConnected.Checked);
			};
			relKeepConnected.GestureRecognizers.Add(tapRelKeepConnected);

			if (Device.OS == TargetPlatform.iOS)
			{
				var tapCheck = new TapGestureRecognizer();
				tapCheck.NumberOfTapsRequired = 1;
				tapCheck.Tapped += (s, e) =>
				{
					cbKeepConnected.Checked = !cbKeepConnected.Checked;
					System.Diagnostics.Debug.WriteLine("Click tapCheck keep connected: " + cbKeepConnected.Checked);
				};
				cbKeepConnected.GestureRecognizers.Add(tapCheck);
			}

			var moreInfoTap = new TapGestureRecognizer();
			moreInfoTap.Tapped += async (s, e) =>
			{
				if (!App.GlobalBlockClick)
				{
					App.GlobalBlockClick = true;
					await Utils.FadeView(moreInfo);
					await Navigation.PushAsync(new MoreInfoView());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(250).ContinueWith((o) =>
						{
							App.GlobalBlockClick = false;
						});
					});
				}
			};
			moreInfo.GestureRecognizers.Add(moreInfoTap);

			var termsTap = new TapGestureRecognizer();
			termsTap.Tapped += async (s, e) =>
			{
				if (!App.GlobalBlockClick)
				{
					App.GlobalBlockClick = true;
					await Utils.FadeView(lbTerms);
					await Navigation.PushAsync(new TermsAndPoliticView());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(250).ContinueWith((o) =>
						{
							App.GlobalBlockClick = false;
						});
					});
				}
			};
			lbTerms.GestureRecognizers.Add(termsTap);
		}
	}
}
