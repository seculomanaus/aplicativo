﻿using System;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ContentTestsDetail : AbstractContentPage
	{

		Image btClose;
		Command cmdClick;
		ConteudoDisciplina ConteudoDisciplina;
		string Disciplina;
		Grid GridModal;

		public ContentTestsDetail(string Disciplina, ConteudoDisciplina conteudoDisciplina)
		{
			this.Disciplina = Disciplina;
			this.ConteudoDisciplina = conteudoDisciplina;

			cmdClick = new Command((obj) => onClick(obj));

			var layout = new RelativeLayout();

			layout.BackgroundColor = Color.FromHex("#143f58");

			var backgroundImg = new CachedImage
			{
				Source = "aluno_bg.png",
				Aspect = Aspect.Fill,
				DownsampleToViewSize = true,
				TransparencyEnabled = false
			};

			layout.Children.Add(
				backgroundImg,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);


			var headerLayout = CreateHeaderLayout();
			layout.Children.Add(
				headerLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .1; })
			);

			layout.Children.Add(
				CreateTabAndContent(),
				Constraint.Constant(0),
				Constraint.RelativeToView(headerLayout, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .9; })
			);

			GridModal = new Grid
			{
				IsVisible = false
			};

			RelContentModal = GridModal;
			relContentModalExtends = RelContentModal;
			abstractContentPageExtends = this;

			layout.Children.Add(
				GridModal,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			Content = layout;
		}

		RelativeLayout CreateTabAndContent()
		{
			var layout = new RelativeLayout
			{
				Margin = new Thickness(10, 0, 10, 10),
				BackgroundColor = Color.White
			};

			var tabsLayout = CreateTabsLayout();

			layout.Children.Add(
				tabsLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .1; })
			);

			var descContent = new CustomLabel
			{
				Text = ConteudoDisciplina.Conteudo,
				FontSize = 15,
				FontType = "Medium",
				TextColor = Color.Black,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				Margin = new Thickness(20, 25, 20, 0)
			};

			layout.Children.Add(
				descContent,
				Constraint.Constant(0),
				Constraint.RelativeToView(tabsLayout, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .9; })
			);

			return layout;
		}

		RelativeLayout CreateTabsLayout()
		{
			var layout = new RelativeLayout
			{
				BackgroundColor = Color.FromHex("#FFAB00")
			};

			var tabTitle = new CustomLabel
			{
				Text = Disciplina,
				FontSize = 18,
				FontType = "Medium",
				TextColor = Color.Black,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				LineBreakMode = LineBreakMode.TailTruncation
			};

			layout.Children.Add(
				tabTitle,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return layout;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(btClose.ClassId))
				{
					await Utils.FadeView(btClose);
					await Navigation.PopAsync();
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(250).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		RelativeLayout CreateHeaderLayout()
		{
			var layout = new RelativeLayout();

			btClose = new Image
			{
				ClassId = "btClose",
				Source = "back_ic.png",
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			var btCloseLayout = new StackLayout
			{
				WidthRequest = 30,
				Children = { btClose }
			};
			btCloseLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btClose.ClassId });

			var titleLabel = new CustomLabel
			{
				Text = SeculosBO.Instance?.DependenteConnected?.NomeAluno,
				FontType = "Medium",
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = 19,
				Margin = new Thickness(45, 0, 12, 0),
				LineBreakMode = LineBreakMode.TailTruncation
			};

			layout.Children.Add(
				btCloseLayout,
				Constraint.Constant(10),
				Constraint.Constant(0),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			layout.Children.Add(
				titleLabel,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return layout;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RelContentModal = GridModal;
			relContentModalExtends = GridModal;
		}
	}
}
