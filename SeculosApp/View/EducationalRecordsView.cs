﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace SeculosApp
{
	public partial class EducationalRecordsView : AbstractContentPage
	{
		CalendarView _calendarView;

		public EducationalRecordsView()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = RelContentModal;
			abstractContentPageExtends = this;
			lblStudentName.Text = SeculosBO.Instance.DependenteConnected.NomeAluno;
			lblCursoName.Text = SeculosBO.Instance.DependenteConnected.NomeCurso;
			imgStudentPhoto.Source = SeculosBO.Instance.DependenteConnected.Foto;

			var back = new TapGestureRecognizer();
			var isBack = false;
			var calendar = false;
			back.Tapped += async (s, e) =>
			{
				if (!isBack && !calendar)
				{
					isBack = true;
					System.Diagnostics.Debug.WriteLine("back");
					await Utils.FadeView(imgBackStudent);

					try
					{
						await Navigation.PopAsync();
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("EducationalRecordsView back exception: " + exception.Message + " Type: " + exception.GetType());
					}
				}
				System.Diagnostics.Debug.WriteLine("calendar: " + calendar + " isBack: " + isBack);
			};
			imgBackStudent.GestureRecognizers.Add(back);

			var hasErrorVisible = false;
			var hasErrorConnectionVisible = false;
			var calendarTap = new TapGestureRecognizer();
			calendarTap.Tapped += async (s, e) =>
			{
				if (!isBack && !calendar && !load.IsVisible)
				{
					calendar = true;
					System.Diagnostics.Debug.WriteLine("calendarTap");

					await Utils.FadeView(btnCalendar);

					if (_calendarView == null)
					{
						createCalendar();
					}

					stackCalendar.IsVisible = !stackCalendar.IsVisible;

					stackContentItens.IsVisible = !stackCalendar.IsVisible;

					if (hasErrorVisible)
					{
						lblErrorOccurrence.IsVisible = true;
						hasErrorVisible = false;
					}

					if (stackCalendar.IsVisible && lblErrorOccurrence.IsVisible && !hasErrorVisible)
					{
						lblErrorOccurrence.IsVisible = false;
						hasErrorVisible = true;
					}

					if (hasErrorConnectionVisible)
					{
						stkError.IsVisible = true;
						hasErrorConnectionVisible = false;
					}

					if (stackCalendar.IsVisible && stkError.IsVisible && !hasErrorConnectionVisible)
					{
						stkError.IsVisible = false;
						hasErrorConnectionVisible = true;
					}

					Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((obj) =>
						{
							calendar = false;
						});
					});
				}
				System.Diagnostics.Debug.WriteLine("calendar: " + calendar + " isBack: " + isBack);
			};
			btnCalendar.GestureRecognizers.Add(calendarTap);

			var connectionTap = new TapGestureRecognizer();
			connectionTap.Tapped += async (s, e) =>
			{
				if (!App.GlobalBlockClick && !calendar && !load.IsVisible)
				{
					App.GlobalBlockClick = true;
					System.Diagnostics.Debug.WriteLine("connectionTap");

					await Utils.FadeView(stkError);

					initLoad();
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((obj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
                                changeDate(DateTime.Now.ToString("dd/MM/yyyy"));
							});
						});
					});

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(150).ContinueWith((obj) =>
						{
							App.GlobalBlockClick = false;
						});
					});
				}
			};
			stkError.GestureRecognizers.Add(connectionTap);

			lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[DateTime.Now.Month]);
			lblDay.Text = DateTime.Now.Day.ToString();

			//"02/08/2016"
			initLoad();

			Task.Factory.StartNew(() =>
			{
				Task.Delay(200).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
                        changeDate(DateTime.Now.ToString("dd/MM/yyyy"));
					});
				});
			});
		}

		public StackLayout createItem(Ocorrencia item)
		{
			var lblTitle = new CustomLabel
			{
				Text = item.NmTipo,
				TextColor = Color.Black,
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15
			};

			var lblDesc = new CustomLabel
			{
				Text = item.DsResgistro,
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Book",
				FontSize = 14.5,
				Margin = new Thickness(0, -5, 0, 0)
			};
			var stack = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { lblTitle, lblDesc },
				Padding = new Thickness(0, 8, 0, 8)

			};

			return stack;
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			stackContentItens.IsVisible = false;
			lblErrorOccurrence.IsVisible = false;
		}

		public void changeDate(string date)
		{
			SeculosBO.Instance.getOccurrenceWait(SeculosBO.Instance.DependenteConnected, date, (msg) =>
			{
				var listOccor = SeculosBO.Instance.DependenteConnected.listOcorrenciaByDate<Ocorrencia>(date);
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listOccor == null || listOccor.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_OCCURRENCE_EMPTY, lblErrorOccurrence, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_OCCURRENCE_ERROR))
				{
					if (listOccor == null || listOccor.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_OCCURRENCE_ERROR, lblErrorOccurrence, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listOccor == null || listOccor.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}
				stackContentItens.Children.Clear();
				if (listOccor != null && listOccor.Count > 0)
				{
					foreach (Ocorrencia ocorrencia in listOccor)
					{
						stackContentItens.Children.Add(createItem(ocorrencia));
					}
				}
				load.IsVisible = false;
				stackContentItens.IsVisible = true;
			});
		}

		public void createCalendar()
		{
			_calendarView = new CalendarView()
			{
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				MinDate = CalendarView.FirstDayOfMonth(new DateTime(DateTime.Now.Year, 1, 1)),
				MaxDate = CalendarView.LastDayOfMonth(new DateTime(DateTime.Now.Year, 12, 1)),
				ShouldHighlightDaysOfWeekLabels = false,
				SelectionBackgroundStyle = CalendarView.BackgroundStyle.CircleFill,
				ShowNavigationArrows = true,
				NavigationArrowsColor = Color.FromHex("#3ea6e1"),
				Margin = new Thickness(10, -15, 10, 20),
				DateBackgroundColor = Color.White,
				BackgroundColor = Color.Transparent,
				DateSeparatorColor = Color.White,
				SelectedDateBackgroundColor = Color.FromHex("#3ea6e1"),
				SelectedDateForegroundColor = Color.White

			};

			if (Device.OS == TargetPlatform.iOS)
			{
				_calendarView.WidthRequest = App.ScreenWidth;
				_calendarView.Layout(new Rectangle(0, App.ScreenWidth, App.ScreenWidth, _calendarView.HeightRequest));
				if (App.ScreenWidth > 340)
				{
					_calendarView.Margin = new Thickness(42, 0, 0, 0);
				}
				else
				{
					_calendarView.Margin = new Thickness(0, -5, 0, 0);
				}
			}

			stackCalendar.Children.Add(_calendarView);

			_calendarView.DateSelected += (object sender, DateTime e) =>
			{
				initLoad();
				Task.Factory.StartNew(() =>
				{
					Task.Delay(150).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							changeDate(e.ToString("dd/MM/yyyy"));
						});
					});
				});
				lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[e.Month]);
				lblDay.Text = e.Day.ToString();
				stackCalendar.IsVisible = false;
				Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);
			};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
		}
	}
}
