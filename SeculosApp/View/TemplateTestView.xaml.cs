﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class TemplateTestView : RelativeLayout, ITabView
	{
		Command cmdClick;
		CustomLabel lblBimSelected;
		bool initializedView;

		public TemplateTestView(int position)
		{
			InitializeComponent();

			cmdClick = new Command((obj) => onClick(obj));

			lblP1.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblP1.ClassId });
			lblP2.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblP2.ClassId });
			lblMaic.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblMaic.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(roundedP1);
			Utils.fixRoudedView(roundedP2);
			Utils.fixRoudedView(roundedMaic);

#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			initLoad();
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

			if (position == 1)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getGabaritos();
						});
					});
				});
			}
		}

		public async Task initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			await fadeAllBim();
			stackTestsTypes.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public void getGabaritos()
		{
			SeculosBO.Instance.getGabaritoWS(SeculosBO.Instance.DependenteConnected, async (msg) =>
			{
				var listGabarito = SeculosBO.Instance.DependenteConnected.mapGabarito;
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listGabarito == null || listGabarito.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.TEMPLATE_ERROR))
				{
					if (listGabarito == null || listGabarito.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.TEMPLATE_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listGabarito == null || listGabarito.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listGabarito != null && listGabarito.Count > 0)
				{
					await initView();
				}
				else
				{
					load.IsVisible = false;
				}
			});
		}

		public async Task initView()
		{
			createAllitens();
			await selectTemplateType(roundedP1, lblP1, stackContentItensP1, scrollContentItensP1, listTestLayoutsP1);
			stackTestsTypes.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		List<Xamarin.Forms.View> listTestLayoutsP1;
		List<Xamarin.Forms.View> listTestLayoutsP2;
		List<Xamarin.Forms.View> listTestLayoutsMAIC;

		public void createAllitens()
		{
			listTestLayoutsP1 = new List<Xamarin.Forms.View>();
			listTestLayoutsP2 = new List<Xamarin.Forms.View>();
			listTestLayoutsMAIC = new List<Xamarin.Forms.View>();
			foreach (String key in SeculosBO.Instance.DependenteConnected.mapGabarito.Keys)
			{
				System.Diagnostics.Debug.WriteLine("createAllitens key: " + key);

				if (key != null && !key.Equals(String.Empty))
				{
					var listTemplateByType = SeculosBO.Instance.DependenteConnected.listGabaritoByType<Gabarito>(key);

					foreach (Gabarito templateByType in listTemplateByType)
					{
						StackLayout item = CreateItemLayout(templateByType);
						item.ClassId = key;
						switch (key)
						{
							case "P1":
								listTestLayoutsP1.Add(item);
								break;
							case "P2":
								listTestLayoutsP2.Add(item);
								break;
							case "MAIC":
								listTestLayoutsMAIC.Add(item);
								break;
						}
					}
				}

				stackContentItensP1.IsVisible = true;
				stackContentItensP2.IsVisible = true;
				stackContentItensMaic.IsVisible = true;
			}

			if(createNotYet(listTestLayoutsP1, "P1"))
				stackContentItensP1.HorizontalOptions = LayoutOptions.CenterAndExpand;
			
			if(createNotYet(listTestLayoutsP2, "P2"))
				stackContentItensP2.HorizontalOptions = LayoutOptions.CenterAndExpand;
			
			if(createNotYet(listTestLayoutsMAIC, "MAIC"))
				stackContentItensMaic.HorizontalOptions = LayoutOptions.CenterAndExpand;
				
		}

		public async Task selectTemplateType(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, List<Xamarin.Forms.View> listTestLayouts)
		{
			roundedP1.Color = Color.Transparent;
			lblP1.TextColor = Color.FromHex("#a3a3a3");

			roundedP2.Color = Color.Transparent;
			lblP2.TextColor = Color.FromHex("#a3a3a3");

			roundedMaic.Color = Color.Transparent;
			lblMaic.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (stackContentItens.Children.Count == 0)
			{
				foreach (Xamarin.Forms.View view in listTestLayouts)
				{
					stackContentItens.Children.Add(view);
				}
			}

			if (lblBimSelected == null || !lblBimSelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllBim();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblBimSelected = labelSel;
		}

		public async Task fadeAllBim()
		{
			await Task.WhenAll(
				stackContentItensP1.FadeTo(0, 0, null),
				stackContentItensP2.FadeTo(0, 0, null),
				stackContentItensMaic.FadeTo(0, 0, null)
			);
			scrollContentItensP1.IsVisible = false;
			scrollContentItensP2.IsVisible = false;
			scrollContentItensMaic.IsVisible = false;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(lblP1.ClassId))
				{
					await selectTemplateType(roundedP1, lblP1, stackContentItensP1, scrollContentItensP1, listTestLayoutsP1);
				}
				else if (str.Equals(lblP2.ClassId))
				{
					await selectTemplateType(roundedP2, lblP2, stackContentItensP2, scrollContentItensP2, listTestLayoutsP2);
				}
				else if (str.Equals(lblMaic.ClassId))
				{
					await selectTemplateType(roundedMaic, lblMaic, stackContentItensMaic, scrollContentItensMaic, listTestLayoutsMAIC);
				}
				else if (str.Equals(stkError.ClassId))
				{
					await initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getGabaritos();
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}
		public bool createNotYet(List<Xamarin.Forms.View> listTestLayouts, string typeTest)
		{
			if (listTestLayouts.Count == 0)
			{
				var lblErrorNotYet = new CustomLabel
				{
					Text = "As avaliações da " + typeTest + "\n ainda não iniciaram.",
					TextColor = Color.FromHex("#000000"),
					FontType = "Medium",
					FontSize = 15,
					Margin = new Thickness(30, 80, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};
				listTestLayouts.Add(lblErrorNotYet);
				return true;
			}

			return false;
		}

		public void createEmptyitem(StackLayout stackContentItens)
		{
			if (stackContentItens.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 60, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				stackContentItens.HorizontalOptions = LayoutOptions.CenterAndExpand;
			}
		}

		public StackLayout CreateItemLayout(Gabarito gabarito)
		{
			var itemLabel = new CustomLabel
			{
				Text = Utils.PascalCase(gabarito.DcDisciplina),
				TextColor = Color.Black,
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			var itemTestCode = new CustomLabel
			{
				Text = gabarito.NrProva,
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 13.5,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				VerticalOptions = LayoutOptions.End,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};
			var layout = new StackLayout
			{
				Margin = new Thickness(10, 0, 10, 0),
				HeightRequest = 74,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				WidthRequest = 600
			};
			layout.Children.Add(itemLabel);
			layout.Children.Add(itemTestCode);
			layout.Children.Add(lineSeperator);

			layout.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						await Utils.FadeView(layout);
						await Navigation.PushAsync(new TemplateTestDetailView(gabarito));
						await Task.Factory.StartNew(() =>
						{
							Task.Delay(200).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}
				}),
				CommandParameter = layout
			});

			return layout;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_GRADE, ((string)App.Current.Resources["template_test"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
