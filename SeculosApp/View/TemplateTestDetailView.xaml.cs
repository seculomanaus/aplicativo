﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class TemplateTestDetailView : AbstractContentPage
	{
		Gabarito gabarito;
		int totalQuestion, i;
		List<StackLayout> templateListFirst, templateListSecond;

		public TemplateTestDetailView(Gabarito gabarito)
		{
			InitializeComponent();
			this.gabarito = gabarito;
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;
			var back = new TapGestureRecognizer();
			back.Tapped += async (s, e) =>
			{
				await Utils.FadeView(imgBack);
				try
				{
					i = 9999;
					await Navigation.PopAsync();
				}
				catch (System.Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("TemplateTestDetailView back exception: " + exception.Message + " Type: " + exception.GetType());
				}
			};
			imgBack.GestureRecognizers.Add(back);

			updateView();
		}

		void updateView()
		{
			lblTemplateName.Text = Utils.PascalCase(gabarito.DcDisciplina) + " - " + gabarito.NmProva;
			lblStudentName.Text = SeculosBO.Instance.DependenteConnected.NomeAluno;
			lblTestName.Text = gabarito.NrProva;
			lblBimesterNumber.Text = gabarito.CdBimestre + "º";
			totalQuestion = gabarito.DcGabarito.Length;
			lblQuestionNumber.Text = totalQuestion + " " + (App.Current.Resources["questions"] as String);

			double rightQuestion = 0;
			uint timeDefault = 50;
			uint totalTime = 0;
			try
			{
				rightQuestion = int.Parse(gabarito.NrAcerto);
				totalTime = (uint)(timeDefault * rightQuestion);
				if (int.Parse(gabarito.NrAcerto) > 1)
				{
					lblAmoutRight.Text = rightQuestion + " " + App.Current.Resources["tests_right"] as String;
				}
				else
				{
					lblAmoutRight.Text = gabarito.NrAcerto + " " + App.Current.Resources["test_right"] as String;
				}
			}
			catch
			{
				lblAmoutRight.Text = App.Current.Resources["not_informed"] as String;
			}

			double progress = rightQuestion / totalQuestion;
			progressRightQuestions.ProgressTo(progress, totalTime, Easing.CubicOut);

			lblEmptyNumber.Text = gabarito.NrBranco;
			lblNullNumber.Text = gabarito.NrNulo;
			lblEmptyNumber.WidthRequest = (App.ScreenWidth / 2) - stackTestInfo.Padding.Left;

			int half = 0;
			if (totalQuestion % 2 == 0)
			{
				half = totalQuestion / 2;
				templateListFirst = createLineTemplate(0, half);
				templateListSecond = createLineTemplate(half, totalQuestion);
			}
			else
			{
				var tempValue = totalQuestion - 1;
				half = tempValue / 2;
				templateListFirst = createLineTemplate(0, half);
				templateListSecond = createLineTemplate(half + 1, tempValue + 1);
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			addNotesAnswersLayout();
		}

		void addNotesAnswersLayout()
		{
			if (i == 0)
			{
				Task.Factory.StartNew(async () =>
				{
					int delay = 200;
					if (Device.OS.Equals(TargetPlatform.iOS))
					{
						delay = 100;
					}
					int countFirst = 0;
					int countSecond = 0;
					for (i = 1; i <= totalQuestion;)
					{
						await Task.Delay(delay).ContinueWith((obj) =>
					 	{
							Device.BeginInvokeOnMainThread(() =>
							{
								if (i % 2 == 0)
								{
									if (templateListFirst != null && countFirst < templateListFirst.Count)
									{
										stackTemplateListFirst.Children.Add(templateListFirst[countFirst]);
										templateListFirst[countFirst].FadeTo(1, 400);
										countFirst++;
								  	}
							  	}
								else if (i % 2 != 0)
								{
									if (templateListSecond != null && countSecond < templateListSecond.Count)
									{
										stackTemplateListSecond.Children.Add(templateListSecond[countSecond]);
										templateListSecond[countSecond].FadeTo(1, 450);
										countSecond++;
									}
								}
								i++;
							});
						});
					}
			  	});
			}
		}

		protected override bool OnBackButtonPressed()
		{
			base.OnBackButtonPressed();
			i = 9999;
			return false;
		}

		List<StackLayout> createLineTemplate(double initI, int totalQuestionTemp)
		{
			List<StackLayout> templateList = new List<StackLayout>();
			try
			{
				for (; initI < totalQuestionTemp; initI++)
				{
					var textTemplate = gabarito.DcGabarito.Replace("/n", "").Substring((int)initI, 1);
					var textAnswer = gabarito.DcResposta.Replace("/n", "").Substring((int)initI, 1);

					var stackItem = new StackLayout
					{
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Padding = new Thickness(0, 5, 0, 5),
						Opacity = 0
					};

					var itemLabel = new CustomLabel
					{
						Text = (initI + 1).ToString(),
						TextColor = Color.White,
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						FontType = "Medium",
						FontSize = 15
					};

					var imgCircle = new CachedImage
					{
						Source = "gabarito_oficial_ic.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
						DownsampleToViewSize = true,
						TransparencyEnabled = false,
						WidthRequest = 28,
						HeightRequest = 28
					};

					var relTemplate = new RelativeLayout
					{
						HorizontalOptions = LayoutOptions.CenterAndExpand
					};

					relTemplate.Children.Add(
						imgCircle,
						Constraint.Constant(0),
						Constraint.Constant(0)
					);

					var lblTemplate = new CustomLabel
					{
						Text = textTemplate,
						TextColor = Color.White,
						VerticalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
						HorizontalTextAlignment = TextAlignment.Center,
						WidthRequest = 28,
						HeightRequest = 28
					};

					relTemplate.Children.Add(
						lblTemplate,
						Constraint.Constant(0),
						Constraint.Constant(0)
					);

					var imgCircleAnswer = new CachedImage
					{
						Source = "erradas_ic.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
						DownsampleToViewSize = true,
						TransparencyEnabled = false,
						WidthRequest = 28,
						HeightRequest = 28
					};

					if (textTemplate.Equals(textAnswer))
					{
						imgCircleAnswer.Source = "corretas_ic.png";
					}

					var relAnswer = new RelativeLayout
					{
						HorizontalOptions = LayoutOptions.CenterAndExpand
					};

					relAnswer.Children.Add(
						imgCircleAnswer,
						Constraint.Constant(0),
						Constraint.Constant(0)
					);

					var lblAnswer = new CustomLabel
					{
						Text = textAnswer,
						TextColor = Color.White,
						VerticalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
						HorizontalTextAlignment = TextAlignment.Center,
						WidthRequest = 28,
						HeightRequest = 28
					};

					relAnswer.Children.Add(
						lblAnswer,
						Constraint.Constant(0),
						Constraint.Constant(0)
					);

					stackItem.Children.Add(itemLabel);
					stackItem.Children.Add(relTemplate);
					stackItem.Children.Add(relAnswer);

					templateList.Add(stackItem);
				}

			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("BO createLineTemplate exception: " + exception.Message + " Type: " + exception.GetType() + " i: " + i + " totalQuestionTemp: " + totalQuestionTemp + " gabarito.DcResposta.Length: " + gabarito.DcResposta.Length);
			}
			return templateList;
		}
	}
}
