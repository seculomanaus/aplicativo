﻿using Xamarin.Forms;
using System.Threading.Tasks;

namespace SeculosApp
{
	public class AcademicSemiView : RelativeLayout, ITabView
	{
		Command cmdClick;
        RelativeLayout agendaLayout, conteudoProgramaticoLayout, historicoEscolarLayout, acompanhamentoInfantilLayout, horarioLayout, gabaritoProvasLayout;
		bool blockClick;

		public AcademicSemiView()
		{
			ClassId = "AcademicSemiView";
			cmdClick = new Command((obj) => onClick(obj));
			BackgroundColor = Color.White;

            agendaLayout = Utils.CreateItemLayout("Calendário");
            conteudoProgramaticoLayout = Utils.CreateItemLayout("Conteúdo Programático");
            historicoEscolarLayout = Utils.CreateItemLayout("Notas e Frequência"); //historico escolar
			acompanhamentoInfantilLayout = Utils.CreateItemLayout("Acompanhamento Infantil");
			horarioLayout = Utils.CreateItemLayout("Horário de Aula");
            gabaritoProvasLayout = Utils.CreateItemLayout("Gabarito de Provas");

			agendaLayout.ClassId = "AgendaView";
            conteudoProgramaticoLayout.ClassId = "GradesView";
            historicoEscolarLayout.ClassId = "AcademicView";
			acompanhamentoInfantilLayout.ClassId = "AcompanhamentoView";
			horarioLayout.ClassId = "ClassScheduleView";
			gabaritoProvasLayout.ClassId = "GabaritoProvasView";

			agendaLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = agendaLayout.ClassId });
            conteudoProgramaticoLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = conteudoProgramaticoLayout.ClassId });
            historicoEscolarLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = historicoEscolarLayout.ClassId });
			acompanhamentoInfantilLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = acompanhamentoInfantilLayout.ClassId });
			horarioLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = horarioLayout.ClassId });
			gabaritoProvasLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = gabaritoProvasLayout.ClassId });

			var stack = new StackLayout
			{
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { agendaLayout, conteudoProgramaticoLayout, historicoEscolarLayout, acompanhamentoInfantilLayout, horarioLayout, gabaritoProvasLayout },
				Margin = new Thickness(0, 8, 0, 0)

			};

			var scroll = new ScrollView
			{
				Content = stack
			};

			//Aluno do ensino infantil
			if (!SeculosBO.Instance.DependenteConnected.IsInfantil())
			{
				acompanhamentoInfantilLayout.IsVisible = false;
				gabaritoProvasLayout.IsVisible = true;

			}

            if (SeculosBO.Instance.DependenteConnected.IsInfantil() || SeculosBO.Instance.DependenteConnected.IsFundI())
			{
				gabaritoProvasLayout.IsVisible = false;

			}


			Children.Add(
				scroll,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);
		}

		async void onClick(object obj)
		{
			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(agendaLayout.ClassId))
				{
					await Utils.FadeView(agendaLayout);
					await Navigation.PushAsync(new AgendaExpandedView(1, "CalendarAcademicView"));
				}
				else if (str.Equals(conteudoProgramaticoLayout.ClassId))
				{
					await Utils.FadeView(conteudoProgramaticoLayout);
					//await Navigation.PushAsync(new AcademicSemiExpandedView(1, "GradesView"));
					await Navigation.PushAsync(new ProgramContentWebPage());
					
				}
				else if (str.Equals(historicoEscolarLayout.ClassId))
				{
					await Utils.FadeView(historicoEscolarLayout);
					await Navigation.PushAsync(new AcademicSemiExpandedView(1, "AcademicView"));
				}
				else if (str.Equals(acompanhamentoInfantilLayout.ClassId))
				{
					await Utils.FadeView(acompanhamentoInfantilLayout);
					await Navigation.PushAsync(new AcompanhamentoInfantilWebViewPage());
				}
				else if (str.Equals(horarioLayout.ClassId))
				{
					await Utils.FadeView(horarioLayout);
					await Navigation.PushAsync(new ClassScheduleWebView());
				}
				else if (str.Equals(gabaritoProvasLayout.ClassId))
				{
					await Utils.FadeView(gabaritoProvasLayout);
					await Navigation.PushAsync(new GabaritoDeProvaWebViewPage());
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(500).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL, (string)App.Current.Resources["academic"]);
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}


