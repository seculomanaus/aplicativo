﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ClassScheduleView : RelativeLayout, ITabView
	{

		Command cmdClick;
		CustomLabel lblDaySelected;
		bool initializedView;

		public ClassScheduleView(int position)
		{
			InitializeComponent();

			cmdClick = new Command((obj) => onClick(obj));

			lblSeg.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSeg.ClassId });
			lblTer.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblTer.ClassId });
			lblQua.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblQua.ClassId });
			lblQui.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblQui.ClassId });
			lblSex.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSex.ClassId });
			lblSab.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSab.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(roundedSeg);
			Utils.fixRoudedView(roundedTer);
			Utils.fixRoudedView(roundedQua);
			Utils.fixRoudedView(roundedQui);
			Utils.fixRoudedView(roundedSex);
			Utils.fixRoudedView(roundedSab);

			if (App.ScreenWidth <= 340)
			{
				lblSeg.WidthRequest = 43;
				lblTer.WidthRequest = 43;
				lblQua.WidthRequest = 43;
				lblQui.WidthRequest = 43;
				lblSex.WidthRequest = 43;
				lblSab.WidthRequest = 43;

				roundedSeg.WidthRequest = 43;
				roundedTer.WidthRequest = 43;
				roundedQua.WidthRequest = 43;
				roundedQui.WidthRequest = 43;
				roundedSex.WidthRequest = 43;
				roundedSab.WidthRequest = 43;
			}

			initLoad();

			if (position == 3)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getTempos();
						});
					});
				});
			}
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			fadeAllDays();
			stackDaysOfTheWeek.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public void getTempos()
		{
			SeculosBO.Instance.getTempoWS(SeculosBO.Instance.DependenteConnected, (msg) =>
			{
				var listTempo = SeculosBO.Instance.DependenteConnected.listTempo;

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listTempo == null || listTempo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.TEMPO_ERROR))
				{
					if (listTempo == null || listTempo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.TEMPO_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listTempo == null || listTempo.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listTempo != null && listTempo.Count > 0)
				{
					initView();
				}
				else
				{ 
                    stackDaysOfTheWeek.IsVisible = false;
					boxSeparator.IsVisible = false;
					load.IsVisible = false;
				}
			});
		}

		public void initView()
		{
			SeculosBO.Instance.DependenteConnected.createListTempoDia();
			selectDay(roundedSeg, lblSeg, stackContentItensSeg, scrollContentItensSeg, SeculosBO.Instance.DependenteConnected.listSeg);
			stackDaysOfTheWeek.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		public async void selectDay(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, ObservableCollection<TempoDia> listItens)
		{
			roundedSeg.Color = Color.Transparent;
			lblSeg.TextColor = Color.FromHex("#a3a3a3");

			roundedTer.Color = Color.Transparent;
			lblTer.TextColor = Color.FromHex("#a3a3a3");

			roundedQua.Color = Color.Transparent;
			lblQua.TextColor = Color.FromHex("#a3a3a3");

			roundedQui.Color = Color.Transparent;
			lblQui.TextColor = Color.FromHex("#a3a3a3");

			roundedSex.Color = Color.Transparent;
			lblSex.TextColor = Color.FromHex("#a3a3a3");

			roundedSab.Color = Color.Transparent;
			lblSab.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (lblDaySelected == null || !lblDaySelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllDays();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblDaySelected = labelSel;

			if (stackContentItens.Children.Count == 0)
			{
				createItens(SeculosBO.Instance.DependenteConnected.GetListByDay(listItens, "A"), stackContentItens);
				createItens(SeculosBO.Instance.DependenteConnected.GetListByDay(listItens, "B"), stackContentItens);
			}
		}

		public async Task fadeAllDays()
		{
			await Task.WhenAll(
				stackContentItensSeg.FadeTo(0, 0, null),
				stackContentItensTer.FadeTo(0, 0, null),
				stackContentItensQua.FadeTo(0, 0, null),
				stackContentItensQui.FadeTo(0, 0, null),
				stackContentItensSex.FadeTo(0, 0, null),
				stackContentItensSab.FadeTo(0, 0, null)
			);
			scrollContentItensSeg.IsVisible = false;
			scrollContentItensTer.IsVisible = false;
			scrollContentItensQua.IsVisible = false;
			scrollContentItensQui.IsVisible = false;
			scrollContentItensSex.IsVisible = false;
			scrollContentItensSab.IsVisible = false;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(lblSeg.ClassId))
				{
					selectDay(roundedSeg, lblSeg, stackContentItensSeg, scrollContentItensSeg, SeculosBO.Instance.DependenteConnected.listSeg);
				}
				else if (str.Equals(lblTer.ClassId))
				{
					selectDay(roundedTer, lblTer, stackContentItensTer, scrollContentItensTer, SeculosBO.Instance.DependenteConnected.listTer);
				}
				else if (str.Equals(lblQua.ClassId))
				{
					selectDay(roundedQua, lblQua, stackContentItensQua, scrollContentItensQua, SeculosBO.Instance.DependenteConnected.listQua);
				}
				else if (str.Equals(lblQui.ClassId))
				{
					selectDay(roundedQui, lblQui, stackContentItensQui, scrollContentItensQui, SeculosBO.Instance.DependenteConnected.listQui);
				}
				else if (str.Equals(lblSex.ClassId))
				{
					selectDay(roundedSex, lblSex, stackContentItensSex, scrollContentItensSex, SeculosBO.Instance.DependenteConnected.listSex);
				}
				else if (str.Equals(lblSab.ClassId))
				{
					selectDay(roundedSab, lblSab, stackContentItensSab, scrollContentItensSab, SeculosBO.Instance.DependenteConnected.listSab);
				}
				else if (str.Equals(stkError.ClassId))
				{
					initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getTempos();
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void createItens(ObservableCollection<TempoDia> listTempoDia, StackLayout stackContentItens)
		{
			if (listTempoDia != null && listTempoDia.Count > 0)
			{
				var lblTurno = new CustomLabel
				{
					Text = listTempoDia[0].NomeTurno,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Bold",
					FontSize = 13.5,
					Margin = new Thickness(20, 0, 0, 10)
				};
				stackContentItens.Children.Add(lblTurno);

				foreach (TempoDia tempoDia in listTempoDia)
				{
					stackContentItens.Children.Add(createItem(tempoDia));
				}

				var boxSeparatorList = new BoxView
				{
					BackgroundColor = Color.FromHex("#cccccc"),
					HeightRequest = 1,
					Opacity = 0.5,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Margin = new Thickness(20, 0, 20, 0)
				};

				stackContentItens.Children.Add(boxSeparatorList);
			}
			else
			{
				if (stackContentItens.Children.Count == 0)
				{
					var lblErrorDayOfTheWeek = new CustomLabel
					{
						Text = App.Current.Resources["general_empty"] as String,
						TextColor = Color.FromHex("#a3a3a3"),
						FontType = "Medium",
						FontSize = 14,
						Margin = new Thickness(30, 60, 30, 0),
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.CenterAndExpand
					};

					stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				}
			}
		}

		public StackLayout createItem(TempoDia item)
		{
			var lblTempo = new CustomLabel
			{
				Text = item.Tempo,
				TextColor = Color.FromHex("#643ea6e1"),
				VerticalOptions = LayoutOptions.Start,
				HorizontalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 36,
				WidthRequest = 66
			};

			var lineDividerView = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(0,0,0,10),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			var stackMain = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { lblTempo },
				Margin = new Thickness(0, 0, 0, 0)
			};

			var lblTitle = new CustomLabel
			{
				Text = item.NomeDisciplina,
				TextColor = Color.Black,
				VerticalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var lblHorario = new CustomLabel
			{
				Text = item.Horario,
				TextColor = Color.FromHex("#a3a3a3"),
				FontType = "Medium",
				FontSize = 13.5
			};

			var stack = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				Spacing = 0,
				VerticalOptions = LayoutOptions.Start,
				Margin = new Thickness(0, 10, 0, 10),
				Children = { lblTitle, lblHorario }
			};

			var imgArrow = new Image
			{
				Source = "arrow_up_yellow_ic.png",
				Aspect = Aspect.AspectFit,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				Margin = new Thickness(0,0,20,0),
				WidthRequest = 20,
				Rotation = 180
			};

			//Hidden content
			var profLabel = new CustomLabel
			{
				Text = "Professor",
				TextColor = Color.Black,
				FontType = "Medium",
				Margin = new Thickness(20, 0, 20, 0),
				FontSize = 15
			};

			var profDescLabel = new CustomLabel
			{
				Text = item.Professor,
				TextColor = Color.FromHex("#a3a3a3"),
				FontType = "Medium",
				Margin = new Thickness(20, 0, 20, 0),
				FontSize = 15
			};

			var profLayout = new StackLayout
			{
				Spacing = 3,
				Children = { lineDividerView, profLabel, profDescLabel }
			};

			var contentLabel = new CustomLabel
			{
				Text = "Conteúdo",
				TextColor = Color.Black,
				FontType = "Medium",
				FontSize = 15
			};

			var contentDescLabel = new CustomLabel
			{
				Text = item.Conteudo,
				TextColor = Color.FromHex("#a3a3a3"),
				FontType = "Medium",
				FontSize = 15
			};

			var contentLayout = new StackLayout
			{
				Spacing = 3,
				Margin = new Thickness(20, 0, 20, 0),
				Children = { contentLabel, contentDescLabel }
			};

			var hiddenLayout = new StackLayout
			{
				HeightRequest = 0,
				Spacing = 8,
				IsVisible = false,
				Children = { profLayout, contentLayout }
			};

			var lineUpView = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = 1,
				IsVisible = false,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			var lineBottomView = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = 1,
				IsVisible = false,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			stackMain.Children.Add(stack);
			stackMain.Children.Add(imgArrow);

			var click = new Command(async (obj) =>
			{
				await Utils.FadeView(stackMain);

				if (!hiddenLayout.IsVisible)
				{
					hiddenLayout.IsVisible = true;
					lineUpView.IsVisible = true;
					lineBottomView.IsVisible = true;
					imgArrow.Rotation = 0;
					hiddenLayout.HeightRequest = -1;
				}
				else
				{
					hiddenLayout.IsVisible = false;
					lineUpView.IsVisible = false;
					lineBottomView.IsVisible = false;
					imgArrow.Rotation = 180;
					hiddenLayout.HeightRequest = 0;
				}
			});

			stackMain.GestureRecognizers.Add(new TapGestureRecognizer { Command = click });

			var rootLayout = new StackLayout
			{
				Spacing = 10,
				Children = { lineUpView, stackMain, hiddenLayout, lineBottomView }
			};

			return rootLayout;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC, ((string)App.Current.Resources["class_schedule"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}