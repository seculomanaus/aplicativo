﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;

namespace SeculosApp
{
    public partial class CardapioView : RelativeLayout, ITabView
    {

        Command cmdClick;
        CustomLabel lblDaySelected;
        bool initializedView;

        public CardapioView(int position)
        {
            InitializeComponent();

            cmdClick = new Command((obj) => onClick(obj));

            lblSeg.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSeg.ClassId });
            lblTer.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblTer.ClassId });
            lblQua.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblQua.ClassId });
            lblQui.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblQui.ClassId });
            lblSex.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSex.ClassId });
            lblSab.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblSab.ClassId });

            stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

            Utils.fixRoudedView(roundedSeg);
            Utils.fixRoudedView(roundedTer);
            Utils.fixRoudedView(roundedQua);
            Utils.fixRoudedView(roundedQui);
            Utils.fixRoudedView(roundedSex);
            Utils.fixRoudedView(roundedSab);

            if (App.ScreenWidth <= 340)
            {
                lblSeg.WidthRequest = 43;
                lblTer.WidthRequest = 43;
                lblQua.WidthRequest = 43;
                lblQui.WidthRequest = 43;
                lblSex.WidthRequest = 43;
                lblSab.WidthRequest = 43;

                roundedSeg.WidthRequest = 43;
                roundedTer.WidthRequest = 43;
                roundedQua.WidthRequest = 43;
                roundedQui.WidthRequest = 43;
                roundedSex.WidthRequest = 43;
                roundedSab.WidthRequest = 43;
            }

            initLoad();

            if (position == 3)
            {
                InitializeView();
            }

        }

        public void InitializeView()
        {
            if (!initializedView)
            {
                initializedView = true;
                Task.Factory.StartNew(() =>
                {
                    Task.Delay(30).ContinueWith((obj) =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            getCardapio();
                        });
                    });
                });
            }
        }

        public void getCardapio()
        {
            SeculosBO.Instance.GetCardapio(SeculosBO.Instance.DependenteConnected, (msg) =>
            {
                var listCardapio = SeculosBO.Instance.DependenteConnected.Cardapios;
                if (msg.Equals(EnumCallback.SUCCESS))
                {
                    if (listCardapio == null || listCardapio.Count == 0)
                    {
                        SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
                    }
                }
                else if (msg.Equals(EnumCallback.CARDAPIO_ERROR))
                {
                    if (listCardapio == null || listCardapio.Count == 0)
                    {
                        SeculosBO.Instance.showViewError(EnumCallback.CARDAPIO_ERROR, lblError, null);
                    }
                }
                else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
                {
                    if (listCardapio == null || listCardapio.Count == 0)
                    {
                        stkError.IsVisible = true;
                    }
                }

                if (listCardapio != null && listCardapio.Count > 0)
                {
                    initView();
                }
                else
                {
                    stackDaysOfTheWeek.IsVisible = true;
                    boxSeparator.IsVisible = true;
                    load.IsVisible = false;
                }
            });
        }

        public void initView()
        {
            List<String> days = new List<String>();
            days.Add(DayOfWeek.Tuesday+"");
            days.Add(DayOfWeek.Wednesday + "");
            days.Add(DayOfWeek.Thursday + "");
            days.Add(DayOfWeek.Friday + "");

            Cardapio cardapio;

            System.Diagnostics.Debug.WriteLine("day of week: " + DateTime.Now.DayOfWeek);

            System.Diagnostics.Debug.WriteLine("contens day of week: " + days.Contains(DateTime.Now.DayOfWeek + ""));

            System.Diagnostics.Debug.WriteLine("mondey if: " + DayOfWeek.Monday);

            if(days.Contains(DateTime.Now.DayOfWeek+"")){
                cardapio = selectCardapioByDay(DateTime.Now.DayOfWeek+""); 
            }
            else{
                cardapio = selectCardapioByDay(DayOfWeek.Monday+"");
            }

            selectDay(roundedSeg, lblSeg, stackContentItensSeg, scrollContentItensSeg, cardapio);
            stackDaysOfTheWeek.IsVisible = true;
            boxSeparator.IsVisible = true;
            load.IsVisible = false;
        }

        Cardapio selectCardapioByDay(string day)
        {
            ObservableCollection<Cardapio> cardapios = SeculosBO.Instance.DependenteConnected.Cardapios;

            var fiteredCardapios = from card in cardapios
                                   where card.dcDia.Trim().Equals(day)
                                   select card;

            if (fiteredCardapios.Count() > 0)
            {
                return fiteredCardapios.First();
            }

            return null;
        }

        public async void selectDay(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, Cardapio cardapio)
        {
            roundedSeg.Color = Color.Transparent;
            lblSeg.TextColor = Color.FromHex("#a3a3a3");

            roundedTer.Color = Color.Transparent;
            lblTer.TextColor = Color.FromHex("#a3a3a3");

            roundedQua.Color = Color.Transparent;
            lblQua.TextColor = Color.FromHex("#a3a3a3");

            roundedQui.Color = Color.Transparent;
            lblQui.TextColor = Color.FromHex("#a3a3a3");

            roundedSex.Color = Color.Transparent;
            lblSex.TextColor = Color.FromHex("#a3a3a3");

            roundedSab.Color = Color.Transparent;
            lblSab.TextColor = Color.FromHex("#a3a3a3");

            roundedSel.Color = Color.FromHex("#3ea6e1");
            labelSel.TextColor = Color.White;

            if (lblDaySelected == null || !lblDaySelected.Text.Equals(labelSel.Text))
            {
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
                Utils.FadeView(labelSel);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
                await fadeAllDays();
                scroll.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
                stackContentItens.FadeTo(1, 100, null);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
            }
            lblDaySelected = labelSel;

            createItens(cardapio, stackContentItens);
        }

        public void createItens(Cardapio cardapio, StackLayout stackContentItens)
        {

            stackContentItens.Children.Clear();
            lblError.IsVisible = false;

            if (cardapio != null)
            {
                var statckManha = CreateCardapioItem(cardapio.dsManha, "Lanche da Manhã", true);
                var statckAlmoco = CreateCardapioItem(cardapio.dsAlmoco, "Almoço", false);
                var statckLantarde = CreateCardapioItem(cardapio.dsTarde, "Lanche da Tarde", true);

                stackContentItens.HorizontalOptions = LayoutOptions.FillAndExpand;

                stackContentItens.Children.Add(statckManha);
                stackContentItens.Children.Add(statckAlmoco);
                stackContentItens.Children.Add(statckLantarde);
            }
            else
            {
                SeculosBO.Instance.showViewError(EnumCallback.CARDAPIO_EMPTY, lblError, null);
            }
        }

        StackLayout CreateCardapioItem(string valueText, string label, bool changeColor)
        {
            var lblType = new CustomLabel
            {
                Text = label,
                TextColor = Color.FromHex("#3ea6e1"),
                FontType = "Bold",
                FontSize = 13.5,
            };

            var lblValue = new CustomLabel
            {
                Text = valueText,
                TextColor = Color.FromHex("#6f787d"),
                FontType = "Bold",
                FontSize = 13.5,
            };

            var stackContent = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = { lblType, lblValue },
                BackgroundColor = changeColor ? Color.FromHex("#E0F1FB") : Color.FromHex("#ffffff"),
                Padding = new Thickness(10, 10, 10, 10),
                Spacing = 0
            };

            return stackContent;
        }


        async void onClick(object obj)
        {
            if (!App.GlobalBlockClick)
            {
                App.GlobalBlockClick = true;
                var str = (string)obj;
                Cardapio cardapio;

                if (str.Equals(lblSeg.ClassId))
                {
                    cardapio = selectCardapioByDay("Monday");
                    selectDay(roundedSeg, lblSeg, stackContentItensSeg, scrollContentItensSeg, cardapio);
                }
                else if (str.Equals(lblTer.ClassId))
                {
                    cardapio = selectCardapioByDay("Tuesday");
                    selectDay(roundedTer, lblTer, stackContentItensTer, scrollContentItensTer, cardapio);
                }
                else if (str.Equals(lblQua.ClassId))
                {
                    cardapio = selectCardapioByDay("Wednesday");
                    selectDay(roundedQua, lblQua, stackContentItensQua, scrollContentItensQua, cardapio);
                }
                else if (str.Equals(lblQui.ClassId))
                {
                    cardapio = selectCardapioByDay("Thursday");
                    selectDay(roundedQui, lblQui, stackContentItensQui, scrollContentItensQui, cardapio);
                }
                else if (str.Equals(lblSex.ClassId))
                {
                    cardapio = selectCardapioByDay("Friday");
                    selectDay(roundedSex, lblSex, stackContentItensSex, scrollContentItensSex, cardapio);
                }
                else if (str.Equals(lblSab.ClassId))
                {
                    cardapio = selectCardapioByDay("Saturday");
                    selectDay(roundedSab, lblSab, stackContentItensSab, scrollContentItensSab, cardapio);
                }
                else if (str.Equals(stkError.ClassId))
                {
                    initLoad();
                    await Utils.FadeView(stkError);
                    await Task.Factory.StartNew(() =>
                    {
                        Task.Delay(200).ContinueWith((objj) =>
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                getCardapio();
                            });
                        });
                    });
                }

                await Task.Factory.StartNew(() =>
                {
                    Task.Delay(200).ContinueWith((o) =>
                    {
                        App.GlobalBlockClick = false;
                    });
                });
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
            }
        }

        public async Task fadeAllDays()
        {
            await Task.WhenAll(
                stackContentItensSeg.FadeTo(0, 0, null),
                stackContentItensTer.FadeTo(0, 0, null),
                stackContentItensQua.FadeTo(0, 0, null),
                stackContentItensQui.FadeTo(0, 0, null),
                stackContentItensSex.FadeTo(0, 0, null),
                stackContentItensSab.FadeTo(0, 0, null)
            );
            scrollContentItensSeg.IsVisible = false;
            scrollContentItensTer.IsVisible = false;
            scrollContentItensQua.IsVisible = false;
            scrollContentItensQui.IsVisible = false;
            scrollContentItensSex.IsVisible = false;
            scrollContentItensSab.IsVisible = false;
        }

        public void initLoad()
        {
            stkError.IsVisible = false;
            load.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
            fadeAllDays();
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
            stackDaysOfTheWeek.IsVisible = false;
            boxSeparator.IsVisible = false;
            lblError.IsVisible = false;
        }

        public void OnPageAppeared()
        {
        }

        public void OnPageAppearing()
        {
            App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_CANTINA, (string)App.Current.Resources["cardapio_title"]);
            InitializeView();
        }

        public void OnPageDisappeared()
        {
        }

        public void OnPageDisappearing()
        {
        }
    }
}
