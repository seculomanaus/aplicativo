﻿using Xamarin.Forms;
using System.Threading.Tasks;

namespace SeculosApp
{
	public class AcademicView : RelativeLayout, ITabView
	{
		Command cmdClick;
		//RelativeLayout boletimLayout, notasLayout, frequenciaLayout, registrosDiariosLayout, registrosPedagogicosLayout;
        RelativeLayout notasLayout, registrosDiariosLayout, desempenhoLayout;
        bool blockClick;

		public AcademicView()
		{
			ClassId = "AcademicView";
			cmdClick = new Command((obj) => onClick(obj));
			BackgroundColor = Color.White;

            //boletimLayout = Utils.CreateItemLayout("Boletim");
            //frequenciaLayout = Utils.CreateItemLayout("Frequência");
			
            notasLayout = Utils.CreateItemLayout("Demonstrativo de Notas");
            registrosDiariosLayout = Utils.CreateItemLayout("Registros Diários");
            desempenhoLayout = Utils.CreateItemLayout("Desempenho");
            //registrosPedagogicosLayout = Utils.CreateItemLayout("Registros Pedagógicos");

            //boletimLayout.ClassId = "GradeBoletimView";
            //frequenciaLayout.ClassId = "FrequenciaView";
            desempenhoLayout.ClassId = "desempenhoView";
            notasLayout.ClassId = "StatementNotesView";
            registrosDiariosLayout.ClassId = "RegistrosDiariosView";
			//registrosPedagogicosLayout.ClassId = "EducationalRecordsView";

            //boletimLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = boletimLayout.ClassId });
            //frequenciaLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = frequenciaLayout.ClassId });
            notasLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = notasLayout.ClassId });
            registrosDiariosLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = registrosDiariosLayout.ClassId });
			//registrosPedagogicosLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = registrosPedagogicosLayout.ClassId });
            desempenhoLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = desempenhoLayout.ClassId});

			var stack = new StackLayout
			{
				VerticalOptions = LayoutOptions.StartAndExpand,
				//Children = { boletimLayout, frequenciaLayout,notasLayout,registrosPedagogicosLayout},
                Children = {notasLayout, desempenhoLayout },
                Margin = new Thickness(0, 8, 0, 0)

			};

			var scroll = new ScrollView
			{
				Content = stack
			};

            //pai conectado
			/*if (!SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
			{
				registrosPedagogicosLayout.IsVisible = false;
			}*/

			Children.Add(
				scroll,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);
		}



		async void onClick(object obj)
		{
			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

                /*if (str.Equals(boletimLayout.ClassId))
                {
                    await Utils.FadeView(boletimLayout);
                    await Navigation.PushAsync(new AcademicExpandedView(0, "GradeBoletimView"));
                }
                else if (str.Equals(frequenciaLayout.ClassId))
				{
					await Utils.FadeView(frequenciaLayout);
					await Navigation.PushAsync(new AcademicExpandedView(1, "FrequenciaView"));
				}*/
                if (str.Equals(notasLayout.ClassId))
                {
                    await Utils.FadeView(notasLayout);
                    await Navigation.PushAsync(new AcademicExpandedView(2, "StatementNotesView"));
                }
                else if (str.Equals(registrosDiariosLayout.ClassId))
				{
					await Utils.FadeView(registrosDiariosLayout);
					await Navigation.PushAsync(new AcademicExpandedView(3, "RegistrosDiariosView"));
				}
				/*else if (str.Equals(registrosPedagogicosLayout.ClassId))
				{
					await Utils.FadeView(registrosPedagogicosLayout);
					await Navigation.PushAsync(new EducationalRecordsView());
				}*/
                else if (str.Equals(desempenhoLayout.ClassId))
                {
                    await Utils.FadeView(desempenhoLayout);
                    await Navigation.PushAsync(new desempenhoView());
                }

                await Task.Factory.StartNew(() =>
				{
					Task.Delay(500).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC, (string)App.Current.Resources["school_history"]);
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}


