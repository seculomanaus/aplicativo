﻿using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class NotificationView : AbstractContentPage, IBindingContextView
	{
		private NotificationViewModel ViewModel
		{
			get
			{
				return BindingContext as NotificationViewModel;
			}
		}

		public void changeBindingContext()
		{
			var tapUpdate = new TapGestureRecognizer();
			tapUpdate.Tapped += (s, e) =>
			{
				Utils.FadeView(ViewModel.imgCloud);
				Utils.FadeView(ViewModel.lblError);
				Utils.FadeView(ViewModel.lblErrorUpdate);

				ViewModel.LoadNotificationCommand.Execute(null);
				System.Diagnostics.Debug.WriteLine("tapUpdate LoadNotificationCommand: ");
			};
			ViewModel.viewError.GestureRecognizers.Add(tapUpdate);
			System.Diagnostics.Debug.WriteLine("LoadNotificationCommand: changeBindingContext: " + ViewModel.lblError.Text);
		}

		public NotificationView(string notificationCode) : this()
		{
			System.Diagnostics.Debug.WriteLine("notificationCode: " + notificationCode);
			System.Diagnostics.Debug.WriteLine("SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications: " + SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications);
			SeculosBO.Instance.NotificationSelected = SeculosBO.Instance.searchNotificatioByCode(notificationCode, SeculosBO.Instance.SeculosGeneral.UserConnected.listNotifications);
			Navigation.PushAsync(new NotificationDetailView());
		}

		public NotificationView()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;
			var back = new TapGestureRecognizer();
			back.Tapped += async (s, e) =>
			{
				await Utils.FadeView(imgBack);

				try
				{
					await Navigation.PopAsync();
				}
				catch (System.Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("NotificationView back exception: " + exception.Message + " Type: " + exception.GetType());
				}
			};
			imgBack.GestureRecognizers.Add(back);

			BindingContext = new NotificationViewModel();

			ViewModel.lblError = lblErrorMain;
			ViewModel.lblErrorUpdate = lblErrorMainUpdate;
			ViewModel.imgCloud = imgCloudMain;
			ViewModel.lblErrorEmpty = lblEmpty;
			ViewModel.viewError = stkErrorMain;

			var tapUpdate = new TapGestureRecognizer();
			tapUpdate.Tapped += (s, e) =>
			{
				Utils.FadeView(ViewModel.imgCloud);
				Utils.FadeView(ViewModel.lblError);
				Utils.FadeView(ViewModel.lblErrorUpdate);

				ViewModel.LoadNotificationCommand.Execute(null);
				System.Diagnostics.Debug.WriteLine("tapUpdate LoadNotificationCommand: ");
			};
			ViewModel.viewError.GestureRecognizers.Add(tapUpdate);

			Task.Factory.StartNew(() =>
		 	{
	 			Task.Delay(200).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						ViewModel.LoadNotificationCommand.Execute(null);
					});
				});
			 });

			listViewNotifications.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{

				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				if (!App.GlobalBlockClick)
				{
					SeculosBO.Instance.NotificationSelected = (Notificacao)((ListView)sender).SelectedItem;
					SeculosBO.Instance.ComunicationSelected = (Notificacao)((ListView)sender).SelectedItem;
				}
				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect

				if (!App.GlobalBlockClick)
				{
					App.GlobalBlockClick = true;

					if (SeculosBO.Instance.NotificationSelected.isNotComunication)// abre a tela de detalhes da noticia
					{
						await Navigation.PushAsync(new NotificationDetailView());

                    }else if(SeculosBO.Instance.NotificationSelected.CdEvento.Equals("9999")){// abre o navegador para comunicados do portal
                        SeculosBO.Instance.IMessage.Msg("Abrindo comunicado no seu navegador...");
                        Device.OpenUri(new System.Uri(SeculosBO.Instance.NotificationSelected.DsAlert));
                    }
                    else{
                        //SeculosBO.Instance.OpenModal(EnumModal.COMUNICATION, false, this);
                        await Navigation.PushAsync(new NotificationDetailView());
                    }

					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(500).ContinueWith((obj) =>
						{
							App.GlobalBlockClick = false;
						});
				 	});
				}
			};

			var tapCloseError = new TapGestureRecognizer();
			tapCloseError.Tapped += async (s, e) =>
			{
				await Utils.FadeView(btnCloseError);
				await stkErrorMain.TranslateTo(0, stkErrorMain.Height, 100);
				stkErrorMain.IsVisible = false;
				await stkErrorMain.TranslateTo(0, 0, 0);
				System.Diagnostics.Debug.WriteLine("tapCloseError btnCloseError: ");
			};
			btnCloseError.GestureRecognizers.Add(tapCloseError);
		}

		protected override void OnAppearing()
		{
			MessagingCenter.Subscribe<App>(this, MessagingConstants.ALERTA_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					// update ListView
					ViewModel.LoadNotificationCommand.Execute(null);
				}
				catch { }
			});
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
		}
	}
}
