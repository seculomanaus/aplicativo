﻿using Xamarin.Forms;

namespace SeculosApp
{
	public partial class AcompanhamentoInfantilWebViewPage : AbstractContentPage
	{

        string baseURl = "https://seculomanaus.com.br/componentes/portal/acompanhamento/inicio?";
        string contextParam = "Context=[CodSistema=S;CodUsuario=";


        public AcompanhamentoInfantilWebViewPage()
		{

            InitializeComponent();

            setupBackArrow();

            lblName.Text = "Acompanhamento Infantil";

            string cpf = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;

            System.Diagnostics.Debug.WriteLine("User cdUsuario: " + cpf);

            string url = baseURl + contextParam + cpf + ";]";

            System.Diagnostics.Debug.WriteLine("URL: " + url);

            navegador.Source = url;

		}

        private void setupBackArrow()
        {
            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) =>
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch (System.Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("webview back exception: " + exception.Message + " Type: " + exception.GetType());
                }

            };
            imgBack.GestureRecognizers.Add(back);
        }

        private void PagOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            load.IsVisible = true;
        }
        private void PagOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            load.IsVisible = false;
        }

    }
}