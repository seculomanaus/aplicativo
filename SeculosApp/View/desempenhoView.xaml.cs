﻿using Xamarin.Forms;

namespace SeculosApp
{
    public partial class desempenhoView : AbstractContentPage
    {
        string baseURL = "https://seculomanaus.com.br/componentes/portal/desempenho/inicio?ra=";

        public desempenhoView()
        {

            InitializeComponent();

            setupBackArrow();

            lblName.Text = "Desempenho";

            bool isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

            string studentCode;

            if (isResp)
            {
                studentCode = SeculosBO.Instance.StudentSelected.CdAluno;
            }
            else
            {
                studentCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;

            }

            System.Diagnostics.Debug.WriteLine("Aluno seecionado cdAluno: " + studentCode);

            string url = baseURL + studentCode;

            System.Diagnostics.Debug.WriteLine("URL: " + url);

            navegador.Source = url;

        }

        private void setupBackArrow()
        {
            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) =>
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch (System.Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("webview back exception: " + exception.Message + " Type: " + exception.GetType());
                }

            };
            imgBack.GestureRecognizers.Add(back);
        }

        private void PagOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            load.IsVisible = true;
        }
        private void PagOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            load.IsVisible = false;
        }

    }
}