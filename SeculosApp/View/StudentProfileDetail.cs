﻿using Xamarin.Forms;

namespace SeculosApp
{
    public class StudentProfileDetail : BaseExpandedView
    {

        // FinanceDetailView FinanceDetailView;
        FinanceBilletByStudentView financeBilletByStudentView;

        public StudentProfileDetail(int _position, string _pageId) : base(_position, _pageId)
        {
            // position = Page in carousel
            // 0 -> AcademicSemiView
            // 1 -> CantinaView ou cardapioView (Se aluno conectado) -- 
            // 2 -> FinanceBilletByStudentView
            // ---------------------------
            // default -> AcademicSemiView

            tabsLayout.BackgroundColor = Color.FromHex(SeculosBO.Instance.DependenteConnected.ColorSerie);

            btClose.Source = "x_ic.png";

            MessagingCenter.Subscribe<App>(this, MessagingConstants.BOLETO_HAS_BEEN_UPDATED, (obj) =>
            {
                try
                {
                    financeBilletByStudentView.financeView.ViewModel.LoadBilletsCommand.Execute(null);
                }
                catch { }
            });

            Content = layout;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL, (obj, arg) =>
            {
                try
                {
                    var str = (string)arg;
                    tabTitle.Text = str;
                }
                catch { }
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL);
        }

        public override void handleCarouselPage()
        {
            var title = (string)App.Current.Resources["academic"];

            System.Diagnostics.Debug.WriteLine("position student clicked: "+position);

            if (position < carouselMotora.Children.Count)
            {
                if (position == 1)
                {
                    title = (string)App.Current.Resources["cantina"];
                }
                else if (position == 2)
                {
                    title = (string)App.Current.Resources["finance"];
                }
            }

            tabTitle.Text = title;
        }

        public override Grid GenerateCarouselMotora()
        {
            Grid grid = new Grid
            {
                BackgroundColor = Color.White
            };
            
            grid.Children.Add(new AcademicSemiView(), 0, 1, 0, 1);

            //pai conectado
            if (SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
            {
                 grid.Children.Add(new CantinaView(this), 0, 1, 0, 1);

                financeBilletByStudentView = new FinanceBilletByStudentView(this);
                grid.Children.Add(financeBilletByStudentView, 0, 1, 0, 1);
            }
            else {
                //grid.Children.Add(new CardapioView(3), 0, 1, 0, 1);
            }

            return grid;
        }

	}
}


