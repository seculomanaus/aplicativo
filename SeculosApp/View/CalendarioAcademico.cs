﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XamForms.Controls;

namespace SeculosApp
{
	public class CalendarioAcademico : RelativeLayout, ITabView
	{
		bool initializedView;
		CustomLabel errorLabel;
		Calendar calendar;
		ScrollView scrollDesc;
		BoxView legendSeperator;

		List<Tuple<DateTime, CalendarAcademic>> detailDateList;

		ActivityIndicator loadIndicator;
		StackLayout layoutDesc;
		BaseExpandedView Page;

		public CalendarioAcademico(BaseExpandedView page, int position)
		{
			ClassId = "CalendarAcademicView";

			this.Page = page;

			this.BackgroundColor = Color.White;

			scrollDesc = new ScrollView();
			layoutDesc = new StackLayout();

			scrollDesc.Content = layoutDesc;

			var errorLabelColor = (Color)App.Current.Resources["gray_light"];

			errorLabel = new CustomLabel
			{
				TextColor = errorLabelColor,
				FontSize = 14,
				FontType = "Medium",
				IsVisible = false,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center
			};

            errorLabel.Text = (string)App.Current.Resources["data_calendario_academic_error"];

			legendSeperator = new BoxView
			{
				BackgroundColor = Color.Gray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(10, 0, 10, 0),
				HeightRequest = .5
			};

			loadIndicator = new ActivityIndicator
			{
				Color = (Color)App.Current.Resources["blue_light_label"],
				IsRunning = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				IsVisible = false
			};

			this.Children.Add(
				errorLabel,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				scrollDesc,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				loadIndicator,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			if (position == 1)
			{
				InitializeView();
			}
		}

		void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				loadIndicator.IsVisible = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getDates();
						});
					});
				});
			}
		}

		void getDates()
		{
			SeculosBO.Instance.getCalendarWait(SeculosBO.Instance.DependenteConnected.CdMatricula, (msg) =>
			{
				var isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);
				ObservableCollection<CalendarAcademic> listCalendar= null;

				if (isResp)
				{
					listCalendar = SeculosBO.Instance.StudentSelected.listCalendarAcademic;
				}
				else

				{
					listCalendar = SeculosBO.Instance.DependenteConnected.listCalendarAcademic;
				}

                System.Diagnostics.Debug.WriteLine("msg calendario academico: "+msg);

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_CALENDARIO_ACADEMICO_EMPTY, errorLabel, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_CALENDARIO_ACADEMICO_ERROR))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_CALENDARIO_ACADEMICO_ERROR, errorLabel, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						System.Diagnostics.Debug.WriteLine("calendario academico: 1");
					}
				}

				loadIndicator.IsVisible = false;

                System.Diagnostics.Debug.WriteLine("calendario academico: 2");

				if (listCalendar != null && listCalendar.Count > 0)
				{
					errorLabel.IsVisible = false;
					initView(listCalendar);
					calendar.IsVisible = true;
					
				}
				else
				{
					errorLabel.IsVisible = true;
                    System.Diagnostics.Debug.WriteLine("calendario academico: 3");
				}
			});
		}

		void initView(ObservableCollection<CalendarAcademic> listCalendar)
		{

			calendar = new Calendar
			{
				BorderColor = Color.White,
				BorderWidth = 3,
				DatesTextColor = Color.Gray,
				DatesTextColorOutsideMonth = Color.FromHex("#d3d3d3"),
				BackgroundColor = Color.White,
				DatesBackgroundColorOutsideMonth = Color.White,
				SelectedBorderColor = Color.Transparent,
				TitleLabelFormat = "MMMM",	
				StartDate = DateTime.Now
			};

            calendar.DateClicked += (sender, e) => handleCalendarEvent(e.DateTime);

			var specialDatesList = new List<SpecialDate>();

			var dateTimeToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
			var specialDateToday = new SpecialDate(dateTimeToday);
			specialDateToday.BorderColor = Color.FromHex("#FFD04B");
			specialDateToday.TextColor = Color.Black;

			specialDatesList.Add(specialDateToday);

			detailDateList = new List<Tuple<DateTime, CalendarAcademic>>();

			foreach (var date in listCalendar)
			{
				if (!date.DtCalendario.Equals(SeculosBO.EMPTY))
				{
					var dateTime = DateTime.ParseExact(date.DtCalendario, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
					var specialDate = new SpecialDate(dateTime)
					{
						Selectable = true,
				    };

					detailDateList.Add(new Tuple<DateTime, CalendarAcademic>(dateTime, date));

					if (!date.TpColor.Equals(SeculosBO.EMPTY))
					{
						specialDate.BackgroundColor = Color.FromHex(date.TpColor);
						specialDate.TextColor = Color.White;
					}

					specialDatesList.Add(specialDate);
				}
			}

			calendar.SpecialDates = specialDatesList;

			// Calendar events
			calendar.LeftArrowClicked += (sender, e) => handleCalendarEvent(e.DateTime);
			calendar.RightArrowClicked += (sender, e) => handleCalendarEvent(e.DateTime);

			layoutDesc.Children.Add(calendar);

			// Initiate layout for current month
			handleCalendarEvent(DateTime.Now);

		}

		void handleCalendarEvent(DateTime dateSelected)
		{
			var layout = new StackLayout
			{
				Spacing = 30,
				Margin = new Thickness(0, 0, 0, 20),
				Children = { legendSeperator }
			};

			//INSERINDO LEGENDA CALENDARIO
			layout.Children.Add(CreateLegendCalendar());

			var layoutDate = new StackLayout
			{
				Margin = new Thickness(10, 0, 20, 5),
				Orientation = StackOrientation.Horizontal,
				Spacing = 5
			};

			var dateString = dateSelected.ToString("dd/MM/yyyy");

			var label_date = new CustomLabel
			{
				Text = "Data: " + dateString,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 250,
				TextColor = Color.Gray,
				FontSize = 16,
				FontType = "Medium"
			};

			layoutDate.Children.Add(label_date);

			layout.Children.Add(layoutDate);


			foreach (var desc in detailDateList)
			{

				// Same month and same year, match!
				if (desc.Item1.Day == dateSelected.Day && desc.Item1.Month == dateSelected.Month && desc.Item1.Year == dateSelected.Year)
				{

					CalendarAcademic calendarAcademic = desc.Item2;

					// Check if description and color exists 
					if (!calendarAcademic.DescCalendario.Equals(SeculosBO.EMPTY) && !calendarAcademic.TpColor.Equals(SeculosBO.EMPTY))
					{

						var legendItem = CreateLegendItem(calendarAcademic, dateString);
						layout.Children.Add(legendItem);

					}
				}
			}

			legendSeperator.IsVisible = (layout.Children.Count > 0);

			if (layoutDesc.Children.Count >= 2)
			{
				layoutDesc.Children.RemoveAt(1);
			}

			layout.Children.Add(CreateBtnCalendar());

			layoutDesc.Children.Add(layout);
		}

		StackLayout CreateBtnCalendar()
        {
			var layout = new StackLayout
			{
				Margin = new Thickness(10, 0, 10, 5),
				Orientation = StackOrientation.Horizontal,
				Spacing = 5
			};

			var btn = new CustomButton
			{
                Text = "CALENDÁRIO COMPLETO",
				BackgroundColor = Color.FromHex("#3ea6e1"),
				TextColor = Color.White,
                BorderRadius = 5,
                FontSize = 18,
                HeightRequest = 65,
                HorizontalOptions = LayoutOptions.FillAndExpand
			};

            btn.Clicked += async (sender, e) =>
			{
				redirectToPDF();
			};

			layout.Children.Add(btn);
			return layout;

		}

		private void redirectToPDF()
		{

			string baseUrl = "https://seculomanaus.com.br/componentes/portal/calendario/letivo/inicio?ra=";

			bool isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

			string studentCode;

			if (isResp)
			{
				studentCode = SeculosBO.Instance.StudentSelected.CdAluno;
			}
			else
			{
				studentCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;
			}

			string url = baseUrl + studentCode;

			System.Diagnostics.Debug.WriteLine("calendario academico: URL calendario completo " + url);

			Device.OpenUri(new Uri(url));

		}

		StackLayout CreateLegendItem(CalendarAcademic calendarAcademic, string dateString)
		{
			var layout = new StackLayout
			{
				Margin = new Thickness(10, 0, 20, 5),
				Orientation = StackOrientation.Horizontal,
				Spacing = 5,
				GestureRecognizers = {
				new TapGestureRecognizer {
						Command = new Command (()=>{
							SeculosBO.Instance.OpenModalCalendar(calendarAcademic, Page, dateString);
						}),
				},
		        }
			};

			var boxView = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex(calendarAcademic.TpColor),
				VerticalOptions = LayoutOptions.Center
			};

			var label = new CustomLabel
			{
				Text = calendarAcademic.DescCalendario,
				VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Gray,
				FontSize = 15,
				FontType = "Medium"
			};

			layout.Children.Add(boxView);
			layout.Children.Add(label);

			return layout;
		}

		StackLayout CreateLegendCalendar()
		{
			var layout = new StackLayout
			{
				Margin = new Thickness(10, 0, 20, 5),
				Orientation = StackOrientation.Horizontal,
				Spacing = 5
			};

			var hoje = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex("#FFD04B"),
				VerticalOptions = LayoutOptions.Center
			};

			var label_hoje = new CustomLabel
			{
				Text = "Hoje",
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 40,
				TextColor = Color.Gray,
				FontSize = 11,
				FontType = "Medium"
			};

			layout.Children.Add(hoje);
			layout.Children.Add(label_hoje);

			var feriado = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex("#FF0000"),
				VerticalOptions = LayoutOptions.Center
			};

			var label_feriado = new CustomLabel
			{
				Text = "Feriado",
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 60,
				TextColor = Color.Gray,
				FontSize = 11,
				FontType = "Medium"
			};

			layout.Children.Add(feriado);
			layout.Children.Add(label_feriado);

			var avaliacao = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex("#008000"),
				VerticalOptions = LayoutOptions.Center
			};

			var label_avaliacao = new CustomLabel
			{
				Text = "Avaliação",
				VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Gray,
				WidthRequest = 80,
				FontSize = 11,
				FontType = "Medium"
			};

			layout.Children.Add(avaliacao);
			layout.Children.Add(label_avaliacao);

            /*var aula = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex("#00BFFF"),
				VerticalOptions = LayoutOptions.Center
			};

			var label_aula = new CustomLabel
			{
				Text = "AULA NORMAL",
				VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Gray,
				FontSize = 11,
				FontType = "Medium"
			};*/

            //layout.Children.Add(aula);
			//layout.Children.Add(label_aula);
             
			return layout;
		}

		public void OnPageAppeared()
		{
			// Do nothing
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_AGENDA, ((string)App.Current.Resources["calendarioAcademic"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappeared()
		{
			// Do nothing
		}

		public void OnPageDisappearing()
		{
			// Do nothing
		}
	}
}

