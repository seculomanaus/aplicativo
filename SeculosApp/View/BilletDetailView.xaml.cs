﻿using System;
using System.Threading.Tasks;
//using Plugin.Clipboard;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class BilletDetailView : AbstractContentPage
	{
		Command cmdClick;
		public static bool updateByPayCreditCard;

		public BilletDetailView()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;

			cmdClick = new Command((obj) => onClick(obj));
            btnCreditCard.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnCreditCard.ClassId });
			
			var back = new TapGestureRecognizer();
			var isBack = false;
			back.Tapped += async (s, e) =>
			{
				if (!isBack)
				{
					isBack = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(imgBackBillet);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					try
					{
						await Navigation.PopAsync();
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("BilletDetailView back exception: " + exception.Message + " Type: " + exception.GetType());
					}
				}
			};
			imgBackBillet.GestureRecognizers.Add(back);

			updateView();

			MessagingCenter.Subscribe<App>(this, MessagingConstants.BOLETO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					updateView();
				}
				catch { }
			});
		}

		void updateView()
		{
			topBillet.BackgroundColor = Color.FromHex(SeculosBO.Instance.BilletSelected.ColorBoleto);
			lblTitle.Text = SeculosBO.Instance.BilletSelected.NomeProduto;
			imgBillet.Source = SeculosBO.Instance.BilletSelected.ImgBoleto;
			lblStudentBillet.Text = SeculosBO.Instance.BilletSelected.NomeAluno;
			lblBilletDueDate.Text = SeculosBO.Instance.BilletSelected.DataVencimento;
			try
			{
				lblBilletValue.Text = "R$ " + Utils.getCashValue(SeculosBO.Instance.BilletSelected.VlBoleto);
			}
			catch
			{
				lblBilletValue.Text = "R$ 0,0";
			}

			if (SeculosBO.Instance.BilletSelected.FlProtesto.Equals("S") 
			    || (SeculosBO.Instance.BilletSelected.FLVencido.Equals("S") && !SeculosBO.Instance.BilletSelected.Situacao.ToLower().Equals(Application.Current.Resources["paid"] as String)))
			{
				lblBilletCod.Text = Application.Current.Resources["more_info_secretary"] as String;
				boxPayment.IsVisible = false;
				btnCreditCard.IsVisible = false;
			}
			else if (SeculosBO.Instance.BilletSelected.Situacao.ToLower().Equals(Application.Current.Resources["paid"] as String))
			{
				boxPayment.IsVisible = false;
				btnCreditCard.IsVisible = false;
				lblDueDateOrMonth.Text = Application.Current.Resources["month"] as String;
				lblBilletDueDate.Text = SeculosBO.Instance.BilletSelected.DcReferencia;
				lblBilletOrReceived.Text = Application.Current.Resources["received"] as String;
				lblBilletOrReceived.IsVisible = true;
				lblBilletCod.IsVisible = true;
				try
				{
					lblBilletCod.Text = "R$ " + Utils.getCashValue(SeculosBO.Instance.BilletSelected.VlRecebido);
				}
				catch
				{
					lblBilletCod.Text = (Application.Current.Resources["not_informed"] as String).ToLower();
				}
			}
			else
			{
				lblBilletOrReceived.IsVisible = false;
				lblBilletCod.IsVisible = false;
			}
		}

		protected override void OnAppearing()
		{
			
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
		}

		protected override bool OnBackButtonPressed()
		{
			var superBack = base.OnBackButtonPressed();

			if (PayCreditModal.updateByEvent || PayCreditModal.updateByBillet)
			{
				return false;
			}
			else
			{
				return superBack;
			}
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

                if (str.Equals(btnCreditCard.ClassId))
				{
					await Utils.FadeView(btnCreditCard);
                    if (SeculosBO.Instance.BilletSelected.CodigoBarra != null)
                    {
                        //Plugin.Clipboard.CrossClipboard.Current.SetText(SeculosBO.Instance.BilletSelected.CodigoBarra);
						SeculosBO.Instance.IMessage.Msg("Código de barras copiado.");
					}
				
				}
				
				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}


	}
}
