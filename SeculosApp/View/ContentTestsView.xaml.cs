﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ContentTestsView : RelativeLayout, ITabView
	{
		Command cmdClick;
		CustomLabel lblBimSelected;
		bool initializedView;

		public ContentTestsView(int position)
		{
			InitializeComponent();

			cmdClick = new Command(async (obj) => await onClick(obj));

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl1Bim.ClassId });
			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl2Bim.ClassId });
			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl3Bim.ClassId });
			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl4Bim.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(rounded1Bim);
			Utils.fixRoudedView(rounded2Bim);
			Utils.fixRoudedView(rounded3Bim);
			Utils.fixRoudedView(rounded4Bim);
			initLoad();

			if (position == 0)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getConteudos();
						});
					});
				});
			}
		}

		public async void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			await fadeAllBim();
			stackBimesters.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public void getConteudos()
		{
			SeculosBO.Instance.getConteudoWS(SeculosBO.Instance.DependenteConnected, async (msg) =>
			{
                
                var listConteudo = SeculosBO.Instance.DependenteConnected.listConteudo;
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listConteudo == null || listConteudo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONTEUDO_ERROR))
				{
					if (listConteudo == null || listConteudo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.CONTEUDO_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listConteudo == null || listConteudo.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listConteudo != null && listConteudo.Count > 0)
				{
                    stackBimesters.IsVisible = true;
                    await initView();
				}
				else
				{
					stackBimesters.IsVisible = false;
					boxSeparator.IsVisible = true;
					load.IsVisible = false;
				}
			});
		}

		public async Task initView()
		{
           
            SeculosBO.Instance.DependenteConnected.createListConteudoBim();
			await selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, SeculosBO.Instance.DependenteConnected.Conteudo1Bim, SeculosBO.Instance.DependenteConnected.Conteudo1Bim.mapConteudoDisciplina);
			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		public async Task populateTabs<T>(Dictionary<string, ObservableCollection<T>> map, ConteudoBim ConteudoBim, StackLayout stackContentItens)
		{
            try
            {
                if (stackContentItens.Children.Count == 0)
                {
                    foreach (String key in map.Keys)
                    {
                        if (key != null && !key.Equals(String.Empty))
                        {
                            Task<StackLayout> task = CreateItemLayout(key, ConteudoBim);
                            if (task != null)
                            {
                                StackLayout item = await task;
                                item.ClassId = key;
                                stackContentItens.Children.Add(item);
                                item.IsVisible = true;
                            }
                        }
                    }
                    stackContentItens.IsVisible = true;
                    createEmptyitem(stackContentItens);
                }
            }
            catch { }
		}

		public async Task selectBim<T>(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, ConteudoBim ConteudoBim, Dictionary<string, ObservableCollection<T>> map)
		{
			rounded1Bim.Color = Color.Transparent;
			lbl1Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded2Bim.Color = Color.Transparent;
			lbl2Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded3Bim.Color = Color.Transparent;
			lbl3Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded4Bim.Color = Color.Transparent;
			lbl4Bim.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (lblBimSelected == null || !lblBimSelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllBim();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblBimSelected = labelSel;

            /////////////
            try { await populateTabs(map, ConteudoBim, stackContentItens); }
            catch { }
          
		}

		public async Task fadeAllBim()
		{
			await Task.WhenAll(
				stackContentItens1Bim.FadeTo(0, 0, null),
				stackContentItens2Bim.FadeTo(0, 0, null),
				stackContentItens3Bim.FadeTo(0, 0, null),
				stackContentItens4Bim.FadeTo(0, 0, null)
			);
			scrollContentItens1Bim.IsVisible = false;
			scrollContentItens2Bim.IsVisible = false;
			scrollContentItens3Bim.IsVisible = false;
			scrollContentItens4Bim.IsVisible = false;
		}

		async Task onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

                Debug.WriteLine("ola");

                if (str.Equals(lbl1Bim.ClassId))
				{
					await selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, SeculosBO.Instance.DependenteConnected.Conteudo1Bim, SeculosBO.Instance.DependenteConnected.Conteudo1Bim.mapConteudoDisciplina);
				}
				else if (str.Equals(lbl2Bim.ClassId))
				{
					await selectBim(rounded2Bim, lbl2Bim, stackContentItens2Bim, scrollContentItens2Bim, SeculosBO.Instance.DependenteConnected.Conteudo2Bim, SeculosBO.Instance.DependenteConnected.Conteudo2Bim.mapConteudoDisciplina);
				}
				else if (str.Equals(lbl3Bim.ClassId))
				{
					await selectBim(rounded3Bim, lbl3Bim, stackContentItens3Bim, scrollContentItens3Bim, SeculosBO.Instance.DependenteConnected.Conteudo3Bim, SeculosBO.Instance.DependenteConnected.Conteudo3Bim.mapConteudoDisciplina);
				}
				else if (str.Equals(lbl4Bim.ClassId))
				{
					await selectBim(rounded4Bim, lbl4Bim, stackContentItens4Bim, scrollContentItens4Bim, SeculosBO.Instance.DependenteConnected.Conteudo4Bim, SeculosBO.Instance.DependenteConnected.Conteudo4Bim.mapConteudoDisciplina);
				}
				else if (str.Equals(stkError.ClassId))
				{
					initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(100).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getConteudos();
                                stackBimesters.IsVisible = true;
                                boxSeparator.IsVisible = true;
                                load.IsVisible = false;
                            });
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(100).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void createEmptyitem(StackLayout stackContentItens)
		{
			if (stackContentItens.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 60, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				stackContentItens.HorizontalOptions = LayoutOptions.CenterAndExpand;
			}
		}

		public async Task<StackLayout> CreateItemLayout(string text, ConteudoBim ConteudoBim)
		{
			var layout = new StackLayout
			{
				Padding = new Thickness(0, 6, 0, 0),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				WidthRequest = 600
			};

			var layoutlabelArrow = new StackLayout
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
             	Margin = new Thickness(10, 0, 10, 0)
			};

			var itemLabel = new CustomLabel
			{
				Text = text,
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				HeightRequest = 28,
				FontSize = 15.5,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand				                               
			};

			var imgArrow = new Image
			{
				Source = "arrow_right_yellow_ic.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				WidthRequest = 26,
				HeightRequest = 18,
				Rotation = 90
			};

			layoutlabelArrow.Children.Add(itemLabel);
			layoutlabelArrow.Children.Add(imgArrow);

			layout.Children.Add(layoutlabelArrow);

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				VerticalOptions = LayoutOptions.End,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(10, 15, 10, 0),
			};

			layout.Children.Add(lineSeperator);

			var stackContent = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 0,
				Margin = new Thickness(0, -1, 0, 0),
				Padding = new Thickness(0, 7, 0, 1),
				Spacing = 0
			};

			layout.Children.Add(stackContent);

			await stackContent.FadeTo(0, 0, null);

			ObservableCollection<Model.ConteudoDisciplina> listConteudoDisciplina = ConteudoBim.listConteudoDisciplinaByDisc<ConteudoDisciplina>(text);

			var backGroundColor = "#ffffff";
			foreach (ConteudoDisciplina conteudoDisciplina in listConteudoDisciplina)
			{
				if (backGroundColor.Equals("#f3f3f3"))
				{
					backGroundColor = "#ffffff";
				}
				else
				{
					backGroundColor = "#f3f3f3";
				}

				var lblTitle = new CustomLabel
				{
					Text = conteudoDisciplina.TipoProva,
					TextColor = Color.FromHex("#3ea6e1"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Bold",
					FontSize = 15,
				};

				var lblContentTitle = new CustomLabel
				{
					Text = App.Current.Resources["content"] as String,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5,
					Margin = new Thickness(0, 5, 0, -5)
				};

				var lblContent = new CustomLabel
				{
					Text = conteudoDisciplina.Conteudo,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5,
					LineBreakMode = LineBreakMode.TailTruncation,
					Margin = new Thickness(0, 0, 15, 0)
				};

				var stack = new StackLayout
				{
					Orientation = StackOrientation.Vertical,
					VerticalOptions = LayoutOptions.StartAndExpand,
					Children = { lblTitle, lblContentTitle, lblContent },
					BackgroundColor = Color.FromHex(backGroundColor),
					Padding = new Thickness(10, 20, 10, 20),
					Margin = new Thickness(0, -6, 0, 0)
				};

				stackContent.Children.Add(stack);

				stack.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async (obj) =>
					{
						await Utils.FadeView(stack);
						await Navigation.PushAsync(new ContentTestsDetail(text, conteudoDisciplina));
					}),
					CommandParameter = stack
				});
			}

			if (stackContent.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 10, 30, 10),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContent.Children.Add(lblErrorDayOfTheWeek);
			}

			layout.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					await Utils.FadeView(itemLabel);

					var rotate = 90;
					var fade = 0;
					uint time = 50;
					if (imgArrow.Rotation == 90)
					{
						rotate = -90;
						fade = 1;
						stackContent.HeightRequest = -1;
						time = 200;
						itemLabel.TextColor = Color.Black;
					}
					else
					{
						itemLabel.TextColor = Color.FromHex("#a3a3a3");
					}
					await imgArrow.RotateTo(rotate, 200, Easing.SpringOut);
					await stackContent.FadeTo(fade, time, null);
					if (rotate == 90)
					{
						stackContent.HeightRequest = 0;
					}

				}),
				CommandParameter = layout
			});

			return layout;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_GRADE, ((string)App.Current.Resources["content_tests"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
