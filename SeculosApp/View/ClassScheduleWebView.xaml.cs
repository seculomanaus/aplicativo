﻿using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ClassScheduleWebView : AbstractContentPage
	{

        string baseURl = "https://seculomanaus.com.br/componentes/portal/horario/aula/inicio?ra=";
        bool isResp;

        public ClassScheduleWebView()
		{

            InitializeComponent();

            setupBackArrow();

            lblName.Text = "Horário de Aula";

            isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

            string studentCode;

            if (isResp)
            {
                studentCode = SeculosBO.Instance.StudentSelected.CdAluno;
            }
            else
            {
                studentCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;

            }
            
            System.Diagnostics.Debug.WriteLine("cd aluno: " + studentCode);

            string url = baseURl + studentCode;

            System.Diagnostics.Debug.WriteLine("Program Content URL: " + url);

            navegador.Source = url;

		}

        private void setupBackArrow()
        {
            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) =>
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch (System.Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("webview back exception: " + exception.Message + " Type: " + exception.GetType());
                }

            };
            imgBack.GestureRecognizers.Add(back);
        }

        private void PagOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            load.IsVisible = true;
        }
        private void PagOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            load.IsVisible = false;
        }

    }
}