﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using SeculosApp.ViewModel;
using Xamarin.Forms;

namespace SeculosApp.View
{
	public partial class NewsView : RelativeLayout, IBindingContextView
	{
		AbstractContentPage AbstractContentPage;
		public NewsViewModel ViewModel
		{
			get 
			{
				return BindingContext as NewsViewModel; 
			}
		}

		public void changeBindingContext()
		{
			AbstractContentPage.BindingContext = this.BindingContext;
			var tapUpdate = new TapGestureRecognizer();
			tapUpdate.Tapped += (s, e) =>
			{
				Utils.FadeView(ViewModel.imgCloud);
				Utils.FadeView(ViewModel.lblError);
				Utils.FadeView(ViewModel.lblErrorUpdate);
				ViewModel.LoadNewsCommand.Execute(null);
				System.Diagnostics.Debug.WriteLine("tapUpdate LoadNewsCommand: ");
			};
			ViewModel.viewError.GestureRecognizers.Add(tapUpdate);
		}

		public NewsView()
		{
			InitializeComponent();
			AbstractContentPage = AbstractContentPage._AbstractContentPage;
			BindingContext = new NewsViewModel();

			ViewModel.lblError = (AbstractContentPage as MainScreenPage).LabelError as CustomLabel;
			ViewModel.lblErrorUpdate = (AbstractContentPage as MainScreenPage).LabelUpdate as CustomLabel;
			ViewModel.imgCloud = (AbstractContentPage as MainScreenPage).ImageCloud as Image;
			ViewModel.lblErrorEmpty = lblErrorNewsEmpty;
			ViewModel.viewError = (AbstractContentPage as MainScreenPage).ViewError as Xamarin.Forms.View;


			System.Diagnostics.Debug.WriteLine("ViewModel.viewError.IsVisible: " + ViewModel.viewError.IsVisible);


			changeBindingContext();

			Task.Factory.StartNew(() =>
		 	{
	 			Task.Delay(200).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						ViewModel.LoadNewsCommand.Execute(null);
					});
				});
			 });

			lblHeader.Text = Application.Current.Resources["welcome_sr"] as String + " " + SeculosBO.Instance.SeculosGeneral.UserConnected.NomeUsuario;


			var newsSelected = false;
			listViewNews.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{

				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				if (!newsSelected)
				{
					SeculosBO.Instance.NoticiaSelected = (SeculosApp.Model.Noticia)((ListView)sender).SelectedItem;
					if (SeculosBO.Instance.NoticiaSelected.isNotification)
					{
						// do something with e.SelectedItem
						((ListView)sender).SelectedItem = null; // de-select the row after ripple effect
						return;
					}
				}

				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect

				if (!newsSelected)
				{
					newsSelected = true;

					await Navigation.PushAsync(new NewsDetail());

					await Task.Factory.StartNew(() =>
						 {
							 Task.Delay(500).ContinueWith((obj) =>
							 {
								 newsSelected = false;
								 System.Diagnostics.Debug.WriteLine("newsSelected: " + newsSelected);
							 });

						 });

				}

			};

			btnReadMore.Clicked += async (sender, e) =>
			{
				ViewModel.AddMoreNews();
			};
		}
	}
}
