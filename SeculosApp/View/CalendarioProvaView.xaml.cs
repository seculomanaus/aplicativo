﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class CalendarioProvaView : RelativeLayout, ITabView
	{

		Command cmdClick;
		CustomLabel lblBimSelected;
		bool initializedView;

		public CalendarioProvaView(int position)
		{
			InitializeComponent();

			cmdClick = new Command(async (obj) => await onClick(obj));

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl1Bim.ClassId });
			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl2Bim.ClassId });
			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl3Bim.ClassId });
			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl4Bim.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(rounded1Bim);
			Utils.fixRoudedView(rounded2Bim);
			Utils.fixRoudedView(rounded3Bim);
			Utils.fixRoudedView(rounded4Bim);

			if (App.ScreenWidth <= 340)
			{
				lbl1Bim.WidthRequest = 52;
				lbl2Bim.WidthRequest = 52;
				lbl3Bim.WidthRequest = 52;
				lbl4Bim.WidthRequest = 52;

				rounded1Bim.WidthRequest = 52;
				rounded2Bim.WidthRequest = 52;
				rounded3Bim.WidthRequest = 52;
				rounded4Bim.WidthRequest = 52;
			}

            initLoad();

			if (position == 4)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							GetCaledarioProva();
						});
					});
				});
			}
		}


		public void GetCaledarioProva()
		{
			SeculosBO.Instance.GetCalendarioProva(SeculosBO.Instance.DependenteConnected, (msg) =>
			{
				var caledarioProva = SeculosBO.Instance.DependenteConnected.CalendarioProva;
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (caledarioProva == null || caledarioProva.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.BOLETIM_ERROR))
				{
					if (caledarioProva == null || caledarioProva.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.CALENDARIO_PROVA_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (caledarioProva == null || caledarioProva.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (caledarioProva != null && caledarioProva.Count > 0)
				{
					initView();
				}
				else
				{
					stackBimesters.IsVisible = true;
					boxSeparator.IsVisible = true;
					load.IsVisible = false;
				}

			});
		}

		public void initView()
		{
			//createHeaderBim();
			selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, 0);
			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		public async Task initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			await fadeAllBim();
			stackBimesters.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public async Task fadeAllBim()
		{
			await Task.WhenAll(
				stackContentItens1Bim.FadeTo(0, 0, null),
				stackContentItens2Bim.FadeTo(0, 0, null),
				stackContentItens3Bim.FadeTo(0, 0, null),
				stackContentItens4Bim.FadeTo(0, 0, null)
			);
			scrollContentItens1Bim.IsVisible = false;
			scrollContentItens2Bim.IsVisible = false;
			scrollContentItens3Bim.IsVisible = false;
			scrollContentItens4Bim.IsVisible = false;
		}

		public async void selectBim(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, int i)
		{
			rounded1Bim.Color = Color.Transparent;
			lbl1Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded2Bim.Color = Color.Transparent;
			lbl2Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded3Bim.Color = Color.Transparent;
			lbl3Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded4Bim.Color = Color.Transparent;
			lbl4Bim.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (lblBimSelected == null || !lblBimSelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllBim();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblBimSelected = labelSel;

			createItem(i, stackContentItens);
		}

		public void createItem(int i, StackLayout stackSel)
		{
			if (stackSel.Children.Count > 1)
				return;

			var calProva = SeculosBO.Instance.DependenteConnected.CalendarioProva;

			if (calProva == null || calProva.Count == 0)
				return;

			double[] widthsArray = getWidthLabel();

			int index = 0;

			stackSel.Children.Clear();

			foreach (Prova prova in calProva)
			{
				if (prova.Bimestre == i+1)
				{
					CreateBodyContetnt(prova, stackSel, widthsArray, (index % 2 == 0));
					index++;
				}	
			}

			stackSel.IsVisible = true;

			if (stackSel.Children.Count <= 0)
			{
				SeculosBO.Instance.showViewError(EnumCallback.CALENDARIO_PROVA_EMPTY, lblError, null);
			}
		}

		void CreateBodyContetnt(Prova prova, StackLayout stackSel, double[] widthsArray, bool colorChange)
		{
			var periodoLbl = "Periodo: {0}";
			var cursoLbl = "Curso: {0}";
			var serieLbl = "Série: {0}";
			var notaLbl = "Nota: {0}";
			var dataLbl = "Data: {0}";
			var tipoLbl = "Tipo: {0}";
			var chamadaLbl = "Chamada: {0}";

			var periodoTxt = new CustomLabel
			{
				Text = String.Format(periodoLbl, prova.Periodo),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var cursoTxt = new CustomLabel
			{
				Text = String.Format(cursoLbl, prova.NomeCurso),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var serieTxt = new CustomLabel
			{
				Text = String.Format(serieLbl, prova.Serie),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var notaTxt = new CustomLabel
			{
				Text = String.Format(notaLbl, prova.Nota),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var dataTxt = new CustomLabel
			{
				Text = String.Format(dataLbl, prova.DataProva),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var tipoTxt = new CustomLabel
			{
				Text = String.Format(tipoLbl, prova.Tipo),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};

			var chamadaTxt = new CustomLabel
			{
				Text = String.Format(chamadaLbl, prova.NmChamada),
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				FontType = "Medium",
				FontSize = 15,
			};


			var stackChildren = new StackLayout
			{
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				BackgroundColor = colorChange ? Color.FromHex("#E0F1FB") : Color.FromHex("#ffffff"),
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { periodoTxt, cursoTxt, serieTxt, notaTxt, dataTxt, tipoTxt, chamadaTxt },
				Padding = new Thickness(20, 20, 20, 20)
			};

			stackSel.Children.Add(stackChildren);
		}

		double[] getWidthLabel(double calc = 4)
		{
			double paddingLefRight = (App.ScreenWidth * 12) / 359.75;

			double width = App.ScreenWidth - (paddingLefRight * 2);

			double firstLabelWidth = width * 0.39;

			double fourthLabelWidth = (width * 0.61) / calc;

			double twoLabelWidth = (width * 0.61) / 2;

			double[] values = new double[4];
			values[0] = paddingLefRight;
			values[1] = firstLabelWidth;
			values[2] = fourthLabelWidth;
			values[3] = twoLabelWidth;

			return values;
		}

		async Task onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(lbl1Bim.ClassId))
				{
					lblError.IsVisible = false;
					selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, 0);
				}
				else if (str.Equals(lbl2Bim.ClassId))
				{
					lblError.IsVisible = false;
					selectBim(rounded2Bim, lbl2Bim, stackContentItens2Bim, scrollContentItens2Bim, 1);
				}
				else if (str.Equals(lbl3Bim.ClassId))
				{
					lblError.IsVisible = false;
					selectBim(rounded3Bim, lbl3Bim, stackContentItens3Bim, scrollContentItens3Bim, 2);
				}
				else if (str.Equals(lbl4Bim.ClassId))
				{
					lblError.IsVisible = false;
					selectBim(rounded4Bim, lbl4Bim, stackContentItens4Bim, scrollContentItens4Bim, 3);
				}

				else if (str.Equals(stkError.ClassId))
				{
					await initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								GetCaledarioProva();
								stackBimesters.IsVisible = true;
								boxSeparator.IsVisible = true;
								load.IsVisible = false;
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppeared()
		{
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, (string)App.Current.Resources["test_calendar_title"]);
			InitializeView();
		}

		public void OnPageDisappeared()
		{
		}

		public void OnPageDisappearing()
		{
		}
	}
}
