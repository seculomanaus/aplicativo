﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace SeculosApp
{
	public partial class NoteBookView : RelativeLayout, ITabView
	{
		CalendarView _calendarView;
		string dateSelected;
		bool initializedView = false;

		public NoteBookView(int position)
		{
			InitializeComponent();

			var calendar = false;
			var hasErrorVisible = false;
			var hasErrorConnectionVisible = false;
			var calendarTap = new TapGestureRecognizer();
			calendarTap.Tapped += async (s, e) =>
			{
				if (!App.GlobalBlockClick && !calendar && !load.IsVisible)
				{
					calendar = true;
					App.GlobalBlockClick = true;
					System.Diagnostics.Debug.WriteLine("calendarTap");

					await Utils.FadeView(btnCalendar);

					if (_calendarView == null)
					{
						createCalendar();
					}

					stackCalendar.IsVisible = !stackCalendar.IsVisible;
					boxBackCalendar.IsVisible = stackCalendar.IsVisible;
					dayActivities.IsVisible = !stackCalendar.IsVisible;

					if (hasErrorVisible)
					{
						lblError.IsVisible = true;
						hasErrorVisible = false;
					}

					if (stackCalendar.IsVisible && lblError.IsVisible && !hasErrorVisible)
					{
						lblError.IsVisible = false;
						hasErrorVisible = true;
					}

					if (hasErrorConnectionVisible)
					{
						stkError.IsVisible = true;
						hasErrorConnectionVisible = false;
					}

					if (stackCalendar.IsVisible && stkError.IsVisible && !hasErrorConnectionVisible)
					{
						stkError.IsVisible = false;
						hasErrorConnectionVisible = true;
					}

					Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((obj) =>
						{
							calendar = false;
							App.GlobalBlockClick = false;
						});
					});
				}
			};
			btnCalendar.GestureRecognizers.Add(calendarTap);

			var connectionTap = new TapGestureRecognizer();
			connectionTap.Tapped += async (s, e) =>
			{
				if (!App.GlobalBlockClick && !calendar && !load.IsVisible)
				{
					App.GlobalBlockClick = true;
					System.Diagnostics.Debug.WriteLine("connectionTap");

					await Utils.FadeView(stkError);

					initLoad();
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((obj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								changeDate(DateTime.Now.ToString("d"));
							});
						});
					});

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(150).ContinueWith((obj) =>
						{
							App.GlobalBlockClick = false;
						});
					});
				}
			};
			stkError.GestureRecognizers.Add(connectionTap);

			lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[DateTime.Now.Month]);
			lblDay.Text = DateTime.Now.Day.ToString();
			dayActivities.Text = "Atividades do dia " + lblDay.Text + " de " + lblMonth.Text;

			initLoad();

			if (position == 0)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{ 
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							changeDate(DateTime.Now.ToString("d"));
						});
					});
				});
			}
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			lblError.IsVisible = false;
			load.IsVisible = true;
			stackContentItens.IsVisible = false;
		}

		public void changeDate(string date)
		{
			dateSelected = date;
			SeculosBO.Instance.getDiarioWS(SeculosBO.Instance.DependenteConnected, date, (msg) =>
			{
				var listDiario = SeculosBO.Instance.DependenteConnected.listDiarioByDate<Diario>(date);
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listDiario == null || listDiario.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_DIARIO_ERROR))
				{
					if (listDiario == null || listDiario.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_DIARIO_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listDiario == null || listDiario.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}
				stackContentItens.Children.Clear();
				if (listDiario != null && listDiario.Count > 0)
				{
					initView();
				}
				else
				{
					load.IsVisible = false;
					stackContentItens.IsVisible = true;
				}
			});
		}

		public void initView()
		{ 
			var listDiario = SeculosBO.Instance.DependenteConnected.listDiarioByDate<Diario>(dateSelected);
			var backGroundColor = "#643ea6e1";
			foreach (Diario diario in listDiario)
			{
				stackContentItens.Children.Add(createItem(diario, backGroundColor));
				if (backGroundColor.Equals("#643ea6e1"))
				{
					backGroundColor = "#87e0f1fb";
				}
				else
				{
					backGroundColor = "#643ea6e1";
				}
			}

			load.IsVisible = false;
			stackContentItens.IsVisible = true;
		}

		public StackLayout createItem(Diario item, string backGroundColor)
		{
			var lblTitle = new CustomLabel
			{
				Text = item.NmDisciplina,
				TextColor = Color.FromHex("#3ea6e1"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Bold",
				FontSize = 15,
			};

			var stack = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { lblTitle },
				Padding = new Thickness(15, 15, 15, 15),
				Margin = new Thickness(0, -3, 0, -3),
				BackgroundColor = Color.FromHex(backGroundColor)

			};

			if (item.NomeProfessor != null && !item.NomeProfessor.Equals(string.Empty))
			{
				var lblTeacher = new CustomLabel
				{
					Text = App.Current.Resources["teacher"] as String,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5,
					Margin = new Thickness(0, 5, 0, -6)
				};

				var lblTeacherName = new CustomLabel
				{
					Text = item.NomeProfessor,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5
				};

				stack.Children.Add(lblTeacher);
				stack.Children.Add(lblTeacherName);
			}

			if (item.DcConteudo != null && !item.DcConteudo.Equals(string.Empty))
			{
				var lblContent = new CustomLabel
				{
					Text = App.Current.Resources["content"] as String,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5,
					Margin = new Thickness(0, 5, 0, -6)
				};

				var lblContentDesc = new CustomLabel
				{
					Text = item.DcConteudo,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5
				};

				stack.Children.Add(lblContent);
				stack.Children.Add(lblContentDesc);
			}

			if (item.DcTarefa != null && !item.DcTarefa.Equals(string.Empty))
			{
				var lblTask = new CustomLabel
				{
					Text = App.Current.Resources["task"] as String,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5,
					Margin = new Thickness(0, 5, 0, -6)
				};

				var lblTaskDesc = new CustomLabel
				{
					Text = item.DcTarefa,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 14.5
				};

				stack.Children.Add(lblTask);
				stack.Children.Add(lblTaskDesc);
			}

			return stack;
		}


		public void createCalendar()
		{
			_calendarView = new CalendarView()
			{
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.Center,
				MinDate = CalendarView.FirstDayOfMonth(new DateTime(DateTime.Now.Year, 1, 1)),
				MaxDate = CalendarView.LastDayOfMonth(new DateTime(DateTime.Now.Year, 12, 1)),
				ShouldHighlightDaysOfWeekLabels = false,
				SelectionBackgroundStyle = CalendarView.BackgroundStyle.CircleFill,
				ShowNavigationArrows = true,
				NavigationArrowsColor = Color.FromHex("#3ea6e1"),
				DateBackgroundColor = Color.White,
				BackgroundColor = Color.White,
				DateSeparatorColor = Color.White,
				SelectedDateBackgroundColor = Color.FromHex("#3ea6e1"),
				SelectedDateForegroundColor = Color.White,
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				_calendarView.WidthRequest = App.ScreenWidth;
				_calendarView.Layout(new Rectangle(0, App.ScreenWidth, App.ScreenWidth - 30, _calendarView.HeightRequest));
				if (App.ScreenWidth > 340)
				{
					_calendarView.Margin = new Thickness(37, -20, 0, 0);
				}
				else
				{
					_calendarView.WidthRequest = App.ScreenWidth - 30;
					_calendarView.Margin = new Thickness(16, -15, 0, 0);
					stackCalendar.WidthRequest = App.ScreenWidth;
				}
			}
			else
			{
				_calendarView.Margin = new Thickness(0, -10, 0, 0);
			}

			stackCalendar.Children.Add(_calendarView);

			_calendarView.DateSelected += (object sender, DateTime e) =>
			{
				initLoad();
				Task.Factory.StartNew(() =>
				{
					Task.Delay(150).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							changeDate(e.ToString("d"));
						});
					});
				});

				lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[e.Month]);
				lblDay.Text = e.Day.ToString();
				dayActivities.Text = "Atividades do dia " + lblDay.Text + " de " + lblMonth.Text;
				stackCalendar.IsVisible = false;
				dayActivities.IsVisible = true;
				boxBackCalendar.IsVisible = false;
				Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);
			};
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, ((string)App.Current.Resources["nootbook"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
