﻿using System;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class NotificationDetailView : AbstractContentPage
	{
		public NotificationDetailView()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;
			var back = new TapGestureRecognizer();
			back.Tapped += async (s, e) =>
			{
				await Utils.FadeView(imgBack);

				try
				{
					await Navigation.PopAsync();
				}
				catch (System.Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("NotificationDetailView back exception: " + exception.Message + " Type: " + exception.GetType());
				}
			};
			imgBack.GestureRecognizers.Add(back);

			var Notification = SeculosBO.Instance.NotificationSelected;
			if (Notification != null)
			{
				SeculosBO.Instance.saveReadNotification(Notification);

				var html = new HtmlWebViewSource();

				var htmlTemp = @"<html>
							<body style='padding: 0px 30px 0px 30px;'>
								<label style='color: #000; font-size: 60px; font-weight: bold; display: inline-block;'>" + Notification.NomeAlerta + "</label>" +
							   "<label style='color: #a3a3a3; font-size: 36px; margin-top: 8px; width: 100%; text-align: left; display: inline-block;'>" + Notification.DataCadastro + "</label>";

				if (!Notification.DsFoto.Equals(SeculosBO.EMPTY))
				{
					htmlTemp += "<img width='100%' src='" + Notification.DsFoto + "' style='display: inline-block; margin-top: 10px;'/>";
				}

				htmlTemp += "<div style='color: #a3a3a3; margin-top: 10px; display: inline-block; font-size: 42px;'>" + Notification.DsAlert + "</div>" +
							"</body>" +
							"</html>";
				
				html.Html = htmlTemp;
				webViewDesc.Source = html;

				webViewDesc.Navigating += (s, e) =>
				{
					if (e.Url.StartsWith("http") || e.Url.StartsWith("https"))
					{
						try
						{
							var uri = new Uri(e.Url);
							Device.OpenUri(uri);
						}
						catch (Exception exception)
						{
							System.Diagnostics.Debug.WriteLine("webViewDesc Exception: "+exception.Message);
						}

						e.Cancel = true;
					}
				};

				webViewDesc.Navigated += (sender, e) =>
				{
					System.Diagnostics.Debug.WriteLine("webViewDesc Navigated");
				};
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;
		}
	}
}
