﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class RelatorioPedagogicoInfantilView : RelativeLayout, ITabView
	{
		Command cmdClick;
		CustomLabel lblBimSelected;

		public RelatorioPedagogicoInfantilView()
		{
			InitializeComponent();

			cmdClick = new Command((obj) => onClick(obj));

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl1Bim.ClassId });
			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl2Bim.ClassId });
			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl3Bim.ClassId });
			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl4Bim.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(rounded1Bim);
			Utils.fixRoudedView(rounded2Bim);
			Utils.fixRoudedView(rounded3Bim);
			Utils.fixRoudedView(rounded4Bim);

			initLoad();

			Task.Factory.StartNew(() =>
			{
				Task.Delay(200).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						getAcompanhamentoInfantil();
					});
				});
			});
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			fadeAllBim();
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			stackBimesters.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public async Task fadeAllBim()
		{
			await Task.WhenAll(
				stackContentItens1Bim.FadeTo(0, 0, null),
				stackContentItens2Bim.FadeTo(0, 0, null),
				stackContentItens3Bim.FadeTo(0, 0, null),
				stackContentItens4Bim.FadeTo(0, 0, null)
			);
			scrollContentItens1Bim.IsVisible = false;
			scrollContentItens2Bim.IsVisible = false;
			scrollContentItens3Bim.IsVisible = false;
			scrollContentItens4Bim.IsVisible = false;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(lbl1Bim.ClassId))
				{
					selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim);
				}
				else if (str.Equals(lbl2Bim.ClassId))
				{
					selectBim(rounded2Bim, lbl2Bim, stackContentItens2Bim, scrollContentItens2Bim);
				}
				else if (str.Equals(lbl3Bim.ClassId))
				{
					selectBim(rounded3Bim, lbl3Bim, stackContentItens3Bim, scrollContentItens3Bim);
				}
				else if (str.Equals(lbl4Bim.ClassId))
				{
					selectBim(rounded4Bim, lbl4Bim, stackContentItens4Bim, scrollContentItens4Bim);
				}
				else if (str.Equals(stkError.ClassId))
				{
					initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(250).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getAcompanhamentoInfantil();
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public async void selectBim(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll)
		{
			rounded1Bim.Color = Color.Transparent;
			lbl1Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded2Bim.Color = Color.Transparent;
			lbl2Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded3Bim.Color = Color.Transparent;
			lbl3Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded4Bim.Color = Color.Transparent;
			lbl4Bim.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (lblBimSelected == null || !lblBimSelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllBim();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblBimSelected = labelSel;
		}

		public void createEmptyitem(StackLayout stackContentItens)
		{
			if (stackContentItens.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 60, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				stackContentItens.HorizontalOptions = LayoutOptions.CenterAndExpand;
			}
		}

		public void getAcompanhamentoInfantil()
		{
			SeculosBO.Instance.getAcompanhamentoInfantilWS(SeculosBO.Instance.DependenteConnected, (msg) =>
			{
				var listAcompanhamentoInfantil = SeculosBO.Instance.DependenteConnected.listAcompanhamentoInfantil;
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listAcompanhamentoInfantil == null || listAcompanhamentoInfantil.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.INFANTIL_ERROR))
				{
					if (listAcompanhamentoInfantil == null || listAcompanhamentoInfantil.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.INFANTIL_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.INFANTIL_ERROR))
				{
					if (listAcompanhamentoInfantil == null || listAcompanhamentoInfantil.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listAcompanhamentoInfantil != null && listAcompanhamentoInfantil.Count > 0)
				{
					selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim);
					createAllitens();
				}
			});
		}

		public async void createAllitens()
		{
			stackContentItens1Bim.IsVisible = false;
			stackContentItens2Bim.IsVisible = false;
			stackContentItens3Bim.IsVisible = false;
			stackContentItens4Bim.IsVisible = false;
			SeculosBO.Instance.DependenteConnected.createListInfantilBim();

			foreach (String key in SeculosBO.Instance.DependenteConnected.Infantil1Bim.mapAcompanhamentoInfantilDivisao.Keys)
			{
				System.Diagnostics.Debug.WriteLine("createAllitens key: " + key);
				if (key != null && !key.Equals(String.Empty))
				{
					Task<RelativeLayout> task = CreateItemLayout(key, SeculosBO.Instance.DependenteConnected.Infantil1Bim);
					if (task != null)
					{
						RelativeLayout item = await task;
						item.ClassId = key;
						stackContentItens1Bim.Children.Add(item);
						item.IsVisible = true;
					}
				}
			}

			createEmptyitem(stackContentItens1Bim);

			foreach (String key in SeculosBO.Instance.DependenteConnected.Infantil2Bim.mapAcompanhamentoInfantilDivisao.Keys)
			{
				System.Diagnostics.Debug.WriteLine("createAllitens key: " + key);
				if (key != null && !key.Equals(String.Empty))
				{
					Task<RelativeLayout> task = CreateItemLayout(key, SeculosBO.Instance.DependenteConnected.Infantil2Bim);
					if (task != null)
					{
						RelativeLayout item = await task;
						item.ClassId = key;
						stackContentItens2Bim.Children.Add(item);
						item.IsVisible = true;
					}
				}
			}
			stackContentItens2Bim.IsVisible = true;
			createEmptyitem(stackContentItens2Bim);

			foreach (String key in SeculosBO.Instance.DependenteConnected.Infantil3Bim.mapAcompanhamentoInfantilDivisao.Keys)
			{
				System.Diagnostics.Debug.WriteLine("createAllitens key: " + key);
				if (key != null && !key.Equals(String.Empty))
				{
					Task<RelativeLayout> task = CreateItemLayout(key, SeculosBO.Instance.DependenteConnected.Infantil3Bim);
					if (task != null)
					{
						RelativeLayout item = await task;
						item.ClassId = key;
						stackContentItens3Bim.Children.Add(item);
						item.IsVisible = true;
					}
				}
			}
			stackContentItens3Bim.IsVisible = true;
			createEmptyitem(stackContentItens3Bim);

			foreach (String key in SeculosBO.Instance.DependenteConnected.Infantil4Bim.mapAcompanhamentoInfantilDivisao.Keys)
			{
				System.Diagnostics.Debug.WriteLine("createAllitens key: " + key);
				if (key != null && !key.Equals(String.Empty))
				{
					Task<RelativeLayout> task = CreateItemLayout(key, SeculosBO.Instance.DependenteConnected.Infantil4Bim);
					if (task != null)
					{
						RelativeLayout item = await task;
						item.ClassId = key;
						stackContentItens4Bim.Children.Add(item);
						item.IsVisible = true;
					}
				}
			}
			createEmptyitem(stackContentItens4Bim);

			stackContentItens1Bim.IsVisible = true;
			stackContentItens2Bim.IsVisible = true;
			stackContentItens3Bim.IsVisible = true;
			stackContentItens4Bim.IsVisible = true;


			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		public async Task<RelativeLayout> CreateItemLayout(string text, InfantilBim InfantilBim)
		{
			var layout = new RelativeLayout
			{
				Margin = new Thickness(0, -8, 0, 5),
			};

			var itemLabel = new CustomLabel
			{
				Text = text,
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				HeightRequest = 34,
				FontSize = 15,
				Margin = new Thickness(10, 0, 30, 0)
			};

			var imgArrow = new Image
			{
				Source = "arrow_right_yellow_ic.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				WidthRequest = 26,
				HeightRequest = 18,
				Margin = new Thickness(0, 0, 20, 0),
				Rotation = 90
			};

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(10, 8, 10, 0)
			};

			layout.Children.Add(
				imgArrow,
				Constraint.RelativeToParent((parent) => { return parent.Width - 36; }),
				Constraint.Constant(18)
			);

			layout.Children.Add(
				itemLabel,
				Constraint.Constant(0),
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			layout.Children.Add(
				lineSeperator,
				Constraint.Constant(0),
				Constraint.RelativeToView(itemLabel, (parent, view) => { return view.Y + view.Height + 6; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			var stackContent = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				HeightRequest = 0,
				Margin = new Thickness(0, -1, 0, 0),
				Padding = new Thickness(0, 1, 0, 1),
				Spacing = 0
			};

			layout.Children.Add(
				stackContent,
				Constraint.Constant(0),
				Constraint.RelativeToView(lineSeperator, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			await stackContent.FadeTo(0, 0, null);

			ObservableCollection<Model.InfantilDivisao> listInfantilDivisao = InfantilBim.listConteudoDisciplinaByDisc<InfantilDivisao>(text);

			var backGroundColor = "#ffffff";
			foreach (InfantilDivisao infantilDivisao in listInfantilDivisao)
			{
				if (backGroundColor.Equals("#f3f3f3"))
				{
					backGroundColor = "#ffffff";
				}
				else
				{
					backGroundColor = "#f3f3f3";
				}

				var lblTitle = new CustomLabel
				{
					Text = infantilDivisao.Pergunta,
					WidthRequest = App.ScreenWidth * 0.6,
					TextColor = Color.FromHex("#555555"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					FontType = "Medium",
					FontSize = 13.5,
				};

				var lblResp = new CustomLabel
				{
					Text = infantilDivisao.Resposta,
					WidthRequest = App.ScreenWidth * 0.3,
					TextColor = Color.FromHex("#a3a3a3"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 13.5,
				};

				if (infantilDivisao.Resposta.ToUpper().Equals("SIM"))
				{
					lblResp.TextColor = Color.FromHex("#41c454");
				}
				else if (infantilDivisao.Resposta.ToUpper().Equals("ÀS VEZES"))
				{
					lblResp.TextColor = Color.FromHex("#f8ce33");
				}
				else if (infantilDivisao.Resposta.ToUpper().Equals("NÃO"))
				{
					lblResp.TextColor = Color.FromHex("#d74e54");
				}

				var stack = new StackLayout
				{
					Orientation = StackOrientation.Horizontal,
					VerticalOptions = LayoutOptions.StartAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { lblTitle, lblResp },
					BackgroundColor = Color.FromHex(backGroundColor),
					Padding = new Thickness(10, 10, 10, 10),
					Spacing = 0
				};

				stackContent.Children.Add(stack);

			}

			if (stackContent.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 10, 30, 10),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContent.Children.Add(lblErrorDayOfTheWeek);
			}

			layout.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					await Utils.FadeView(itemLabel);

					var rotate = 90;
					var fade = 0;
					uint time = 50;
					if (imgArrow.Rotation == 90)
					{
						rotate = -90;
						fade = 1;
						stackContent.HeightRequest = -1;
						time = 200;
						itemLabel.TextColor = Color.Black;
					}
					else
					{
						itemLabel.TextColor = Color.FromHex("#a3a3a3");
					}
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					imgArrow.RotateTo(rotate, 200, Easing.SpringOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					await stackContent.FadeTo(fade, time, null);
					if (rotate == 90)
					{
						stackContent.HeightRequest = 0;
					}

				}),
				CommandParameter = layout
			});

			return layout;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, ((string)App.Current.Resources["relatorio_pedagogico_infantil"]).ToUpper());
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
