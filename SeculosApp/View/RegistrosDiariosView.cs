﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace SeculosApp
{
	public class RegistrosDiariosView : RelativeLayout, ITabView
	{
		CustomLabel lblErrorRegistro;
		ActivityIndicator load;
		ObservableCollection<RegistroDiario> listRegistro;
		CalendarView _calendarView;
		StackLayout stackCalendar;
		Image imgCalendar;
		Image imgArrow;
		MotoraListView lvRegistro;
		StackLayout calendarImgsLayout;
		Command click;
		CustomLabel monthLabel;
		CustomLabel daySelectedLabel;
		CustomLabel legendLabel;
		bool initializedView;

		public RegistrosDiariosView(int position)
		{
			BackgroundColor = Color.White;
			ClassId = "RegistrosDiariosView";

			click = new Command((obj) => onClick(obj));

			var calendarioHeader = CreateCalendarHeaderItem();

			var lineSeparator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(20, -6, 20, 0)
			};

			stackCalendar = new StackLayout
			{
				BackgroundColor = Color.White
			};

			lblErrorRegistro = new CustomLabel
			{
				FontSize = 14, 
				TextColor = (Color) App.Current.Resources["gray_light"],
				Margin = new Thickness(30, 0, 30, 0),
				FontType = "Medium",
				VerticalTextAlignment = TextAlignment.Center, 
				HorizontalTextAlignment = TextAlignment.Center,
				IsVisible = true
			};

			legendLabel = new CustomLabel
			{
				FontSize = 14,
				Margin = new Thickness(20, 0, 0, 0),
				TextColor = Color.FromHex("#d3d3d3"),
				FontType = "Medium"
			};

			load = new ActivityIndicator
			{
				Color = (Color) App.Current.Resources["blue_light_label"],
				IsVisible = false,
				IsRunning = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			lvRegistro = new MotoraListView
			{
				HasUnevenRows = true,
				SeparatorVisibility = SeparatorVisibility.None,
				ItemTemplate = new DataTemplate(typeof(RegistroDiarioCell)),
				//ItemsSource = listRegistro,
				IsVisible = false
			};

			lvRegistro.ItemTapped += (sender, e) =>
			{
				if (e == null) return; // has been set to null, do not 'process' tapped event
				((ListView)sender).SelectedItem = null; // de-select the row
			};

			var mainStack = new StackLayout
			{
				Children = { calendarioHeader, lineSeparator, legendLabel }
			};

			this.Children.Add(
				mainStack,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; })
			);

			this.Children.Add(
				lblErrorRegistro,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .5; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				lvRegistro,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .85; })
			);

			this.Children.Add(
				stackCalendar,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				load,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			if (position == 6)
			{
				InitializeView();
			}
		}

		void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				load.IsVisible = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							changeDate(DateTime.Now);
						});
					});
				});
			}
		}


		async void onClick(object obj)
		{
			var str = (string)obj;

			if (str.Equals(calendarImgsLayout.ClassId))
			{
				await Utils.FadeView(calendarImgsLayout);

				if (_calendarView == null)
				{
					createCalendar();
				}
				else if (stackCalendar.Height == 0)
				{
					stackCalendar.HeightRequest = _calendarView.Height;
				}
				else
				{
					stackCalendar.HeightRequest = 0;
				}

				Utils.rotateArrowCalendar(imgArrow, imgCalendar);
			}
		}

		void createCalendar()
		{
			_calendarView = new CalendarView()
			{
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				MinDate = CalendarView.FirstDayOfMonth(new DateTime(DateTime.Now.Year, 1, 1)),
				MaxDate = CalendarView.LastDayOfMonth(new DateTime(DateTime.Now.Year, 12, 1)),
				ShouldHighlightDaysOfWeekLabels = false,
				SelectionBackgroundStyle = CalendarView.BackgroundStyle.CircleFill,
				ShowNavigationArrows = true,
				NavigationArrowsColor = Color.FromHex("#3ea6e1"),
				Margin = new Thickness(10, -15, 10, 20),
				DateBackgroundColor = Color.White,
				BackgroundColor = Color.Transparent,
				DateSeparatorColor = Color.White,
				SelectedDateBackgroundColor = Color.FromHex("#3ea6e1"),
				SelectedDateForegroundColor = Color.White
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				_calendarView.WidthRequest = App.ScreenWidth;
				_calendarView.Layout(new Rectangle(0, App.ScreenWidth, App.ScreenWidth, _calendarView.HeightRequest));
				if (App.ScreenWidth > 340)
				{
					_calendarView.Margin = new Thickness(42, 0, 0, 0);
				}
				else
				{
					_calendarView.Margin = new Thickness(0, -5, 0, 0);
				}
			}

			stackCalendar.Children.Add(_calendarView);

			_calendarView.DateSelected += (object sender, DateTime e) =>
			{
				Task.Factory.StartNew(() =>
				{
					Task.Delay(150).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							changeDate(e);
						});
					});
				});

				stackCalendar.HeightRequest = 0;

				Utils.rotateArrowCalendar(imgArrow, imgCalendar);
			};
		}

		void changeDate(DateTime date)
		{
			monthLabel.Text = Utils.PascalCase(SeculosBO.Instance.Months[date.Month]);
			daySelectedLabel.Text = date.Day.ToString();
			legendLabel.Text = "Registros do dia " + date.Day + " de " + Utils.PascalCase(SeculosBO.Instance.Months[date.Month]);

			load.IsVisible = true;

			SeculosBO.Instance.getRegistroDiarioWait(SeculosBO.Instance.DependenteConnected, date.ToString("d"), (msg) =>
			{
				listRegistro = SeculosBO.Instance.DependenteConnected.listRegistroDiarioByDate<RegistroDiario>(date.ToString("d"));

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listRegistro == null || listRegistro.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_REGISTRO_DIARIO_EMPTY, lblErrorRegistro, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_REGISTRO_DIARIO_ERROR))
				{
					if (listRegistro == null || listRegistro.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_REGISTRO_DIARIO_ERROR, lblErrorRegistro, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listRegistro == null || listRegistro.Count == 0)
					{
						//stkError.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_REGISTRO_DIARIO_ERROR, lblErrorRegistro, null);
					}
				}

				lvRegistro.IsVisible = false;

				if (listRegistro != null && listRegistro.Count > 0)
				{
					//foreach (RegistroDiario registro in listRegistro)
					//{
					//	stackContentItens.Children.Add(createItem(ocorrencia));
					//}
					lvRegistro.IsVisible = true;
					lblErrorRegistro.IsVisible = false;
					lvRegistro.ItemsSource = listRegistro;

				}
				else 
				{
					lblErrorRegistro.IsVisible = true;
				}

				load.IsVisible = false;
				//stackContentItens.IsVisible = true;
			});
		}

		StackLayout CreateCalendarHeaderItem()
		{
			monthLabel = new CustomLabel
			{
				TextColor = Color.Black,
				FontSize = 15,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontType = "Medium",
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center
			};

			imgCalendar = new Image
			{
				Source = "calendar_ic.png",
				Aspect = Aspect.AspectFit,
				WidthRequest = 28,
				Margin = new Thickness(0, 0, 5, 0)
			};

			daySelectedLabel = new CustomLabel
			{
				FontSize = 14,
				Margin = new Thickness(0, 0, 35, 0),
				TextColor = (Color) App.Current.Resources["gray"],
				VerticalOptions = LayoutOptions.Center
			};

			imgArrow = new Image
			{
				Source = "arrow_up_yellow_ic.png",
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 5, 0),
				WidthRequest = 20,
				Rotation = 180
			};

			calendarImgsLayout = new StackLayout
			{
				ClassId = "calendarImgsLayout",
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center,
				Margin = new Thickness(0, 0, 5, 0),
				Children = { imgCalendar, daySelectedLabel, imgArrow }
			};
			calendarImgsLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = calendarImgsLayout.ClassId });

			var calendarHeaderLayout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Center,
				Margin = new Thickness(20, 10, 10, 15),
				Children = { monthLabel, calendarImgsLayout }
			};

			return calendarHeaderLayout;
		}

		public void OnPageAppeared()
		{
			// Do nothing
		}

		public void OnPageAppearing()
		{
			InitializeView();
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, ((string)App.Current.Resources["registos_diarios"]).ToUpper());
		}

		public void OnPageDisappeared()
		{
			// Do nothing
		}

		public void OnPageDisappearing()
		{
			// Do nothing
		}
	}

	class RegistroDiarioCell : ViewCell
	{
		public RegistroDiarioCell()
		{
			var registerLabel = new CustomLabel
			{
				FontSize = 14,
				FontType = "Bold",
				TextColor = Color.FromHex("#939393")
			};
			registerLabel.SetBinding(Label.TextProperty, "Registro");

			var titleLabel = new CustomLabel
			{
				FontSize = 17,
				FontType = "Bold",
				TextColor = (Color)App.Current.Resources["blue_light_label"]
			};
			titleLabel.SetBinding(Label.TextProperty, "Tipo");

			var textLabel = new CustomLabel
			{
				FontSize = 15,
				FontType = "Medium",
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				TextColor = Color.FromHex("#a8a8a8")
			};
			textLabel.SetBinding(Label.TextProperty, "DsRegistroTruncated");

			var lineSeparator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Margin = new Thickness(0, 10, 0, 10),
				Opacity = .5
			};

			var layout = new StackLayout
			{
				Margin = new Thickness(20,10,20,0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 10,
				Children = { registerLabel, titleLabel, textLabel, lineSeparator }
			};

			this.View = layout;
		}
	}
}