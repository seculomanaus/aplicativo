﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class CantinaView : RelativeLayout, ITabView
	{
		Command cmdClick;
		RelativeLayout cardapioLayout, addCreditoLayout, limiteLayout;
		bool blockClick;
		public BaseExpandedView Page;

		public CantinaView(BaseExpandedView Page)
		{
			ClassId = "CantinaView";
			this.Page = Page;
			cmdClick = new Command((obj) => onClick(obj));

			BackgroundColor = Color.White;

            cardapioLayout = Utils.CreateItemLayout("Cardápio Semanal");
			addCreditoLayout = Utils.CreateItemLayout("Crédito e Extrato");
			limiteLayout = Utils.CreateItemLayout("Limite de Crédito Diário");

			cardapioLayout.ClassId = "cardapioLayout";
			addCreditoLayout.ClassId = "addCreditoLayout";
			limiteLayout.ClassId = "limiteLayout";
			
			cardapioLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = cardapioLayout.ClassId });
			addCreditoLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = addCreditoLayout.ClassId });
			limiteLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = limiteLayout.ClassId });
			
			var stack = new StackLayout
			{
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { cardapioLayout, addCreditoLayout, limiteLayout},
				Margin = new Thickness(0, 8, 0, 0)

			};

            Children.Add(
				stack,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);
		}


		async void onClick(object obj)
		{

			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(cardapioLayout.ClassId))
				{
					await Utils.FadeView(cardapioLayout);
					await Navigation.PushAsync(new CardapioWebView());
				}
                else if (str.Equals(addCreditoLayout.ClassId))
				{
					await Utils.FadeView(addCreditoLayout);
					await Navigation.PushAsync(new AddBalancePage());
				}
				else if (str.Equals(limiteLayout.ClassId))
				{
					await Utils.FadeView(limiteLayout);
					SeculosBO.Instance.OpenModal(EnumModal.CREDIT_LIMIT, true, Page);
				}
				

                    await Task.Factory.StartNew(() =>
				{
					Task.Delay(500).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppearing()
		{
            App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL, (string)App.Current.Resources["cantina"]);
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
