﻿using Xamarin.Forms;

namespace SeculosApp
{
	public partial class MoreInfoView : ContentPage
	{
		public MoreInfoView()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				Padding = new Thickness(0, 20, 0, 0);
			}


			var back = new TapGestureRecognizer();
			back.Tapped += async (s, e) =>
			{
				await Utils.FadeView(imgBack);

				try
				{
					await Navigation.PopAsync();
				}
				catch (System.Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("MoreInfoView back exception: " + exception.Message + " Type: " + exception.GetType());
				}
			};
			imgBack.GestureRecognizers.Add(back);
		}
	}
}
