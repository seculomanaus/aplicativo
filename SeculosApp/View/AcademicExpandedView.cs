﻿using Xamarin.Forms;

namespace SeculosApp
{
	public class AcademicExpandedView : BaseExpandedView
	{
		public AcademicExpandedView(int _position, string _pageId) : base(_position, _pageId)
		{
            // position = Page in carousel
            // 0 -> GradeBoletimView
            // 1 -> FrequenciaView
            // 2 -> StatementNotesView
            // 3 -> RegistrosDiariosView
            // 4 -> RelatorioPedagogicoInfantilView (somente existe no caso de aluno do tipo INFANTIL)
            tabsLayout.BackgroundColor = Color.FromHex("#FFAB00");
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, (obj, arg) =>
			{
				try
				{
					var str = (string)arg;
                    if (((string)App.Current.Resources["relatorio_pedagogico_infantil"]).ToUpper().Equals(str))
                    {
                        if (Device.OS.Equals(TargetPlatform.iOS))
                        {
                            if (App.ScreenWidth <= 340)
                            {
                                tabTitle.FontSize = 12.5;
                            }
                            else if (App.ScreenWidth <= 375)
                            {
                                tabTitle.FontSize = 15;
                            }
                        }
                        else
                        {
                            // A5
                            if (App.ScreenWidth == 359.75)
                            {
                                tabTitle.FontSize = 12;
                            }
                            else
                            {
                                tabTitle.FontSize = 13.3;
                            }
                        }
                    }
                    else if (((string)App.Current.Resources["statement_notes"]).ToUpper().Equals(str))
                    {
                        if (Device.OS.Equals(TargetPlatform.iOS))
                        {
                            if (App.ScreenWidth <= 340)
                            {
                                tabTitle.FontSize = 14;
                            }
                            else if (App.ScreenWidth <= 375)
                            {
                                tabTitle.FontSize = 15;
                            }
                        }
                        else
                        {
                            // A5
                            if (App.ScreenWidth == 359.75)
                            {
                                tabTitle.FontSize = 15;
                            }
                            else
                            {
                                tabTitle.FontSize = 16;
                            }
                        }
                    }
                    else
                    {
                        tabTitle.FontSize = 18;
                    }
					tabTitle.Text = str;
				}
				catch { }
			});
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC);
		}

		public override void handleCarouselPage()
		{
			var title = "";
			tabTitle.FontSize = 18;

			if (position < carouselMotora.Children.Count)
			{
				if (position == 0)
				{
					title = ((string)App.Current.Resources["grade"]).ToUpper();
				}
				else if (position == 1)
				{
					title = ((string)App.Current.Resources["frequency"]).ToUpper();
					
				}
				else if (position == 2)
				{
					title = ((string)App.Current.Resources["statement_notes"]).ToUpper();
                    if (Device.OS.Equals(TargetPlatform.iOS))
                    {
                        if (App.ScreenWidth <= 340)
                        {
                            tabTitle.FontSize = 15.5;
                        }
                        else
                        {
                            tabTitle.FontSize = 16.5;
                        }
                    }
                    else
                    {
                        // A5
                        if (App.ScreenWidth == 359.75)
                        {
                            tabTitle.FontSize = 15.5;
                        }
                        else
                        {
                            tabTitle.FontSize = 16.5;
                        }
                    }
                }
				else if (position == 3)
				{
					title = ((string)App.Current.Resources["registos_diarios"]).ToUpper();
				}
				else if (position == 4)
				{
                    if (Device.OS.Equals(TargetPlatform.iOS))
                    {
                        if (App.ScreenWidth <= 340)
                        {
                            tabTitle.FontSize = 12.5;
                        }
                        else if (App.ScreenWidth <= 375)
                        {
                            tabTitle.FontSize = 15;
                        }
                    }
                    else
                    {
                        // A5
                        if (App.ScreenWidth == 359.75)
                        {
                            tabTitle.FontSize = 12;
                        }
                        else
                        {
                            tabTitle.FontSize = 13.3;
                        }
                    }
                    title = ((string)App.Current.Resources["relatorio_pedagogico_infantil"]).ToUpper();
				}
				else if (position == 5)
				{
					if (SeculosBO.Instance.DependenteConnected.IsInfantil())
					{
						title = ((string)App.Current.Resources["relatorio_pedagogico_infantil"]).ToUpper();
						if (Device.OS.Equals(TargetPlatform.iOS))
						{
							if (App.ScreenWidth <= 340)
							{
								tabTitle.FontSize = 12.5;
							}
							else if (App.ScreenWidth <= 375)
							{
								tabTitle.FontSize = 15;
							}
						}
						else
						{
							// A5
							if (App.ScreenWidth == 359.75)
							{
								tabTitle.FontSize = 12;
							}
							else
							{
								tabTitle.FontSize = 13.3;
							}
						}
					}
				}
			}

			tabTitle.Text = title;
		}

		public override Grid GenerateCarouselMotora()
		{
			Grid grid = new Grid
			{
				BackgroundColor = Color.White
			};

            //Boletim
            grid.Children.Add(new GradeBoletimView(position), 0, 1, 0, 1);
            //Frequencia View
            grid.Children.Add(new FrequenciaView(position), 0, 1, 0, 1);
            //Notas
            grid.Children.Add(new StatementNotesView(position), 0, 1, 0, 1);

			return grid;
		}
	}
}


