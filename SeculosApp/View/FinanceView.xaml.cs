﻿using Xamarin.Forms;
using System.Threading.Tasks;
using SeculosApp.Model;
using System;

namespace SeculosApp.View
{
	public partial class FinanceView : RelativeLayout, IBindingContextView
	{
		TabView TabView;
		Dependente studentSelected;
		public CustomLabel LblStudentSelected { get; set; }
		public CustomLabel LblMonthSelected { get; set; }
		AbstractContentPage AbstractContentPage;
		BaseExpandedView BaseExpandedView;
		public static bool StudentDetail;

		public FinanceViewModel ViewModel
		{
			get
			{
				return BindingContext as FinanceViewModel;
			}
		}

		public void changeBindingContext()
		{
			AbstractContentPage.BindingContext = this.BindingContext;
			var tapUpdate = new TapGestureRecognizer();
			tapUpdate.Tapped += (s, e) =>
			{
				Utils.FadeView(ViewModel.imgCloud);
				Utils.FadeView(ViewModel.lblError);
				Utils.FadeView(ViewModel.lblErrorUpdate);

				ViewModel.LoadBilletsCommand.Execute(null);
				System.Diagnostics.Debug.WriteLine("tapUpdate LoadBilletsCommand: ");
			};
			ViewModel.viewError.GestureRecognizers.Add(tapUpdate);
			System.Diagnostics.Debug.WriteLine("FinanceViewModel: changeBindingContext: " + ViewModel.lblError.Text);
		}

		public FinanceView(Dependente student, BaseExpandedView BaseExpandedView) : this(BaseExpandedView)
		{
			studentSelected = student;
			this.BaseExpandedView = BaseExpandedView;
		}

		public FinanceView(BaseExpandedView BaseExpandedView = null)
		{
			InitializeComponent();
			AbstractContentPage = AbstractContentPage._AbstractContentPage;
			LblStudentSelected = lblStudentSelected;
			LblMonthSelected = lblMonthSelected;

			BindingContext = new FinanceViewModel();

			var _Page = AbstractContentPage as MainScreenPage;
			if (AbstractContentPage.GetType() == typeof(MainScreenPage))
			{
				ViewModel.lblError = (AbstractContentPage as MainScreenPage).LabelError as CustomLabel;
				ViewModel.lblErrorUpdate = (AbstractContentPage as MainScreenPage).LabelUpdate as CustomLabel;
				ViewModel.imgCloud = (AbstractContentPage as MainScreenPage).ImageCloud as Image;
				ViewModel.lblErrorEmpty = lblErrorBilletsEmpty;
				ViewModel.viewError = (AbstractContentPage as MainScreenPage).ViewError as Xamarin.Forms.View;
			}
			else if(BaseExpandedView != null)
			{
				ViewModel.lblError = BaseExpandedView.lblErrorMain;
				ViewModel.lblErrorUpdate = BaseExpandedView.lblErrorMainUpdate;
				ViewModel.imgCloud = BaseExpandedView.imgCloudError;
				ViewModel.lblErrorEmpty = lblErrorBilletsEmpty;
				ViewModel.viewError = BaseExpandedView.stackContentError;

				var tapUpdate = new TapGestureRecognizer();
				tapUpdate.Tapped += (s, e) =>
				{
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(ViewModel.imgCloud);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(ViewModel.lblError);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(ViewModel.lblErrorUpdate);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

					ViewModel.LoadBilletsCommand.Execute(null);
					System.Diagnostics.Debug.WriteLine("tapUpdate LoadBilletsCommand: ");
				};
				ViewModel.viewError.GestureRecognizers.Add(tapUpdate);
			}

			var studentOrMonth = false;
			var billetSelected = false;
			listViewBillets.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{

				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				if (!billetSelected)
				{
					SeculosBO.Instance.BilletSelected = (Boleto)((ListView)sender).SelectedItem;
				}

				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect

				if (!billetSelected && !studentOrMonth)
				{
					billetSelected = true;
					await Navigation.PushAsync(new BilletDetailView());
					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							billetSelected = false;
							System.Diagnostics.Debug.WriteLine("billetSelected: " + billetSelected);
						});

					});
				}

			};

			pickerStudents.SelectedIndexChanged += (sender, args) =>
			{
				if (pickerStudents.SelectedIndex != -1)
				{
					string student = pickerStudents.Items[pickerStudents.SelectedIndex];
					lblStudentSelected.Text = student;
					SeculosBO.Instance.studentSelectedPicker = student;
					ViewModel.LoadBilletsCommandOffLine.Execute(null);
					System.Diagnostics.Debug.WriteLine("pickerStudents: student: "+student);
				}
			};

			var tapStudents = new TapGestureRecognizer();
			tapStudents.Tapped += async (s, e) =>
			{
				if (!billetSelected && !studentOrMonth)
				{
					studentOrMonth = true;
					var listDependente = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;
					pickerStudents.Items.Clear();
					pickerStudents.Items.Add(Application.Current.Resources["all"] as String);
					foreach (Dependente aluno in listDependente)
					{
						pickerStudents.Items.Add(aluno.NomeAluno);
					}
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(btnSelectStudent);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Device.BeginInvokeOnMainThread(() => pickerStudents.Focus());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(1500).ContinueWith((obj) =>
				   {
							studentOrMonth = false;
						});
					});
				}
			};
			btnSelectStudent.GestureRecognizers.Add(tapStudents);

			pickerMonths.SelectedIndexChanged += (sender, args) =>
			{
				if (pickerMonths.SelectedIndex != -1)
				{
					string month = pickerMonths.Items[pickerMonths.SelectedIndex];
					lblMonthSelected.Text = month;

					if (studentSelected == null)
					{
						SeculosBO.Instance.monthSelectedPicker = month;
					}
					else
					{
						SeculosBO.Instance.monthStudentSelectedPicker = month;
					}
					ViewModel.LoadBilletsCommandOffLine.Execute(null);
					System.Diagnostics.Debug.WriteLine("pickerMonths: month: " + month);
				}
			};

			var tapMonths = new TapGestureRecognizer();
			tapMonths.Tapped += async (s, e) =>
			{
				if (!billetSelected && !studentOrMonth)
				{
					studentOrMonth = true;
					var months = SeculosBO.Instance.Months;
					pickerMonths.Items.Clear();
					foreach (string monthValue in months)
					{
						pickerMonths.Items.Add(monthValue);
					}
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(btnSelectMonth);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Device.BeginInvokeOnMainThread(()=> pickerMonths.Focus());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(1500).ContinueWith((obj) =>
					   {
						   studentOrMonth = false;
					   });
					});
				}
			};
			btnSelectMonth.GestureRecognizers.Add(tapMonths);

			Task.Factory.StartNew(() =>
		 	{
	 			Task.Delay(300).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						if (studentSelected == null)
						{
							ViewModel.LoadBilletsCommand.Execute(null);
						}
						else
						{
							btnSelectStudent.GestureRecognizers.Clear();

							headerBillet.IsVisible = false;
							headerBillet.HeightRequest = 0;
							imgArrowBtnStudent.IsVisible = false;

							lblStudentSelected.Text = studentSelected.NomeAluno;
							SeculosBO.Instance.studentSelectedPicker = studentSelected.NomeAluno;

							if (SeculosBO.Instance.monthStudentSelectedPicker == null)
							{
								SeculosBO.Instance.monthStudentSelectedPicker = lblMonthSelected.Text;
							}
							else
							{
								lblMonthSelected.Text = SeculosBO.Instance.monthStudentSelectedPicker;
							}

							ViewModel.LoadBilletsCommandOffLine.Execute(true);
						}
					});
				});
			 });
		}
	}
}
