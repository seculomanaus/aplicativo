﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace SeculosApp
{
    public partial class AcompanhamentoInfantilView : AbstractContentPage
    {
        CalendarView _calendarView;

        public AcompanhamentoInfantilView()
        {
            InitializeComponent();
            RelContentModal = relContentModal;
            relContentModalExtends = relContentModal;
            abstractContentPageExtends = this;

            lblStudentName.Text = SeculosBO.Instance.DependenteConnected.NomeAluno;
            lblCursoName.Text = SeculosBO.Instance.DependenteConnected.NomeCurso;
            imgStudentPhoto.Source = SeculosBO.Instance.DependenteConnected.Foto;

            var back = new TapGestureRecognizer();
            var isBack = false;
            var calendar = false;
            back.Tapped += async (s, e) =>
            {
                if (!isBack && !calendar)
                {
                    isBack = true;
                    System.Diagnostics.Debug.WriteLine("back");
                    await Utils.FadeView(imgBackStudent);

                    try
                    {
                        await Navigation.PopAsync();
                    }
                    catch (System.Exception exception)
                    {
                        System.Diagnostics.Debug.WriteLine("AcompanhamentoInfantilView back exception: " + exception.Message + " Type: " + exception.GetType());
                    }
                }
                System.Diagnostics.Debug.WriteLine("calendar: " + calendar + " isBack: " + isBack);
            };
            imgBackStudent.GestureRecognizers.Add(back);

            var hasErrorVisible = false;
            var hasErrorConnectionVisible = false;
            var calendarTap = new TapGestureRecognizer();
            calendarTap.Tapped += async (s, e) =>
            {
                if (!isBack && !calendar && !load.IsVisible)
                {
                    calendar = true;
                    System.Diagnostics.Debug.WriteLine("calendarTap");

                    await Utils.FadeView(btnCalendar);

                    if (_calendarView == null)
                    {
                        createCalendar();
                    }

                    stackCalendar.IsVisible = !stackCalendar.IsVisible;

                    stackContentItens.IsVisible = !stackCalendar.IsVisible;

                    if (hasErrorVisible)
                    {
                        lblErrorDaily.IsVisible = true;
                        hasErrorVisible = false;
                    }

                    if (stackCalendar.IsVisible && lblErrorDaily.IsVisible && !hasErrorVisible)
                    {
                        lblErrorDaily.IsVisible = false;
                        hasErrorVisible = true;
                    }

                    if (hasErrorConnectionVisible)
                    {
                        stkError.IsVisible = true;
                        hasErrorConnectionVisible = false;
                    }

                    if (stackCalendar.IsVisible && stkError.IsVisible && !hasErrorConnectionVisible)
                    {
                        stkError.IsVisible = false;
                        hasErrorConnectionVisible = true;
                    }

                    Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);

                    await Task.Factory.StartNew(() =>
                    {
                        Task.Delay(200).ContinueWith((obj) =>
                        {
                            calendar = false;
                        });
                    });
                }
                System.Diagnostics.Debug.WriteLine("calendar: " + calendar + " isBack: " + isBack);
            };
            btnCalendar.GestureRecognizers.Add(calendarTap);

            var connectionTap = new TapGestureRecognizer();
            connectionTap.Tapped += async (s, e) =>
            {
                if (!App.GlobalBlockClick && !calendar && !load.IsVisible)
                {
                    App.GlobalBlockClick = true;
                    System.Diagnostics.Debug.WriteLine("connectionTap");

                    await Utils.FadeView(stkError);

                    initLoad();
                    await Task.Factory.StartNew(() =>
                    {
                        Task.Delay(200).ContinueWith((obj) =>
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                changeDate(DateTime.Now);
                            });
                        });
                    });

                    await Task.Factory.StartNew(() =>
                    {
                        Task.Delay(150).ContinueWith((obj) =>
                        {
                            App.GlobalBlockClick = false;
                        });
                    });
                }
            };
            stkError.GestureRecognizers.Add(connectionTap);

            lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[DateTime.Now.Month]);
            lblDay.Text = DateTime.Now.Day.ToString();

            //"02/08/2016"
            initLoad();

            Task.Factory.StartNew(() =>
            {
                Task.Delay(200).ContinueWith((obj) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        changeDate(DateTime.Now);
                    });
                });
            });
        }

        public StackLayout createItem(string sourceImg, string title, string desc)
        {
            var lblTitle = new CustomLabel
            {
                Text = Utils.PascalCase(title),
                TextColor = Color.Black,
                VerticalTextAlignment = TextAlignment.Center,
                FontType = "Medium",
                FontSize = 15
            };

            var lblDesc = new CustomLabel
            {
                Text = Utils.PascalCase(desc),
                TextColor = Color.FromHex("#a3a3a3"),
                VerticalTextAlignment = TextAlignment.Center,
                FontType = "Medium",
                FontSize = 15,
                Margin = new Thickness(0, -5, 0, 0)
            };

            var stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { lblTitle, lblDesc }

            };

            if (lblDesc.Text.Contains("Não"))
            {
                lblDesc.TextColor = Color.FromHex("#d74e54");
                sourceImg = sourceImg + "_red_ic.png";
            }
            else
            {
                sourceImg = sourceImg + "_green_ic.png";
            }

            var imgItem = new Image
            {
                Source = sourceImg,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                WidthRequest = 32,
                HeightRequest = 32,
                Margin = new Thickness(0, 0, 15, 0)
            };

            var stackMain = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Padding = new Thickness(0, 10, 0, 10),
                Children = { imgItem, stack }
            };

            return stackMain;
        }

        public void initLoad()
        {
            stkError.IsVisible = false;
            load.IsVisible = true;
            stackContentItens.IsVisible = false;
            lblErrorDaily.IsVisible = false;
        }

        public void changeDate(DateTime datetime)
        {

            String date;
            if(Device.OS == TargetPlatform.iOS){
                date = datetime.ToString("d/M/yyyy");
            }else{
                date = datetime.ToString("d");
            }

                System.Diagnostics.Debug.WriteLine("Data acompanhamento pos if: "+date);

                SeculosBO.Instance.getAcompanhamentoDiarioWS(SeculosBO.Instance.DependenteConnected, date, (msg) =>
            {
                var listOcompanhamentoInfantil = SeculosBO.Instance.DependenteConnected.listAcompanhamentoDiarioByDate<AcompanhamentoDiario>(date);
                if (msg.Equals(EnumCallback.SUCCESS))
                {
                    if (listOcompanhamentoInfantil == null || listOcompanhamentoInfantil.Count == 0)
                    {
                        SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblErrorDaily, null);
                    }
                }
                else if (msg.Equals(EnumCallback.DAILY_MONITORING_ERROR))
                {
                    if (listOcompanhamentoInfantil == null || listOcompanhamentoInfantil.Count == 0)
                    {
                        SeculosBO.Instance.showViewError(EnumCallback.DAILY_MONITORING_ERROR, lblErrorDaily, null);
                    }
                }
                else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
                {
                    if (listOcompanhamentoInfantil == null || listOcompanhamentoInfantil.Count == 0)
                    {
                        stkError.IsVisible = true;
                    }
                }
                stackContentItens.Children.Clear();
                if (listOcompanhamentoInfantil != null && listOcompanhamentoInfantil.Count > 0)
                {
                    stackContentItens.Children.Add(createItem("colacao", "Colação", listOcompanhamentoInfantil[0].Colacao));
                    stackContentItens.Children.Add(createItem("almoco", "Almoço", listOcompanhamentoInfantil[0].Almoco));
                    stackContentItens.Children.Add(createItem("lanche", "Lanche", listOcompanhamentoInfantil[0].Lanche));
                    stackContentItens.Children.Add(createItem("sono", "Sono / Descanso", listOcompanhamentoInfantil[0].Sono));
                    stackContentItens.Children.Add(createItem("evacuacao", "Evacuação", listOcompanhamentoInfantil[0].Evacuacao));
                }
                load.IsVisible = false;
                stackContentItens.IsVisible = true;
            });
        }

        public void createCalendar()
        {
            _calendarView = new CalendarView()
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                MinDate = CalendarView.FirstDayOfMonth(new DateTime(DateTime.Now.Year, 1, 1)),
                MaxDate = CalendarView.LastDayOfMonth(new DateTime(DateTime.Now.Year, 12, 1)),
                ShouldHighlightDaysOfWeekLabels = false,
                SelectionBackgroundStyle = CalendarView.BackgroundStyle.CircleFill,
                ShowNavigationArrows = true,
                NavigationArrowsColor = Color.FromHex("#3ea6e1"),
                Margin = new Thickness(10, -15, 10, 20),
                DateBackgroundColor = Color.White,
                BackgroundColor = Color.Transparent,
                DateSeparatorColor = Color.White,
                SelectedDateBackgroundColor = Color.FromHex("#3ea6e1"),
                SelectedDateForegroundColor = Color.White

            };

            if (Device.OS == TargetPlatform.iOS)
            {
                _calendarView.WidthRequest = App.ScreenWidth;
                _calendarView.Layout(new Rectangle(0, App.ScreenWidth, App.ScreenWidth, _calendarView.HeightRequest));
                if (App.ScreenWidth > 340)
                {
                    _calendarView.Margin = new Thickness(42, 0, 0, 0);
                }
                else
                {
                    _calendarView.Margin = new Thickness(0, -5, 0, 0);
                }
            }

            stackCalendar.Children.Add(_calendarView);

            _calendarView.DateSelected += (object sender, DateTime e) =>
            {
                initLoad();
                Task.Factory.StartNew(() =>
                {
                    Task.Delay(150).ContinueWith((obj) =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            changeDate(e);
                        });
                    });
                });
                lblMonth.Text = Utils.PascalCase(SeculosBO.Instance.Months[e.Month]);
                lblDay.Text = e.Day.ToString();
                stackCalendar.IsVisible = false;
                Utils.rotateArrowCalendar(imgArrowCalendar, imgCalendar);
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            RelContentModal = relContentModal;
            relContentModalExtends = relContentModal;
        }
    }
}
