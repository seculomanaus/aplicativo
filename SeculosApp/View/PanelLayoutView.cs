﻿using System.Threading.Tasks;
using Plugin.FirebasePushNotification;
using SeculosApp.View;
using Xamarin.Forms;

namespace SeculosApp
{
	public class PanelLayoutView
	{
		public PanelLayout PanelLayout { get; set; }

		public static bool btnClicked;

		public PanelLayoutView(StackLayout configPanel, BoxView panelBack, Image btnConfig, Image btnNoti, Image btnChat, RelativeLayout btnContact,
                               RelativeLayout btnAddCard, RelativeLayout btnExit, INavigation Navigation, AbstractContentPage Page)
		{
			PanelLayout = new PanelLayout(configPanel, panelBack);
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += async (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Tap no Config");
				PanelLayout.tooglePanel();
				await Utils.FadeView(btnConfig);
			};

			btnConfig.GestureRecognizers.Add(tapGestureRecognizer);

			var tapGestureRecognizerNoti = new TapGestureRecognizer();
			tapGestureRecognizerNoti.Tapped += async (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Tap Notification");
				if (!btnClicked)
				{
					btnClicked = true;
					await Utils.FadeView(btnNoti);
					await Navigation.PushAsync(new NotificationView());

					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							PanelLayoutView.btnClicked = false;
							System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnNoti: " + btnClicked);
						});

				  	});
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Click Notification Blocked");
				}
			};

			btnNoti.GestureRecognizers.Add(tapGestureRecognizerNoti);

			var tapGestureRecognizerChat = new TapGestureRecognizer();
			tapGestureRecognizerChat.Tapped += async (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Tap Chat");
				if (!btnClicked)
				{
					btnClicked = true;
					await Utils.FadeView(btnChat);
					await Navigation.PushAsync(new ChatWebView());

					await Task.Factory.StartNew(() =>
					{
						Task.Delay(1500).ContinueWith((obj) =>
						{
							PanelLayoutView.btnClicked = false;
							System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnChat: " + btnClicked);
						});

					});
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Click Chat Blocked");
				}
			};

			btnChat.GestureRecognizers.Add(tapGestureRecognizerChat);


			//var tapComunicationsHistory = new TapGestureRecognizer();
			//tapComunicationsHistory.Tapped += async (s, e) =>
			//{
			//	System.Diagnostics.Debug.WriteLine("Click tapComunicationsHistory");
			//	if (!btnClicked)
			//	{
			//                 await Utils.FadeView(btnNoti);

			//		btnClicked = true;
			//		PanelLayout.tooglePanel();

			//		await Navigation.PushAsync(new ComunicationsHistoryView());

			//		await Task.Factory.StartNew(() =>
			//	 	{
			// 			Task.Delay(1500).ContinueWith((obj) =>
			//			{
			//				btnClicked = false;
			//				System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnClicked: " + btnClicked);
			//			});

			//		 });
			//	}
			//	else
			//	{
			//		System.Diagnostics.Debug.WriteLine("Click tapComunicationsHistory Blocked");
			//	}
			//};
			//btnComunicationsHistory.GestureRecognizers.Add(tapComunicationsHistory);

			/*var tapConsumption = new TapGestureRecognizer();
			tapConsumption.Tapped += async (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Click Consumption");
				if (!btnClicked)
				{
					await Utils.FadeView(btnDayleConsumption);

					SeculosBO.Instance.OpenModal(EnumModal.CREDIT_LIMIT, false, Page);

					PanelLayoutView.btnClicked = true;
					PanelLayout.tooglePanel();

					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							PanelLayoutView.btnClicked = false;
							System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnClicked: " + btnClicked);
						});

					 });
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Click Consumption Blocked");
				}
			};
			btnDayleConsumption.GestureRecognizers.Add(tapConsumption);*/

			var tapContact = new TapGestureRecognizer();
			tapContact.Tapped += async (s, e) =>
			{
				if (!btnClicked)
				{
					System.Diagnostics.Debug.WriteLine("Click no tapContact");
					await Utils.FadeView(btnContact);

					SeculosBO.Instance.OpenModal(EnumModal.CONTACT, false, Page);

					btnClicked = true;
					PanelLayout.tooglePanel();

					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							btnClicked = false;
							System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnClicked: " + btnClicked);
						});

					 });
				}
			};
			btnContact.GestureRecognizers.Add(tapContact);

			var tapAddCredit = new TapGestureRecognizer();
			tapAddCredit.Tapped += async (s, e) =>
			{
				if (!btnClicked)
				{
					System.Diagnostics.Debug.WriteLine("Click no Add credit");
					await Utils.FadeView(btnAddCard);

					//SeculosBO.Instance.OpenModal(EnumModal.BUY_CREDIT, false, Page);

					await Navigation.PushAsync(new AddBalancePage());

					btnClicked = true;
					PanelLayout.tooglePanel();

					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							btnClicked = false;
							System.Diagnostics.Debug.WriteLine("PanelLayoutView.btnClicked: " + btnClicked);
						});

					 });
				}
			};
			btnAddCard.GestureRecognizers.Add(tapAddCredit);

			var tapExit = new TapGestureRecognizer();
			tapExit.Tapped += async (s, e) =>
			{
				System.Diagnostics.Debug.WriteLine("Click no Logout");

				SeculosBO.Instance.logout(() =>
				{
                    CrossFirebasePushNotification.Current.UnsubscribeAll();
					Navigation.PushAsync(new LoginPage());
					try
					{
						Navigation.RemovePage(Page);
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("MainScreenPage btnLogout.Clicked exception: " + exception.Message + " Type: " + exception.GetType());
					}

				});
				await Utils.FadeView(btnExit);
			};
			btnExit.GestureRecognizers.Add(tapExit);

		}
	}
}
