﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public static class Utils
	{

		public static async Task FadeView(Xamarin.Forms.View view)
		{
			if (view != null)
			{
				view.Opacity = 0.5;
				await view.FadeTo(1, 100, null);
			}
		}

		public static async Task Reload(Layout layout, int delay)
		{
			await Task.Delay(delay);
			layout.ForceLayout();
		}

		public static string ExtractString(string s, string startTag, string endTag)
		{
			int startIndex = s.IndexOf(startTag, StringComparison.Ordinal) + startTag.Length;
			int endIndex = s.IndexOf(endTag, startIndex, StringComparison.Ordinal);
			return s.Substring(startIndex, endIndex - startIndex);
		}

		public static string HtmlStrip(this string input)
		{
			input = Regex.Replace(input, "<style>(.|\n)*?</style>", string.Empty);
			input = Regex.Replace(input, @"<xml>(.|\n)*?</xml>", string.Empty); // remove all <xml></xml> tags and anything inbetween.  
			return Regex.Replace(input, @"<(.|\n)*?>", string.Empty); // remove any tags but not there content "<p>bob<span> johnson</span></p>" becomes "bob johnson"
		}

		public static string PascalCase(string text, string stringSeparator = " ")
		{
			if (text == null || text.Equals(string.Empty) || text.Equals(SeculosBO.EMPTY))
			{
				return SeculosBO.EMPTY;
			}

			var textLowe = text.ToLower();

			string[] stringSeparators = new string[] { stringSeparator };
			string [] textSplit = textLowe.Split(stringSeparators, StringSplitOptions.None);
			var textReturn = string.Empty;
			string tempValue;

			foreach (var item in textSplit)
			{
				if (item.Length > 1 && !item.Equals("da") && !item.Equals("de") && !item.Equals("de") && !item.Equals("do") && !item.Equals("du"))
				{
					var index = 0;
					if (item.Substring(0, 1).Equals("(") || item.Substring(0, 1).Equals(".") || item.Substring(0, 1).Equals(","))
					{
						index++;
						textReturn = textReturn + item.Substring(0, 1);
					}
					tempValue = item[index].ToString().ToUpper();
					textReturn = textReturn + tempValue + item.Substring(index+1) + stringSeparator;
				}
				else
				{
					textReturn = textReturn + item + " ";
				}
			}
			return textReturn;
		}

		public static string getCashValue(string value)
		{
			if (value == null || value.Equals(string.Empty) || value.Equals(SeculosBO.EMPTY))
			{
				return SeculosBO.EMPTY;
			}

			string tempValue = "";
			try
			{
				tempValue = decimal.Parse(value, CultureInfo.InvariantCulture).ToString();
			}
			catch
			{
				return SeculosBO.EMPTY;
			}

			string[] stringSeparators = new string[] { "," };
			string[] textSplit = tempValue.Split(stringSeparators, StringSplitOptions.None);
			if (textSplit.Length == 1)
			{
				tempValue = tempValue + ",00";
			}
			return tempValue;
		}

		public static string getCashValueDecimal(string value)
		{
			if (value == null || value.Equals(string.Empty) || value.Equals(SeculosBO.EMPTY))
			{
				return SeculosBO.EMPTY;
			}

			string tempValue = "";
			try
			{
				tempValue = decimal.Parse(value, CultureInfo.InvariantCulture).ToString();
			}
			catch
			{
				return SeculosBO.EMPTY;
			}

			string[] stringSeparators = new string[] { "," };
			string[] textSplit = tempValue.Split(stringSeparators, StringSplitOptions.None);
			if (textSplit.Length == 1)
			{
				tempValue = tempValue + ".00";
			}
			return tempValue;
		}

		public static string formatCurrency(int maxlenght, string text)
		{
			if (text != null && !text.Equals(string.Empty) && !text.Equals(SeculosBO.EMPTY))
			{
				if (text.Length > maxlenght)
				{
					text = text.Substring(2);
				}

				var numberFormatInfo = (NumberFormatInfo)new CultureInfo("pt-BR").NumberFormat.Clone();
				numberFormatInfo.CurrencySymbol = "";

				var tempValue = text.ToString().Trim().Replace(" ", "").Replace(",", "").Replace(".", "");
				if (!tempValue.Equals(string.Empty))
				{
					decimal intValue = 0;
					try
					{
						intValue = Convert.ToDecimal(tempValue);
					}
					catch
					{
						return SeculosBO.EMPTY;
					}

					if (intValue > 0 || intValue < 0)
					{
						intValue = intValue / 100;
					}
					else
					{
						return string.Empty;
					}

					System.Diagnostics.Debug.WriteLine("TextChangedHandler intValue: " + intValue);

					text = string.Format(numberFormatInfo, "{0:C2}", intValue);

					System.Diagnostics.Debug.WriteLine("TextChangedHandler valorFormatado: " + text);
				}
				else
				{
					text = SeculosBO.EMPTY;
				}
			}
			else 
			{
				text = SeculosBO.EMPTY;
			}
			return text;
		}

		public static string stringToDecimal(string text)
		{
			if (text != null && !text.Equals(string.Empty) && !text.Equals(SeculosBO.EMPTY))
			{
				var tempValue = text.Trim().Replace(" ", "").Replace(",", "").Replace(".", "");
				if (!tempValue.Equals(string.Empty))
				{
					try
					{
						tempValue = (Convert.ToDecimal(tempValue) / 100).ToString().Replace(",", ".");
					}
					catch
					{
						return SeculosBO.EMPTY;
					}

					string[] stringSeparators = new string[] { "." };
					string[] textSplit = tempValue.Split(stringSeparators, StringSplitOptions.None);
					if (textSplit.Length == 1)
					{
						tempValue = tempValue + ".00";
					}

					textSplit = tempValue.Split(stringSeparators, StringSplitOptions.None);
					if (textSplit.Length > 1 && textSplit[1].Length == 1)
					{
						tempValue = textSplit[0] +"."+ textSplit[1] + "0";
					}

					return tempValue;
				}
				else
				{
					return SeculosBO.EMPTY;
				}
			}

			return SeculosBO.EMPTY;
		}

		public static RelativeLayout CreateItemLayout(string text)
		{
			var layout = new RelativeLayout
			{
				Margin = new Thickness(10, 2, 10, 0)
			};

			var itemLabel = new CustomLabel
			{
				Text = text,
				TextColor = Color.Gray,
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				HeightRequest = 28,
				FontSize = 15.5
			};

			var imgArrow = new Image
			{
				ClassId = "btnArrowItem",
				Source = SeculosBO.Instance?.DependenteConnected?.ArrowRight,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				WidthRequest = 26,
				HeightRequest = 18
			};

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(0, 8, 0, 0)
			};

			layout.Children.Add(
				imgArrow,
				Constraint.RelativeToParent((parent) => { return parent.Width - 26; }),
				Constraint.Constant(18)
			);

			layout.Children.Add(
				itemLabel,
				Constraint.Constant(0),
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			layout.Children.Add(
				lineSeperator,
				Constraint.Constant(0),
				Constraint.RelativeToView(itemLabel, (parent, view) => { return view.Y + view.Height + 6; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			return layout;
		}

		public static void rotateArrowCalendar(Image imgArrowCalendar, Image imgCalendar) 
		{
			var rotate = 0;
			var scale = 1.05;

			if (imgArrowCalendar.Rotation == 0)
			{
				rotate = 180;
				scale = 1;
			}
			imgArrowCalendar.RotateTo(rotate, 200, Easing.SpringOut);

			imgCalendar.ScaleTo(scale, 200, Easing.CubicInOut);
		}

		public static void fixRoudedView(RoundedBoxView rounded)
		{
			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				rounded.CornerRadius = rounded.CornerRadius / 2;

			}
			else
			{
				rounded.CornerRadius = rounded.CornerRadius + (App.ScreenHeight - 592) / 5;
			}
		}
	}
}
