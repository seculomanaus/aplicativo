﻿using Xamarin.Forms;

namespace SeculosApp
{
    public class AcademicSemiExpandedView : BaseExpandedView
    {
        public AcademicSemiExpandedView(int _position, string _pageId) : base(_position, _pageId)
        {
            // position = Page in carousel
            // 0 -> agendaView
            //  -> conteudoProgramaticoLayout (GradesView)//mudou pra webview
            // 1 -> historicoEscolarLayout (AcademicView)
            // 2 -> ClassScheduleView
            tabsLayout.BackgroundColor = Color.FromHex("#FFAB00");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC, (obj, arg) =>
            {
                try
                {
                    var str = (string)arg;
                    if (((string)App.Current.Resources["school_history"]).ToUpper().Equals(str))
                    {
                        if (Device.OS.Equals(TargetPlatform.iOS))
                        {
                            if (App.ScreenWidth <= 340)
                            {
                                tabTitle.FontSize = 16;
                            }
                            else
                            {
                                // A5
                                if (App.ScreenWidth == 359.75)
                                {
                                    tabTitle.FontSize = 16;
                                }
                                else
                                {
                                    tabTitle.FontSize = 17;
                                }
                            }
                        }
                        else
                        {
                            tabTitle.FontSize = 17;
                        }
                    }
                    else if (((string)App.Current.Resources["program_content"]).ToUpper().Equals(str) && Device.OS.Equals(TargetPlatform.iOS) && (App.ScreenWidth <= 340))
                    {
                        tabTitle.FontSize = 14;
                    }
                    else
                    {
                        tabTitle.FontSize = 16;
                    }
                    tabTitle.Text = str;
                }
                catch { }
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC);
        }

        public override void handleCarouselPage()
        {
            var title = "";
            tabTitle.FontSize = 18;

            if (position < carouselMotora.Children.Count)
            {
                if (position == 0)
                {
                    title = ((string)App.Current.Resources["nootbook_str"]).ToUpper();
                }
                else if (position == 1)
                {
                    title = ((string)App.Current.Resources["school_history"]).ToUpper();
                    if (Device.OS.Equals(TargetPlatform.iOS))
                    {
                        if (App.ScreenWidth <= 340)
                        {
                            tabTitle.FontSize = 16;
                        }
                    }
                }
                else if (position == 2)
                {
                    title = ((string)App.Current.Resources["class_schedule"]).ToUpper();
                   
                }
               
            }

            tabTitle.Text = title;
        }

        public override Grid GenerateCarouselMotora()
        {
            Grid grid = new Grid
            {
                BackgroundColor = Color.White
            };

            grid.Children.Add(new AgendaView(), 0, 1, 0, 1);
            grid.Children.Add(new AcademicView(), 0, 1, 0, 1);

            return grid;
        }
    }
}


