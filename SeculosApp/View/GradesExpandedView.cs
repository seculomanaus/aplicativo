﻿using Xamarin.Forms;

namespace SeculosApp
{
    public class GradesExpandedView : BaseExpandedView
    {
        public GradesExpandedView(int _position, string _pageId) : base(_position, _pageId)
        {
            // position = Page in carousel
            // 1 -> TemplateTestView -- removido
            // 0 -> ContentTestsView

            // ---------------------------
            // default -> GradeBoletimView
            // azul 3ea6e1
            tabsLayout.BackgroundColor = Color.FromHex("#FFAB00");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_GRADE, (obj, arg) =>
            {
                try
                {
                    var str = (string)arg;
                   
                    if (App.ScreenWidth == 359.75)
                    {
                        tabTitle.FontSize = 16.5;
                    }
                    else
                    {
                        tabTitle.FontSize = 18;
                    }
                    
                    tabTitle.Text = str;
                }
                catch {}
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_GRADE);
        }

        public override void handleCarouselPage()
        {
            var title = "";
            

            if (App.ScreenWidth == 359.75)
            {
                tabTitle.FontSize = 16.5;
            }
            else
            {
                tabTitle.FontSize = 18;
            }

            if (position == 0)
            {
                title = ((string)App.Current.Resources["content_tests"]).ToUpper();
            }
            else
             {
                title = ((string)App.Current.Resources["template_test"]).ToUpper();
             }

            tabTitle.Text = title;
        }

        public override Grid GenerateCarouselMotora()
        {
            Grid grid = new Grid
            {
                BackgroundColor = Color.White
            };

            //gabarito
            //grid.Children.Add(new TemplateTestView(position), 0, 1, 0, 1);

            //CONTEUDO
            grid.Children.Add(new ContentTestsView(position), 0, 1, 0, 1);

            return grid;
        }
    }
}