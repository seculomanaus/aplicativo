﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SeculosApp.Model;
using SeculosApp.ViewModel;
using Xamarin.Forms;

namespace SeculosApp.View
{
	public partial class StudentView : RelativeLayout, IBindingContextView
	{
		AbstractContentPage AbstractContentPage;

		public StudentViewModel ViewModel
		{
			get 
			{ 
				return BindingContext as StudentViewModel; 
			}
		}

		public void changeBindingContext() 
		{
			AbstractContentPage.BindingContext = this.BindingContext;
			var tapUpdate = new TapGestureRecognizer();
			tapUpdate.Tapped += (s, e) =>
			{
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				Utils.FadeView(ViewModel.imgCloud);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				Utils.FadeView(ViewModel.lblError);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
				Utils.FadeView(ViewModel.lblErrorUpdate);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

				ViewModel.LoadStudentsCommand.Execute(null);
				System.Diagnostics.Debug.WriteLine("tapUpdate LoadStudentsCommand: ");
			};
			ViewModel.viewError.GestureRecognizers.Add(tapUpdate);
			System.Diagnostics.Debug.WriteLine("StudentViewModel: changeBindingContext: "+ViewModel.lblError.Text);
		}

		public StudentView()
		{
			InitializeComponent();
			AbstractContentPage = AbstractContentPage._AbstractContentPage;
			BindingContext = new StudentViewModel();

			ViewModel.lblError = (AbstractContentPage as MainScreenPage).LabelError as CustomLabel;
			ViewModel.lblErrorUpdate = (AbstractContentPage as MainScreenPage).LabelUpdate as CustomLabel;
			ViewModel.imgCloud = (AbstractContentPage as MainScreenPage).ImageCloud as Image;
			ViewModel.lblErrorEmpty = lblErrorStudentsEmpty;
			ViewModel.viewError = (AbstractContentPage as MainScreenPage).ViewError as Xamarin.Forms.View;
			ViewModel.listViewStudent = listViewStudents;

			Task.Factory.StartNew(() =>
		 	{
	 			Task.Delay(500).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						ViewModel.LoadStudentsCommand.Execute(null);
					});
				});
			});

			var studentSelected = false;
			listViewStudents.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{

			   // don't do anything if we just de-selected the row
			   if (e.Item == null)
				{
					return;
				}

				if (!studentSelected)
				{
					SeculosBO.Instance.StudentSelected = (Dependente)((ListView)sender).SelectedItem;
				}

				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect

				if (!studentSelected)
				{
					studentSelected = true;
					await Navigation.PushAsync(new AlunoPage());
					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(1500).ContinueWith((obj) =>
						{
							 studentSelected = false;
							 System.Diagnostics.Debug.WriteLine("studentSelected: " + studentSelected);
						});

					});
				}

			};

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				lblDescStudents.VerticalTextAlignment = TextAlignment.Start;
				lblDescStudents.Margin = new Thickness(0, 5, 0, 0);
			}
		}
	}
}
