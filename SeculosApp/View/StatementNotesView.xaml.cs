﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class StatementNotesView : RelativeLayout, ITabView
	{
		Command cmdClick;
		bool initializedView;

		public StatementNotesView(int position)
		{
			InitializeComponent();

			cmdClick = new Command((obj) => onClick(obj));

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			initLoad();
			InitializeView();
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							btnVerBoletim.Clicked += async (sender, e) =>
							{
								redirectToPDF();
							};
							getDemonstrativoNotas();
						});
					});
				});
			}
		}

		private void redirectToPDF()
        {
			string baseURL = "https://seculomanaus.com.br/componentes/portal/demonstrativo/inicio?ra=";
			//SeculosBO.Instance.IMessage.Msg("Redirecionando ao site de pagamento...");
			
			bool isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

			string studentCode;

			if (isResp)
			{
				studentCode = SeculosBO.Instance.StudentSelected.CdAluno;
			}
			else
			{
				studentCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;

			}

			string url = baseURL + studentCode;

			Device.OpenUri(new Uri( url ));

		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(stkError.ClassId))
				{
					initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getDemonstrativoNotas();
								load.IsVisible = false;
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void createEmptyitem(StackLayout stackContentItens)
		{
			if (stackContentItens.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 60, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				stackContentItens.HorizontalOptions = LayoutOptions.CenterAndExpand;
			}
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			lblError.IsVisible = false;
		}

		public void getDemonstrativoNotas()
		{
			SeculosBO.Instance.getDemonstrativoWS(SeculosBO.Instance.DependenteConnected, (msg) =>
			{
				var listDemonstrativo = SeculosBO.Instance.DependenteConnected.listDemonstrativo;

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listDemonstrativo == null || listDemonstrativo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.DEMONSTRATIVO_ERROR))
				{
					if (listDemonstrativo == null || listDemonstrativo.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DEMONSTRATIVO_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listDemonstrativo == null || listDemonstrativo.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listDemonstrativo != null && listDemonstrativo.Count > 0)
				{
					createAllitens();
				}
				else
				{
					load.IsVisible = false;
				}
			});
		}

		void createAllitens()
		{
			var listDemonstrativo = SeculosBO.Instance.DependenteConnected.listDemonstrativo;

			foreach (Demonstrativo demonstrativo in listDemonstrativo)
			{
				RelativeLayout item = CreateItemLayout(demonstrativo);
				stackContentItens.Children.Add(item);
			}

			stackContentItens.IsVisible = true;
			load.IsVisible = false;
		}

		public RelativeLayout CreateItemLayout(Demonstrativo demonstrativo)
		{
			var layout = new RelativeLayout
			{
				Margin = new Thickness(0, -10, 0, 5)
			};

			var itemLabel = new CustomLabel
			{
				Text = Utils.PascalCase(demonstrativo.NmDisciplina),
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				HeightRequest = 28,
				FontSize = 15.5,
				Margin = new Thickness(10, 0, 20, 0)
			};

			var imgArrow = new Image
			{
				Source = "arrow_right_yellow_ic.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				WidthRequest = 26,
				HeightRequest = 18,
				Margin = new Thickness(0, 0, 20, 0),
				Rotation = 90
			};

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(10, 8, 10, 0)
			};

			layout.Children.Add(
				imgArrow,
				Constraint.RelativeToParent((parent) => { return parent.Width - 36; }),
				Constraint.Constant(18)
			);

			layout.Children.Add(
				itemLabel,
				Constraint.Constant(0),
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			layout.Children.Add(
				lineSeperator,
				Constraint.Constant(0),
				Constraint.RelativeToView(itemLabel, (parent, view) => { return view.Y + view.Height + 6; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			var stackContent = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 0,
				Margin = new Thickness(0, -2, 0, 0),
				Padding = new Thickness(0, 1, 0, 1),
				Spacing = 0
			};

			layout.Children.Add(
				stackContent,
				Constraint.Constant(0),
				Constraint.RelativeToView(lineSeperator, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			stackContent.Opacity = 0;

			var stackBim = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				BackgroundColor = Color.White,
				Margin = new Thickness(0, 1, 0, 1),
				Padding = new Thickness(10, 10, 10, 10)
			};

			RelativeLayout rel1Bim = createRelBim("1º BIM", "#ffffff", "#3ea6e1", LayoutOptions.StartAndExpand);
			RelativeLayout rel2Bim = createRelBim("2º BIM", "#a3a3a3", "#ffffff", LayoutOptions.CenterAndExpand);
			RelativeLayout rel3Bim = createRelBim("3º BIM", "#a3a3a3", "#ffffff", LayoutOptions.CenterAndExpand);
			RelativeLayout rel4Bim = createRelBim("4º BIM", "#a3a3a3", "#ffffff", LayoutOptions.EndAndExpand);

			var stack1Bim = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};
			stack1Bim.Children.Add(createHeaderBim("1º BIM"));
			var stack = createHeaderBim("1º BIM", demonstrativo);
			stack.BackgroundColor = Color.FromHex("#f6f6f6");
			stack1Bim.Children.Add(stack);
			var lblLegendaTitle1 = new CustomLabel
			{
				Text = "LEGENDA",
				TextColor = Color.Black,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				BackgroundColor = Color.White,
				FontType = "Medium",
				FontSize = 13.5,
				Margin = new Thickness(12, 10, 0, -3)
			};
			stack1Bim.Children.Add(lblLegendaTitle1);

			var lblLegenda1 = new CustomLabel
			{
				Text = demonstrativo.Legenda13,
				BackgroundColor = Color.White,
				TextColor = Color.FromHex("#98999a"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 14,
				Margin = new Thickness(12, 0, 12, 10)
			};
			stack1Bim.Children.Add(lblLegenda1);

			var stack2Bim = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				IsVisible = false,
				Opacity = 0
			};
			stack2Bim.Children.Add(createHeaderBim("2º BIM"));
			stack = createHeaderBim("2º BIM", demonstrativo);
			stack.BackgroundColor = Color.FromHex("#f6f6f6");
			stack2Bim.Children.Add(stack);
			var lblLegendaTitle2 = new CustomLabel
			{
				Text = "LEGENDA",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 13.5,
				Margin = new Thickness(12, 10, 0, -3)
			};
			stack2Bim.Children.Add(lblLegendaTitle2);

			var lblLegenda2 = new CustomLabel
			{
				Text = demonstrativo.Legenda24,
				TextColor = Color.FromHex("#98999a"),
				BackgroundColor = Color.White,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 14,
				Margin = new Thickness(12, 0, 12, 10)
			};
			stack2Bim.Children.Add(lblLegenda2);

			var stack3Bim = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				IsVisible = false,
				Opacity = 0
			};
			stack3Bim.Children.Add(createHeaderBim("3º BIM"));
			stack = createHeaderBim("3º BIM", demonstrativo);
			stack.BackgroundColor = Color.FromHex("#f6f6f6");
			stack3Bim.Children.Add(stack);
			var lblLegendaTitle3 = new CustomLabel
			{
				Text = "LEGENDA",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 13.5,
				Margin = new Thickness(12, 10, 0, -3)
			};
			stack3Bim.Children.Add(lblLegendaTitle3);

			var lblLegenda3 = new CustomLabel
			{
				Text = demonstrativo.Legenda13,
				BackgroundColor = Color.White,
				TextColor = Color.FromHex("#98999a"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 14,
				Margin = new Thickness(12, 0, 12, 10)
			};
			stack3Bim.Children.Add(lblLegenda3);

			var stack4Bim = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				IsVisible = false,
				Opacity = 0
			};
			stack4Bim.Children.Add(createHeaderBim("4º BIM"));
			stack = createHeaderBim("4º BIM", demonstrativo);
			stack.BackgroundColor = Color.FromHex("#f6f6f6");
			stack4Bim.Children.Add(stack);
			var lblLegendaTitle4 = new CustomLabel
			{
				Text = "LEGENDA",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 13.5,
				Margin = new Thickness(12, 10, 0, -3)
			};
			stack4Bim.Children.Add(lblLegendaTitle4);

			var lblLegenda4 = new CustomLabel
			{
				Text = demonstrativo.Legenda24,
				BackgroundColor = Color.White,
				TextColor = Color.FromHex("#98999a"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				FontType = "Medium",
				FontSize = 14,
				Margin = new Thickness(12, 0, 12, 10)
			};
			stack4Bim.Children.Add(lblLegenda4);

			CustomLabel lbl1Bim = null;
			RoundedBoxView rounded1Bim = null;
			foreach (Xamarin.Forms.View view in rel1Bim.Children)
			{
				if (view.ClassId.Equals("lbl1º BIM"))
				{
					lbl1Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded1º BIM"))
				{
					rounded1Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl2Bim = null;
			RoundedBoxView rounded2Bim = null;
			foreach (Xamarin.Forms.View view in rel2Bim.Children)
			{
				if (view.ClassId.Equals("lbl2º BIM"))
				{
					lbl2Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded2º BIM"))
				{
					rounded2Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl3Bim = null;
			RoundedBoxView rounded3Bim = null;
			foreach (Xamarin.Forms.View view in rel3Bim.Children)
			{
				if (view.ClassId.Equals("lbl3º BIM"))
				{
					lbl3Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded3º BIM"))
				{
					rounded3Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl4Bim = null;
			RoundedBoxView rounded4Bim = null;
			foreach (Xamarin.Forms.View view in rel4Bim.Children)
			{
				if (view.ClassId.Equals("lbl4º BIM"))
				{
					lbl4Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded4º BIM"))
				{
					rounded4Bim = view as RoundedBoxView;
				}
			}

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap1Bim: ");
						await Utils.FadeView(rel1Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;
						stack1Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack1Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#ffffff");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#3ea6e1");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#ffffff");
						await Task.Factory.StartNew(() =>
						{
							Task.Delay(200).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl1Bim
			});

			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap2Bim: ");
						await Utils.FadeView(rel2Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack2Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack2Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#ffffff");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#3ea6e1");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#ffffff");

						if (stack2Bim.ClassId == null)
						{
							stack2Bim.ClassId = "stack2Bim";
							stackContent.Children.Add(stack2Bim);
						}

						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl2Bim
			});

			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap3Bim: ");
						await Utils.FadeView(rel3Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack3Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack3Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#ffffff");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#3ea6e1");
						rounded4Bim.Color = Color.FromHex("#ffffff");

						if (stack3Bim.ClassId == null)
						{
							stack3Bim.ClassId = "stack2Bim";
							stackContent.Children.Add(stack3Bim);
						}

						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl3Bim
			});

			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap4Bim: ");
						await Utils.FadeView(rel4Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack4Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack4Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#ffffff");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#3ea6e1");
						if (stack4Bim.ClassId == null)
						{
							stack4Bim.ClassId = "stack4Bim";
							stackContent.Children.Add(stack4Bim);
						}
						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl4Bim
			});

			layout.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					await Utils.FadeView(itemLabel);

					var rotate = 90;
					var fade = 0;
					uint time = 50;
					if (imgArrow.Rotation == 90)
					{
						rotate = -90;
						fade = 1;
						stackContent.HeightRequest = -1;
						time = 200;
						itemLabel.TextColor = Color.Black;
					}
					else
					{
						itemLabel.TextColor = Color.FromHex("#a3a3a3");
					}

#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					imgArrow.RotateTo(rotate, 200, Easing.SpringOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					stackContent.FadeTo(fade, time, null);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					if (rotate == 90)
					{
						stackContent.HeightRequest = 0;
					}

					// initialize content bimesters and the frist tab that is a 1 bim.
					if (stackContent.Children.Count == 0)
					{
						stackContent.Children.Add(stackBim);
						stackContent.Children.Add(stack1Bim);
					}

				}),
				CommandParameter = layout
			});

			stackBim.Children.Add(rel1Bim);
			stackBim.Children.Add(rel2Bim);
			stackBim.Children.Add(rel3Bim);
			stackBim.Children.Add(rel4Bim);

			return layout;
		}

		RelativeLayout createRelBim(string text, string textColor, string backgroundColor, LayoutOptions options)
		{
			var roundedBim = new RoundedBoxView
			{
				ClassId = "rounded" + text,
				WidthRequest = 60,
				HeightRequest = 32,
				CornerRadius = 30,
				Color = Color.FromHex(backgroundColor)
			};

			Utils.fixRoudedView(roundedBim);

			var lblroundedBim = new CustomLabel
			{
				ClassId = "lbl" + text,
				Text = text,
				WidthRequest = 60,
				HeightRequest = 32,
				FontSize = 15.5,
				FontType = "Medium",
				TextColor = Color.FromHex(textColor),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center
			};

			var relBim = new RelativeLayout
			{
				ClassId = text,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = options,
				Padding = new Thickness(0, 0, 0, 0)
			};

			relBim.Children.Add(
				roundedBim,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);

			relBim.Children.Add(
				lblroundedBim,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);

			return relBim;
		}

		public StackLayout createHeaderBim(string bim, Demonstrativo demonstrativo = null)
		{

			var lblP1 = new CustomLabel
			{
				Text = "N1",
				WidthRequest = 60,
				TextColor = Color.FromHex("#555555"),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15,
			};

			var lblMaic = new CustomLabel
			{
				Text = "MAIC",
				WidthRequest = 60,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15,
			};

			var lblP2 = new CustomLabel
			{
				Text = "N2",
				WidthRequest = 60,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15,
			};

			var lblMbt = new CustomLabel
			{
				Text = "MB",
				WidthRequest = 60,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				TextColor = Color.FromHex("#555555"),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				FontSize = 15,
			};

            var lblF = new CustomLabel 
            {
                Text = "F",
                WidthRequest = 60,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                TextColor = Color.FromHex("#555555"),
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                FontType = "Medium",
                FontSize = 15,
            };

            var stackHeader = new StackLayout
			{
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { lblP1, lblMaic, lblP2, lblMbt, lblF },
				Padding = new Thickness(10, 10, 10, 10),
				BackgroundColor = Color.White
			};

			if (bim.Equals("2º BIM") || bim.Equals("4º BIM"))
			{
				var lblNrec = new CustomLabel
				{
					Text = "NREC",
					WidthRequest = 60,
					HorizontalOptions = LayoutOptions.EndAndExpand,
					TextColor = Color.FromHex("#555555"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 15,
				};

				lblMbt.HorizontalOptions = LayoutOptions.CenterAndExpand;

				stackHeader.Children.Add(lblNrec);

				if (demonstrativo != null)
				{
					if (bim.Equals("1º BIM"))
					{
						lblNrec.Text = demonstrativo.Nrec1B;
					}
					else if (bim.Equals("2º BIM"))
					{
						lblNrec.Text = demonstrativo.Nrec2B;
					}
					else if (bim.Equals("3º BIM"))
					{
						lblNrec.Text = demonstrativo.Nrec3B;
					}
					else if (bim.Equals("4º BIM"))
					{
						lblNrec.Text = demonstrativo.Nrec4B;
					}
				}
			}

			if (demonstrativo != null)
			{
				if (bim.Equals("1º BIM"))
				{
					lblP1.Text = demonstrativo.P11B;
					lblMaic.Text = demonstrativo.Maic1B;
					lblP2.Text = demonstrativo.P21B;
					lblMbt.Text = demonstrativo.Mbt1B;
				}
				else if (bim.Equals("2º BIM"))
				{
					lblP1.Text = demonstrativo.P12B;
					lblMaic.Text = demonstrativo.Maic2B;
					lblP2.Text = demonstrativo.P22B;
					lblMbt.Text = demonstrativo.Mbt2B;
				}
				else if (bim.Equals("3º BIM"))
				{
					lblP1.Text = demonstrativo.P13B;
					lblMaic.Text = demonstrativo.Maic3B;
					lblP2.Text = demonstrativo.P23B;
					lblMbt.Text = demonstrativo.Mbt3B;
				}
				else if (bim.Equals("4º BIM"))
				{
					lblP1.Text = demonstrativo.P14B;
					lblMaic.Text = demonstrativo.Maic4B;
					lblP2.Text = demonstrativo.P24B;
					lblMbt.Text = demonstrativo.Mbt4B;
				}
			}

			return stackHeader;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, ((string)App.Current.Resources["statement_notes"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
