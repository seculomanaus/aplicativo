﻿using Xamarin.Forms;

namespace SeculosApp
{
	public partial class CardapioWebView : AbstractContentPage
	{

        string baseURl = "https://seculomanaus.com.br/componentes/portal/cardapio/imprimir/mobile";

        public CardapioWebView()
		{

            InitializeComponent();

            setupBackArrow();

            lblName.Text = "Cardápio Semanal";

            string url = baseURl;

            System.Diagnostics.Debug.WriteLine("Program Content URL: " + url);

            navegador.Source = url;

		}

        private void setupBackArrow()
        {
            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) =>
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch (System.Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("webview back exception: " + exception.Message + " Type: " + exception.GetType());
                }

            };
            imgBack.GestureRecognizers.Add(back);
        }

        private void PagOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            load.IsVisible = true;
        }
        private void PagOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            load.IsVisible = false;
        }

    }
}