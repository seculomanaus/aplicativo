﻿using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ChatWebView : AbstractContentPage
	{

        string baseURl = "https://seculomanaus.com.br/componentes/portal/chat/inicio?codusuario=";
        bool isResp;

        public ChatWebView()
		{

            InitializeComponent();

            setupBackArrow();

            lblName.Text = "Chat Século";

            isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

            string userCode;

            if (isResp)
            {
                userCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;
            }
            else
            {
                userCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;

            }
            
            System.Diagnostics.Debug.WriteLine("cd aluno: " + userCode);

            string url = baseURl + userCode;

            System.Diagnostics.Debug.WriteLine("Program Content URL: " + url);

            navegador.Source = url;

		}

        private void setupBackArrow()
        {
            var back = new TapGestureRecognizer();
            back.Tapped += async (s, e) =>
            {
                try
                {
                    await Navigation.PopAsync();
                }
                catch (System.Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("webview back exception: " + exception.Message + " Type: " + exception.GetType());
                }

            };
            imgBack.GestureRecognizers.Add(back);
        }

        private void PagOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            load.IsVisible = true;
        }
        private void PagOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            load.IsVisible = false;
        }

    }
}