﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class GradeBoletimView : RelativeLayout, ITabView
	{
		Command cmdClick;
		CustomLabel lblBimSelected;
		bool initializedView;

		public GradeBoletimView(int position)
		{
			InitializeComponent();

			cmdClick = new Command(async (obj) => await onClick(obj));

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl1Bim.ClassId });
			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl2Bim.ClassId });
			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl3Bim.ClassId });
			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lbl4Bim.ClassId });
			lblMf.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = lblMf.ClassId });

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			Utils.fixRoudedView(rounded1Bim);
			Utils.fixRoudedView(rounded2Bim);
			Utils.fixRoudedView(rounded3Bim);
			Utils.fixRoudedView(rounded4Bim);
			Utils.fixRoudedView(roundedMf);

			if (App.ScreenWidth <= 340)
			{
				lbl1Bim.WidthRequest = 52;
				lbl2Bim.WidthRequest = 52;
				lbl3Bim.WidthRequest = 52;
				lbl4Bim.WidthRequest = 52;
				lblMf.WidthRequest = 52;

				rounded1Bim.WidthRequest = 52;
				rounded2Bim.WidthRequest = 52;
				rounded3Bim.WidthRequest = 52;
				rounded4Bim.WidthRequest = 52;
				roundedMf.WidthRequest = 52;
			}

#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
			initLoad();
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

			if (position == 0)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getBoletim();
						});
					});
				});
			}
		}

		public async void selectBim(RoundedBoxView roundedSel, CustomLabel labelSel, StackLayout stackContentItens, ScrollView scroll, int i)
		{
			rounded1Bim.Color = Color.Transparent;
			lbl1Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded2Bim.Color = Color.Transparent;
			lbl2Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded3Bim.Color = Color.Transparent;
			lbl3Bim.TextColor = Color.FromHex("#a3a3a3");

			rounded4Bim.Color = Color.Transparent;
			lbl4Bim.TextColor = Color.FromHex("#a3a3a3");

			roundedMf.Color = Color.Transparent;
			lblMf.TextColor = Color.FromHex("#a3a3a3");

			roundedSel.Color = Color.FromHex("#3ea6e1");
			labelSel.TextColor = Color.White;

			if (lblBimSelected == null || !lblBimSelected.Text.Equals(labelSel.Text))
			{
				await Utils.FadeView(labelSel);
				await fadeAllBim();
				scroll.IsVisible = true;
				await stackContentItens.FadeTo(1, 350, null);
			}
			lblBimSelected = labelSel;

			createItem(i, stackContentItens);
		}

		async Task onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(lbl1Bim.ClassId))
				{
					selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, 0);
				}
				else if (str.Equals(lbl2Bim.ClassId))
				{
					selectBim(rounded2Bim, lbl2Bim, stackContentItens2Bim, scrollContentItens2Bim, 1);
				}
				else if (str.Equals(lbl3Bim.ClassId))
				{
					selectBim(rounded3Bim, lbl3Bim, stackContentItens3Bim, scrollContentItens3Bim, 2);
				}
				else if (str.Equals(lbl4Bim.ClassId))
				{
					selectBim(rounded4Bim, lbl4Bim, stackContentItens4Bim, scrollContentItens4Bim, 3);
				}
				else if (str.Equals(lblMf.ClassId))
				{
					selectBim(roundedMf, lblMf, stackContentItensMf, scrollContentItensMf, 4);
				}
				else if (str.Equals(stkError.ClassId))
				{
					await initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								getBoletim();
								stackBimesters.IsVisible = true;
								boxSeparator.IsVisible = true;
								load.IsVisible = false;
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void createEmptyitem(StackLayout stackContentItens)
		{
			if (stackContentItens.Children.Count == 0)
			{
				var lblErrorDayOfTheWeek = new CustomLabel
				{
					Text = App.Current.Resources["general_empty"] as String,
					TextColor = Color.FromHex("#a3a3a3"),
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(30, 60, 30, 0),
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.CenterAndExpand
				};

				stackContentItens.Children.Add(lblErrorDayOfTheWeek);
				stackContentItens.HorizontalOptions = LayoutOptions.CenterAndExpand;
			}
		}

		public async Task initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			await fadeAllBim();
			stackBimesters.IsVisible = false;
			boxSeparator.IsVisible = false;
			lblError.IsVisible = false;
		}

		public async Task fadeAllBim()
		{
			await Task.WhenAll(
				stackContentItens1Bim.FadeTo(0, 0, null),
				stackContentItens2Bim.FadeTo(0, 0, null),
				stackContentItens3Bim.FadeTo(0, 0, null),
				stackContentItens4Bim.FadeTo(0, 0, null),
				stackContentItensMf.FadeTo(0, 0, null)
			);
			scrollContentItens1Bim.IsVisible = false;
			scrollContentItens2Bim.IsVisible = false;
			scrollContentItens3Bim.IsVisible = false;
			scrollContentItens4Bim.IsVisible = false;
			scrollContentItensMf.IsVisible = false;
		}

		public void getBoletim()
		{
			SeculosBO.Instance.getBoletimWS(SeculosBO.Instance.DependenteConnected, (msg) =>
			{
				var listBoletim = SeculosBO.Instance.DependenteConnected.listBoletim;
				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listBoletim == null || listBoletim.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.BOLETIM_ERROR))
				{
					if (listBoletim == null || listBoletim.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.BOLETIM_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listBoletim == null || listBoletim.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listBoletim != null && listBoletim.Count > 0)
				{
					initView();
				}
				else
				{ 
					stackBimesters.IsVisible = true;
					boxSeparator.IsVisible = true;
					load.IsVisible = false;
				}

			});
		}

		public void initView()
		{
			createHeaderBim();
			selectBim(rounded1Bim, lbl1Bim, stackContentItens1Bim, scrollContentItens1Bim, 0);
			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			stackBimesters.IsVisible = true;
			boxSeparator.IsVisible = true;
			load.IsVisible = false;
		}

		public void createItem(int i, StackLayout stackSel)
		{
			if (stackSel.Children.Count > 1)
				return;
			
			var listBoletim = SeculosBO.Instance.DependenteConnected.listBoletim;

			if (listBoletim == null || listBoletim.Count == 0)
				return;

			double[] widthsArray = getWidthLabel();
			var backGroundColor = "#f6f6f6";
			if (i < 4)
			{
				foreach (Boletim boletim in listBoletim)
				{
					var lblDisciplinaName = new CustomLabel
					{
						Text = boletim.NmDisciplina.ToUpper(),
						TextColor = Color.FromHex("#555555"),
						WidthRequest = widthsArray[1],
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Start,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblMediaValue = new CustomLabel
					{
						WidthRequest = widthsArray[3],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblFaltasValue = new CustomLabel
					{
						WidthRequest = widthsArray[3],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var stackChildren = new StackLayout
					{
						Spacing = 0,
						Orientation = StackOrientation.Horizontal,
						BackgroundColor = Color.FromHex(backGroundColor),
						VerticalOptions = LayoutOptions.StartAndExpand,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = { lblDisciplinaName, lblMediaValue, lblFaltasValue },
						Padding = new Thickness(widthsArray[0], 5, widthsArray[0], 5)
					};
					stackSel.Children.Add(stackChildren);

					switch (i)
					{
						case 0:
							lblMediaValue.Text = boletim.Mb1B;
							lblFaltasValue.Text = boletim.F1B;
							break;
						case 1:
							lblMediaValue.Text = boletim.Mb2B;
							lblFaltasValue.Text = boletim.F2B;
							break;
						case 2:
							lblMediaValue.Text = boletim.Mb3B;
							lblFaltasValue.Text = boletim.F3B;
							break;
						case 3:
							lblMediaValue.Text = boletim.Mb4B;
							lblFaltasValue.Text = boletim.F4B;
							break;
					}

					if (backGroundColor.Equals("#f6f6f6"))
					{
						backGroundColor = "#ffffff";
					}
					else
					{
						backGroundColor = "#f6f6f6";
					}
				}

				var lblObsTitle = new CustomLabel
				{
					Text = "OBSERVAÇÃO",
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(widthsArray[0], 10, 0, -3)
				};
				stackSel.Children.Add(lblObsTitle);

				var lblObs = new CustomLabel
				{
					Text = listBoletim[0].LgObs,
					TextColor = Color.FromHex("#98999a"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 13.5,
					Margin = new Thickness(widthsArray[0], 0, widthsArray[0], 10)
				};
				stackSel.Children.Add(lblObs);

				stackSel.IsVisible = true;
			}
			else
			{ 
				if (App.ScreenWidth <= 340)
				{
					widthsArray = getWidthLabel(4.2);
				}

				backGroundColor = "#f6f6f6";
				foreach (Boletim boletim in listBoletim)
				{
					var lblDisciplinaName = new CustomLabel
					{
						Text = boletim.NmDisciplina.ToUpper(),
						TextColor = Color.FromHex("#555555"),
						WidthRequest = widthsArray[1],
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Start,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblMaValue = new CustomLabel
					{
						Text = boletim.Maf,
						WidthRequest = widthsArray[2],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblNrfValue = new CustomLabel
					{
						Text = boletim.Nrf,
						WidthRequest = widthsArray[2],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblMarfValue = new CustomLabel
					{
						Text = boletim.Marf,
						WidthRequest = widthsArray[2],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var lblTfValue = new CustomLabel
					{
						Text = boletim.Tf,
						WidthRequest = widthsArray[2],
						TextColor = Color.FromHex("#555555"),
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalTextAlignment = TextAlignment.Center,
						FontType = "Medium",
						FontSize = 15,
					};

					var stackChildren = new StackLayout
					{
						Spacing = 0,
						BackgroundColor = Color.FromHex(backGroundColor),
						Orientation = StackOrientation.Horizontal,
						VerticalOptions = LayoutOptions.StartAndExpand,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = { lblDisciplinaName, lblMaValue, lblNrfValue, lblMarfValue, lblTfValue },
						Padding = new Thickness(widthsArray[0], 5, widthsArray[0], 5)
					};
					stackSel.Children.Add(stackChildren);

					if (backGroundColor.Equals("#f6f6f6"))
					{
						backGroundColor = "#ffffff";
					}
					else
					{
						backGroundColor = "#f6f6f6";
					}

				}
				var lblLegendTitle = new CustomLabel
				{
					Text = "LEGENDA",
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(widthsArray[0], 10, 0, -3)
				};
				stackSel.Children.Add(lblLegendTitle);

				var lblLegend = new CustomLabel
				{
					Text = listBoletim[0].LgMedia,
					TextColor = Color.FromHex("#98999a"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 13.5,
					Margin = new Thickness(widthsArray[0], 0, widthsArray[0], 5)
				};
				stackSel.Children.Add(lblLegend);

				var lblObsTitle = new CustomLabel
				{
					Text = "OBSERVAÇÃO",
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 13.5,
					Margin = new Thickness(widthsArray[0], 10, 0, -3)
				};
				stackSel.Children.Add(lblObsTitle);

				var lblObs = new CustomLabel
				{
					Text = listBoletim[0].LgObs,
					TextColor = Color.FromHex("#98999a"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					HorizontalOptions = LayoutOptions.Start,
					FontType = "Medium",
					FontSize = 14,
					Margin = new Thickness(widthsArray[0], 0, widthsArray[0], 10)
				};
				stackSel.Children.Add(lblObs);
				stackSel.IsVisible = true;
			}
		}

		public void createHeaderBim()
		{
			var listBoletim = SeculosBO.Instance.DependenteConnected.listBoletim;

			double[] widthsArray = getWidthLabel();

			for (int i = 0; i < 5; i++)
			{
				if (i == 4 && App.ScreenWidth <= 340)
				{
					widthsArray = getWidthLabel(4.2);
				}

				var lblDisciplina = new CustomLabel
				{
					Text = "DISCIPLINA",
					TextColor = Color.FromHex("#555555"),
					WidthRequest = widthsArray[1],
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Start,
					FontType = "Medium",
					FontSize = 15,
				};

				var lblMedia = new CustomLabel
				{
					Text = "MEDIA",
					WidthRequest = widthsArray[3],
					TextColor = Color.FromHex("#555555"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 15,
				};

				var lblFaltas = new CustomLabel
				{
					Text = "FALTAS",
					WidthRequest = widthsArray[3],
					TextColor = Color.FromHex("#555555"),
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalTextAlignment = TextAlignment.Center,
					FontType = "Medium",
					FontSize = 15,
				};

				var stackHeader = new StackLayout
				{
					Spacing = 0,
					Orientation = StackOrientation.Horizontal,
					VerticalOptions = LayoutOptions.StartAndExpand,
					Children = { lblDisciplina, lblMedia, lblFaltas },
					Padding = new Thickness(widthsArray[0], 5, 10, widthsArray[0])
				};

				switch (i)
				{
					case 0:
						stackContentItens1Bim.Children.Add(stackHeader);
						break;
					case 1:
						stackContentItens2Bim.Children.Add(stackHeader);
						break;
					case 2:
						stackContentItens3Bim.Children.Add(stackHeader);
						break;
					case 3:
						stackContentItens4Bim.Children.Add(stackHeader);
						break;
					case 4:
						lblMedia.Text = "MA";
						lblMedia.WidthRequest = widthsArray[2];
						lblFaltas.Text = "NRF";
						lblFaltas.WidthRequest = widthsArray[2];
						var lblMarf = new CustomLabel
						{
							Text = "MARF",
							WidthRequest = widthsArray[2],
							TextColor = Color.FromHex("#555555"),
							VerticalTextAlignment = TextAlignment.Center,
							HorizontalTextAlignment = TextAlignment.Center,
							FontType = "Medium",
							FontSize = 15,
						};
						var lblTF = new CustomLabel
						{
							Text = "TF",
							WidthRequest = widthsArray[2],
							TextColor = Color.FromHex("#555555"),
							VerticalTextAlignment = TextAlignment.Center,
							HorizontalTextAlignment = TextAlignment.Center,
							FontType = "Medium",
							FontSize = 15,
						};
						stackHeader.Children.Add(lblMarf);
						stackHeader.Children.Add(lblTF);
						stackContentItensMf.Children.Add(stackHeader);
						break;
				}
			}
		}

		double[] getWidthLabel(double calc = 4)
		{
			double paddingLefRight = (App.ScreenWidth * 12) / 359.75;

			double width = App.ScreenWidth - (paddingLefRight * 2);

			double firstLabelWidth = width * 0.39;

			double fourthLabelWidth = (width * 0.61) / calc;

			double twoLabelWidth = (width * 0.61) / 2;

			double[] values = new double[4];
			values[0] = paddingLefRight;
			values[1] = firstLabelWidth;
			values[2] = fourthLabelWidth;
			values[3] = twoLabelWidth;

			return values;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, ((string)App.Current.Resources["grade"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
