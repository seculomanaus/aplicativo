﻿namespace SeculosApp
{
	public static class MessagingConstants
	{
		public static readonly string UPDATE_TITLE_CAROUSEL = "update_title_carousel";
		public static readonly string UPDATE_TITLE_CAROUSEL_ACADEMIC = "update_title_carousel_academic";
        public static readonly string UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC = "update_title_carousel_semi_academic";
        public static readonly string UPDATE_TITLE_CAROUSEL_GRADE = "update_title_carousel_grade";
        public static readonly string UPDATE_TITLE_CAROUSEL_AGENDA = "update_title_carousel_agenda";
        public static readonly string UPDATE_TITLE_CAROUSEL_FINANCE = "update_title_carousel_finance";
        public static readonly string UPDATE_TITLE_CAROUSEL_CANTINA = "update_title_carousel_cantina";

        public static readonly string BASE_PHOTO_CHANGED = "base_photo_changed";
		public static readonly string SALDO_HAS_BEEN_UPDATED = "saldo_has_been_updated";
		public static readonly string ALERTA_HAS_BEEN_UPDATED = "alerta_has_been_updated";
		public static readonly string BOLETO_HAS_BEEN_UPDATED = "boleto_has_been_updated";
		public static readonly string LIMITE_DIARIO_HAS_BEEN_UPDATED = "dayle_limite_has_been_updated";

	}
}

