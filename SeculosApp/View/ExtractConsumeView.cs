﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace SeculosApp
{
	public class ExtractConsumeView : RelativeLayout, ITabView
	{
		CustomLabel monthLabel;
		Image imgCalendar;
		CustomLabel daySelectedLabel;
		CustomLabel errorLabel;
		Image imgArrow;
		StackLayout calendarImgsLayout;
		Command click;
		StackLayout stackCalendar;
		CalendarView _calendarView;
		bool initializedView;
		ActivityIndicator load;
		List<Consumo> listConsumo;
		StackLayout consumeLayout;
		CustomLabel subTotalValueLabel;
		CustomLabel comprarCreditoLabel;
		CustomLabel limiteConsumoLabel;
		CustomLabel saldoAvaliableValueLabel, limiteAvaliableValueLabel;
		public BaseExpandedView Page;
		BoxView boxLoading;

		public ExtractConsumeView(int position, BaseExpandedView Page)
		{
			ClassId = "ExtractConsumeView";
			this.Page = Page;
			BackgroundColor = Color.White;

			click = new Command((obj) => onClick(obj));

			var headerItem = CreateCalendarHeaderItem();

			var lineSeparator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(20, -6, 20, 0)
			};

			var lineSeparatorBottom = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(20, 0, 20, 0)
			};


			var headLayout = new StackLayout
			{
				Children = { headerItem, lineSeparator }
			};

			errorLabel = new CustomLabel
			{
				FontSize = 15,
				FontType = "Medium",
				Margin = new Thickness(20, 0, 20, 0),
				HorizontalTextAlignment = TextAlignment.Center
			};

			stackCalendar = new StackLayout
			{
				BackgroundColor = Color.White
			};

			load = new ActivityIndicator
			{
				Color = (Color)App.Current.Resources["blue_light_label"],
				IsVisible = false,
				IsRunning = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			consumeLayout = new StackLayout
			{
				Spacing = 10
			};

			var consumeScroll = new ScrollView
			{
				Content = consumeLayout
			};

			var subTotalLabel = new CustomLabel
			{
				Text = "Subtotal: ",
				FontSize = 15,
				TextColor = Color.Black,
				FontType = "Medium"
			};

			subTotalValueLabel = new CustomLabel
			{
				FontSize = 15,
				TextColor = Color.FromHex("#ff3232"),
				FontType = "Medium",
				Text = "R$ -"
			};

			boxLoading = new BoxView
			{
				BackgroundColor = Color.Black,
				Opacity = .3,
				IsVisible = false,
			};

			var subTotalLayout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Margin = new Thickness(0, 0, 20, 0),
				HorizontalOptions = LayoutOptions.EndAndExpand,
				Children = { subTotalLabel, subTotalValueLabel }
			};

			var saldoAvaliableLabel = new CustomLabel
			{
				Text = "Saldo disponível de Crédito:",
				FontSize = 15,
				TextColor = Color.Black,
				Margin = new Thickness(20, 0, 20, 0),
				FontType = "Medium"
			};

			saldoAvaliableValueLabel = new CustomLabel
			{
				TextColor = Color.FromHex("#00b200"),
				Text = "R$ " + SeculosBO.Instance.DependenteConnected.SaldoCash,
				Margin = new Thickness(20, 0, 20, 0),
				FontSize = 15,
				FontType = "Medium"
			};

			if (SeculosBO.Instance.DependenteConnected.SaldoCash.Contains("-"))
			{
				saldoAvaliableValueLabel.TextColor = Color.FromHex("#ff3232");
			}

			var limiteAvaliableLabel = new CustomLabel
			{
				Text = "Limite de uso diário:",
				Margin = new Thickness(20, 0, 20, 0),
				FontSize = 15,
				TextColor = Color.Black,
				FontType = "Medium"
			};

			limiteAvaliableValueLabel = new CustomLabel
			{
				Text = "R$ " + SeculosBO.Instance.DependenteConnected.LimiteCash,
				TextColor = Color.FromHex("#00b200"),
				Margin = new Thickness(20, 0, 20, 0),
				FontSize = 15,
				FontType = "Medium"
			};

			comprarCreditoLabel = new CustomLabel
			{
				ClassId = "comprarCreditoLabel",
				Text = "COMPRAR CRÉDITO",
				FontSize = 15,
				Margin = new Thickness(0, 10, 0, 10),
				HorizontalTextAlignment = TextAlignment.Center,
				TextColor = (Color)App.Current.Resources["blue_light_label"],
				FontType = "Medium"
			};

			limiteConsumoLabel = new CustomLabel
			{
				ClassId = "limiteConsumoLabel",
				Text = "LIMITE DE CONSUMO DIÁRIO",
				FontSize = 15,
				Margin = new Thickness(0, 0, 0, 10),
				HorizontalTextAlignment = TextAlignment.Center,
				TextColor = (Color)App.Current.Resources["blue_light_label"],
				FontType = "Medium"
			};

			comprarCreditoLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = comprarCreditoLabel.ClassId });
			limiteConsumoLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = limiteConsumoLabel.ClassId });

			var buttonsLayout = new StackLayout
			{
				Children = { comprarCreditoLabel, limiteConsumoLabel }
			};

			this.Children.Add(
				headLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; })
			);

			this.Children.Add(
				errorLabel,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .35; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				consumeScroll,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .35; })
			);

			this.Children.Add(
				subTotalLayout,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .52; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				saldoAvaliableLabel,
				Constraint.Constant(0),
				Constraint.RelativeToView(subTotalLayout, (parent, view) => { return view.Y + view.Height + 5; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				saldoAvaliableValueLabel,
				Constraint.Constant(0),
				Constraint.RelativeToView(saldoAvaliableLabel, (parent, view) => { return view.Y + view.Height + 5; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				limiteAvaliableLabel,
				Constraint.Constant(0),
				Constraint.RelativeToView(saldoAvaliableValueLabel, (parent, view) => { return view.Y + view.Height + 10; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				limiteAvaliableValueLabel,
				Constraint.Constant(0),
				Constraint.RelativeToView(limiteAvaliableLabel, (parent, view) => { return view.Y + view.Height + 5; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				lineSeparatorBottom,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - 100; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				buttonsLayout,
				Constraint.Constant(0),
				Constraint.RelativeToView(lineSeparatorBottom, (parent, view) => { return view.Y + view.Height + 10; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				stackCalendar,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height * .15; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				boxLoading,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				load,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			if (position == 1)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getConsume(DateTime.Now);
						});
					});
				});
			}
		}

		public void UpdateView()
		{
			saldoAvaliableValueLabel.Text = "R$ " + SeculosBO.Instance.DependenteConnected.SaldoCash;
			limiteAvaliableValueLabel.Text = "R$ " + SeculosBO.Instance.DependenteConnected.LimiteCash;

			if (SeculosBO.Instance.DependenteConnected.SaldoCash.Contains("-"))
			{
				saldoAvaliableValueLabel.TextColor = Color.FromHex("#ff3232");
			}
		}

		void createCalendar()
		{
			_calendarView = new CalendarView()
			{
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				MinDate = CalendarView.FirstDayOfMonth(new DateTime(DateTime.Now.Year, 1, 1)),
				MaxDate = CalendarView.LastDayOfMonth(new DateTime(DateTime.Now.Year, 12, 1)),
				ShouldHighlightDaysOfWeekLabels = false,
				SelectionBackgroundStyle = CalendarView.BackgroundStyle.CircleFill,
				ShowNavigationArrows = true,
				NavigationArrowsColor = Color.FromHex("#3ea6e1"),
				Margin = new Thickness(10, -15, 10, 20),
				DateBackgroundColor = Color.White,
				BackgroundColor = Color.Transparent,
				DateSeparatorColor = Color.White,
				SelectedDateBackgroundColor = Color.FromHex("#3ea6e1"),
				SelectedDateForegroundColor = Color.White
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				_calendarView.WidthRequest = App.ScreenWidth;
				_calendarView.Layout(new Rectangle(0, App.ScreenWidth, App.ScreenWidth, _calendarView.HeightRequest));
				if (App.ScreenWidth > 340)
				{
					_calendarView.Margin = new Thickness(42, 0, 0, 0);
				}
				else
				{
					_calendarView.Margin = new Thickness(0, -5, 0, 0);
				}
			}

			stackCalendar.Children.Add(_calendarView);

			_calendarView.DateSelected += (object sender, DateTime e) =>
			{
				Task.Factory.StartNew(() =>
				{
					Task.Delay(150).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getConsume(e);
						});
					});
				});

				stackCalendar.HeightRequest = 0;

				Utils.rotateArrowCalendar(imgArrow, imgCalendar);
			};
		}


		void getConsume(DateTime date)
		{
			load.IsVisible = true;
			boxLoading.IsVisible = true;
			errorLabel.IsVisible = false;

			monthLabel.Text = Utils.PascalCase(SeculosBO.Instance.Months[date.Month]);
			daySelectedLabel.Text = date.Day.ToString();

            System.Diagnostics.Debug.WriteLine("data consumo: " +daySelectedLabel.Text);

			SeculosBO.Instance.getConsumoWait(SeculosBO.Instance.DependenteConnected, date.ToString("dd/MM/yyyy"), (msg) =>
			{
				listConsumo = SeculosBO.Instance.DependenteConnected.listConsumo;

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listConsumo == null || listConsumo.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, errorLabel, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_CONSUMO_EMPTY))
				{
					errorLabel.IsVisible = true;
					SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, errorLabel, null);
				}
				else if (msg.Equals(EnumCallback.DATA_CONSUMO_ERROR))
				{
					errorLabel.IsVisible = true;
					SeculosBO.Instance.showViewError(EnumCallback.DATA_CONSUMO_ERROR, errorLabel, null);
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					errorLabel.IsVisible = true;
					SeculosBO.Instance.showViewError(EnumCallback.CONNECTION_ERROR, errorLabel, null);
				}

				load.IsVisible = false;
				boxLoading.IsVisible = false;

				consumeLayout.Children.Clear();
				subTotalValueLabel.Text = "R$ -";

				if (listConsumo != null && listConsumo.Count > 0 && !errorLabel.IsVisible)
				{
					errorLabel.IsVisible = false;
					handleConsumeItens();
				}
				else
				{
					errorLabel.IsVisible = true;
				}
			});
		}

		void handleConsumeItens()
		{
			var cdComprasList = new List<string>();

			double subtotal = 0;

			foreach (var consumo in listConsumo)
			{
				try
				{
					subtotal += double.Parse(Utils.formatCurrency(20, consumo.VlUnitario));
				}
				catch { }

				if (!cdComprasList.Contains(consumo.CdVenda))
				{
					cdComprasList.Add(consumo.CdVenda);
					var listConsumoQuery = from _consumo in listConsumo where _consumo.CdVenda == consumo.CdVenda select _consumo;
					consumeLayout.Children.Add(CreateItemLayout(listConsumoQuery.ToList()));
				}
			}

			subTotalValueLabel.Text = "R$ - " + Utils.getCashValue("" + subtotal);
		}

		StackLayout CreateItemLayout(List<Consumo> list)
		{
			var img = new Image
			{
				Source = "extrato_ic.png",
				Aspect = Aspect.AspectFit,
				VerticalOptions = LayoutOptions.Start,
				WidthRequest = 25,
				HeightRequest = 25
			};

			var title = new CustomLabel
			{
				Text = "Compra na Cantina",
				FontSize = 15,
				TextColor = Color.Black,
				Margin = new Thickness(10, 0, 0, 0),
				FontType = "Medium"
			};

			var codigoLabel = new CustomLabel
			{
				Text = list[0].CdVenda,
				FontSize = 10,
				TextColor = Color.Gray,
				FontType = "Medium",
				HorizontalOptions = LayoutOptions.EndAndExpand
			};

			var descsLayout = new StackLayout
			{
				Spacing = 8
			};

			var pricesLayout = new StackLayout
			{
				HorizontalOptions = LayoutOptions.EndAndExpand
			};

			descsLayout.Children.Add(title);

			pricesLayout.Children.Add(codigoLabel);

			foreach (var item in list)
			{
				var itemDescLabel = new CustomLabel
				{
					Text = item.NmProduto,
					FontSize = 12,
					LineBreakMode = LineBreakMode.TailTruncation,
					TextColor = Color.Gray,
					HorizontalOptions = LayoutOptions.Start,
					Margin = new Thickness(10, 0, 0, 0),
					FontType = "Medium"
				};

				var priceLabel = new CustomLabel
				{
					Text = "-" + item.VlUnitario.Trim(),
					FontSize = 13,
					TextColor = Color.Gray,
					HorizontalOptions = LayoutOptions.EndAndExpand,
					FontType = "Medium"
				};

				descsLayout.Children.Add(itemDescLabel);
				pricesLayout.Children.Add(priceLabel);

			}

			var mainlayout = new StackLayout
			{
				Margin = new Thickness(20, 0, 20, 0),
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				Children = { img, descsLayout, pricesLayout }
			};

			var lineSeparator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(20, 0, 20, 0)
			};

			return new StackLayout
			{
				Children = { mainlayout, lineSeparator }
			};
		}

		async void onClick(object obj)
		{
			var str = (string)obj;

			if (str.Equals(calendarImgsLayout.ClassId))
			{
				await Utils.FadeView(calendarImgsLayout);

				if (_calendarView == null)
				{
					createCalendar();
				}
				else if (stackCalendar.Height == 0)
				{
					stackCalendar.HeightRequest = _calendarView.Height;
				}
				else
				{
					stackCalendar.HeightRequest = 0;
				}

				Utils.rotateArrowCalendar(imgArrow, imgCalendar);
			}
			else if (str.Equals(limiteConsumoLabel.ClassId))
			{
				await Utils.FadeView(limiteConsumoLabel);
				SeculosBO.Instance.OpenModal(EnumModal.CREDIT_LIMIT, true, Page);
			}
			else if (str.Equals(comprarCreditoLabel.ClassId))
			{
				await Utils.FadeView(comprarCreditoLabel);

				SeculosBO.Instance.OpenModal(EnumModal.BUY_CREDIT, true, Page);

			}
		}

		public void OnPageAppeared()
		{
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_CANTINA, ((string)App.Current.Resources["extract_consume"]).ToUpper());
		}

		public void OnPageDisappeared()
		{
		}

		public void OnPageDisappearing()
		{
		}


		StackLayout CreateCalendarHeaderItem()
		{
			monthLabel = new CustomLabel
			{
				TextColor = Color.Black,
				FontSize = 15,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontType = "Medium",
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center
			};

			imgCalendar = new Image
			{
				Source = "calendar_ic.png",
				Aspect = Aspect.AspectFit,
				WidthRequest = 28,
				Margin = new Thickness(0, 0, 5, 0)
			};

			daySelectedLabel = new CustomLabel
			{
				FontSize = 14,
				Margin = new Thickness(0, 0, 35, 0),
				TextColor = (Color)App.Current.Resources["gray"],
				VerticalOptions = LayoutOptions.Center
			};

			imgArrow = new Image
			{
				Source = "arrow_up_yellow_ic.png",
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 5, 0),
				WidthRequest = 20,
				Rotation = 180
			};

			calendarImgsLayout = new StackLayout
			{
				ClassId = "calendarImgsLayout",
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center,
				Margin = new Thickness(0, 0, 5, 0),
				Children = { imgCalendar, daySelectedLabel, imgArrow }
			};
			calendarImgsLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = calendarImgsLayout.ClassId });

			var calendarHeaderLayout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Center,
				Margin = new Thickness(20, 10, 10, 15),
				Children = { monthLabel, calendarImgsLayout }
			};

			return calendarHeaderLayout;
		}
	}
}