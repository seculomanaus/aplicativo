﻿using System;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class NewsDetail : AbstractContentPage
	{
		public NewsDetail()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			var news = SeculosBO.Instance.NoticiaSelected;

			var html = new HtmlWebViewSource();

			var htmlTemp = @"<html>
							<body style='padding: 0px 30px 0px 30px;'>
								<label style='color: #000; font-size: 60px; font-weight: bold; display: inline-block;'>" +news.NmTitulo + "</label>" +
						   "<label style='color: #a3a3a3; font-size: 36px; margin-top: 8px; width: 100%; text-align: left; display: inline-block;'>" + news.Day + "</label>";

			if (!news.LkImagem.Equals(SeculosBO.EMPTY))
			{
				htmlTemp += "<img width='100%' src='" + news.LkImagem + "' style='display: inline-block; margin-top: 10px;'/>";
			}
			else
			{
				htmlTemp += "<img width='100%' src='broken_img_ic.png' style='display: inline-block; margin-top: 10px; background-size: cover;'/>";
			}

			htmlTemp += "<div style='color: #a3a3a3; margin-top: 10px; display: inline-block; font-size: 42px; text-align: justify;'>" + news.DcNoticia + "</div>" +
						"</body>" +
						"</html>";

			html.Html = htmlTemp;
			webViewDesc.Source = html;

			webViewDesc.Navigating += (s, e) =>
			{
				if (e.Url.StartsWith("http") || e.Url.StartsWith("https"))
				{
					try
					{
						var uri = new Uri(e.Url);
						Device.OpenUri(uri);
					}
					catch (Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("webViewDesc Exception: " + exception.Message);
					}

					e.Cancel = true;
				}
			};

			webViewDesc.Navigated += (sender, e) =>
			{
				System.Diagnostics.Debug.WriteLine("webViewDesc Navigated");
			};

			var back = new TapGestureRecognizer();
			var isBack = false;
			back.Tapped += async (s, e) =>
			{
				if (!isBack)
				{
					isBack = true;
					System.Diagnostics.Debug.WriteLine("back");
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(imgBackNews);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					try
					{
						await Navigation.PopAsync();
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("NewsDetail back exception: " + exception.Message + " Type: " + exception.GetType());
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("back has been pressed!");
				}
			};
			imgBackNews.GestureRecognizers.Add(back);

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;
		}
	}
}