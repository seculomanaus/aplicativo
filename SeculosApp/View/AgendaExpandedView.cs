﻿using Xamarin.Forms;

namespace SeculosApp
{
    public class AgendaExpandedView : BaseExpandedView
    {
        public AgendaExpandedView(int _position, string _pageId) : base(_position, _pageId)
        {
            // position = Page in carousel
            // 0 -> AgendaEventosView
            // 1 -> CalendarioAcademico

            // ---------------------------
            
            // azul 3ea6e1
            tabsLayout.BackgroundColor = Color.FromHex("#FFAB00");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_AGENDA, (obj, arg) =>
            {
                try
                {
                    var str = (string)arg;
                   
                    if (App.ScreenWidth == 359.75)
                    {
                        tabTitle.FontSize = 16;
                    }
                    else
                    {
                        tabTitle.FontSize = 17;
                    }
                    
                    tabTitle.Text = str;
                }
                catch {}
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_GRADE);
        }

        public override void handleCarouselPage()
        {
            var title = "";
            

            if (App.ScreenWidth == 359.75)
            {
                tabTitle.FontSize = 16;
            }
            else
            {
                tabTitle.FontSize = 17;
            }

            if (position == 0)
            {
                title = ((string)App.Current.Resources["agenda_eventos_str"]).ToUpper();
            }
            else
             {
                title = ((string)App.Current.Resources["calendarioAcademic"]).ToUpper();
             }

            tabTitle.Text = title;
        }

        public override Grid GenerateCarouselMotora()
        {
            Grid grid = new Grid
            {
                BackgroundColor = Color.White
            };

            //agenda eventos
            //grid.Children.Add(new AgendaEventosView(position), 0, 1, 0, 1);

            //calendario
            grid.Children.Add(new CalendarioAcademico(this, position), 0, 1, 0, 1);

            return grid;
        }
    }
}