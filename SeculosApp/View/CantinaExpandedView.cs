﻿using SeculosApp.View;
using Xamarin.Forms;

namespace SeculosApp
{
	public class CantinaExpandedView : BaseExpandedView
	{
		ExtractConsumeView extractConsumeView;

		public CantinaExpandedView(int _position, string _pageId) : base(_position, _pageId)
		{
            // position = Page in carousel
            // 0 -> CardapioView
            // 1 -> ExtractConsumeView
            // ---------------------------
            // default -> CardapioView

            tabsLayout.BackgroundColor = Color.FromHex(SeculosBO.Instance.DependenteConnected.ColorSerie);

			btClose.Source = "x_ic.png";

			Content = layout;
		}

		protected override void OnAppearing()
		{
			MessagingCenter.Subscribe<App, object>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_CANTINA, (obj, arg) =>
			{
				try
				{
					var str = (string)arg;
					tabTitle.Text = str;
					tabTitle.FontSize = 18;
					if (((string)App.Current.Resources["extract_consume"]).ToUpper().Equals(str))
					{
						if (Device.OS.Equals(TargetPlatform.iOS))
						{
							if (App.ScreenWidth <= 340)
							{
								tabTitle.FontSize = 14;
							}
							else if (App.ScreenWidth <= 375)
							{
								tabTitle.FontSize = 16;
							}
						}
						else
						{
							// A5
							if (App.ScreenWidth == 359.75)
							{
								tabTitle.FontSize = 13;
							}
							else
							{
								tabTitle.FontSize = 16;
							}
						}
					}
				}
				catch { }
			});

			MessagingCenter.Subscribe<App>(this, MessagingConstants.LIMITE_DIARIO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					extractConsumeView.UpdateView();
				}
				catch { }
			});


			MessagingCenter.Subscribe<App>(this, MessagingConstants.SALDO_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					extractConsumeView.UpdateView();
				}
				catch { }
			});

			FinanceView.StudentDetail = true;

			base.OnAppearing();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			FinanceView.StudentDetail = false;
			MessagingCenter.Unsubscribe<App>(this, MessagingConstants.UPDATE_TITLE_CAROUSEL_CANTINA);
			MessagingCenter.Unsubscribe<App>(this, MessagingConstants.SALDO_HAS_BEEN_UPDATED);
		}

		public override void handleCarouselPage()
		{
			var title = ((string)App.Current.Resources["cardapio_title"]).ToUpper();

			if (position == 1)
			{
				title = ((string)App.Current.Resources["extract_consume"]).ToUpper();
				if (Device.OS.Equals(TargetPlatform.iOS))
				{
					if (App.ScreenWidth <= 340)
					{
						tabTitle.FontSize = 14;
					}
					else if (App.ScreenWidth <= 375)
					{
						tabTitle.FontSize = 16;
					}
				}
				else
				{
					// A5
					if (App.ScreenWidth == 359.75)
					{
						tabTitle.FontSize = 13;
					}
					else
					{
						tabTitle.FontSize = 16;
					}
				}
			}
			else
			{
				tabTitle.FontSize = 17;
			}

			tabTitle.Text = title;
		}

		public override Grid GenerateCarouselMotora()
		{
			Grid grid = new Grid
			{
				BackgroundColor = Color.White
			};

			extractConsumeView = new ExtractConsumeView(position, this);

            grid.Children.Add(new CardapioView(3), 0, 1, 0, 1);

            grid.Children.Add(extractConsumeView, 0, 1, 0, 1);

			return grid;
		}
	}
}