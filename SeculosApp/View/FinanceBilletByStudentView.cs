﻿using System;
using System.Threading.Tasks;
using SeculosApp.View;
using Xamarin.Forms;

namespace SeculosApp
{
	public class FinanceBilletByStudentView : RelativeLayout, ITabView
	{
		Image btClose;
		Command cmdClick;
		public FinanceView financeView;

		public FinanceBilletByStudentView(BaseExpandedView BaseExpandedView)
		{
			ClassId = "FinanceBilletByStudentView";

			cmdClick = new Command((obj) => onClick(obj));

			BackgroundColor = Color.White;

			financeView = new FinanceView(SeculosBO.Instance.StudentSelected, BaseExpandedView);

			this.Children.Add(
				financeView,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);
		}

		RelativeLayout CreateTabsLayout()
		{
			var layout = new RelativeLayout
			{
				BackgroundColor = Color.FromHex(SeculosBO.Instance?.StudentSelected?.ColorSerie)
			};

			var tabTitle = new CustomLabel
			{
				Text = (App.Current.Resources["billets"] as String).ToUpper(),
				FontSize = 18,
				FontType = "Medium",
				TextColor = Color.Black,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				LineBreakMode = LineBreakMode.TailTruncation
			};

			layout.Children.Add(
				tabTitle,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return layout;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(btClose.ClassId))
				{
					await Utils.FadeView(btClose);
					await Navigation.PopAsync();
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(250).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		RelativeLayout CreateHeaderLayout()
		{
			var layout = new RelativeLayout();

			btClose = new Image
			{
				ClassId = "btClose",
				Source = "back_ic.png",
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			var btCloseLayout = new StackLayout
			{
				WidthRequest = 30,
				Children = { btClose }
			};
			btCloseLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btClose.ClassId });

			var titleLabel = new CustomLabel
			{
				Text = SeculosBO.Instance?.StudentSelected?.NomeAluno,
				FontType = "Medium",
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = 19,
				Margin = new Thickness(45, 0, 12, 0),
				LineBreakMode = LineBreakMode.TailTruncation
			};

			layout.Children.Add(
				btCloseLayout,
				Constraint.Constant(10),
				Constraint.Constant(0),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			layout.Children.Add(
				titleLabel,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return layout;
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL, ((string)App.Current.Resources["finance"]).ToUpper());
		}

		public void OnPageDisappearing()
		{
		}

		public void OnPageAppeared()
		{
		}

		public void OnPageDisappeared()
		{
		}
	}
}
