﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{

	public interface ITabView
	{
		void OnPageAppearing();
		void OnPageDisappearing();

		void OnPageAppeared();
		void OnPageDisappeared();
	}

	public class TabView
	{
		public Image tabSelector;
		public double width;
		public double widthPortion;
		public int tabSelected;
		public string tabSelectedName = string.Empty;
		public Label[] textLabelArray;
		public Grid carouselMainScreen { get; set; }
		public bool progress { get; set; }
		public Rectangle _dimen;
		public Rectangle _dimen1;
		string tabActual = string.Empty;
		string tabNext = string.Empty;
		public string[] tabNameArray;

		public TabView(Grid carouselMainScreen, Image tabSelector, double width, params RelativeLayout[] tabsParams)
		{
			this.carouselMainScreen = carouselMainScreen;
			this.tabSelector = tabSelector;
			this.width = width;

			RelativeLayout[] tabs = null;

			if (tabsParams != null && tabsParams.Length > 0)
			{
				tabs = tabsParams;
				textLabelArray = new Label[tabs.Length];

				widthPortion = width / tabs.Length;

				tabNameArray = new string[tabs.Length];

				for (int i = carouselMainScreen.Children.Count - 1; i >= 0; i--)
				{
					tabNameArray[i] = carouselMainScreen.Children[i].ClassId;
				}

				var cmd = new Command(async (arg) =>
				{
					int i = (int)arg;
					System.Diagnostics.Debug.WriteLine("Click na Tab: " + i);
					await changeTab(tabNameArray[(tabs.Length - 1) - i], i + 1, tabs[i]);
				});

				for (int i = 0; i < tabs.Length; i++)
				{
					if (textLabelArray != null)
					{
						addText(tabs[i], i);
						tabs[i].GestureRecognizers.Add(new TapGestureRecognizer { Command = cmd, CommandParameter = i });
					}
				}
			}
			else
			{
				tabs = new RelativeLayout[carouselMainScreen.Children.Count];
				for (int i = carouselMainScreen.Children.Count - 1; i >= 0; i--)
				{
					tabs[i] = carouselMainScreen.Children[i] as RelativeLayout;
				}

				tabNameArray = new string[tabs.Length];

				for (int i = carouselMainScreen.Children.Count - 1; i >= 0; i--)
				{
					tabNameArray[i] = carouselMainScreen.Children[i].ClassId;
				}
			}

			// init first Tab
			initTab(1, tabNameArray[tabs.Length - 1]);
		}

		public void addPrevNext(Xamarin.Forms.View prev, Xamarin.Forms.View next)
		{
			var cmd = new Command(async (arg) =>
			{
				string direction = (string)arg;
				string tabSelect = string.Empty;
				int index = 0;
				int turn = 0;
				Xamarin.Forms.View turnPressed = null;

				for (int i = 0; i < tabNameArray.Length; i++)
				{
					if (tabNameArray[i].Equals(tabSelectedName))
					{
						index = i;
						turnPressed = prev;
						break;
					}
				}

				if (direction.Equals("prev"))
				{
					if (index == tabNameArray.Length - 1)
					{
						tabSelect = tabNameArray[0];
					}
					else
					{
						tabSelect = tabNameArray[index + 1];
					}
					turnPressed = prev;
					turn = 0;
				}
				else if (direction.Equals("next"))
				{
					if (index == 0)
					{
						tabSelect = tabNameArray[tabNameArray.Length - 1];
					}
					else
					{
						tabSelect = tabNameArray[index - 1];
					}
					turnPressed = next;
					turn = 1;
				}
				await changeTab(tabSelect, index + 1, turnPressed, turn);
			});

			prev.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmd, CommandParameter = "prev" });
			next.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmd, CommandParameter = "next" });
		}

		public void initTab(int index, string name)
		{ 
			tabSelected = index;
			tabSelectedName = name;
			tabActual = name;

			if (textLabelArray != null)
			{
				enabledTab(index - 1);
			}
		}

		public async Task changeTab(string tabName, int tab, Xamarin.Forms.View tabPressed = null, int direction = -1)
		{
			if (!tabName.Equals(tabSelectedName) && !progress)
			{
				progress = true;
				uint duration = 60;
				double translateX = 0;
				var dif = tab - tabSelected;

				if (dif > 1 ||  dif < -1)
				{
					duration = duration + 10;
				}

				tabActual = tabSelectedName;
				tabNext = tabName;
				tabSelected = tab;
				tabSelectedName = tabNext;

				if (textLabelArray != null)
				{
					enabledTab(tab - 1);
				}

				RelativeLayout tabActualClass = null;
				RelativeLayout tabNextClass = null;
				if (carouselMainScreen != null)
				{
					foreach (RelativeLayout rel in carouselMainScreen.Children)
					{
						System.Diagnostics.Debug.WriteLine("Class ID:" + rel.ClassId);
						if (tabActual.Equals(rel.ClassId))
						{
							tabActualClass = rel;
						}
						else if (tabNext.Equals(rel.ClassId))
						{
							tabNextClass = rel;
						}
					}

					if ((tabActualClass as ITabView) != null)
					{
						(tabActualClass as ITabView).OnPageDisappearing();
					}

					if ((tabNextClass as ITabView) != null)
					{
						(tabNextClass as ITabView).OnPageAppearing();
					}

					if (direction != -1)
					{
						dif = direction;
					}

					if (dif > 0)
					{
						tabNextClass.Layout(new Rectangle(-width, tabNextClass.Y, tabNextClass.Width, carouselMainScreen.Height));
						_dimen = new Rectangle(tabActualClass.X + width, tabActualClass.Y, tabActualClass.Width, carouselMainScreen.Height);
					}
					else 
					{
						tabNextClass.Layout(new Rectangle(width, tabNextClass.Y, tabNextClass.Width, carouselMainScreen.Height));
						_dimen = new Rectangle(tabActualClass.X - width, tabActualClass.Y, tabActualClass.Width, carouselMainScreen.Height);
					}
					_dimen1 = new Rectangle(0, tabNextClass.Y, tabNextClass.Width, carouselMainScreen.Height);

					List<Task> tasks = new List<Task>();

					tasks.Add(tabActualClass.LayoutTo(_dimen, duration, Easing.CubicOut));
					tasks.Add(tabNextClass.LayoutTo(_dimen1, duration, Easing.CubicOut));

					if (tabSelector != null)
					{
						translateX = (dif * widthPortion) + tabSelector.TranslationX;
						tasks.Add(tabSelector.TranslateTo(translateX, tabSelector.TranslationY, duration));
					}

					if ((tabNextClass as IBindingContextView) != null)
					{
						(tabNextClass as IBindingContextView).changeBindingContext();
					}
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					Utils.FadeView(tabPressed);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					await Task.WhenAll(tasks);
					if ((tabActualClass as ITabView) != null)
					{
						(tabActualClass as ITabView).OnPageDisappeared();
					}

					if ((tabNextClass as ITabView) != null)
					{
						(tabNextClass as ITabView).OnPageAppeared();
					}
				}

				carouselMainScreen.RaiseChild(tabNextClass);
				progress = false;
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("Same tab selected or in progress: " + tab);
			}
		}

		public void addText(RelativeLayout tab, int index)
		{
			foreach (Xamarin.Forms.View view in tab.Children)
			{
				if (view is Label)
				{
					textLabelArray[index] = (Label)view;
				}
			}
		}

		public void enabledTab(int index)
		{
			for (int i = 0; i < textLabelArray.Length; i++)
			{
				var textColor = "";
				if (index == i)
				{
					textColor = "white";
				}
				else
				{
					textColor = "gray_light";
				}

				textLabelArray[i].TextColor = (Xamarin.Forms.Color)App.Current.Resources[textColor];
			}
		}
	}
}
