﻿using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp.View
{
	public partial class ComunicationsHistoryView : AbstractContentPage
	{
		public CustomLabel LblStudentSelected { get; set; }
		public CustomLabel LblMonthSelected { get; set; }

		public ComunicationsHistoryView()
		{
			InitializeComponent();
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			abstractContentPageExtends = this;

			LblStudentSelected = lblStudentSelected;
			LblMonthSelected = lblMonthSelected;

			// init ListView
			setListComunications(SeculosBO.Instance.studentSelectedComunicationsPicker, SeculosBO.Instance.monthSelectedComunicationsPicker);

			var studentOrMonth = false;

			if (!SeculosBO.Instance.studentSelectedComunicationsPicker.Equals(string.Empty))
			{
				lblStudentSelected.Text = SeculosBO.Instance.studentSelectedComunicationsPicker;
			}

			if (!SeculosBO.Instance.monthSelectedComunicationsPicker.Equals(string.Empty))
			{
				lblMonthSelected.Text = SeculosBO.Instance.monthSelectedComunicationsPicker;
			}

			pickerStudents.SelectedIndexChanged += (sender, args) =>
			{
				if (pickerStudents.SelectedIndex != -1)
				{
					string student = pickerStudents.Items[pickerStudents.SelectedIndex];
					lblStudentSelected.Text = student;
					SeculosBO.Instance.studentSelectedComunicationsPicker = student;
					// update ListView
					setListComunications(SeculosBO.Instance.studentSelectedComunicationsPicker, SeculosBO.Instance.monthSelectedComunicationsPicker);
					System.Diagnostics.Debug.WriteLine("pickerStudents: student: " + student);
				}
			};

			var tapStudents = new TapGestureRecognizer();
			tapStudents.Tapped += async (s, e) =>
			{
				if (!studentOrMonth)
				{
					studentOrMonth = true;
					var listDependente = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;
					pickerStudents.Items.Clear();
					pickerStudents.Items.Add(Application.Current.Resources["all"] as String);
					foreach (Dependente aluno in listDependente)
					{
						pickerStudents.Items.Add(aluno.NomeAluno);
					}
					await Utils.FadeView(btnSelectStudent);
					Device.BeginInvokeOnMainThread(() => pickerStudents.Focus());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(1500).ContinueWith((obj) =>
						{
							studentOrMonth = false;
						});
					});
				}
			};
			btnSelectStudent.GestureRecognizers.Add(tapStudents);

			pickerMonths.SelectedIndexChanged += (sender, args) =>
			{
				if (pickerMonths.SelectedIndex != -1)
				{
					string month = pickerMonths.Items[pickerMonths.SelectedIndex];
					lblMonthSelected.Text = month;

					SeculosBO.Instance.monthSelectedComunicationsPicker = month;
					// update ListView
					setListComunications(SeculosBO.Instance.studentSelectedComunicationsPicker, SeculosBO.Instance.monthSelectedComunicationsPicker);
					System.Diagnostics.Debug.WriteLine("pickerMonths: month: " + month);
				}
			};

			var tapMonths = new TapGestureRecognizer();
			tapMonths.Tapped += async (s, e) =>
			{
				if (!studentOrMonth)
				{
					studentOrMonth = true;
					var months = SeculosBO.Instance.Months;
					pickerMonths.Items.Clear();
					foreach (string monthValue in months)
					{
						pickerMonths.Items.Add(monthValue);
					}
					await Utils.FadeView(btnSelectMonth);
					Device.BeginInvokeOnMainThread(() => pickerMonths.Focus());
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(1500).ContinueWith((obj) =>
					   {
						   studentOrMonth = false;
					   });
					});
				}
			};
			btnSelectMonth.GestureRecognizers.Add(tapMonths);

			var back = new TapGestureRecognizer();
			back.Tapped += async (s, e) =>
			{
				await Utils.FadeView(imgBack);

				try
				{
					await Navigation.PopAsync();
				}
				catch (Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("ComunicationsHistoryView back exception: " + exception.Message + " Type: " + exception.GetType());
				}
			};
			imgBack.GestureRecognizers.Add(back);

			listViewComunications.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{

				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				if (!App.GlobalBlockClick)
				{
					SeculosBO.Instance.ComunicationSelected = (Notificacao)((ListView)sender).SelectedItem;
				}
				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect

				if (!App.GlobalBlockClick)
				{
					App.GlobalBlockClick = true;
					SeculosBO.Instance.OpenModal(EnumModal.COMUNICATION, false, this);
					await Task.Factory.StartNew(() =>
				 	{
			 			Task.Delay(500).ContinueWith((obj) =>
						{
							App.GlobalBlockClick = false;
						});
				 	});
				}
			};
		}

		void setListComunications(string StudentName = null, string Month = null)
		{
			var listComunications = SeculosBO.Instance.SeculosGeneral.UserConnected.getAllListComunications(StudentName, Month);
			if (listComunications.Count > 0)
			{
				lblErrorComunicationsEmpty.IsVisible = false;
				listViewComunications.ItemsSource = listComunications;
			}
			else
			{
				lblErrorComunicationsEmpty.IsVisible = true;
				listViewComunications.ItemsSource = null;
			}
		}

		protected override void OnAppearing()
		{
			RelContentModal = relContentModal;
			relContentModalExtends = relContentModal;
			MessagingCenter.Subscribe<App>(this, MessagingConstants.ALERTA_HAS_BEEN_UPDATED, (obj) =>
			{
				try
				{
					// init ListView
					setListComunications(SeculosBO.Instance.studentSelectedComunicationsPicker, SeculosBO.Instance.monthSelectedComunicationsPicker);
				}
				catch { }
			});
			base.OnAppearing();

		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			MessagingCenter.Unsubscribe<App>(this, MessagingConstants.ALERTA_HAS_BEEN_UPDATED);
		}
	}
}
