﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class AgendaView : RelativeLayout, ITabView
	{
		Command cmdClick;
		RelativeLayout  agendaEventosLayout, calendarioAcademicoLayout;
		bool blockClick;

		public AgendaView()
		{
			ClassId = "AgendaView";
			cmdClick = new Command((obj) => onClick(obj));

			BackgroundColor = Color.White;

				agendaEventosLayout = Utils.CreateItemLayout("Agenda de Eventos");
                calendarioAcademicoLayout = Utils.CreateItemLayout("Calendário Acadêmico");

                agendaEventosLayout.ClassId = "AgendaEventosView";
                calendarioAcademicoLayout.ClassId = "CalendarAcademicView";

                agendaEventosLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = agendaEventosLayout.ClassId });
                calendarioAcademicoLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = calendarioAcademicoLayout.ClassId });

                var stack = new StackLayout
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
                    Children = { calendarioAcademicoLayout }, // agendaEventosLayout removido
					Margin = new Thickness(0, 8, 0, 0)
				};

				Children.Add(
					stack,
					Constraint.Constant(0),
					Constraint.Constant(0),
					Constraint.RelativeToParent((parent) => { return parent.Width; }),
					Constraint.RelativeToParent((parent) => { return parent.Height; })
				);
			//}
		}

		async void onClick(object obj)
		{
			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

                if (str.Equals(agendaEventosLayout.ClassId))
                {
                    await Utils.FadeView(agendaEventosLayout);
                    await Navigation.PushAsync(new AgendaExpandedView(0, "AgendaEventosView"));
                }
                else if (str.Equals(calendarioAcademicoLayout.ClassId))
                {
                    await Utils.FadeView(calendarioAcademicoLayout);
                    await Navigation.PushAsync(new AgendaExpandedView(1, "CalendarAcademicView"));
                }


                    await Task.Factory.StartNew(() =>
				{
					Task.Delay(500).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC, (string)App.Current.Resources["calendarioAcademic"]);
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
