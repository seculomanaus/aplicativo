﻿using System;
namespace SeculosApp
{
	public class EnumModal
	{
		public const string BUY_CREDIT = "buy_credit";
		public const string COMUNICATION = "comunication_modal";
		public const string CONTACT = "contact_modal";
		public const string CREDIT_LIMIT = "credit_limit";
		public const string EVENT_STUDENT_SOLD = "pay_with_credit_event";
		public const string PAY_CREDIT = "pay_credit";
		public const string EVENT_CALENDAR = "event_calendar";
	}
}
