﻿using System;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class EventCalendarModal : RelativeLayout, AbstractModal
	{
		public static bool loading;

		public EventCalendarModal(ContentPage Page, CalendarAcademic calendarAcademic, string dateString)
		{
			InitializeComponent();

			lblDescricao.Text = calendarAcademic.DescCalendario;

			lblData.Text = dateString;

            lblNota.Text = "Nota: " + calendarAcademic.NotaProva;

            lblInfo.Text = calendarAcademic.InfoProva;

            btnCancel.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    await Utils.FadeView(btnCancel);
                    Page.SendBackButtonPressed();
                })
            });


			System.Diagnostics.Debug.WriteLine("calendario academico:Anexo " + calendarAcademic.Anexo);
			if (calendarAcademic.Anexo.Trim().Contains("(vazio)"))
            {
				btnVerAnexo.IsVisible = false;
			}

			btnVerAnexo.Clicked += async (sender, e) =>
			{
				redirectToPDF(calendarAcademic.Anexo);
			};
		}

		private void redirectToPDF(string baseUrl)
		{

			bool isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);

			string studentCode;

			if (isResp)
			{
				studentCode = SeculosBO.Instance.StudentSelected.CdAluno;
			}
			else
			{
				studentCode = SeculosBO.Instance.SeculosGeneral.UserConnected.CdUsuario;
			}

			string url = baseUrl + "?ra="+ studentCode;

			System.Diagnostics.Debug.WriteLine("calendario academico: URL Evento "+ url);

			Device.OpenUri(new Uri(url));

		}

		public void showModal()
		{
			var margin = Math.Abs((App.ScreenHeight - mainContentModalContact.Height) / 2);
			mainContentModalContact.Margin = new Thickness(mainContentModalContact.Margin.Left, margin, mainContentModalContact.Margin.Right, 0);
			this.FadeTo(1, 50, null);
		}

        
	}
}
