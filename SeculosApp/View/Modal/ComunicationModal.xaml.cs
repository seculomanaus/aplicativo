using System;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ComunicationModal : RelativeLayout, AbstractModal
	{
		AbstractContentPage Page;
		public Notificacao comunicationSelected { get; set; }
		public static bool loading;
		public static bool updateByPayCreditCard;
		public static bool updateByPayCreditStudent;

		public ComunicationModal(AbstractContentPage Page)
		{
			InitializeComponent();
			updateByPayCreditCard = false;
			this.Page = Page;
			comunicationSelected = SeculosBO.Instance.ComunicationSelected;
			Command cmdClick = new Command((obj) => onClick(obj));
			//btnCreditCard.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnCreditCard.ClassId });
			//btnStudentCredit.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnStudentCredit.ClassId });
			//btnRefuse.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnRefuse.ClassId });
			btnRefuseSimple.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnRefuseSimple.ClassId });
			btnConfirm.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnConfirm.ClassId });
			btnClose.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btnClose.ClassId });
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

              
				//if (str.Equals(btnCreditCard.ClassId))
				//{
				//	await Utils.FadeView(btnCreditCard);
				//	if (btnClose.IsVisible)
				//	{
				//		string base64EncodedExternalAccount = SeculosBO.Instance.CreateEventPayment(comunicationSelected);
		        //      System.Diagnostics.Debug.WriteLine(Application.Current.Resources["hotsite_pay_event"] as String+ base64EncodedExternalAccount);
				//		Device.OpenUri(new Uri(Application.Current.Resources["hotsite_pay_event"] as String + base64EncodedExternalAccount));
				//		updateByPayCreditCard = true;
				//		App.GlobalBlockClick = false;
				//		Page.SendBackButtonPressed();
				//	}
				//	else
				//	{
				//		loading = true;
				//		viewLoadingComunicationModal.IsVisible = true;
				//		loadComunicationModal.IsVisible = true;
				//		await SeculosBO.Instance.ConfirmEvent(comunicationSelected, "S", (result, msg) =>
				//		{
				//			SeculosBO.Instance.IMessage.Msg(msg);
				//			loading = false;
				//			viewLoadingComunicationModal.IsVisible = false;
				//			loadComunicationModal.IsVisible = false;
				//			if (result)
				//			{
				//				string base64EncodedExternalAccount = SeculosBO.Instance.CreateEventPayment(comunicationSelected);
				//				System.Diagnostics.Debug.WriteLine(Application.Current.Resources["hotsite_pay_event"] as String+ base64EncodedExternalAccount);
				//				Device.OpenUri(new Uri(Application.Current.Resources["hotsite_pay_event"] as String + base64EncodedExternalAccount));
				//				updateByPayCreditCard = true;
				//				App.GlobalBlockClick = false;
				//				Page.SendBackButtonPressed();
				//				App._Current.SendMessage(MessagingConstants.ALERTA_HAS_BEEN_UPDATED);
				//			}
				//		});
				//	}

				//}
				//else if (str.Equals(btnStudentCredit.ClassId))
				//{
				//	await Utils.FadeView(btnStudentCredit);
				//	if (btnClose.IsVisible)
				//	{
				//		Page.SendBackButtonPressed();
				//		PayCreditModal.fromComunicado = true;
				//		SeculosBO.Instance.OpenModal(EnumModal.PAY_CREDIT, false, Page);
				//		App.GlobalBlockClick = false;
				//	}
				//	else 
				//	{
				//		loading = true;
				//		viewLoadingComunicationModal.IsVisible = true;
				//		loadComunicationModal.IsVisible = true;
				//		await SeculosBO.Instance.ConfirmEvent(comunicationSelected, "S", async (result, msg) =>
				//		{
				//			if (result)
				//			{
				//				updateByPayCreditStudent = true;
				//				await SeculosBO.Instance.OnResumeUpdate();
				//				SeculosBO.Instance.IMessage.Msg(msg);
				//				loading = false;
				//				viewLoadingComunicationModal.IsVisible = false;
				//				loadComunicationModal.IsVisible = false;
				//				Page.SendBackButtonPressed();
				//				PayCreditModal.fromComunicado = true;
				//				SeculosBO.Instance.OpenModal(EnumModal.PAY_CREDIT, false, Page);
				//				App.GlobalBlockClick = false;
				//			}
				//			else
				//			{
				//				loading = false;
				//				viewLoadingComunicationModal.IsVisible = false;
				//				loadComunicationModal.IsVisible = false;
				//			}
				//		});
				//	}
				//}
				//else 
				if (str.Equals(btnRefuseSimple.ClassId))
				{
					loading = true;
					viewLoadingComunicationModal.IsVisible = true;
					loadComunicationModal.IsVisible = true;
					await Utils.FadeView(btnRefuseSimple);
					await SeculosBO.Instance.ConfirmEvent(comunicationSelected, "N", (result, msg) =>
					{
						SeculosBO.Instance.IMessage.Msg(msg);
						loading = false;
						viewLoadingComunicationModal.IsVisible = false;
						loadComunicationModal.IsVisible = false;
						if (result)
						{
							App.GlobalBlockClick = false;
							Page.SendBackButtonPressed();
							App._Current.SendMessage(MessagingConstants.ALERTA_HAS_BEEN_UPDATED);
						}
					});
				}
				else if (str.Equals(btnConfirm.ClassId))
				{
					loading = true;
					viewLoadingComunicationModal.IsVisible = true;
					loadComunicationModal.IsVisible = true;
					await Utils.FadeView(btnConfirm);
					await SeculosBO.Instance.ConfirmEvent(comunicationSelected, "S", async (result, msg) =>
					{
						if (result)
						{
							if (comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_GRATIS))
							{
								SeculosBO.Instance.IMessage.Msg(msg);
								loading = false;
								viewLoadingComunicationModal.IsVisible = false;
								loadComunicationModal.IsVisible = false;
								App.GlobalBlockClick = false;
								Page.SendBackButtonPressed();
								App._Current.SendMessage(MessagingConstants.ALERTA_HAS_BEEN_UPDATED);
							}
							else
							{
								updateByPayCreditStudent = true;
								await SeculosBO.Instance.OnResumeUpdate();
								SeculosBO.Instance.IMessage.Msg(msg);
								loading = false;
								viewLoadingComunicationModal.IsVisible = false;
								loadComunicationModal.IsVisible = false;
								Page.SendBackButtonPressed();
								App.GlobalBlockClick = false;
							}
						}
						else
						{
							SeculosBO.Instance.IMessage.Msg(msg);
							loading = false;
							viewLoadingComunicationModal.IsVisible = false;
							loadComunicationModal.IsVisible = false;
						}
					});
				}
				else if (str.Equals(btnClose.ClassId))
				{
					await Utils.FadeView(btnClose);
					App.GlobalBlockClick = false;
					Page.SendBackButtonPressed();
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		void UpdateViewModal()
		{
			if (comunicationSelected.Dependente == null)
			{
				SeculosBO.Instance.IMessage.Msg(Application.Current.Resources["details_not_avaliable"] as String);
				Page.SendBackButtonPressed();
				return;
			}

			lblDescModal.Text = comunicationSelected.DescricaoAlerta;
			imgStudentProfile.Source = comunicationSelected.Dependente?.Foto;
			lblStudentName.Text = comunicationSelected.Dependente?.NomeAluno;
			lblStudentLevel.Text = comunicationSelected.Dependente?.NomeCurso;
			lblStudentLevel.TextColor = Color.FromHex(comunicationSelected.Dependente?.ColorSerie);

			lblActivity.Text = comunicationSelected.NomeAlerta;
			lblDate.Text = comunicationSelected.DtEvento;
			lblClock.Text = comunicationSelected.DsHorario;

			if (comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_GRATIS) || comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_PAGO))
			{
				// evento grátis
				stkBtnsEventFree.IsVisible = true;
				if (!comunicationSelected.Status.Equals(Application.Current.Resources["opened"] as String))
				{
					btnClose.IsVisible = true;
					stkBtnsEventFree.IsVisible = false;
					lblDescComunicationSelection.IsVisible = false;
				}

				if (comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_GRATIS))
				{
					lblDescComunicationSelection.IsVisible = false;
					lblValueTitle.IsVisible = false;
					lblValue.IsVisible = false;
				}
				else
				{
					btnConfirm.Text = Application.Current.Resources["confirm"] as String;
					lblValue.Text = comunicationSelected.ValorEventoMoeda;
				}
			}
			else
			{
				SeculosBO.Instance.IMessage.Msg(Application.Current.Resources["details_not_avaliable"] as String);
				Page.SendBackButtonPressed();
			}

			//if (comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_GRATIS))
			//{
			//	// evento grátis
			//	btnCreditCard.IsVisible = false;
			//	btnStudentCredit.IsVisible = false;
			//	btnRefuse.IsVisible = false;
			//	stkBtnsEventFree.IsVisible = true;
			//	lblValueTitle.IsVisible = false;
			//	lblValue.IsVisible = false;
			//	if (!comunicationSelected.Status.Equals(Application.Current.Resources["opened"] as String))
			//	{
			//		btnClose.IsVisible = true;
			//		stkBtnsEventFree.IsVisible = false;
			//		lblDescComunicationSelection.IsVisible = false;
			//	}
			//}
			//else if (comunicationSelected.GetNotificationType.Equals(Notificacao.NOTIFICACAO_EVENTO_PAGO))
			//{
			//	// evento pago
			//	btnCreditCard.IsVisible = true;
			//	btnStudentCredit.IsVisible = true;
			//	btnRefuse.IsVisible = true;
			//	stkBtnsEventFree.IsVisible = false;
			//	lblValue.Text = comunicationSelected.ValorEventoMoeda;
			//	btnConfirm.Text = Application.Current.Resources["continue"] as String;

			//	if (comunicationSelected.Status.Equals(Application.Current.Resources["confirmed"] as String)
			//	   || comunicationSelected.Status.Equals(Application.Current.Resources["refused"] as String))
			//	{
			//		btnClose.IsVisible = true;
			//		btnCreditCard.IsVisible = false;
			//		btnStudentCredit.IsVisible = false;
			//		btnRefuse.IsVisible = false;
			//		lblDescComunicationSelection.IsVisible = false;
			//	}
			//	else if (comunicationSelected.Status.Equals(Application.Current.Resources["pendent_payment"] as String))
			//	{
			//		btnCreditCard.IsVisible = true;
			//		btnStudentCredit.IsVisible = true;
			//		btnRefuse.IsVisible = false;
			//		btnClose.IsVisible = true;
			//	}
			//	else if (!comunicationSelected.Status.Equals(Application.Current.Resources["opened"] as String))
			//	{
			//		btnClose.IsVisible = true;
			//		btnCreditCard.IsVisible = false;
			//		btnStudentCredit.IsVisible = false;
			//		btnRefuse.IsVisible = false;
			//		lblDescComunicationSelection.IsVisible = false;
			//	}
			//}
			//else
			//{
			//	SeculosBO.Instance.IMessage.Msg(Application.Current.Resources["details_not_avaliable"] as String);
			//	Page.SendBackButtonPressed();
			//}

			if (!SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected))
			{
				btnClose.IsVisible = true;
				//btnCreditCard.IsVisible = false;
				//btnStudentCredit.IsVisible = false;
				//btnRefuse.IsVisible = false;
				lblDescComunicationSelection.IsVisible = false;
			}

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				btnClose.IsVisible = true;
			}
		}

		public void showModal()
		{
			UpdateViewModal();

			var extraMargin = 0;

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				extraMargin = 40;
				if (((mainContentComunicationModal.Height / App.ScreenHeight) * 100) > 69)
				{
					if (comunicationSelected.Status.Equals(Application.Current.Resources["pendent_payment"] as String)
				   	|| comunicationSelected.Status.Equals(Application.Current.Resources["opened"] as String))
					{
						mainContentComunicationModal.HeightRequest = App.ScreenHeight * 0.8;
					}
				}
			}
			else
			{
				if (((mainContentComunicationModal.Height / App.ScreenHeight) * 100) > 85)
				{
					mainContentComunicationModal.HeightRequest = App.ScreenHeight * 0.85;
				}
			}

			var margin = Math.Abs((App.ScreenHeight - mainContentComunicationModal.Height) / 2);

			mainContentComunicationModal.Margin = new Thickness(mainContentComunicationModal.Margin.Left, margin - extraMargin, mainContentComunicationModal.Margin.Right, 0);
			this.FadeTo(1, 50, null);

		}
	}
}
