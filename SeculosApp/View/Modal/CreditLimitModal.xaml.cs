﻿using System;
using System.Collections.ObjectModel;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class CreditLimitModal : RelativeLayout, AbstractModal
	{
		ObservableCollection<Dependente> listStudents;
		public static bool loading;
		bool individual;

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			var entry = sender as Entry;
			entry.TextChanged -= Handle_TextChanged;
			var tempValue = Utils.formatCurrency(9, entry.Text);
			if (tempValue.Equals(SeculosBO.EMPTY))
			{
				entry.Text = "";
			}
			else
			{
				entry.Text = tempValue;
			}
			entry.TextChanged += Handle_TextChanged;
		}


		public CreditLimitModal(AbstractContentPage Page, bool individual = false)
		{
			InitializeComponent();
			this.individual = individual;

			btnCancel.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					await Utils.FadeView(btnCancel);
					Page.SendBackButtonPressed();
				})
			});

			btnConfirm.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					loading = true;
					viewLoadingLimit.IsVisible = true;
					loadLimit.IsVisible = true;
					await Utils.FadeView(btnConfirm);
					var ok = false;
					var success = false;
					var error = false;
					var noConnection = false;
					foreach (Dependente student in listStudents)
					{
						//if (!student.LimitDecimal.Equals("0") && !student.LimitDecimal.Equals(student.Limite))
						//{
							ok = true;
							// call api
							var msg = await SeculosBO.Instance.setLimitDaily(student);
							if (msg.Equals(EnumCallback.CONNECTION_ERROR))
							{
								noConnection = true;
								SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[msg] as String);
								break;
							}
							else if (msg.Equals(EnumCallback.LIMIT_DAILY_ERROR))
							{
								error = true;
							}
							else if (msg.Equals(EnumCallback.LIMIT_DAILY_SUCCESS))
							{
								success = true;
							}
						//}
					}

					if (!noConnection)
					{
						if (ok)
						{
							if (success && !error)
							{
								SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.LIMIT_DAILY_SUCCESS] as String);
							}
							else if (success && error)
							{
								SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.LIMIT_DAILY_SUCCESS_ERROR] as String);
							}
							else if (!success && error)
							{
								SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.LIMIT_DAILY_ERROR] as String);
							}
							App._Current.SendMessage(MessagingConstants.LIMITE_DIARIO_HAS_BEEN_UPDATED);
							loading = false;
							Page.SendBackButtonPressed();
						}
						else
						{
							SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.LIMIT_EMPTY] as String);
						}
					}
					loading = false;
					viewLoadingLimit.IsVisible = false;
					loadLimit.IsVisible = false;

				})
			});

			listViewModalStudents.ItemTapped += (object sender, ItemTappedEventArgs e) =>
			{
				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect
			};
		}

		public void showModal()
		{
			listStudents = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;
			foreach (Dependente student in listStudents)
			{
				student.LimitCurrency = student.LimiteCash;
			}

			if (individual)
			{
				listStudents = new ObservableCollection<Dependente>();
				SeculosBO.Instance.StudentSelected.LimitCurrency = SeculosBO.Instance.StudentSelected.LimiteCash;
				listStudents.Add(SeculosBO.Instance.StudentSelected);
			}

			if (listStudents.Count > 1)
			{
				lblDescModal.Text = Application.Current.Resources["desc_consume_dayle_modal_plural"] as String;
			}

			listViewModalStudents.ItemsSource = listStudents;

			double height = 53.75;

			if (listStudents.Count > 4)
			{
				listViewModalStudents.HeightRequest = height * 4;
			}
			else
			{
				listViewModalStudents.HeightRequest = height * listStudents.Count + 2;
			}

			var margin = Math.Abs((App.ScreenHeight - mainContentModalCredit.Height) / 2);

			mainContentModalCredit.Margin = new Thickness(mainContentModalCredit.Margin.Left, margin, mainContentModalCredit.Margin.Right, 0);
			this.FadeTo(1, 50, null);
		}
	}
}
