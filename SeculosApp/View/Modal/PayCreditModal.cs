﻿using System;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace SeculosApp
{
	public class PayCreditModal : RelativeLayout, AbstractModal
	{
		StackLayout mainContentModal;
		CustomLabel valueAfterPaymentDescLabel;
		CustomLabel errorLabel;
		CustomLabel cancelLabel;
		CustomLabel confirmLabel;
		Command click;
		StackLayout valueAfterPaymentLayout;
		AbstractContentPage page;
		BoxView boxLoading;
		ActivityIndicator load;

		public static bool updateByBillet;
		public static bool updateByEvent;
		public static bool fromComunicado;
		public static bool loading;

		public PayCreditModal(AbstractContentPage Page)
		{
			this.page = Page;
			updateByBillet = false;
			updateByEvent = false;
			click = new Command((obj) => onClick(obj));


			if (!fromComunicado)
			{
				foreach (var dep in SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente)
				{
					if (dep.CdMatricula != null && dep.CdMatricula.Equals(SeculosBO.Instance.BilletSelected.CdAluno)
						|| dep.CdAluno != null && dep.CdAluno.Equals(SeculosBO.Instance.BilletSelected.CdAluno))
					{
						SeculosBO.Instance.StudentSelected = dep;
						break;
					}
				}
			}
			else
			{
				foreach (var dep in SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente)
				{
					if (dep.CdMatricula != null && dep.CdMatricula.Equals(SeculosBO.Instance.ComunicationSelected.CdAluno)
						|| dep.CdAluno != null && dep.CdAluno.Equals(SeculosBO.Instance.ComunicationSelected.CdAluno))
					{
						SeculosBO.Instance.StudentSelected = dep;
						break;
					}
				}
			}

			InitializeView();

			try
			{
				var saldo = double.Parse(SeculosBO.Instance.DependenteConnected.SaldoCash);
				var boletoBill = double.Parse(Utils.getCashValue(fromComunicado ? SeculosBO.Instance.ComunicationSelected.VlEvento : SeculosBO.Instance.BilletSelected.VlBoleto));

				var diff = saldo - boletoBill;

				if (diff > 0)
				{
					valueAfterPaymentDescLabel.Text = string.Format("{0:C}", diff);
				}
				else
				{
					errorLabel.IsVisible = true;
					cancelLabel.IsVisible = false;
					valueAfterPaymentLayout.IsVisible = false;
					confirmLabel.Text = "FECHAR";
				}
			}
			catch 
			{
				errorLabel.IsVisible = true;
				cancelLabel.IsVisible = false;
				valueAfterPaymentLayout.IsVisible = false;
				confirmLabel.Text = "FECHAR";
			}
		}

		async void onClick(object obj)
		{
			var str = (string)obj;

			if (str.Equals(confirmLabel.ClassId))
			{
				await Utils.FadeView(confirmLabel);
				if (cancelLabel.IsVisible)
				{
					loading = true;
					string boletoId = "";
					string eventoId = "";

					boxLoading.IsVisible = true;
					load.IsVisible = true;

					if (fromComunicado)
					{
						eventoId = SeculosBO.Instance.ComunicationSelected.CdEvento;
					}
					else
					{
						boletoId = SeculosBO.Instance.BilletSelected.CdBoleto;
					}

					await SeculosBO.Instance.SetPagamentoCredito(SeculosBO.Instance.DependenteConnected, eventoId, boletoId, async (result, msg) =>
					{
						if (result)
						{
							if (boletoId.Equals(""))
							{
								updateByBillet = true;
							}
							else
							{
								updateByEvent = true;
							}
							await SeculosBO.Instance.OnResumeUpdate();
							loading = false;
							page.SendBackButtonPressed();
						}
						else
						{
							loading = false;
						}
						boxLoading.IsVisible = false;
						load.IsVisible = false;
						SeculosBO.Instance.IMessage.Msg(msg);
					});
				}
				// user does not have money so just close it
				else
				{
					page.SendBackButtonPressed();
				}
			}
			else if (str.Equals(cancelLabel.ClassId))
			{
				await Utils.FadeView(cancelLabel);
				page.SendBackButtonPressed();
			}

			fromComunicado = false;
		}

		public void showModal()
		{
			var margin = Math.Abs((App.ScreenHeight - mainContentModal.Height) / 2);

			var extraMargin = 0;

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				extraMargin = 40;
			}

			mainContentModal.Margin = new Thickness(mainContentModal.Margin.Left, margin - extraMargin, mainContentModal.Margin.Right, 0);
			this.FadeTo(1, 50, null);

		}

		void InitializeView()
		{
			this.BackgroundColor = Color.Transparent;
			this.Opacity = 0;
			
			var viewBackground = new BoxView
			{
				BackgroundColor = Color.Black,
				Opacity = .3
			};

			var titleLabel = new CustomLabel
			{
				Text = "Pagar com crédito do aluno",
				TextColor = Color.White,
				FontSize = 18,
				FontType = "Medium", 
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				HeightRequest = 50,
				BackgroundColor = (Color) App.Current.Resources["blue_light_label"],
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			var descLabel = new CustomLabel
			{
				Text = "Tem certeza que deseja pagar a conta " + (fromComunicado ? SeculosBO.Instance.ComunicationSelected.NomeAlerta.ToUpper() : SeculosBO.Instance.BilletSelected.NomeProduto.ToUpper())
					+ " com os créditos do seguinte aluno?",
				FontSize = 15,
				FontType = "Book",
				TextColor = (Color)App.Current.Resources["gray_light"],
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(20, 5, 20, 15)
			};

			var imgStudent = new CachedImage
			{
				CacheType = FFImageLoading.Cache.CacheType.Disk,
				Aspect = Aspect.AspectFill,
				HeightRequest = 40,
				WidthRequest = 40,
				BackgroundColor = (Color)App.Current.Resources["blue_dark"],
				DownsampleToViewSize = true,
				RetryCount = 1,
				Source = SeculosBO.Instance.StudentSelected.Foto,
				RetryDelay = 250,
				ErrorPlaceholder = "no_perfil_asset.png"
			};

			var nameLabel = new CustomLabel
			{
				TextColor = Color.Black,
				FontSize = 15,
				HorizontalOptions = LayoutOptions.Start,
				Text = SeculosBO.Instance.StudentSelected.NomeAluno,
				LineBreakMode = LineBreakMode.TailTruncation,
				FontType = "Medium",
				VerticalTextAlignment = TextAlignment.Center
			};
				
			var cashLabel = new CustomLabel
			{
				FontSize = 15,
				FontType = "Medium",
				TextColor = (Color)App.Current.Resources["gray_light"],
				HorizontalOptions = LayoutOptions.EndAndExpand,
				LineBreakMode = LineBreakMode.TailTruncation,
				MinimumWidthRequest = 100,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center
			};

			try
			{
				cashLabel.Text = "R$ " + SeculosBO.Instance.DependenteConnected.SaldoCash;
				if (SeculosBO.Instance.DependenteConnected.SaldoCash.Contains("-"))
				{
					cashLabel.TextColor = Color.FromHex("#ff3232");
				}
			}
			catch 
			{
				cashLabel.Text = SeculosBO.EMPTY;
			}

			var studentInfoLayout = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Margin = new Thickness(20, 0, 20, 0),
				Children = { imgStudent, nameLabel, cashLabel }
			};

			var lineSeparator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(20, 0, 20, 0)
			};

			var valueToPayLabel = new CustomLabel
			{
				TextColor = Color.Black,
				FontSize = 15,
				Text = "Valor a ser pago",
				FontType = "Medium"
			};

			var valueToPayDescLabel = new CustomLabel
			{
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 15,
				Text = "R$ " + Utils.getCashValue(fromComunicado ? SeculosBO.Instance.ComunicationSelected.VlEvento : SeculosBO.Instance.BilletSelected.VlBoleto),
				FontType = "Medium"
			};

			var valueToPayLayout = new StackLayout
			{
				Spacing = 2,
				Margin = new Thickness(20, 0, 20, 0),
				Children = { valueToPayLabel, valueToPayDescLabel }
			};

			var valueAfterPaymentLabel = new CustomLabel
			{
				TextColor = Color.Black,
				FontSize = 15,
				Text = "Seu saldo após pagamento",
				FontType = "Medium"
			};

			valueAfterPaymentDescLabel = new CustomLabel
			{
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 15,
				Text = "R$ - ",
				FontType = "Medium"
			};

			valueAfterPaymentLayout = new StackLayout
			{
				Spacing = 2,
				Margin = new Thickness(20, 0, 20, 0),
				Children = { valueAfterPaymentLabel, valueAfterPaymentDescLabel }
			};

			errorLabel = new CustomLabel
			{
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 15,
				Text = "Crédito insuficiente para realizar a transação",
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				IsVisible = false,
				Margin = new Thickness(0, 20, 0, 20),
				FontType = "Medium"
			};

			cancelLabel = new CustomLabel
			{
				ClassId = "cancelLabel",
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 15,
				Text = "CANCELAR",
				Margin = new Thickness(10, 10, 10, 10),
				FontType = "Medium"
			};
			cancelLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = cancelLabel.ClassId });

		 	confirmLabel = new CustomLabel
			{
				ClassId = "confirmLabel",
				TextColor = (Color)App.Current.Resources["blue_light_label"],
				FontSize = 15,
				Text = "CONFIRMAR",
				Margin = new Thickness(10, 10, 10, 10),
				FontType = "Medium"
			};
			confirmLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = click, CommandParameter = confirmLabel.ClassId });

			var buttonsLabel = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				Margin = new Thickness(0, 0, 0, 20),
				Children = { cancelLabel, confirmLabel }
			};

			mainContentModal = new StackLayout
			{
				BackgroundColor = Color.White,
				Margin = new Thickness(15, 0, 15, 0),
				Children = { titleLabel, descLabel, studentInfoLayout, lineSeparator, valueToPayLayout, valueAfterPaymentLayout, errorLabel, buttonsLabel }
			};

			boxLoading = new BoxView
			{
				BackgroundColor = Color.Black,
				Opacity = .3,
				IsVisible = false
			};

			load = new ActivityIndicator
			{
				Color = (Color)App.Current.Resources["blue_light_label"],
				IsVisible = false,
				IsRunning = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			this.Children.Add(
				viewBackground,
				widthConstraint: Constraint.RelativeToParent((parent) => { return parent.Width; }),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				mainContentModal,
				widthConstraint: Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			this.Children.Add(
				boxLoading,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				load,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);
		}
	}
}