﻿using System;
using System.Collections.ObjectModel;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class BuyCreditModal : RelativeLayout, AbstractModal
	{
		ObservableCollection<Dependente> listStudents;
		public static bool loading;
		public static bool buyCredit;

		public Dependente studentSelected { get; set; }
		bool individual;

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			var entry = sender as Entry;
			entry.TextChanged -= Handle_TextChanged;
			var tempValue = Utils.formatCurrency(9, entry.Text);
			if (tempValue.Equals(SeculosBO.EMPTY))
			{
				entry.Text = "";
			}
			else
			{
				entry.Text = tempValue;
			}
			entry.TextChanged += Handle_TextChanged;

			totalAmoutCredit.Text = string.Empty;
			foreach (Dependente student in listStudents)
			{
				decimal intValue = 0;
				if (!student.LimitCurrency.Equals(string.Empty) && !student.LimitCurrency.Equals(SeculosBO.EMPTY))
				{
					intValue = Convert.ToDecimal(student.LimitCurrency);
				}
				decimal intValue2 = 0;
				if (!totalAmoutCredit.Text.Equals(string.Empty) && !totalAmoutCredit.Text.Equals(SeculosBO.EMPTY))
				{
					intValue2 = Convert.ToDecimal(totalAmoutCredit.Text);
				}

				totalAmoutCredit.Text = Utils.formatCurrency(15, (intValue2 + intValue) + "");
			}

			if (totalAmoutCredit.Text.Equals(string.Empty))
			{
				totalAmoutCredit.Text = "00,00";
			}
		}

		public BuyCreditModal(ContentPage Page, bool individual = false)
		{
			InitializeComponent();
			this.individual = individual;
			btnCancel.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					await Utils.FadeView(btnCancel);
					Page.SendBackButtonPressed();
				})
			});

			btnConfirm.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{

					await Utils.FadeView(btnConfirm);
					var ok = false;

					foreach (Dependente student in listStudents)
					{
						if (student.LimitDecimal != null && !student.LimitDecimal.Equals(Application.Current.Resources["value_00"] as String) && !student.LimitDecimal.Equals("0") && !student.LimitDecimal.Equals(SeculosBO.EMPTY))
						{
							ok = true;
							break;
						}
					}

					if (ok)
					{
						// call browser
						SeculosBO.Instance.IMessage.Msg("Redirecionando ao site de pagamento...");
						string base64EncodedExternalAccount = SeculosBO.Instance.createBuyCreditObject(listStudents, totalAmoutCredit.Text);
						Device.OpenUri(new Uri(Application.Current.Resources["hotsite_buy_credit"] as String + base64EncodedExternalAccount));
						System.Diagnostics.Debug.WriteLine(Application.Current.Resources["hotsite_buy_credit"] as String + base64EncodedExternalAccount);
						buyCredit = true;
						Page.SendBackButtonPressed();
					}
					else
					{
						SeculosBO.Instance.IMessage.Msg(Application.Current.Resources[EnumCallback.ADD_CREDIT_EMPTY] as String);
					}
				})
			});

			listViewModalStudents.ItemTapped += (object sender, ItemTappedEventArgs e) =>
			{
				// don't do anything if we just de-selected the row
				if (e.Item == null)
				{
					return;
				}

				// do something with e.SelectedItem
				((ListView)sender).SelectedItem = null; // de-select the row after ripple effect
			};
		}

		public void showModal()
		{
			listStudents = SeculosBO.Instance.SeculosGeneral.UserConnected.listDependente;
			foreach (Dependente student in listStudents)
			{
				student.LimitCurrency = string.Empty;
			}

			if (individual)
			{
				listStudents = new ObservableCollection<Dependente>();
				SeculosBO.Instance.StudentSelected.LimitCurrency = string.Empty;
				listStudents.Add(SeculosBO.Instance.StudentSelected);
			}

			if (listStudents.Count > 1)
			{
				lblDescModal.Text = Application.Current.Resources["desc_buy_credit_modal_plural"] as String;
			}

			listViewModalStudents.ItemsSource = listStudents;

			double height = 53.75;

			if (listStudents.Count > 4)
			{
				listViewModalStudents.HeightRequest = height * 4;
			}
			else
			{
				listViewModalStudents.HeightRequest = height * listStudents.Count + 2;
			}

			var margin = Math.Abs((App.ScreenHeight - mainContentModalCredit.Height) / 2);

			var extraMargin = 0;

			if (Device.OS.Equals(TargetPlatform.iOS))
			{
				extraMargin = 40;
			}

			mainContentModalCredit.Margin = new Thickness(mainContentModalCredit.Margin.Left, margin - extraMargin, mainContentModalCredit.Margin.Right, 0);
			this.FadeTo(1, 50, null);

		}
	}
}
