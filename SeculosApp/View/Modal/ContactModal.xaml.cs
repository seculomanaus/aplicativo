﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class ContactModal : RelativeLayout, AbstractModal
	{
		public static bool loading;

		public ContactModal(ContentPage Page)
		{
			InitializeComponent();

			lblTitleContactModal.Text = Utils.PascalCase(lblTitleContactModal.Text);

			btnCancel.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					await Utils.FadeView(btnCancel);
					Page.SendBackButtonPressed();
				})
			});

			btnSend.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
				{
					await Utils.FadeView(btnSend);
				
					if (editMsg.Text.Trim().Equals(string.Empty) || editMsg.Text.Trim().Equals(Application.Current.Resources["write_your_message"] as String))
					{
						SeculosBO.Instance.IMessage.Msg(Application.Current.Resources["without_message"] as String);
					}
					else
					{
						// send contact
						var ISendEmail = DependencyService.Get<ISendEmail>();

						string emailDp = getSelectedDpEmail(pckSelectDP.SelectedIndex);

						var user = SeculosBO.Instance.SeculosGeneral.UserConnected;

						ISendEmail.SendMail(emailDp, user.Nome, user.NrCelular, "", editMsg.Text, Application.Current.Resources["subject"] as String);
						Page.SendBackButtonPressed();
					}
				})
			});
		}

		public void showModal()
		{
			var margin = Math.Abs((App.ScreenHeight - mainContentModalContact.Height) / 2);
			mainContentModalContact.Margin = new Thickness(mainContentModalContact.Margin.Left, margin, mainContentModalContact.Margin.Right, 0);
			this.FadeTo(1, 50, null);
		}

        private string getSelectedDpEmail(int index)
        {
			string email = "";
            switch (index)
            {
				case 0:
                    {
					    email = Application.Current.Resources["dp_secretaria_email"] as string;
					    break;
                    }
				case 1:
                    {
						email = Application.Current.Resources["dp_infantil_email"] as string;
						break;
					}
				case 2:
					{
						email = Application.Current.Resources["dp_fund_I_email"] as string;
						break;
					}
				case 3:
					{
						email = Application.Current.Resources["dp_fund_II_medio_email"] as string;
						break;
					}
				case 4:
                    {
                        email = Application.Current.Resources["dp_ouvidoria_email"] as string;
						break;
                    }
			}

			return email;
        }
	}
}
