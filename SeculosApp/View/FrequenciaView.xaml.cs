﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class FrequenciaView : RelativeLayout, ITabView
	{

		bool initializedView;
		Command cmdClick;

		public FrequenciaView(int position)
		{
			InitializeComponent();

			cmdClick = new Command((obj) => onClick(obj));

			stkError.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = stkError.ClassId });

			initLoad();

			if (position == 4)
			{
				InitializeView();
			}
		}

		public void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							GetFrequenciaAluno();
						});
					});
				});
			}
		}

		public void initLoad()
		{
			stkError.IsVisible = false;
			load.IsVisible = true;
			lblError.IsVisible = false;
		}

		void GetFrequenciaAluno()
		{
			SeculosBO.Instance.GetFrequenciaAluno(SeculosBO.Instance.DependenteConnected, (msg) => {
				var listaFrequencia = SeculosBO.Instance.DependenteConnected.Frequencia;

				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listaFrequencia == null || listaFrequencia.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DATA_EMPTY, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.DEMONSTRATIVO_ERROR))
				{
					if (listaFrequencia == null || listaFrequencia.Count == 0)
					{
						SeculosBO.Instance.showViewError(EnumCallback.DEMONSTRATIVO_ERROR, lblError, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listaFrequencia == null || listaFrequencia.Count == 0)
					{
						stkError.IsVisible = true;
					}
				}

				if (listaFrequencia != null && listaFrequencia.Count > 0)
				{
					createAllitens();
				}
				else
				{
					load.IsVisible = false;
				}
			});
		}

		void createAllitens()
		{
			var listaFrequencia = SeculosBO.Instance.DependenteConnected.Frequencia;

			foreach (Frequencia frequencia in listaFrequencia)
			{
				RelativeLayout item = CreateItemLayout(frequencia);
				stackContentItens.Children.Add(item);
			}

			stackContentItens.IsVisible = true;
			load.IsVisible = false;
		}

		async void onClick(object obj)
		{
			if (!App.GlobalBlockClick)
			{
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(stkError.ClassId))
				{
					initLoad();
					await Utils.FadeView(stkError);
					await Task.Factory.StartNew(() =>
					{
						Task.Delay(200).ContinueWith((objj) =>
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								//getDemonstrativoNotas();
								load.IsVisible = false;
							});
						});
					});
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(200).ContinueWith((o) =>
					{
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		StackLayout CreateFrequencyTouple(int cargaHora, int numFaltas, int numFaltasToleradas)
		{
			var stack = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				IsVisible = false,
				Opacity = 0
			};

			var cargaHorariaLbl = "Carga Horária";
			var quantFaltasLbl = "Quantidade de ausências";
			var quantFaltasToleradas = "Limite de faltas anual";

			var cargHorValueFormt = "{0} Horas";
			var faltasValueFormt = String.Format("{0} Faltas", numFaltas);

			if (numFaltas == 0)
			{
				faltasValueFormt = "sem falta";
			}
			else if (numFaltas == 1)
			{
				faltasValueFormt = String.Format("{0} Falta", numFaltas);
			}

			var faltasToleradasValueFormt = String.Format("{0} Faltas", numFaltasToleradas);

			if (numFaltasToleradas == 1)
			{
				faltasToleradasValueFormt = String.Format("{0} Falta", numFaltasToleradas);
			}

			int colorFaltas = 0;

			if (numFaltas < numFaltasToleradas)
			{
				colorFaltas = 1;
			}
			else 
			{
				colorFaltas = 2;			
			}

			var cargHor = createHeaderBim(cargaHorariaLbl, String.Format(cargHorValueFormt, cargaHora));
			cargHor.BackgroundColor = Color.FromHex("#f6f6f6");

			var faltasToleradas = createHeaderBim(quantFaltasToleradas, faltasToleradasValueFormt);

			var faltas = createHeaderBim(quantFaltasLbl, faltasValueFormt, colorFaltas);
			faltas.BackgroundColor = Color.FromHex("#f6f6f6");

			stack.Children.Add(cargHor);
			stack.Children.Add(faltasToleradas);
			stack.Children.Add(faltas);

			return stack;
		}


		public RelativeLayout CreateItemLayout(Frequencia frequencia)
		{
			var layout = new RelativeLayout
			{
				Margin = new Thickness(0, -10, 0, 5)
			};

			var itemLabel = new CustomLabel
			{
				Text = Utils.PascalCase(frequencia.NomeDisciplina),
				TextColor = Color.FromHex("#a3a3a3"),
				VerticalTextAlignment = TextAlignment.Center,
				FontType = "Medium",
				HeightRequest = 28,
				FontSize = 15.5,
				Margin = new Thickness(10, 0, 20, 0)
			};

			var imgArrow = new Image
			{
				Source = "arrow_right_yellow_ic.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				WidthRequest = 26,
				HeightRequest = 18,
				Margin = new Thickness(0, 0, 20, 0),
				Rotation = 90
			};

			var lineSeperator = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = .5,
				Margin = new Thickness(10, 8, 10, 0)
			};

			layout.Children.Add(
				imgArrow,
				Constraint.RelativeToParent((parent) => { return parent.Width - 36; }),
				Constraint.Constant(18)
			);

			layout.Children.Add(
				itemLabel,
				Constraint.Constant(0),
				Constraint.Constant(10),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			layout.Children.Add(
				lineSeperator,
				Constraint.Constant(0),
				Constraint.RelativeToView(itemLabel, (parent, view) => { return view.Y + view.Height + 6; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			var stackContent = new StackLayout
			{
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 0,
				Margin = new Thickness(0, -2, 0, 0),
				Padding = new Thickness(0, 1, 0, 1),
				Spacing = 0
			};

			layout.Children.Add(
				stackContent,
				Constraint.Constant(0),
				Constraint.RelativeToView(lineSeperator, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; })
			);

			stackContent.Opacity = 0;

			var stackBim = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				BackgroundColor = Color.White,
				Margin = new Thickness(0, 1, 0, 1),
				Padding = new Thickness(10, 10, 10, 10)
			};

			RelativeLayout rel1Bim = createRelBim("1º BIM", "#ffffff", "#3ea6e1", LayoutOptions.StartAndExpand);
			RelativeLayout rel2Bim = createRelBim("2º BIM", "#a3a3a3", "#ffffff", LayoutOptions.CenterAndExpand);
			RelativeLayout rel3Bim = createRelBim("3º BIM", "#a3a3a3", "#ffffff", LayoutOptions.CenterAndExpand);
			RelativeLayout rel4Bim = createRelBim("4º BIM", "#a3a3a3", "#ffffff", LayoutOptions.EndAndExpand);

			var stack1Bim = CreateFrequencyTouple(frequencia.Grade, frequencia.NumFalta1b, frequencia.TotalAusencia);
			stack1Bim.IsVisible = true;
			stack1Bim.Opacity = 1;

			var stack2Bim = CreateFrequencyTouple(frequencia.Grade, frequencia.NumFalta2b, frequencia.TotalAusencia);
			var stack3Bim = CreateFrequencyTouple(frequencia.Grade, frequencia.NumFalta3b, frequencia.TotalAusencia);
			var stack4Bim = CreateFrequencyTouple(frequencia.Grade, frequencia.NumFalta4b, frequencia.TotalAusencia);

			CustomLabel lbl1Bim = null;
			RoundedBoxView rounded1Bim = null;
			foreach (Xamarin.Forms.View view in rel1Bim.Children)
			{
				if (view.ClassId.Equals("lbl1º BIM"))
				{
					lbl1Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded1º BIM"))
				{
					rounded1Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl2Bim = null;
			RoundedBoxView rounded2Bim = null;
			foreach (Xamarin.Forms.View view in rel2Bim.Children)
			{
				if (view.ClassId.Equals("lbl2º BIM"))
				{
					lbl2Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded2º BIM"))
				{
					rounded2Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl3Bim = null;
			RoundedBoxView rounded3Bim = null;
			foreach (Xamarin.Forms.View view in rel3Bim.Children)
			{
				if (view.ClassId.Equals("lbl3º BIM"))
				{
					lbl3Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded3º BIM"))
				{
					rounded3Bim = view as RoundedBoxView;
				}
			}

			CustomLabel lbl4Bim = null;
			RoundedBoxView rounded4Bim = null;
			foreach (Xamarin.Forms.View view in rel4Bim.Children)
			{
				if (view.ClassId.Equals("lbl4º BIM"))
				{
					lbl4Bim = view as CustomLabel;
				}
				else if (view.ClassId.Equals("rounded4º BIM"))
				{
					rounded4Bim = view as RoundedBoxView;
				}
			}

			lbl1Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap1Bim: ");
						await Utils.FadeView(rel1Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;
						stack1Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack1Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#ffffff");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#3ea6e1");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#ffffff");
						await Task.Factory.StartNew(() =>
						{
							Task.Delay(200).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl1Bim
			});

			lbl2Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap2Bim: ");
						await Utils.FadeView(rel2Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack2Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack2Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#ffffff");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#3ea6e1");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#ffffff");

						if (stack2Bim.ClassId == null)
						{
							stack2Bim.ClassId = "stack2Bim";
							stackContent.Children.Add(stack2Bim);
						}

						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl2Bim
			});

			lbl3Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap3Bim: ");
						await Utils.FadeView(rel3Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack3Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack3Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack3Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#ffffff");
						lbl4Bim.TextColor = Color.FromHex("#e3e3e3");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#3ea6e1");
						rounded4Bim.Color = Color.FromHex("#ffffff");

						if (stack3Bim.ClassId == null)
						{
							stack3Bim.ClassId = "stack2Bim";
							stackContent.Children.Add(stack3Bim);
						}

						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl3Bim
			});

			lbl4Bim.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					if (!App.GlobalBlockClick)
					{
						App.GlobalBlockClick = true;
						System.Diagnostics.Debug.WriteLine("tap4Bim: ");
						await Utils.FadeView(rel4Bim);
						await Task.WhenAll(
							stack1Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack2Bim.FadeTo(0, 0, null),
							stack4Bim.FadeTo(0, 0, null)
						);
						stack1Bim.IsVisible = false;
						stack2Bim.IsVisible = false;
						stack3Bim.IsVisible = false;
						stack4Bim.IsVisible = false;

						stack4Bim.IsVisible = true;
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
						stack4Bim.FadeTo(1, 200, Easing.CubicOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.

						lbl1Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl2Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl3Bim.TextColor = Color.FromHex("#e3e3e3");
						lbl4Bim.TextColor = Color.FromHex("#ffffff");

						rounded1Bim.Color = Color.FromHex("#ffffff");
						rounded2Bim.Color = Color.FromHex("#ffffff");
						rounded3Bim.Color = Color.FromHex("#ffffff");
						rounded4Bim.Color = Color.FromHex("#3ea6e1");
						if (stack4Bim.ClassId == null)
						{
							stack4Bim.ClassId = "stack4Bim";
							stackContent.Children.Add(stack4Bim);
						}
						await Task.Factory.StartNew(() =>
						{
							Task.Delay(100).ContinueWith((o) =>
							{
								App.GlobalBlockClick = false;
							});
						});
					}

				}),
				CommandParameter = lbl4Bim
			});

			layout.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async (obj) =>
				{
					await Utils.FadeView(itemLabel);

					var rotate = 90;
					var fade = 0;
					uint time = 50;
					if (imgArrow.Rotation == 90)
					{
						rotate = -90;
						fade = 1;
						stackContent.HeightRequest = -1;
						time = 200;
						itemLabel.TextColor = Color.Black;
					}
					else
					{
						itemLabel.TextColor = Color.FromHex("#a3a3a3");
					}

#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					imgArrow.RotateTo(rotate, 200, Easing.SpringOut);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
#pragma warning disable CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					stackContent.FadeTo(fade, time, null);
#pragma warning restore CS4014 // Como esta chamada não é aguardada, a execução do método atual continua antes da conclusão da chamada. Considere aplicar o operador 'await' ao resultado da chamada.
					if (rotate == 90)
					{
						stackContent.HeightRequest = 0;
					}

					// initialize content bimesters and the frist tab that is a 1 bim.
					if (stackContent.Children.Count == 0)
					{
						stackContent.Children.Add(stackBim);
						stackContent.Children.Add(stack1Bim);
					}

				}),
				CommandParameter = layout
			});

			stackBim.Children.Add(rel1Bim);
			stackBim.Children.Add(rel2Bim);
			stackBim.Children.Add(rel3Bim);
			stackBim.Children.Add(rel4Bim);

			return layout;
		}

		RelativeLayout createRelBim(string text, string textColor, string backgroundColor, LayoutOptions options)
		{
			var roundedBim = new RoundedBoxView
			{
				ClassId = "rounded" + text,
				WidthRequest = 60,
				HeightRequest = 32,
				CornerRadius = 30,
				Color = Color.FromHex(backgroundColor)
			};

			Utils.fixRoudedView(roundedBim);

			var lblroundedBim = new CustomLabel
			{
				ClassId = "lbl" + text,
				Text = text,
				WidthRequest = 60,
				HeightRequest = 32,
				FontSize = 15.5,
				FontType = "Medium",
				TextColor = Color.FromHex(textColor),
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center
			};

			var relBim = new RelativeLayout
			{
				ClassId = text,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = options,
				Padding = new Thickness(0, 0, 0, 0)
			};

			relBim.Children.Add(
				roundedBim,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);

			relBim.Children.Add(
				lblroundedBim,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);

			return relBim;
		}

		public StackLayout createHeaderBim(string label, string lblValue, int colorFaltas = 0)
		{
			// colorFaltas
			// 0 = #555555 - Default Color
			// 1 - #41c454 - Green Color
			// 2 - #ce1010 - Red Color

			var cargaHorariaLbl = new CustomLabel
			{
				Text = label,
				WidthRequest = 200,
				TextColor = Color.FromHex("#3ea6e1"),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontType = "Medium",
				FontSize = 15,
			};

			var colorHorarioValue = Color.FromHex("#555555");

			if (colorFaltas == 1)
			{
				colorHorarioValue = Color.FromHex("#41c454");
			}
			else if (colorFaltas == 2)
			{
				colorHorarioValue = Color.FromHex("#ce1010");
			}

			var cargaHorariaValue = new CustomLabel
			{
				Text = lblValue,
				WidthRequest = 120,
				TextColor = colorHorarioValue,
				FontType = "Medium",
				FontSize = 15,
			};

			var stackHeader = new StackLayout
			{
				Spacing = 0,
				Orientation = StackOrientation.Vertical,
				Children = { cargaHorariaLbl, cargaHorariaValue},
				Padding = new Thickness(10, 10, 10, 10),
				BackgroundColor = Color.White
			};

			return stackHeader;
		}


		public void OnPageAppeared()
		{
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_ACADEMIC, (string)App.Current.Resources["frequencie_title"]);
			InitializeView();
		}

		public void OnPageDisappeared()
		{
			//Do Nothing
		}

		public void OnPageDisappearing()
		{
			//Do Nothing
		}
	}
}
