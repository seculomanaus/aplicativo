﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class GradesView : RelativeLayout, ITabView
	{
		Command cmdClick;
		RelativeLayout  gabaritoLayout, conteudoProvasLayout;
		bool blockClick;

		public GradesView()
		{
			ClassId = "GradesView";
			cmdClick = new Command((obj) => onClick(obj));

			BackgroundColor = Color.White;

				
				gabaritoLayout = Utils.CreateItemLayout("Gabarito de Provas");
                conteudoProvasLayout = Utils.CreateItemLayout("Conteúdo de Provas");
                
				gabaritoLayout.ClassId = "TemplateTestView";
                conteudoProvasLayout.ClassId = "ContentTestsView";

                gabaritoLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = gabaritoLayout.ClassId });
                conteudoProvasLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = conteudoProvasLayout.ClassId });

                var stack = new StackLayout
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
                    Children = { gabaritoLayout, conteudoProvasLayout },
                    Margin = new Thickness(0, 8, 0, 0)
				};

				Children.Add(
					stack,
					Constraint.Constant(0),
					Constraint.Constant(0),
					Constraint.RelativeToParent((parent) => { return parent.Width; }),
					Constraint.RelativeToParent((parent) => { return parent.Height; })
				);
			//}
		}

		async void onClick(object obj)
		{
			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

                if (str.Equals(conteudoProvasLayout.ClassId))
                {
                    await Utils.FadeView(conteudoProvasLayout);
                    await Navigation.PushAsync(new GradesExpandedView(0, "ContentTestsView"));
                }
                else if (str.Equals(gabaritoLayout.ClassId))
                {
                    await Utils.FadeView(gabaritoLayout);
                    await Navigation.PushAsync(new GabaritoDeProvaWebViewPage());
                }


                    await Task.Factory.StartNew(() =>
				{
					Task.Delay(500).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_SEMI_ACADEMIC, (string)App.Current.Resources["program_content"]);
		}

		public void OnPageDisappearing()
		{
			// do nothing
		}

		public void OnPageAppeared()
		{
			// do nothing
		}

		public void OnPageDisappeared()
		{
			// do nothing
		}
	}
}
