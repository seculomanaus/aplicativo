﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SeculosApp.Model;
using Xamarin.Forms;
using XamForms.Controls;

namespace SeculosApp
{
	public class AgendaEventosView : RelativeLayout, ITabView
	{
		bool initializedView;
		CustomLabel errorLabel;
		Calendar calendar;
		ScrollView scrollDesc;
		BoxView legendSeperator;

		// Tuple < Date related, description related, color related >
		List<Tuple<DateTime, string, string>> detailDateList;

		ActivityIndicator loadIndicator;
		StackLayout layoutDesc;

		public AgendaEventosView(int position)
		{
			ClassId = "AgendaEventosView";

			this.BackgroundColor = Color.White;

			scrollDesc = new ScrollView();
			layoutDesc = new StackLayout();

			scrollDesc.Content = layoutDesc;

			var errorLabelColor = (Color)App.Current.Resources["gray_light"];

			errorLabel = new CustomLabel
			{
				TextColor = errorLabelColor,
				FontSize = 14,
				FontType = "Medium",
				IsVisible = false,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center
			};

			errorLabel.Text = (string)App.Current.Resources["data_calendario_academic_error"];

			legendSeperator = new BoxView
			{
				BackgroundColor = Color.Gray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(10, 0, 10, 0),
				HeightRequest = .5
			};

			loadIndicator = new ActivityIndicator
			{
				Color = (Color)App.Current.Resources["blue_light_label"],
				IsRunning = true,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				IsVisible = false
			};

			this.Children.Add(
				errorLabel,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				scrollDesc,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			this.Children.Add(
				loadIndicator,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			if (position == 0)
			{
				InitializeView();
			}
		}

		void InitializeView()
		{
			if (!initializedView)
			{
				initializedView = true;
				loadIndicator.IsVisible = true;
				Task.Factory.StartNew(() =>
				{
					Task.Delay(30).ContinueWith((obj) =>
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							getDates();
						});
					});
				});
			}
		}

		void getDates()
		{
			SeculosBO.Instance.getAgendaEventosWait((msg) =>
			{
				var isResp = SeculosBO.Instance.isResp(SeculosBO.Instance.SeculosGeneral.UserConnected);
				ObservableCollection<CalendarAcademic> listCalendar=null;

				if (isResp)
				{
					listCalendar = SeculosBO.Instance.StudentSelected.listAgendaEventos;
				}
				else
				{
					listCalendar = SeculosBO.Instance.DependenteConnected.listAgendaEventos;
				}
                    
                System.Diagnostics.Debug.WriteLine("msg agenda eventos: "+msg);


				if (msg.Equals(EnumCallback.SUCCESS))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_CALENDARIO_ACADEMICO_EMPTY, errorLabel, null);
					}
				}
				else if (msg.Equals(EnumCallback.DATA_AGENDA_EVENTOS_ERROR))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_AGENDA_EVENTOS_ERROR, errorLabel, null);
					}
				}
				else if (msg.Equals(EnumCallback.CONNECTION_ERROR))
				{
					if (listCalendar == null || listCalendar.Count == 0)
					{
						errorLabel.IsVisible = true;
						SeculosBO.Instance.showViewError(EnumCallback.DATA_AGENDA_EVENTOS_ERROR, errorLabel, null);

					}
				}

				loadIndicator.IsVisible = false;

                 if (listCalendar != null && listCalendar.Count > 0)
                    {
                        errorLabel.IsVisible = false;
                        initView(listCalendar);
                        calendar.IsVisible = true;
                    }

				else
				{
					//calendar.IsVisible = false;
					errorLabel.IsVisible = true;
				}
			});
		}

		void initView(ObservableCollection<CalendarAcademic> listCalendar)
		{
			calendar = new Calendar
			{
				BorderColor = Color.White,
				BorderWidth = 3,
				DatesTextColor = Color.Gray,
				DatesTextColorOutsideMonth = Color.FromHex("#d3d3d3"),
				BackgroundColor = Color.White,
				DatesBackgroundColorOutsideMonth = Color.White,
				SelectedBorderColor = Color.Transparent,
				TitleLabelFormat = "MMMM",
				StartDate = DateTime.Now
			};

			var specialDatesList = new List<SpecialDate>();
			detailDateList = new List<Tuple<DateTime, string, string>>();

			foreach (var date in listCalendar)
			{
				if (!date.DtEvento.Equals(SeculosBO.EMPTY))
				{
					var dateTime = DateTime.ParseExact(date.DtEvento, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
					var specialDate = new SpecialDate(dateTime);

					detailDateList.Add(new Tuple<DateTime, string, string>(dateTime, date.DescEvento, date.TpColor));

					if (!date.TpColor.Equals(SeculosBO.EMPTY))
					{
						specialDate.BackgroundColor = Color.FromHex(date.TpColor);
						specialDate.TextColor = Color.White;
					}

					specialDatesList.Add(specialDate);
				}
			}

			calendar.SpecialDates = specialDatesList;

			// Calendar events
			calendar.LeftArrowClicked += (sender, e) => handleCalendarEvent(e.DateTime);
			calendar.RightArrowClicked += (sender, e) => handleCalendarEvent(e.DateTime);

			layoutDesc.Children.Add(calendar);

			// Initiate layout for current month
			handleCalendarEvent(DateTime.Now);

			//this.Children.Add(
			//	calendar,
			//	Constraint.Constant(0),
			//	Constraint.Constant(0),
			//	Constraint.RelativeToParent((parent) => { return parent.Width; }),
			//	Constraint.RelativeToParent((parent) => { return parent.Height * .6; })
			//);
		}

		void handleCalendarEvent(DateTime dateSelected)
		{
			var layout = new StackLayout
			{
				Spacing = 30,
				Margin = new Thickness(0, 0, 0, 20),
				Children = { legendSeperator }
			};

			foreach (var desc in detailDateList)
			{
				// Same month and same year, match!
				if (desc.Item1.Month == dateSelected.Month && desc.Item1.Year == dateSelected.Year)
				{
					// Check if description and color exists 
					if (!desc.Item2.Equals(SeculosBO.EMPTY) && !desc.Item3.Equals(SeculosBO.EMPTY))
					{
                        var legendItem = CreateLegendItem(desc.Item1.Day+" - " + desc.Item2, desc.Item3);
						layout.Children.Add(legendItem);

					}
				}
			}

			legendSeperator.IsVisible = (layout.Children.Count > 0);

			if (layoutDesc.Children.Count >= 2)
			{
				layoutDesc.Children.RemoveAt(1);
			}

			layoutDesc.Children.Add(layout);
		}

		StackLayout CreateLegendItem(string text, string boxHexColor)
		{
			var layout = new StackLayout
			{
				Margin = new Thickness(10, 0, 20, 5),
				Orientation = StackOrientation.Horizontal,
				Spacing = 5
			};

			var boxView = new BoxView
			{
				WidthRequest = 20,
				HeightRequest = 20,
				BackgroundColor = Color.FromHex(boxHexColor),
				VerticalOptions = LayoutOptions.Center
			};

			var label = new CustomLabel
			{
				Text = text,
				VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Gray,
				FontSize = 15,
				FontType = "Medium"
			};

			layout.Children.Add(boxView);
			layout.Children.Add(label);

			return layout;
		}

		public void OnPageAppeared()
		{
			// Do nothing
		}

		public void OnPageAppearing()
		{
			App._Current.SendMessage(MessagingConstants.UPDATE_TITLE_CAROUSEL_AGENDA, ((string)App.Current.Resources["agenda_eventos_str"]).ToUpper());
			InitializeView();
		}

		public void OnPageDisappeared()
		{
			// Do nothing
		}

		public void OnPageDisappearing()
		{
			// Do nothing
		}
	}
}
