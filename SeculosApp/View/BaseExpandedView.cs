﻿using System.Threading.Tasks;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace SeculosApp
{
	public abstract class BaseExpandedView : AbstractContentPage
	{
		public Command cmdClick;
		public CustomLabel tabTitle, lblErrorMain, lblErrorMainUpdate;
		public Grid carouselMotora;
		public Image btClose, imgError, imgCloudError;
		public StackLayout btBackLayout, btNextLayout, stackBtnCloseError, stackContentError;
		public int position = 0;
		public string pageId;
		public bool blockClick;
		public RelativeLayout layout, headerLayout, carouselLayout;
		public RelativeLayout tabsLayout { get; set; }
		public BoxView boxViewError;
		public Grid GridModal;

		public BaseExpandedView(int _position, string _pageId)
		{
			position = _position;
			pageId = _pageId;

			layout = new RelativeLayout();

			layout.BackgroundColor = Color.FromHex("#143f58");

			cmdClick = new Command((obj) => onClick(obj));

			headerLayout = CreateHeaderLayout();

			var backgroundImg = new CachedImage
			{
				Source = "aluno_bg.png",
				Aspect = Aspect.Fill,
				DownsampleToViewSize = true,
				TransparencyEnabled = false
			};

			layout.Children.Add(
				backgroundImg,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			layout.Children.Add(
				headerLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .1; })
			);

			carouselLayout = new RelativeLayout
			{
				Margin = new Thickness(8, 0, 8, 8),
				BackgroundColor = Color.White
			};

			tabsLayout = CreateTabsLayout();

			carouselLayout.Children.Add(
				tabsLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .1; })
			);

			layout.Children.Add(
				carouselLayout,
				Constraint.Constant(0),
				Constraint.RelativeToView(headerLayout, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .9; })
			);

			boxViewError = new BoxView
			{
				BackgroundColor = Color.FromHex("#cccccc"),
				HeightRequest = 1,
				Opacity = 0.2
			};

			stackBtnCloseError = new StackLayout
			{
				Padding = new Thickness(7),
				HorizontalOptions = LayoutOptions.End,
				Margin = new Thickness(0,-6,0,0)
			};

			imgError = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = "x_ic.png",
				HeightRequest = 18,
				WidthRequest = 18,
				Margin = new Thickness(4)
			};

			stackBtnCloseError.Children.Add(imgError);

			imgCloudError = new Image
			{
				Aspect = Aspect.AspectFit,
				Source = "no_internet_ic.png",
				HeightRequest = 43,
				WidthRequest = 68,
				Margin = new Thickness(0, -22, 0, 14)
			};

			lblErrorMain = new CustomLabel
			{
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 14,
				FontType = "Medium",
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				Text = (string)App.Current.Resources["connection_error"]
			};

			lblErrorMainUpdate = new CustomLabel
			{
				TextColor = (Color)App.Current.Resources["gray_light"],
				FontSize = 14,
				FontType = "Medium",
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalTextAlignment = TextAlignment.Center,
				Text = (string)App.Current.Resources["update_view"]
			};

			stackContentError = new StackLayout
			{
				BackgroundColor = Color.White,
				HeightRequest = 160,
				IsVisible = false,
				TranslationY = 0,
				Children = { boxViewError, stackBtnCloseError, imgCloudError, lblErrorMain, lblErrorMainUpdate}
			};

			layout.Children.Add(
				stackContentError,
				yConstraint: Constraint.RelativeToParent((parent) => { return parent.Height - 160; }),
				widthConstraint: Constraint.RelativeToParent((parent) => { return parent.Width; })

			);

			GridModal = new Grid
			{
				IsVisible = false
			};

			RelContentModal = GridModal;
			relContentModalExtends = GridModal;
			abstractContentPageExtends = this;

			layout.Children.Add(
				GridModal,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			Content = layout;

			Task.Factory.StartNew(() =>
			{
				Task.Delay(50).ContinueWith((obj) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						createMainLayout();
					});
				});
			});

			var tapCloseError = new TapGestureRecognizer();
			tapCloseError.Tapped += async (s, e) =>
			{
				await Utils.FadeView(stackBtnCloseError);
				await stackContentError.TranslateTo(0, stackContentError.Height, 80);
				stackContentError.IsVisible = false;
				await stackContentError.TranslateTo(0, 0, 0);
				System.Diagnostics.Debug.WriteLine("tapCloseError btnCloseError: ");
			};
			stackBtnCloseError.GestureRecognizers.Add(tapCloseError);
		}

		public async void createMainLayout()
		{
			carouselMotora = GenerateCarouselMotora();

			carouselLayout.Children.Add(
				carouselMotora,
				Constraint.Constant(0),
				Constraint.RelativeToView(tabsLayout, (parent, view) => { return view.Y + view.Height; }),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height * .9; })
			);

			var TabView = new TabView(carouselMotora, null, App.ScreenWidth);
			TabView.addPrevNext(btBackLayout, btNextLayout);
			await TabView.changeTab(pageId, position + 1);

			handleCarouselPage();
		}

		public async void onClick(object obj)
		{
			if (!blockClick && !App.GlobalBlockClick)
			{
				blockClick = true;
				App.GlobalBlockClick = true;
				var str = (string)obj;

				if (str.Equals(btClose.ClassId))
				{
					await FadeView(btClose);
					try
					{
						await Navigation.PopAsync();
					}
					catch (System.Exception exception)
					{
						System.Diagnostics.Debug.WriteLine("onClick back exception: " + exception.Message + " Type: " + exception.GetType());
					}
				}

				await Task.Factory.StartNew(() =>
				{
					Task.Delay(250).ContinueWith((o) =>
					{
						blockClick = false;
						App.GlobalBlockClick = false;
					});
				});
			}
			else
			{
				System.Diagnostics.Debug.WriteLine("onClick click has been blocked");
			}
		}

		RelativeLayout CreateHeaderLayout()
		{
			var headerLayout = new RelativeLayout();

			btClose = new Image
			{
				ClassId = "btClose",
				Source = "back_ic.png",
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			var btCloseLayout = new StackLayout
			{
				WidthRequest = 30,
				Children = { btClose }
			};
			btCloseLayout.GestureRecognizers.Add(new TapGestureRecognizer { Command = cmdClick, CommandParameter = btClose.ClassId });

			var titleLabel = new CustomLabel
			{
				Text = SeculosBO.Instance?.DependenteConnected?.NomeAluno,
				FontType = "Medium",
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = 19,
				Margin = new Thickness(45, 0, 12, 0),
				LineBreakMode = LineBreakMode.TailTruncation
			};

			headerLayout.Children.Add(
				btCloseLayout,
				Constraint.Constant(10),
				Constraint.Constant(0),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			headerLayout.Children.Add(
				titleLabel,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return headerLayout;
		}

		RelativeLayout CreateTabsLayout()
		{
			var tabsLayoutInner = new RelativeLayout
			{
				BackgroundColor = Color.FromHex("#FFAB00")
			};

			var btBack = new Image
			{
				ClassId = "btBack",
				Source = "arrow_ic.png",
				WidthRequest = 36,
				HeightRequest = 36,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				Rotation = 180
			};

			btBackLayout = new StackLayout
			{
				WidthRequest = 44,
				Children = { btBack }
			};

			var btNext = new Image
			{
				ClassId = "btNext",
				Source = "arrow_ic.png",
				WidthRequest = 36,
				HeightRequest = 36,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand
			};

			btNextLayout = new StackLayout
			{
				WidthRequest = 44,
				Children = { btNext }
			};

			tabTitle = new CustomLabel
			{
				FontSize = 18,
				FontType = "Medium",
				TextColor = Color.Black,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				LineBreakMode = LineBreakMode.TailTruncation
			};

			tabsLayoutInner.Children.Add(
				tabTitle,
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Width; }),
				Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			tabsLayoutInner.Children.Add(
				btBackLayout,
				Constraint.Constant(0),
				Constraint.Constant(0),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			tabsLayoutInner.Children.Add(
				btNextLayout,
				Constraint.RelativeToParent((parent) => { return parent.Width - 45; }),
				Constraint.Constant(0),
				heightConstraint: Constraint.RelativeToParent((parent) => { return parent.Height; })
			);

			return tabsLayoutInner;
		}

		public async Task FadeView(Xamarin.Forms.View view)
		{
			view.Opacity = 0.2;
			await view.FadeTo(0.5, 100);
		}

		public abstract void handleCarouselPage();

		public abstract Grid GenerateCarouselMotora();

		protected override void OnAppearing()
		{
			base.OnAppearing();
			relContentModalExtends = GridModal;
			RelContentModal = GridModal;
			SeculosBO.Instance.Navigation = Navigation;
		}
	}
}