﻿using Plugin.FirebasePushNotification;
using Xamarin.Forms;

namespace SeculosApp
{
	public partial class App : Application
	{
		public static App _Current;
		public static double ScreenWidth;
		public static double ScreenHeight;
		public static bool GlobalBlockClick;
		public Page PageSelected;

		public App(string notificationCode) : this()
		{
			if (PageSelected != null)
			{
				AbstractContentPage._AbstractContentPage.openNotification(notificationCode);
			}
		}

		public App()
		{
			InitializeComponent();
			_Current = this;

			CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
			{
				System.Diagnostics.Debug.WriteLine("NOTIFICATION OPEN");

			};

			CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
			{
				System.Diagnostics.Debug.WriteLine("NOTIFICATION RECEIVE");

			};

			var seculosBO = SeculosBO.Instance;

			if (!seculosBO.LoadSeculosGeneral() || !seculosBO.SeculosGeneral.KeepConnected)
			{
				PageSelected = new LoginPage();
			}
			else if (seculosBO.isResp(seculosBO.SeculosGeneral.UserConnected))
			{
				PageSelected = new MainScreenPage();
			}
			else
			{
				PageSelected = new AlunoPage();
				SeculosBO.Instance.fromLogin = false;
			}
			MainPage = new NavigationPage(PageSelected);
			MainPage.BackgroundColor = Color.White;

		}

		public void SendMessage(string msg, object param)
		{
			MessagingCenter.Send(this, msg, param);
		}

		public void SendMessage(string msg)
		{
			MessagingCenter.Send(this, msg);
		}

		protected override void OnStart()
		{
            
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

