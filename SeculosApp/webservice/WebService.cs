﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SeculosApp
{
	public class WebService
	{
		public const string ERROR_NO_SERVER = "Erro no server.";
        

        const string Url = "https://www.seculomanaus.com.br/webservice/";
        //const string Url = "https://www.seculomanaus.com.br/webservice_homolog/";
       
		const string Token = "cesS3CU102016";
		public const string AUTENTICACAO = "autenticacao.php";
		public const string ALUNO = "aluno.php";
		public const string NOTICIA = "noticia.php";
		public const string DEPENDENTE = "dependente.php";
		public const string BOLETO = "boleto.php";
		public const string LIMIT = "limite.php";
		public const string OCORRENCIA = "ocorrencia.php";
		public const string DIARIO = "diario.php";
		public const string TEMPO = "tempo.php";
		public const string CONTEUDO = "conteudo.php";
		public const string ACOMPANHAMENTO = "acompanhamento.php";
		public const string GABARITO = "gabarito.php";
		public const string NOTIFICACAO = "alerta.php";
		public const string BOLETIM = "boletim.php";
		public const string DEMONSTRATIVO = "demonstrativo.php";
		public const string INFANTIL = "infantil.php";
		public const string TOKEN_GCM = "token.php";

		public const string FREQUENCIA = "faltas.php";
		public const string CALENDARIO_PROVA = "prova_calendario.php";

		public const string CALENDARIO_PROVAS = "prova_calendario.php";
		public const string EXTRATO_CONSUMO = "consumo.php";
		public const string CARDAPIO = "cardapio.php";
		public const string CALENDARIO_ESCOLAR = "calendario.php";
		public const string REGISTRO_DIARIO = "registro_diario.php";
		public const string CALENDARIO_EVENTO = "calendario_evento.php";
		public const string CONFIRMACAO_EVENTO = "confirmacao_evento.php";
		public const string CONFIRMACAO_COMUNICADO = "confirmacao_comunicado.php";
		public const string PAGAMENTO_CREDITO = "pagamento_credito.php";

		string[] unicodeChars = { 
			"\\u0090", "\\u0091", "\\u0092", "\\u0093", "\\u0094", "\\u0095", "\\u0096", 
		};

		public async Task<T> GetGenericResult<T>(string url) where T : new()
		{
            var client = new HttpClient();
          
            client.MaxResponseContentBufferSize = 999999;
			client.Timeout = TimeSpan.FromSeconds(60);

			if (!url.Contains("?"))
			{
				url += "?";
			}

			System.Diagnostics.Debug.WriteLine("GetGeneralResult URL: " + (Url + url + "&token=" + Token));

			var uri = Url + url + "&token=" + Token;

			var json = await client.GetStringAsync(uri);

            //System.Diagnostics.Debug.WriteLine("json: " + json);

			foreach (var str in unicodeChars)
			{
				json = json.Replace(str, "");
			}

			if (!json.Equals("Erro no server."))
			{
				//System.Diagnostics.Debug.WriteLine("GetGeneralResult json.ToString(): " + json);
				return JsonConvert.DeserializeObject<T>(json.ToString());

			}
			else
			{
				//System.Diagnostics.Debug.WriteLine("GetGeneralResult erro de parametros: " + json);
				return new T();
			}
		}
	}
}

