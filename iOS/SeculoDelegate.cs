﻿using System;
using FFImageLoading.Forms.Touch;
using Foundation;
using UIKit;
using UserNotifications;
using Xamarin.Forms;
using XLabs.Forms;
using Plugin.FirebasePushNotification;

namespace SeculosApp.iOS
{
    [Register("SeculoDelegate")]
    public class SeculoDelegate : XFormsApplicationDelegate, IUNUserNotificationCenterDelegate
    {
        public NSData DeviceToken { get; set; }
       
        public static SeculoDelegate _SeculoDelegate;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
            _SeculoDelegate = this;
            global::Xamarin.Forms.Forms.Init();
            CachedImageRenderer.Init();
            XamForms.Controls.iOS.Calendar.Init();
            //InitGCM();

            DependencyService.Register<Seculos.DependencyService.IOS.Environment>();
            DependencyService.Register<Seculos.DependencyService.IOS.Message>();

            App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;
            App.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;

            LoadApplication(new App());

            //UIApplication.SharedApplication.StatusBarHidden = true;

            //inicializa firebase
            FirebasePushNotificationManager.Initialize(options, true);

            return base.FinishedLaunching(app, options);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            FirebasePushNotificationManager.RemoteNotificationRegistrationFailed(error);

        }
        // To receive notifications in foregroung on iOS 9 and below.
        // To receive notifications in background in any iOS version
        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {

            FirebasePushNotificationManager.DidReceiveMessage(userInfo);
        }

    }
}