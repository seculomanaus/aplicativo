﻿using SeculosApp;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.IOS.ClipBoard))]
namespace Seculos.DependencyService.IOS
{
	public class ClipBoard : IClipBoard
	{
		public void CopyToClipboard(string text)
		{
			UIPasteboard clipboard = UIPasteboard.General;
			clipboard.String = text;
		}
	}
}