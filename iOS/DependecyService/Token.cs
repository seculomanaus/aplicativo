﻿//using System;
//using Foundation;

//using SeculosApp.IDependecyService;
//using SeculosApp.iOS;

//[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.IOS.Token))]
//namespace Seculos.DependencyService.IOS
//{
//	public class Token : IToken
//	{
//		static Token Instance { get; set; }

//		public string generateTokenGcm()
//		{
//			// do nothing;
//			return null;
//		}

//		public void generateTokenGcm(Action<string> callback)
//		{
//			// Register APNS Token to GCM
//			var options = new NSMutableDictionary();
//			options.SetValueForKey(SeculoDelegate._SeculoDelegate.DeviceToken, Constants.RegisterAPNSOption);

//			#if DEBUG
//				options.SetValueForKey(new NSNumber(true), Constants.APNSServerTypeSandboxOption);
//			#else
//				options.SetValueForKey(new NSNumber(false), Constants.APNSServerTypeSandboxOption);
//			#endif

//			// Get our token
//			InstanceId.SharedInstance.Token(
//			SeculoDelegate._SeculoDelegate.Configuration.GcmSenderId,
//			Constants.ScopeGCM,
//			options,
//			(token, error) =>
//			{
//				System.Diagnostics.Debug.WriteLine("GCM Registration ID: " + token);
//				callback(token);
//			});
//		}

//		public void Subscribe(string token)
//		{
//			// do nothing
//		}

//		public void DeleteToken()
//		{
//			InstanceId.SharedInstance.DeleteToken(
//				SeculoDelegate._SeculoDelegate.Configuration.GcmSenderId,
//				Constants.ScopeGCM,
//				error =>
//				{
//			// Callback, non-null error if there was a problem
//			if (error != null)
//						Console.WriteLine("Deleted Token");
//					else
//						Console.WriteLine("Error deleting token");
//				});
//			}
//	}
//}
