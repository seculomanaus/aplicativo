using MessageUI;
using SeculosApp;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(Seculos.DependencyService.IOS.SendMail))]
namespace Seculos.DependencyService.IOS
{
	public class SendMail : ISendEmail
	{
		static SendMail Instance { get; set; }

		void ISendEmail.SendMail(string to, string name, string phone, string from, string msg, string subject)
		{
			if (MFMailComposeViewController.CanSendMail)
			{
				MFMailComposeViewController mailController = new MFMailComposeViewController();
				var body = msg + "\n\n\n" + name + "\n" + phone + "\n" + from + "";
				mailController.SetToRecipients(new string[] { to });
				mailController.SetSubject(subject);
				mailController.SetMessageBody(body, false);

				mailController.Finished += (object s, MFComposeResultEventArgs args) =>
				{
					UIApplication.SharedApplication.InvokeOnMainThread(() => { 
						args.Controller.DismissViewController(true, null);
					});
				};

				UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(mailController, true, null);
			}
		}
	}
}