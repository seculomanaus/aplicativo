﻿using System;
using SeculosApp.iOS;
using Foundation;
using UIKit;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(OpenGallery))]
namespace SeculosApp.iOS
{
	public class OpenGallery: IOpenGallery
	{
		UIImagePickerController imageController;

		Action<string> _callback;


		public OpenGallery()
		{
			imageController = new UIImagePickerController();
			imageController.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
			imageController.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);

			imageController.FinishedPickingMedia += FinishedPickingImage;
			imageController.Canceled += HandleCanceled;
		}

		public void FetchBitmap(string imageConverted)
		{
			_callback(imageConverted);
		}

		public void GetImage(Action<string> callback)
		{
			_callback = callback;
			UIApplication.SharedApplication.KeyWindow.RootViewController.PresentModalViewController(imageController, true);
		}

		void FinishedPickingImage(object sender, UIImagePickerMediaPickedEventArgs e)
		{
			bool isImage = false;

			string localDataPath = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

			switch (e.Info[UIImagePickerController.MediaType].ToString())
			{
				case "public.image":
					isImage = true;
					break;
			}

			if (isImage)
			{
				// get the original image
				UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;

				if (originalImage != null)
				{
					var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					string jpgFilename = System.IO.Path.Combine(documentsDirectory, "photo_profile.jpg"); // hardcoded filename, overwritten each time

					NSData imgData = originalImage.AsJPEG();
					NSError err = null;

					if (imgData.Save(jpgFilename, false, out err))
					{
						FetchBitmap(Convert.ToBase64String(File.ReadAllBytes(jpgFilename)));
					}
					else
					{
						Console.WriteLine("NOT saved as " + jpgFilename + " because" + err.LocalizedDescription);
					}
				}
			}
			else
			{ // if it's a video
			  // get video url
				NSUrl mediaURL = e.Info[UIImagePickerController.MediaURL] as NSUrl;
				if (mediaURL != null)
				{
					Console.WriteLine(mediaURL.ToString());
				}
			}

			// dismiss the picker
			imageController.DismissModalViewController(true);
		}

		void HandleCanceled(object sender, EventArgs e)
		{
			imageController.DismissModalViewController(true);
		}
	}
}
