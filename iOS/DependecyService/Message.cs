using SeculosApp;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(Seculos.DependencyService.IOS.Message))]
namespace Seculos.DependencyService.IOS
{
	public class Message : IMessage
	{
		static Message Instance { get; set; }

		public void Msg(string text)
		{
			UIAlertView _error = new UIAlertView("Século", text, null, "Ok", null);
			_error.Show();
		}
	}
}