using System;
using System.IO;
using Seculos.Listener;

[assembly: Xamarin.Forms.Dependency(typeof(Environment))]
namespace Seculos.DependencyService.IOS
{
	public class Environment : IEnvioriment
	{
		static Environment Instance { get; set; }

		public Environment() { }

		public string GetPath()
		{
			string documentsPath = null;

			try
			{
				documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			}
			catch (FileNotFoundException) { }

			return documentsPath;
		}

		public string ReadAllText(string filePath)
		{
			return File.ReadAllText(filePath);
		}

		public bool WriteAllText(string filePath, string text)
		{
			try
			{
				File.WriteAllText(filePath, text);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}