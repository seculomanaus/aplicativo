using Seculo.IOS.Renderer;
using SeculosApp;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomViewButton), typeof(CustomViewButtonRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomViewButtonRenderer : VisualElementRenderer<RelativeLayout>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<RelativeLayout> e)
		{
			base.OnElementChanged(e);
			if (Element == null)
				return;
			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, events) =>
			{
				System.Diagnostics.Debug.WriteLine("Tap no Custom");
			};
			e.NewElement.GestureRecognizers.Add(tapGestureRecognizer);

		}
	}
}