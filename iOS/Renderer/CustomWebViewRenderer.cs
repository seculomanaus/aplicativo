﻿using SeculosApp;
using SeculosApp.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]
namespace SeculosApp.iOS.Renderer
{
	public class CustomWebViewRenderer : WebViewRenderer
	{

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			var view = (UIWebView)NativeView;
			view.ScrollView.ScrollEnabled = true;
			view.ScalesPageToFit = true;


		}
	}
}
