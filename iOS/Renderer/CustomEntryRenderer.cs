using Xamarin.Forms;
using SeculosApp;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Seculo.IOS.Renderer;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			var label = (UITextField)Control;

			if (this.Element != null)
			{
				var font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);

				CustomEntry el = (CustomEntry)this.Element;
				if (el.FontType.Equals("Bold"))
				{
					font = UIFont.FromName("Gotham-Bold", (float)Element.FontSize);
				}
				else if (el.FontType.Equals("Book"))
				{
					font = UIFont.FromName("Gotham-Book", (float)Element.FontSize);
				}
				else if (el.FontType.Equals("Medium"))
				{
					font = UIFont.FromName("Gotham-Medium", (float)Element.FontSize);
				}
				else if (el.FontType.Equals("Light"))
				{
					font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);
				}

				label.Font = font;
			}
		}
	}
}