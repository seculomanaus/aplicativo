using Xamarin.Forms;
using SeculosApp;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Seculo.IOS.Renderer;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);
			if (Element == null)
				return;
			var label = (UILabel)Control;
			var font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);

			CustomLabel el = (CustomLabel)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = UIFont.FromName("Gotham-Bold", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Book"))
			{
				font = UIFont.FromName("Gotham-Book", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = UIFont.FromName("Gotham-Medium", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Light"))
			{
				font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);
			}

			label.Font = font;
		}
	}
}