using System.ComponentModel;
using Seculo.IOS.Renderer;
using SeculosApp;
using SeculosApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomCheckBox), typeof(CustomCheckBoxRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomCheckBoxRenderer : ViewRenderer<CustomCheckBox, CheckBoxView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<CustomCheckBox> e)
		{
			base.OnElementChanged(e);

			if (Element == null) return;

			if (e.NewElement != null)
			{
				if (Control == null)
				{
					var checkBox = new CheckBoxView(Bounds);
					checkBox.UserInteractionEnabled = false;
					SetNativeControl(checkBox);
				}

				Control.Checked = e.NewElement.Checked;

			}

			Control.Frame = Frame;
			Control.Bounds = Bounds;
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			System.Diagnostics.Debug.WriteLine("OnElementPropertyChanged: " + Element.Checked + " ± " + Control.Checked);
			Control.Checked = Element.Checked;
		}
	}
}