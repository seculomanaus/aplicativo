﻿using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using System.Drawing;
using Seculo.IOS.Renderer;
using SeculosApp;
using UIKit;

[assembly: ExportRendererAttribute(typeof(RoundedBoxView), typeof(RoundedBoxViewRenderer))]
namespace Seculo.IOS.Renderer
{
	public class RoundedBoxViewRenderer : ViewRenderer<RoundedBoxView, UIView>
	{
		UIView childView;

		protected override void OnElementChanged(ElementChangedEventArgs<RoundedBoxView> e)
		{
			base.OnElementChanged(e);

			var rbv = e.NewElement;
			if (rbv != null)
			{
				var shadowView = new UIView();

				childView = new UIView()
				{
					BackgroundColor = rbv.Color.ToUIColor(),
					Layer = {
						CornerRadius = (float)rbv.CornerRadius,
						BorderColor = rbv.Stroke.ToCGColor(),
						BorderWidth = (float)rbv.StrokeThickness,
						MasksToBounds = true
					},
					AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
				};

				shadowView.Add(childView);

				if (rbv.HasShadow)
				{
					shadowView.Layer.ShadowColor = UIColor.Black.CGColor;
					shadowView.Layer.ShadowOffset = new SizeF(3, 3);
					shadowView.Layer.ShadowOpacity = 1;
					shadowView.Layer.ShadowRadius = 5;
				}

				SetNativeControl(shadowView);
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == RoundedBoxView.CornerRadiusProperty.PropertyName)
				childView.Layer.CornerRadius = (float)this.Element.CornerRadius;
			else if (e.PropertyName == RoundedBoxView.StrokeProperty.PropertyName)
				childView.Layer.BorderColor = this.Element.Stroke.ToCGColor();
			else if (e.PropertyName == RoundedBoxView.StrokeThicknessProperty.PropertyName)
				childView.Layer.BorderWidth = (float)this.Element.StrokeThickness;
			else if (e.PropertyName == BoxView.ColorProperty.PropertyName)
				childView.BackgroundColor = this.Element.Color.ToUIColor();
			else if (e.PropertyName == RoundedBoxView.HasShadowProperty.PropertyName)
			{
				if (Element.HasShadow)
				{
					NativeView.Layer.ShadowColor = UIColor.Black.CGColor;
					NativeView.Layer.ShadowOffset = new SizeF(3, 3);
					NativeView.Layer.ShadowOpacity = 1;
					NativeView.Layer.ShadowRadius = 5;
				}
				else {
					NativeView.Layer.ShadowColor = UIColor.Clear.CGColor;
					NativeView.Layer.ShadowOffset = new SizeF();
					NativeView.Layer.ShadowOpacity = 0;
					NativeView.Layer.ShadowRadius = 0;
				}
			}
		}

	}
}

