using Xamarin.Forms;
using SeculosApp;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Seculo.IOS.Renderer;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomEditorRenderer : EditorRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);
			if (Element == null)
				return;

			var label = (UITextView)Control;
			var font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);

			CustomEditor el = (CustomEditor)this.Element;
			if (el.FontType.Equals("Bold"))
			{
				font = UIFont.FromName("Gotham-Bold", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Book"))
			{
				font = UIFont.FromName("Gotham-Book", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Medium"))
			{
				font = UIFont.FromName("Gotham-Medium", (float)Element.FontSize);
			}
			else if (el.FontType.Equals("Light"))
			{
				font = UIFont.FromName("Gotham-Light", (float)Element.FontSize);
			}

			label.Font = font;
		}
	}
}