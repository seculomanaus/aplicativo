using Seculo.IOS.Renderer;
using SeculosApp;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomImageButton), typeof(CustomImageButtonRenderer))]
namespace Seculo.IOS.Renderer
{
	public class CustomImageButtonRenderer : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			if (Element == null)
				return;
			CustomImageButton CustomImageButton = (CustomImageButton) this.Element;
			var button = (UIButton)Control;

			button.Frame = this.Frame;

			button = UIButton.FromType(UIButtonType.RoundedRect);
			button.SetImage(UIImage.FromFile(CustomImageButton.Source), UIControlState.Normal);
			SetNativeControl(button);

		}
	}
}