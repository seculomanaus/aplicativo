﻿using System;
using CoreGraphics;
using UIKit;

namespace SeculosApp.iOS
{
	public class CheckBoxView : UIButton
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CheckBoxView"/> class.
		/// </summary>
		public CheckBoxView()
		{
			ClipsToBounds = true;
			ContentMode = UIViewContentMode.ScaleAspectFit;
			Initialize();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CheckBoxView"/> class.
		/// </summary>
		/// <param name="bounds">The bounds.</param>
		public CheckBoxView(CGRect bounds) : base(bounds)
		{
			Initialize();
		}

		public bool Checked
		{
			set { Selected = value; }
			get { return Selected; }
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		void Initialize()
		{
			AdjustEdgeInsets();
			ApplyStyle();

			TouchUpInside += (sender, args) => Selected = !Selected;
			// set default color, because type is not UIButtonType.System 
			SetTitleColor(UIColor.DarkTextColor, UIControlState.Normal);
			SetTitleColor(UIColor.DarkTextColor, UIControlState.Selected);
		}

		void AdjustEdgeInsets()
		{
			
			HorizontalAlignment = UIControlContentHorizontalAlignment.Fill;
			VerticalAlignment = UIControlContentVerticalAlignment.Fill;

			ImageEdgeInsets = new UIEdgeInsets(6f, 6f, 8f, 8f);
		}

		void ApplyStyle()
		{
			SetImage(UIImage.FromBundle("Images/checkbox_selected.png"), UIControlState.Selected);
			SetImage(UIImage.FromBundle("Images/checkbox_unselected.png"), UIControlState.Normal);

		}


	
	}
}
