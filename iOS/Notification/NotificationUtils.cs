﻿using Foundation;
using SeculosApp.Model;
using UIKit;

namespace SeculosApp.iOS
{
	public class NotificationUtils
	{
		public static NSString CD_ALERTA = new NSString("cd_alerta");
		public static NSString NM_ALERTA = new NSString("nm_alerta");
		public static NSString DS_ALERTA = new NSString("ds_alerta");
		public static NSString DS_FOTO = new NSString("ds_foto");
		public static NSString DS_FOTO_MINI = new NSString("ds_foto_mini");
		public static NSString DT_CADASTRO = new NSString("dt_cadastro");
		public static NSString NM_TIPO = new NSString("nm_tipo");
		public static NSString CD_EVENTO = new NSString("cd_evento");
		public static NSString CD_ALUNO = new NSString("cd_aluno");
		public static NSString NM_ALUNO = new NSString("nm_aluno");
		public static NSString DT_EVENTO = new NSString("dt_evento");
		public static NSString DS_HORARIO = new NSString("ds_horario");
		public static NSString VL_EVENTO = new NSString("vl_evento");
		public static NSString FL_LIDO = new NSString("fl_lido");
		public static NSString FL_PAGO = new NSString("fl_pago");
		public static NSString FL_CONFIRMACAO = new NSString("fl_confirmacao");

		public static Notificacao createNotificacaoByBundle(NSDictionary userInfo)
		{
			Notificacao notificacao = new Notificacao();
			notificacao.CdAlert = userInfo.ValueForKey(CD_ALERTA).ToString();
			notificacao.NmAlert = userInfo.ValueForKey(NM_ALERTA).ToString();
			notificacao.DsAlert = userInfo.ValueForKey(DS_ALERTA).ToString();
			notificacao.DsFoto = userInfo.ValueForKey(DS_FOTO).ToString();
			notificacao.DsFotoMini = userInfo.ValueForKey(DS_FOTO_MINI).ToString();
			notificacao.DtCadastro = userInfo.ValueForKey(DT_CADASTRO).ToString();
			notificacao.NmTipo = userInfo.ValueForKey(NM_TIPO).ToString();
			notificacao.CdEvento = userInfo.ValueForKey(CD_EVENTO).ToString();
			notificacao.CdAluno = userInfo.ValueForKey(CD_ALUNO).ToString();
			notificacao.NmAluno = userInfo.ValueForKey(NM_ALUNO).ToString();
			notificacao.DtEvento = userInfo.ValueForKey(DT_EVENTO).ToString();
			notificacao.DsHorario = userInfo.ValueForKey(DS_HORARIO).ToString();
			notificacao.VlEvento = userInfo.ValueForKey(VL_EVENTO).ToString();
			notificacao.FlLido = userInfo.ValueForKey(FL_LIDO).ToString();
			notificacao.FlPago = userInfo.ValueForKey(FL_PAGO).ToString();
			notificacao.FlConfirmado = userInfo.ValueForKey(FL_CONFIRMACAO).ToString();

			return notificacao;
		}

		public static void sendNotification(Notificacao notificacao)
		{
			// create the notification
			var notification = new UILocalNotification();

			// set the fire date (the date time in which it will fire)
			notification.FireDate = NSDate.FromTimeIntervalSinceNow(1);

			// configure the alert
			notification.AlertAction = notificacao.NmAlert;
			notification.AlertBody = notificacao.DsAlert;

			var options = new NSMutableDictionary();

			options.SetValueForKey(new NSString(notificacao.CdAlert), CD_ALERTA);
			options.SetValueForKey(new NSString(notificacao.NmAlert), NM_ALERTA);
			options.SetValueForKey(new NSString(notificacao.DsAlert), DS_ALERTA);
			options.SetValueForKey(new NSString(notificacao.DsFoto), DS_FOTO);
			options.SetValueForKey(new NSString(notificacao.DsFotoMini), DS_FOTO_MINI);
			options.SetValueForKey(new NSString(notificacao.DtCadastro), DT_CADASTRO);
			options.SetValueForKey(new NSString(notificacao.NmTipo), NM_TIPO);
			options.SetValueForKey(new NSString(notificacao.CdEvento), CD_EVENTO);
			options.SetValueForKey(new NSString(notificacao.CdAluno), CD_ALUNO);
			options.SetValueForKey(new NSString(notificacao.NmAluno), NM_ALUNO);
			options.SetValueForKey(new NSString(notificacao.DtEvento), DT_EVENTO);
			options.SetValueForKey(new NSString(notificacao.DsHorario), DS_HORARIO);
			options.SetValueForKey(new NSString(notificacao.VlEvento), VL_EVENTO);
			options.SetValueForKey(new NSString(notificacao.FlLido), FL_LIDO);
			options.SetValueForKey(new NSString(notificacao.FlPago), FL_PAGO);
			options.SetValueForKey(new NSString(notificacao.FlConfirmado), FL_CONFIRMACAO);
			options.SetValueForKey(new NSString("local"), new NSString("local"));
			notification.UserInfo = new NSMutableDictionary(options);
			// modify the badge
			notification.ApplicationIconBadgeNumber = 0;

			// set the sound to be the default sound
			notification.SoundName = UILocalNotification.DefaultSoundName;

			// schedule it
			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}

		public static Notificacao verifyNotification(NSDictionary userInfo)
		{
			Notificacao notification = null;
			if (userInfo != null && SeculosBO.Instance.SeculosGeneral != null && SeculosBO.Instance.SeculosGeneral.UserConnected != null && userInfo.ContainsKey(new NSString(CD_ALERTA)))
			{
				notification = createNotificacaoByBundle(userInfo);
				SeculosBO.Instance.addNotificationByGcm(notification);
			}
			return notification;
		}
	}
}